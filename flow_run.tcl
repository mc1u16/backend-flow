# MAIN FLOW RUN SCRIPT - executed when "Start" button is pressed.
# The same run script is used for:
#   * innovus (main)
#   * genus
#   * modus
#
# ORDER OF SCRIPT SOURCING:
# 1. flow_run.tcl                      this script
#      define CELL_NAME and CELL_PATH
# 2. eprocs (eg. epc_procs.tcl)        helper/utility procs
# 3. flow_setup.tcl
# 4. flow_procs.tcl
# 5. flow_step.tcl
# 6. <CELL>_config.tcl
# 7. flow_attribute

################################################################################
## Environment Variables
################################################################################

if { [info exists ::env(CDS_STYLUS_SOURCE_VERBOSE)] &&
     ( [get_db program_short_name] == "genus" || [get_db program_short_name] == "modus") } {
    ## NOTE: Innovus is automatically setting 'source_verbose' according to the
    ##       environment variable CDS_STYLUS_SOURCE_VERBOSE.
    ##       Modus needs to have either true or false, not 0 or 1 (works in genus)
    if { $::env(CDS_STYLUS_SOURCE_VERBOSE) } {
        set_db source_verbose true
    } else {
        set_db source_verbose false
    }
}


## Set default encoding to UTF-8:
# encoding system utf-8

## Get current cell name and cell path from starting CELL-directory ONCE.
## In case of a manually set "CELL_NAME", check for correct start directory.
if { ![info exists ::env(CELL_NAME)] } {
    set ::env(CELL_NAME) [file tail [pwd]]
}
if { ![info exists ::env(CELL_PATH)] } {
    set ::env(CELL_PATH) [string map [list "$::env(OBJECTROOT)" "$::env(DIGITAL_WORK_DIR)"] [pwd]]
}

################################################################################
## Sourcing of Helper & Flow Scripts and Cell Configuration
################################################################################

# source custom epc procs (can be used everywhere -> utility procedures)
set epc_procs "/usr/eda_tooling-a/tools/digital/scripts/utils/epc_procs.tcl"
puts "Sourcing file '$epc_procs'"
source $epc_procs

## source mod_procs for modus, this is used in flow_procs
##   (eg get_list_from_pkg_tester_file  -> getListFromPkgTesterFile )
if { [get_db program_short_name] == "modus" } {
    set mod_procs "/usr/eda_tooling-a/tools/digital/scripts/ebe/eprocs/mod_procs.tcl"
    puts "Sourcing file '$mod_procs'"
    source $mod_procs
}
## Source flow setup files:
set source_dir $::env(CELL_PATH)/$::env(CELL_CFG_DIR)/[get_db program_short_name]_scripts

foreach script {flow_setup flow_procs flow_step} {
    set script_file $source_dir/$script.tcl
    if {![file exists $script_file] } {
        puts "Cannot find source file '$script_file'. Exiting."
        exit -1
    }
    puts "Sourcing file '$script_file' ..."
    source $script_file
}

## Load CELL-specific configuration file:
set cell_config_file $::env(CELL_PATH)/be/[get_db flow_cell_name]_config.tcl
puts "EPC INFO: Sourcing cell configuration file $cell_config_file ..."
if { ![file exists $cell_config_file] } {
    puts "ERROR: Cannot find cell configuration file '$cell_config_file'"
    exit -1
} else {
    source $cell_config_file
}

## Check whether flow cell is a chip top level cell (relevant for flow_attributes.tcl):
set ::cell_is_top_level [regexp "$::env(PROJECT)_(pr|digital_io|digital)_top" [get_db flow_cell_name]]

## Configure attributes:
foreach script {flow_attribute} {
    set script_file $source_dir/$script.tcl
    if { ![file exists $script_file] } {
        puts "Cannot find source file '$script_file'. Exiting."
        exit -1
    }
    puts "Sourcing file '$script_file' ..."
    source $script_file
}

################################################################################
## Flow Initialization
################################################################################

## Define flow:
if {![info exists ::env(CELL_FLOW)] } {
    set_db flow_current flow:std
} else {
    set_db flow_current flow:$::env(CELL_FLOW)
}

## Set start and stop sub-flows:
if { [info exist ::env(CELL_FLOW_START)] } {
    set_db flow_step_start $::env(CELL_FLOW_START)
    if { [info exist ::env(CELL_FLOW_STOP)] } {
        set_db flow_step_stop  $::env(CELL_FLOW_STOP)
    }
}

## Set start and stop sub-flows:
if { [info exist ::env(CELL_FLOW_SUSPEND)] } {
    set_db flow_step_suspend $::env(CELL_FLOW_SUSPEND)
}

## Set metric control:
if { [get_db program_short_name] != "modus" } {
    if { [info exist ::env(CELL_FLOW_METRICS_EN)] } {
        set_db flow_metrics_enable $::env(CELL_FLOW_METRICS_EN)
    }
}

## Generate local CDS library file in working directory on scratch:
#  only for innovus and genus
if { [get_db program_short_name] != "modus" } {
    generate_cds_lib
}

## OA-Mode Initialization (only for Innovus):
if { [get_db program_short_name] == "innovus" } {
    initialize_oa
}

## Create result directory:
if [is_attribute  -obj_type root  epc_flow_result_directory] {
    file mkdir [get_db epc_flow_result_directory]
}

## Determine sub-flows to be executed:
set flow_list [get_epc_flows]

puts  [string repeat "\#" 80]
puts "\# FLOW:             [get_db flow_current]"
puts "\# SUB-FLOWS:        $flow_list"
puts "\# CELL NAME:        [get_db flow_cell_name]"
puts "\# CELL PATH:        [get_db flow_cell_path]"
puts "\# CELL CONF PATH:   [get_db flow_cell_cfg_path]"
puts "\# WORKING DIR:      [get_db flow_working_directory]"
puts  [string repeat "\#" 80]


################################################################################
## Flow Execution
################################################################################

## Define metrics handling
if { [get_db program_short_name] != "modus" } {
    if { [get_db flow_metrics_enable] == true } {
        enable_metrics -on
    } else {
        enable_metrics -off
    }
}


## Execute super-flow:
foreach flow $flow_list {
    if { $flow == [get_db flow_step_suspend] } {
        suspend                 ; # script suspend DO NOT TOUCH THIS LINE!!!
    }
    run_epc_flow $flow
}


################################################################################
## Flow Finishing
################################################################################

## Final result:
if { [get_db flow_current] ne "flow:layout"} {
    puts  [string repeat "\#" 80]
    if { [get_db program_short_name] == "genus" } {
        puts "WORST SLACK of design: [expr [get_db designs .slack] / 1000.0] ns"
    } elseif { [get_db program_short_name] == "innovus" } {
        ### get_db [report_timing -nworst 1]
        #? set t_report [report_timing -nworst 1]
        #? foreach line $t_report {
        #?     if { [string first "Slack:=" $line] != -1 } {
        #?         set max_slack $line
        #?     }
        #? }
    }
    puts  [string repeat "\#" 80]

    ## The following line is required in order to notify EBE of the termination of the flow:
    puts "\n\n\# FLOW STATUS: finished"

} else {
    ## Show layout directly in full screen mode:
    gui_set_ui main -title "Innovus - [get_db flow_cell_name]"
    gui_set_ui main -geometry 1920x1174+2374+44; # Just needs to be big enough.
    gui_show
    gui_fit
    gui_set_draw_view place
}
