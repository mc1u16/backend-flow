# This file is common for Genus and Innovus.
#
# > setup MultiModeMultiCorner setting
# > get the library list from desing config and add the technology lib
# > regenerate local library setting for Innovus (create absolute path/file list)
# > create : library set                    refer to tool
# >          rc corners by qrcTechFile      refer to tool
# >          operating conditions           refer to tool
# >          timing conditions
# >          delay corners
# >          constraint mode refer to available design config .sdc lists
# >          analysis view
# > set analysis view for setup and hold
#

################################################################################
## PROJECT DATA
################################################################################

## Genus and Innovus seem not to use the same concept for libraries.
##
## Genus     uses the 'lib_search_path' attribute for a list of paths,
##           where a set of files can be found.
##
## Innovus   requires the absolute 'path/file.lib' definition.

if { [info exist ::cfg_typical_corner_only] && $::cfg_typical_corner_only == true } {
    set ::libs_typ {}
    if { [info exists ::cfg_hard_macros] } {
        foreach hm_path [dict keys $::cfg_hard_macros] {
            set hm_name [subst [file tail $hm_path]]; # Note substitution of environment variables.
            ## Add liberty file if it was is in the list yet:
            set lib_typ ${hm_name}_typ.lib
            if { [lsearch -exact $::libs_typ $lib_typ] == -1 } {
                lappend ::libs_typ $lib_typ
            }
        }
    }
    imsg_id  -id UFSM-151  -tl 5  -msg "Liberty files for typical corner: $::libs_typ"
} else {
    set ::libs_slow {}
    set ::libs_fast {}
    if { [info exists ::cfg_hard_macros] } {
        foreach hm_path [dict keys $::cfg_hard_macros] {
						puts "hard macro path: $hm_path"
            set hm_name [subst [file tail $hm_path]]; # Note substitution of environment variables.
            ## Add liberty file if it is not in the list yet:
            set lib_slow ${hm_name}_slow.lib
            set lib_fast ${hm_name}_fast.lib
            if { [lsearch -exact $::libs_slow $lib_slow] == -1 } {
                lappend ::libs_slow $lib_slow
                lappend ::libs_fast $lib_fast
            }
        }
    }
    imsg_id  -id UFSM-151  -tl 5  -msg "Liberty files for slow corner: $::libs_slow"
    imsg_id  -id UFSM-151  -tl 5  -msg "Liberty files for fast corner: $::libs_fast"
}

## Set library search paths:
set ::lib_search_paths {}
if { [info exists ::cfg_hard_macros] } {
    foreach hm_path [dict keys $::cfg_hard_macros] {
				#$::env(DIGITAL_WORK_DIR)/digital/blocks/row_driver_ctrl_top/row_driver_ctrl_pair
				#/usr/project-a/epc909/users/mcc/digital/dev/digital/blocks/row_driver_ctrl_top/row_driver_ctrl_pair/be/innovus_outputs/row_driver_ctrl_pair_lib/row_driver_ctrl_pair/timing
				#$::env(ANALOG_WORK_DIR)/epc909/epc909_analog_io_top
        if { [string match "*/digital/*" $hm_path] } {
            ## digital hard macro
            set lib_path [file join $hm_path be innovus_outputs [file tail $hm_path]_lib [file tail $hm_path] timing]
        } else {
            ## analog hard macro
            set lib_path [file join $hm_path timing]
        }
				puts "lib_path: $lib_path"
        ## Add library path if it is not in the list yet:
        if { [lsearch -exact $::lib_search_paths $lib_path] == -1 } {
            lappend ::lib_search_paths $lib_path
        }
    }
}
imsg_id  -id UFSM-151  -tl 5  -msg "Library search paths: $::lib_search_paths"


################################################################################
## Physical Libraries (read_physical of OA libraries)
################################################################################
## Define OA libraries which include the used cell views of:
## technology, digital standard cells, digital and analog macros (including pads)
## The order is important: technology, standard cells, others
## Example: "ohc15l ohc15l_digital ohc15l_io ohc15l_eeprom_cp epc635 epc635_pll adc_analog_ctrl_regs_lib"

## Add technology and standard cell libraries:
set ::oa_ref_libs {
    $::env(TECHNOLOGY_NAME)
    $::env(TECHNOLOGY_NAME)_digital
}

## Append hard macro libraries:
if { [info exists ::cfg_hard_macros] } {
    foreach hm_path [dict keys $::cfg_hard_macros] {
        if { [string match "*/digital/*" $hm_path] } {
            ## digital hard macro
						#$::env(DIGITAL_WORK_DIR)/digital/blocks/row_driver_ctrl_top/row_driver_ctrl_pair
						#row_driver_ctrl_pair_lib
            set ref_lib [file tail $hm_path]_lib
        } else {
            ## analog hard macro
						#$::env(ANALOG_WORK_DIR)/epc909/epc909_analog_io_top
						#epc909
            set ref_lib [lindex [file split $hm_path] end-1]
        }
        ## Add reference library if it is not in the list yet:
        if { [lsearch -exact $::oa_ref_libs $ref_lib] == -1 } {
            lappend ::oa_ref_libs $ref_lib
        }
    }
}
imsg_id  -id UFSM-151  -tl 5  -msg "OA reference libraries: $::oa_ref_libs"


## Store the list of referenced OA libraries as a global attribute for read_physical:
if {[info exist ::oa_ref_libs] && [llength $::oa_ref_libs] > 0 } {
    ## Use a resolved string list (a normal list will cause an "IMPOAX-503" error).
    set_db init_oa_ref_libs [join [subst $::oa_ref_libs]]; # Specifies the list of OA libraries to import. --> would be set as well by 'read_physical'
} else {
    emsg_id_stop -id UFSM-101 -msg "flow_attribute: libraries (oa_ref_libs) not specified -> check flow_mmmc.tcl"
}


## Append technology path to library search path list if not yet contained:
if { [lsearch -exact $::lib_search_paths "[get_db flow_tech_std_path]/liberty"] == -1 } {
    lappend ::lib_search_paths [get_db flow_tech_std_path]/liberty
}
## Append each path once to db - which is only available in Genus
if { [get_db program_short_name] == "genus" } {
    foreach dir [subst $::lib_search_paths] {
        if { [lsearch -exact [get_db lib_search_path] $dir] == -1 } {
            set_db lib_search_path "[get_db lib_search_path] [join $dir]"
        }
    }
    imsg_id -id UFSM-151 -tl 6 -msg "Setting library search path to '[get_db lib_search_path]"
}

## Appending of technology libraries to corner library lists
## First, we extract the tech_file_prefix from a libery file in $STDCELL_PATH/liberty:
##regexp {stdcell_ohc15l_(\w+_\w{2}_\w{1}_\w{2}_\d{1})_\w+} [file tail $::env(STDCELL_PATH)] dummy tech_file_prefix ; # this does not work anymore, name is not in the path!


#% $::env(STDCELL_PATH)/liberty/
#invalid command name "/usr/eda_tooling-a/pdk/epc/ohc15l_1v5/stdcell_ohc15l_1v2r4/liberty/"
#[glob -tails -path $::env(STDCELL_PATH)/liberty/ -- *]
#invalid command name "LF150DC_HS_F_V1_4_typical_conditional.lib 
#                      LF150DC_HS_F_V1_4_worst_worst.lib 
#                      LF150DC_HS_F_V1_4_functional.lib 
#											 lf150dhs9s_worst_worst.lib 
#                      lf150dhs9s_typical_conditional.lib 
#                      LF150DC_HS_F_V1_4_best_best.lib 
#                      lf150dhs9s_functional.lib 
# 										 lf150dhs9s_best_best.lib"

#% puts $tech_file_prefix
#LF150DC_HS_F_V1_4

regexp {(\w+_\w{2}_\w{1}_\w{2}_\d{1})_\w+} [glob -tails -path $::env(STDCELL_PATH)/liberty/ -- *] --> tech_file_prefix

if { [info exist ::cfg_typical_corner_only] && $::cfg_typical_corner_only == true } {
    if { [lsearch -exact $::libs_typ "${tech_file_prefix}_typical_conditional.lib"] == -1 } {
        lappend ::libs_typ  [set tech_file_prefix]_typical_conditional.lib
    }
} else {
    if { [lsearch -exact $::libs_slow "${tech_file_prefix}_worst_worst.lib"] == -1 } {
        lappend ::libs_slow ${tech_file_prefix}_worst_worst.lib
    }
    if { [lsearch -exact $::libs_fast "${tech_file_prefix}_best_best.lib"] == -1 } {
        lappend ::libs_fast [set tech_file_prefix]_best_best.lib
    }
}

## Create the lib file list with absolute paths for Innovus style: {path/file1 path/file2 ... }
if { [get_db program_short_name] == "innovus" } {
    if { [info exist ::cfg_typical_corner_only] && $::cfg_typical_corner_only == true } {
        set libs_typ_abs  [get_absolute_path_list -paths $::lib_search_paths -files $::libs_typ]
        imsg_id -id UFSM-151 -tl 6 -msg "Typ. libraries for Innovus: $libs_typ_abs"
    } else {
        set libs_slow_abs [get_absolute_path_list -paths $::lib_search_paths -files $::libs_slow]
        set libs_fast_abs [get_absolute_path_list -paths $::lib_search_paths -files $::libs_fast]
        imsg_id -id UFSM-151 -tl 6 -msg "Slow libraries for Innovus: $libs_slow_abs"
        imsg_id -id UFSM-151 -tl 6 -msg "Fast libraries for Innovus: $libs_fast_abs"
    }
}

## Define CAP table:
set cap_table_path [get_db flow_tech_path]/ruledecks/LPE/ohc15l_merlion_typical.capTbl
if { [get_db program_short_name] == "genus" } {
    if { [file exists $cap_table_path] } {
        set_db cap_table_file $cap_table_path
    } else {
        wmsg_id -id USFM-120 -tl 0 -msg  "WARNING: CAP table file not found at $cap_table_path"
    }
}

##############################################################################
## LIBRARY SETS depending on genus or innovus style
##############################################################################
if { [get_db program_short_name] == "genus" } {
    if { [info exist ::cfg_typical_corner_only] && $::cfg_typical_corner_only == true } {
        create_library_set  -name lib_typ   -timing $::libs_typ
    } else {
        create_library_set  -name lib_fast  -timing $::libs_fast
        create_library_set  -name lib_slow  -timing $::libs_slow
    }
} elseif { [get_db program_short_name] == "innovus" } {
    if { [info exist ::cfg_typical_corner_only] && $::cfg_typical_corner_only == true } {
        create_library_set  -name lib_typ   -timing $libs_typ_abs
    } else {
        create_library_set  -name lib_fast  -timing $libs_fast_abs
        create_library_set  -name lib_slow  -timing $libs_slow_abs
    }
} else {
    emsg_id_stop  -id USFM-100  -msg "flow_mmmc.tcl: create_library_set not defined for genus nor innovus"
}


##############################################################################
## RC CORNERS
##############################################################################
## **WARN: (IMPEXT-6202): (on initialisation of innovus)
##     In addition to the technology file, the capacitance table file is specified for all the RC corners.
##     If the technology file is already specified for all the RC corners, the capacitance table file is
##     not required for preRoute and postRoute extraction. In a new session, the capacitance table files
##     can be removed from the create_rc_corner command to enable the technology file to be used for
##     preRoute and postRoute (effort level medium/high/signoff) extraction engines.


## define operating conditions; check if configured , else define defaults
## example in config file: set cfg_operating_conditions(<corner>) {<process> <voltage> temperature}
##     corner: fast, typ, slow
## NOTE: Process, voltage and temperature are set such that they are matching to the available technology liberty files.
##       Currently (2019-08-20), we only have technology liberty files for (1.0, 1.98, -40), (1.0, 1.80, 25) and (1.0, 1.62, 125).
imsg_id -id USFM-151 -tl 1 -msg "Executing operation coditions ..."
foreach op_condition {fast typ slow} {
    if [info exist ::cfg_operating_conditions([set op_condition])] {
        if { [llength $::cfg_operating_conditions([set op_condition])] == 3 } {
            set mmmc_op_condition_[set op_condition](process)     [lindex $::cfg_operating_conditions([set op_condition]) 0]
            set mmmc_op_condition_[set op_condition](voltage)     [lindex $::cfg_operating_conditions([set op_condition]) 1]
            set mmmc_op_condition_[set op_condition](temperature) [lindex $::cfg_operating_conditions([set op_condition]) 2]
        } else {
            set mmmc_op_condition_[set op_condition](process)     1.0
            set mmmc_op_condition_[set op_condition](voltage)     [expr [string match [set op_condition] "fast"] ? 1.98:[expr [string match [set op_condition] "slow"] ? 1.62:1.8]]
            set mmmc_op_condition_[set op_condition](temperature) [expr [string match [set op_condition] "fast"] ? -40:[expr [string match [set op_condition] "slow"] ? 125:25]]
        }
    } else {
        set mmmc_op_condition_[set op_condition](process)     1.0
        set mmmc_op_condition_[set op_condition](voltage)     [expr [string match [set op_condition] "fast"] ? 1.98:[expr [string match [set op_condition] "slow"] ? 1.62:1.8]]
        set mmmc_op_condition_[set op_condition](temperature) [expr [string match [set op_condition] "fast"] ? -40:[expr [string match [set op_condition] "slow"] ? 125:25]]
    }
}

if { [get_db program_short_name] == "genus" } {
    if { [info exist ::cfg_typical_corner_only] && $::cfg_typical_corner_only == true } {
        create_rc_corner  -name rc_typ  -temperature $mmmc_op_condition_typ(temperature) \
            -qrc_tech  [get_db flow_tech_path]/ruledecks/LPE/qrcTechFile \
            -cap_table [get_db cap_table_file]
    } else {
        create_rc_corner  -name rc_fast  -temperature $mmmc_op_condition_fast(temperature) \
            -qrc_tech  [get_db flow_tech_path]/ruledecks/LPE/qrcTechFile \
            -cap_table [get_db cap_table_file]
        create_rc_corner  -name rc_slow  -temperature $mmmc_op_condition_slow(temperature) \
            -qrc_tech  [get_db flow_tech_path]/ruledecks/LPE/qrcTechFile \
            -cap_table [get_db cap_table_file]
    }
} elseif { [get_db program_short_name] == "innovus" } {
    if { [info exist ::cfg_typical_corner_only] && $::cfg_typical_corner_only == true } {
        create_rc_corner -name rc_typ -temperature $mmmc_op_condition_typ(temperature) \
            -qrc_tech  [get_db flow_tech_path]/ruledecks/LPE/qrcTechFile
    } else {
        create_rc_corner -name rc_fast -temperature $mmmc_op_condition_fast(temperature) \
            -qrc_tech  [get_db flow_tech_path]/ruledecks/LPE/qrcTechFile
        create_rc_corner -name rc_slow -temperature $mmmc_op_condition_slow(temperature) \
            -qrc_tech  [get_db flow_tech_path]/ruledecks/LPE/qrcTechFile
    }
} else {
    emsg_id_stop -id USFM-100 -msg "flow_mmmc.tcl: create_rc_corner not defined for genus nor innovus"
}


##############################################################################
## OPERATING CONDITIONS depending on genus or innovus style
##############################################################################
## without definition, thay are taken from the liberty files       
## create_opcond - Creates a set of virtual operating conditions in the specified library without actually modifying the library


if {[get_db program_short_name] == "genus"} {
    if { [info exist ::cfg_typical_corner_only] && $::cfg_typical_corner_only == true } {
        create_opcond -name op_typ  -process $mmmc_op_condition_typ(process) -voltage $mmmc_op_condition_typ(voltage)  -temperature  $mmmc_op_condition_typ(temperature) -tree_type balanced_tree
    } else {
        create_opcond -name op_fast -process $mmmc_op_condition_fast(process) -voltage $mmmc_op_condition_fast(voltage) -temperature $mmmc_op_condition_fast(temperature) -tree_type best_case_tree
        create_opcond -name op_slow -process $mmmc_op_condition_slow(process) -voltage $mmmc_op_condition_slow(voltage) -temperature $mmmc_op_condition_slow(temperature) -tree_type worst_case_tree
    }
} elseif {[get_db program_short_name] == "innovus"} {
    if { [info exist ::cfg_typical_corner_only] && $::cfg_typical_corner_only == true } {
        create_opcond -name op_typ  -P $mmmc_op_condition_typ(process) -V $mmmc_op_condition_typ(voltage)  -T  $mmmc_op_condition_typ(temperature)
    } else {
        create_opcond -name op_fast -P $mmmc_op_condition_fast(process) -V $mmmc_op_condition_fast(voltage) -T $mmmc_op_condition_fast(temperature)
        create_opcond -name op_slow -P $mmmc_op_condition_slow(process) -V $mmmc_op_condition_slow(voltage) -T $mmmc_op_condition_slow(temperature)
    }
} else {
    emsg_id_stop -id USFM-100 -msg "flow_mmmc.tcl: create_opcond not defined for genus nor innovus"
}


##############################################################################
## TIMING CONDITIONS
##############################################################################
if { [info exist ::cfg_typical_corner_only] && $::cfg_typical_corner_only == true } {
    create_timing_condition -name tc_typ  -library_sets lib_typ  -opcond op_typ
} else {
    create_timing_condition -name tc_fast -library_sets lib_fast -opcond op_fast
    create_timing_condition -name tc_slow -library_sets lib_slow -opcond op_slow
}


##############################################################################
## DELAY CORNERS
##############################################################################
if { [info exist ::cfg_typical_corner_only] && $::cfg_typical_corner_only == true } {
    create_delay_corner -name dc_typ  -rc_corner rc_typ  -timing_condition tc_typ
} else {
    create_delay_corner -name dc_fast -rc_corner rc_fast -timing_condition tc_fast
    create_delay_corner -name dc_slow -rc_corner rc_slow -timing_condition tc_slow
}


##############################################################################
## CONSTRAINT MODES
##############################################################################

## Quick combination overview of SDC files:
##
## ---------------+------------------------------+------------------------------------------
## contraint mode | unflattened (-sdc_files)     | flattened (-ilm_sdc_files)
## ---------------+------------------------------+------------------------------------------
## cm_main        | main                         | main
## cm_func_main   | main + func_main             | main + func_main + func_ilm
## cm_func_<X>    | main + func_main + func_<X>  | main + func_main + func_<X> + func_ilm
## cm_test        | main + test                  | main + test
## ---------------+------------------------------+------------------------------------------
##
### Functional constraint modes
if ![info exists ::cfg_func_constraint_modes] {
    emsg_id_stop -id USFM-100 -msg "flow_mmmc: List of functional constraint modes is missing.\n       : -> Check [get_db flow_cell_name]_config.tcl."
} else {
    set path_to_main_sdc_file            "[get_db flow_cell_cfg_path]/[get_db flow_cell_name]_main_constraints.sdc"
    set path_to_func_main_sdc_file       "[get_db flow_cell_cfg_path]/[get_db flow_cell_name]_func_main_constraints.sdc"
    set path_to_func_ilm_sdc_file        "[get_db flow_cell_cfg_path]/[get_db flow_cell_name]_func_ilm_constraints.sdc"
    set path_to_func_post_synth_sdc_file "[get_db flow_cell_cfg_path]/[get_db flow_cell_name]_func_post_synth_constraints.sdc"

    if { ![file exists $path_to_main_sdc_file] } {
        wmsg_id -id USFM-120 -msg "flow_mmmc: Main SDC file for functional and test constraint modes could not be found:\n        : $path_to_main_sdc_file"
        set path_to_main_sdc_file ""
    }
    if { ![file exists $path_to_func_main_sdc_file] } {
        wmsg_id -id USFM-120 -msg "flow_mmmc: Main SDC file for functional constraint modes could not be found:\n        : $path_to_func_main_sdc_file"
        set path_to_func_main_sdc_file ""
    }
    if { [info exists ::cfg_hard_macros] && [llength [dict filter $::cfg_hard_macros value ilm]] > 0 } {
        if { ![file exists $path_to_func_ilm_sdc_file] } {
            wmsg_id -id USFM-120 -msg "flow_mmmc: An ILM SDC file for functional constraint modes could not be found:\n        : $path_to_func_ilm_sdc_file"
            set path_to_func_ilm_sdc_file ""
        }
    } else {
        set path_to_func_ilm_sdc_file ""
    }
    if { [get_db program_short_name] == "innovus" } {
        if { ![file exists $path_to_func_post_synth_sdc_file] } {
            wmsg_id -id USFM-120 -msg "flow_mmmc: An SDC file containing functional post-synthesis constraints could not be found:\n        : $path_to_func_post_synth_sdc_file"
            set path_to_func_post_synth_sdc_file ""
        }
    }

    if { [llength $::cfg_func_constraint_modes] > 0} {
        foreach cm $::cfg_func_constraint_modes {
            set path_to_sdc_file "[get_db flow_cell_cfg_path]/[get_db flow_cell_name]_[string range $cm 3 20]_constraints.sdc"
            if { ![file exists $path_to_sdc_file] } {
                emsg_id_stop -id USFM-100 -msg "flow_mmmc: SDC file could not be found at:\n        : $path_to_sdc_file\n        : -> Check [get_db flow_cell_name]_config.tcl."
            } else {
                if { [get_db program_short_name] == "genus" } {
                    if { [info exists ::cfg_hard_macros] && [llength [dict filter $::cfg_hard_macros value ilm]] > 0 } {
                        create_constraint_mode  -name $cm \
                            -sdc_files     "" \
                            -ilm_sdc_files "$path_to_main_sdc_file $path_to_func_main_sdc_file $path_to_func_ilm_sdc_file $path_to_sdc_file"
                    } else {
                        create_constraint_mode  -name $cm \
                            -sdc_files "$path_to_main_sdc_file $path_to_func_main_sdc_file $path_to_sdc_file"
                    }
                } elseif { [get_db program_short_name] == "innovus" } {
                    create_constraint_mode  -name $cm \
                        -sdc_files     "$path_to_main_sdc_file $path_to_func_main_sdc_file $path_to_func_post_synth_sdc_file $path_to_sdc_file" \
                        -ilm_sdc_files "$path_to_main_sdc_file $path_to_func_main_sdc_file $path_to_func_post_synth_sdc_file $path_to_func_ilm_sdc_file $path_to_sdc_file"
                }
            }
        }
    } elseif { $path_to_func_main_sdc_file != "" || $path_to_main_sdc_file != "" } {

        if { $path_to_func_main_sdc_file != "" } {
            set cm cm_func_main
        } else {
            set cm cm_main
            if { $path_to_func_ilm_sdc_file != "" } {
                wmsg_id -id USFM-120 -msg "flow_mmmc: An ILM SDC file for functional constraint modes is not supported for such a minimalist set of constraint files:\n        : $path_to_func_ilm_sdc_file"
                set path_to_func_ilm_sdc_file ""; # ILMs are not supported for such a minimalist set of constraints.
            }
        }

        if { [get_db program_short_name] == "genus" } {
            if { [info exists ::cfg_hard_macros] && [llength [dict filter $::cfg_hard_macros value ilm]] != 0 } {
                create_constraint_mode  -name $cm  \
                    -sdc_files     "" \
                    -ilm_sdc_files "$path_to_main_sdc_file $path_to_func_main_sdc_file $path_to_func_ilm_sdc_file"
            } else {
                create_constraint_mode  -name $cm  \
                    -sdc_files "$path_to_main_sdc_file $path_to_func_main_sdc_file"
            }
        } elseif { [get_db program_short_name] == "innovus" } {
            create_constraint_mode  -name $cm  \
                -sdc_files     "$path_to_main_sdc_file $path_to_func_main_sdc_file $path_to_func_post_synth_sdc_file"  \
                -ilm_sdc_files "$path_to_main_sdc_file $path_to_func_main_sdc_file $path_to_func_post_synth_sdc_file $path_to_func_ilm_sdc_file"
        }
    } else {
        emsg_id_stop -id UFSM-100 -msg "flow_mmmc: Available set of SDC files is not supported."
    }
}


### Test constraint modes (may not be available in initial design)
if { [info exists ::cfg_dft_disabled] && $::cfg_dft_disabled } {
    imsg_id  -id UFSM-151  -tl 0  -msg "flow_mmmc: DFT ist disabled."
} elseif { ![info exists ::cfg_test_constraint_modes] } {
    emsg_id -id USFM-100 -msg "flow_mmmc: List of test constraint modes is missing.\n       : -> Check [get_db flow_cell_name]_config.tcl."
} else {
    foreach cm [subst $::cfg_test_constraint_modes] {
        set path_to_sdc_file "[get_db flow_cell_cfg_path]/[get_db flow_cell_name]_[string range $cm 3 20]_constraints.sdc"
        if { ![file exists $path_to_sdc_file] } {
            emsg_id_stop -id USFM-100 -msg "flow_mmmc: SDC file could not be found at:\n        : $path_to_sdc_file"
        } else {
            if { [get_db program_short_name] == "genus" } {
                if { [info exists ::cfg_hard_macros] && [llength [dict filter $::cfg_hard_macros value ilm]] != 0 } {
                    create_constraint_mode  -name $cm  \
                        -sdc_files     "" \
                        -ilm_sdc_files "$path_to_main_sdc_file $path_to_sdc_file"
                } else {
                    create_constraint_mode  -name $cm  \
                        -sdc_files "$path_to_main_sdc_file $path_to_sdc_file"
                }
            } elseif { [get_db program_short_name] == "innovus" } {
                create_constraint_mode  -name $cm  \
                    -sdc_files     "$path_to_main_sdc_file $path_to_sdc_file"  \
                    -ilm_sdc_files "$path_to_main_sdc_file $path_to_sdc_file"
            }
        }
    }
}


##############################################################################
## ANALYSIS VIEWS
##############################################################################
## Create analysis views related to functional and test constraint modes:
foreach cm [get_db constraint_modes] {
		puts "DEBUG cm: $cm"
    if { [get_db program_short_name] == "genus" } {
        ## example: 'constraint_mode:__default_mmmc_spec/cm_func_0 constraint_mode:__default_mmmc_spec/cm_test'
        set cm_name [lindex [split $cm / ] 1]
    } elseif { [get_db program_short_name] == "innovus" } {
        ## example: 'constraint_mode:cm_test constraint_mode:cm_func_0'
        set cm_name [lindex [split $cm : ] 1]
    }
		puts "DEBUG cm_name: $cm_name"
    if { [string range $cm_name 0 2] != "cm_" } {
        emsg_id_stop -id USFM-100 -msg "flow_mmmc.tcl: invalid constraint mode name: $cm_name"
    } else {
        set cm_short [string range $cm_name 3 end]; # short form of constraint mode name
				puts "DEBUG cm_short: $cm_short"
        if { [info exist ::cfg_typical_corner_only] && $::cfg_typical_corner_only == true } {
            create_analysis_view -name "av_${cm_short}_typ"  -constraint_mode $cm_name -delay_corner dc_typ
        } else {
            create_analysis_view -name "av_${cm_short}_fast" -constraint_mode $cm_name -delay_corner dc_fast
            create_analysis_view -name "av_${cm_short}_slow" -constraint_mode $cm_name -delay_corner dc_slow
        }
    }
}

# DEBUG cm: constraint_mode:cm_func_main
# DEBUG cm_name: cm_func_main
# DEBUG cm_short: func_main

# DEBUG cm: constraint_mode:cm_test
# DEBUG cm_name: cm_test
# DEBUG cm_short: test

# DEBUG analysis_views_setup:av_test_slow av_func_main_slow
# DEBUG analysis_views_hold:av_test_fast av_func_main_fast

##############################################################################
## ACTIVE ANALYSIS VIEWS
##############################################################################

set analysis_views_setup {}
set analysis_views_hold  {}

foreach av [get_db analysis_views] {
    if { [get_db program_short_name] == "genus" } {
        if { [lindex [split [get_db $av .delay_corner] /] 1] == "dc_slow" } {
            lappend analysis_views_setup [get_db $av .name]
        } elseif {[lindex [split [get_db $av .delay_corner] /] 1] == "dc_fast" } {
            lappend analysis_views_hold [get_db $av .name]
        }
    } elseif { [get_db program_short_name] == "innovus" } {
        if { [lindex [split [get_db $av .delay_corner] :] 1] == "dc_slow" } {
            lappend analysis_views_setup [get_db $av .name]
        } elseif { [lindex [split [get_db $av .delay_corner] :] 1] == "dc_fast" } {
            lappend analysis_views_hold [get_db $av .name]
        }
    }
}
puts "DEBUG analysis_views_setup:$analysis_views_setup"
puts "DEBUG analysis_views_hold:$analysis_views_hold"

set_analysis_view  -setup $analysis_views_setup  -hold $analysis_views_hold


