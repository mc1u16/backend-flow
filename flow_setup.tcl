## FLOW SETUP ##################################################################
##
## This file is mandatory for each eBe-based backend flow.
##
## Content:
## > At least one list describing a flow, e.g. the standard flow.
## > Lists of flow steps corresponding to the sub-flows used in flow descriptions.
## > Definitions of ALL root attributes
## > Definitions of user-defined messages (Genus-specific, i.e. not available
##   in Innovus version 17.13)


################################################################################
## Flow Setup: Definition of Flow Steps for Physical Implementation (Innovus)
################################################################################

## The following list defines the sub-flows used in the standard flow 'std'.
## NOTE: In order to define other flows, another list with a name starting
##       with 'flow_list_' needs to be defined. For another flow called
##       'eco', a list called 'flow_list_eco' needs to be defined for example.
## Split flow: Init = non-OA mode -> verilog netlist + OA libs -> save OA fp-cell
##             Std  = OA mode -> load OA fp-cell -> run trough be

set flow_list_init {
    fp
}

set flow_force_load_std { fp }

set flow_list_std {
    prects
    cts
    postcts
    route
    postroute
    finalize
}

## The following balloon lists define the tooltips shown for the sub-flows.
## NOTE: The assignment is based on the order in the list.

set flow_balloon_help_init {
    "fp: init OA - fp - power - save OA\n
--------------------------------------------------\n
    start_flow:     setup scratch, metric\n
    init_design:    read_mmmc, read_physical (OA), read_netlist, read/commit_power_intent, read_def, load Attributes\n
    init_floorplan: (details: flow_step/flow_step_init_floorplan)\n
                    INIT:   global settings, set size, reshape size\n
                    PLACE:  io/pins, powerring+modif, inst (wo.rails), create cells (wo.rails), pin on pads,\n
                            inst_array (wo.rails), halo, MET1 rails, inst_array (on rail), fp blockages\n
    power_route:    refer to cfg_run_power_connection\n
                             cfg_finalize_power_std_cell_connection\n
                             cfg_fix_power_via_violations\n
                    after_fp blockages"
}

set flow_balloon_help_std {
    "prects:\n- global placing\n- reporting (area, timing, power)"
    "cts:\n- clock tree specification\n- clock tree synthesis\n- tie\n- reporting"
    "postcts:\n- optimization (setup and hold) with clock trees\n- reporting"
    "route:\n- adding of filler cells\n- detail routing\n- reporting"
    "postroute:\n- optimization after routing\n- reporting"
    "finalize:\n- parasitic extraction\n- signoff STA\n- writing of netlists"
}


## The following lists with the base name 'flow_step_list_' define the sets of
## flow steps corresponding to a certain sub-flow, e.g. the flow step list
## 'flow_step_list_foo' would define the flow steps of the sub-flow 'foo'.

set flow_step_list_fp {
    start_flow
    init_design
    init_floorplan
    power_route
    finish_flow
}

### @@@ tbl: QnD report skipping --> add a flag in ebe tool, similar as metric disable
set flow_report_enable false

if { $flow_report_enable == "true" } {
    set flow_step_list_prects {
        start_flow
        read_ilms
        add_port_diode
        add_spare_cell    
        run_place_opt
        report_area
        report_timing_late
        report_late_path
        report_power
        finish_flow
    }
    
    set flow_step_list_cts {
        start_flow
        add_clock_tree_spec
        add_clock_tree
        add_tieoffs
        report_area
        report_timing_early
        report_early_path
        report_timing_late
        report_late_path
        report_clock_timing
        report_power
        finish_flow
    }
    
    set flow_step_list_postcts {
        start_flow
        run_opt_postcts_hold
        report_area
        report_timing_early
        report_early_path
        report_timing_late
        report_late_path
        report_clock_timing
        report_power
        finish_flow
    }
    
    set flow_step_list_route {
        start_flow
        add_fillers
        run_route
        fix_antenna
        report_area
        report_timing_early
        report_early_path
        report_timing_late
        report_late_path
        report_clock_timing
        report_power
        report_route_drc
        report_route_density
        finish_flow
    }
    
    set flow_step_list_postroute {
        start_flow
        run_opt_postroute
        fix_trans
        report_area
        report_timing_early
        report_early_path
        report_timing_late
        report_late_path
        report_clock_timing
        report_power
        report_route_drc
        report_route_density
        finish_flow
    }
    
    set flow_step_list_finalize {
        start_flow
        extract
        sign_off
        write_netlist
        finish_flow
    }
} else {
    set flow_step_list_prects {
        start_flow
        read_ilms
        add_port_diode
        add_spare_cell    
        run_place_opt
        finish_flow
    }

    set flow_step_list_cts {
        start_flow
        add_clock_tree_spec
        add_clock_tree
        add_tieoffs
        finish_flow
    }
    
    set flow_step_list_postcts {
        start_flow
        run_opt_postcts_hold
        finish_flow
    }
    
    set flow_step_list_route {
        start_flow
        add_fillers
        run_route
        fix_antenna
        finish_flow
    }
    
    set flow_step_list_postroute {
        start_flow
        run_opt_postroute
        fix_trans
        report_power
        report_route_drc
        report_route_density
        finish_flow
    }
    
    set flow_step_list_finalize {
        start_flow
        extract
        sign_off
        write_netlist
        finish_flow
    }
}

################################################################################
## Flow Setup: Definition of User-Defined ROOT Attributes
################################################################################

## NOTE: The names of attributes which are not predefined by Cadence should
##       start with the prefix 'epc_'.

#- Define attribute for flow script path
if { ![is_attribute -obj_type root flow_source_directory] } {
    define_attribute flow_source_directory -category flow -data_type string  -obj_type root \
        -default_value [file normalize [file dirname [info script]]] \
        -help_string "Flow script source location"
}

## Define attribute for relative path to synthesis output directory on the server:
if { ![is_attribute  -obj_type root  epc_flow_synthesis_output_directory]} {
    define_attribute  epc_flow_synthesis_output_directory  -category flow  -data_type string  -obj_type root \
        -default_value genus_outputs \
        -help_string "Relative path to synthesis output directory on the server"
}

## Define attribute for relative path to place-and-route output directory on the server:
if { ![is_attribute  -obj_type root  epc_flow_place_route_output_directory]} {
    define_attribute  epc_flow_place_route_output_directory  -category flow  -data_type string  -obj_type root \
        -default_value innovus_outputs \
        -help_string "Relative path to place-and-route output directory on the server"
}

#- Define attribute for flow include files
if { ![is_attribute -obj_type root flow_include_files]} {
    define_attribute flow_include_files -category flow -data_type string  -obj_type root \
        -default_value "" \
        -help_string "Files to use in flow customization"
}

#- Define attribute for report name
if { ![is_attribute -obj_type root flow_report_name]} {
    define_attribute flow_report_name -category flow -data_type string  -obj_type root \
        -default_value "" \
        -help_string "Name to use during report generation"
}

## Define attribute for epc_flow:
if { ![is_attribute -obj_type root epc_flow]} {
    define_attribute epc_flow -category flow -data_type string  -obj_type root \
        -default_value "" \
        -help_string "Name of current flow"
}

#- Define attribute for flow step start
if { ![is_attribute -obj_type root flow_step_start]} {
    define_attribute flow_step_start -category flow -data_type string  -obj_type root \
        -default_value "" \
        -help_string "Flow step start name"
}

#- Define attribute for flow step stop
if { ![is_attribute -obj_type root flow_step_stop]} {
    define_attribute flow_step_stop -category flow -data_type string  -obj_type root \
        -default_value "" \
        -help_string "Flow step stop name"
}

#- Define attribute for flow step suspend
if { ![is_attribute -obj_type root flow_step_suspend]} {
    define_attribute flow_step_suspend -category flow -data_type string  -obj_type root \
        -default_value "" \
        -help_string "Flow step suspend name"
}

#- Define attribute for flow metrics generation
if { ![is_attribute -obj_type root flow_metric_enable]} {
    define_attribute flow_metrics_enable -category flow -data_type bool  -obj_type root \
        -default_value 1 \
        -help_string "Flow metrics generation enable control"
}

## Define attribute for relative path to local result directory:
if { ![is_attribute  -obj_type root  epc_flow_result_directory]} {
    define_attribute  epc_flow_result_directory  -category flow  -data_type string  -obj_type root \
        -default_value "results" \
        -help_string "Relative path to local result directory"
}

## Define attribute for report prefix
if { ![is_attribute -obj_type root flow_report_prefix]} {
    define_attribute flow_report_prefix -category flow -data_type string  -obj_type root \
        -default_value "" \
        -help_string "File prefix to be used during report generation"
}

## Project name
if { ![is_attribute -obj_type root flow_project_name] } {
    define_attribute flow_project_name -category flow -data_type string  -obj_type root \
        -default_value "" \
        -help_string "Flow project name"
}

## CELL name, path and config
if { ![is_attribute -obj_type root flow_cell_name] } {
    define_attribute flow_cell_name -category flow -data_type string  -obj_type root \
        -default_value $::env(CELL_NAME) \
        -help_string "Flow cell name"
}

if { ![is_attribute -obj_type root flow_cell_path] } {
    define_attribute flow_cell_path -category flow -data_type string  -obj_type root \
        -default_value $::env(CELL_PATH) \
        -help_string "Flow cell path"
}

if { ![is_attribute -obj_type root flow_cell_cfg_dir] } {
    define_attribute flow_cell_cfg_dir  -category flow  -data_type string  -obj_type root \
        -default_value $::env(CELL_CFG_DIR) \
        -help_string "Flow cell configuration directory"
}

if { ![is_attribute -obj_type root flow_cell_cfg_path] } {
    define_attribute flow_cell_cfg_path -category flow -data_type string  -obj_type root \
        -default_value [file join [get_db flow_cell_path] [get_db flow_cell_cfg_dir]] \
        -help_string "Flow cell configuration path (location of cell configuration file)"
}


## Debug level
if { ![is_attribute -obj_type root flow_debug_level] } {
    define_attribute flow_debug_level -category flow -data_type string  -obj_type root \
        -default_value 0 \
        -help_string "Flow debug level"
}

# EPC log file
if { ![is_attribute -obj_type root flow_epc_log_file] } {
    define_attribute flow_epc_log_file -category flow -data_type string  -obj_type root \
        -default_value "" \
        -help_string "log file for dedicated EPC messages"
}

## EPC report file
if { ![is_attribute -obj_type root flow_epc_rpt_file] } {
    define_attribute flow_epc_rpt_file -category flow -data_type string  -obj_type root \
        -default_value "" \
        -help_string "report file for dedicated EPC messages"
}

## OA library pointer
if { ![is_attribute -obj_type root flow_oa_cds_lib] } {
    define_attribute flow_oa_cds_lib -category flow -data_type string -obj_type root \
        -default_value "" \
        -help_string "Flow OA CDS lib pointer"

}

## Technology
if { ![is_attribute -obj_type root flow_tech_name] } {
    define_attribute flow_tech_name -category flow -data_type string  -obj_type root \
        -default_value "" \
        -help_string "Flow technology name"
}

if { ![is_attribute -obj_type root flow_tech_path] } {
    define_attribute flow_tech_path -category flow -data_type string  -obj_type root \
        -default_value "" \
        -help_string "Flow technology path"
}

if { ![is_attribute -obj_type root flow_tech_std_path] } {
    define_attribute flow_tech_std_path -category flow -data_type string  -obj_type root \
        -default_value "" \
        -help_string "Flow technology standard cell path"
}

if { ![is_attribute -obj_type root flow_tech_process] } {
    define_attribute flow_tech_process -category flow -data_type string  -obj_type root \
        -default_value "" \
        -help_string "Flow technology process"
}

if { ![is_attribute -obj_type root cap_table_file] } {
    define_attribute cap_table_file -category flow -data_type string  -obj_type root \
        -default_value "" \
        -help_string "cap_table_file attribute is only defined in genus"
}

## SDC driver
if { ![is_attribute -obj_type root flow_sdc_weak_driver] } {
    define_attribute flow_sdc_weak_driver -category flow -data_type string  -obj_type root\
        -default_value "BUF_X3" \
        -help_string "SDC weak driver"
}
if { ![is_attribute -obj_type root flow_sdc_weak_driver_pin] } {
    define_attribute flow_sdc_weak_driver_pin -category flow -data_type string  -obj_type root\
        -default_value "Q" \
        -help_string "SDC weak driver pin"
}
if { ![is_attribute -obj_type root flow_sdc_strong_driver] } {
    define_attribute flow_sdc_strong_driver -category flow -data_type string  -obj_type root\
        -default_value "BUF_X8" \
        -help_string "SDC strong driver"
}
if { ![is_attribute -obj_type root flow_sdc_strong_driver_pin] } {
    define_attribute flow_sdc_strong_driver_pin -category flow -data_type string  -obj_type root\
        -default_value "Q" \
        -help_string "SDC strong driver pin"
}


################################################################################
# Flow Setup: define user messages - NOT AVAILABLE in innovus, SKIPPED
################################################################################

if { [get_db program_short_name] == "innovus" } {
    puts "Info: no dedicated user messages in Innovus available, skip 'define_msg'"
} else {
    ## Environment messages
    if { [get_db message_groups UEVM] == "" } {
        define_msg -error -group UEVM -id 100 \
            -long_description  "Selected variable undefined." \
            -short_description "User Environment Variable Undefined Error"

        define_msg -warning -group UEVM -id 120 \
            -long_description  "Defining variable to default value." \
            -short_description "User Environment Variable Default Setting Warning"

        define_msg -info_priority 2 -group UEVM -id 150 \
            -long_description  "Defining environment variable to new value." \
            -short_description "User Environment Variable Define Info"
    }

    ## File messages
    if { [get_db message_groups USFM] == "" } {
        define_msg -error -group USFM -id 100 \
            -long_description  "Specified file cannot be found." \
            -short_description "User Specified File Not Found Error"

        define_msg -warning -group USFM -id 120 \
            -long_description  "Specified file cannot be found." \
            -short_description "User Specified File Not Found Warning"

        define_msg -info_priority 2 -group USFM -id 150 \
            -long_description  "Specified file created." \
            -short_description "User Specified File Created Info"

        define_msg -info_priority 2 -group USFM -id 151 \
            -long_description  "Specified file found." \
            -short_description "User Specified File Found Info"
    }

    ## HDL parser
    if { [get_db message_groups UHDL] == "" } {
        define_msg -info_priority 2 -group UHDL -id 150 \
            -long_description  "HDL parser source file" \
            -short_description "User HDL Parser Info"

        define_msg -info_priority 2 -group UHDL -id 151 \
            -long_description  "HDL parser adding source file" \
            -short_description "User HDL Parser Info"

        define_msg -info_priority 2 -group UHDL -id 152 \
            -long_description  "HDL parser performing line" \
            -short_description "User HDL Parser Info"

        define_msg -info_priority 2 -group UHDL -id 153 \
            -long_description  "HDL source line has been ignored." \
            -short_description "User HDL Parser Info"

        define_msg -warning -group UHDL -id 120 \
            -long_description  "HDL source file has been ignored." \
            -short_description "User HDL Parser Warning"

        define_msg -warning -group UHDL -id 121 \
            -long_description  "HDL parser got unexpected input." \
            -short_description "User HDL Parser Warning"
    }

    ## Flow
    if { [get_db message_groups UFSM] == "" } {
        define_msg -error -group UFSM -id 100 \
            -long_description  "A missing information prevents flow from proceeding." \
            -short_description "Flow System Error"

        define_msg -error -group UFSM -id 101 \
            -long_description  "A procedure argument vs. data mismatch occurred." \
            -short_description "Flow System Error"

        define_msg -error -group UFSM -id 102 \
            -long_description  "Requested attribute not found." \
            -short_description "Flow System Error"

        define_msg -error -group UFSM -id 103 \
            -long_description  "A procedure argument mismatch occurred." \
            -short_description "Flow System Error"

        define_msg -warning -group UFSM -id 120 \
            -long_description  "A missing information was ignored." \
            -short_description "Flow System Warning"

        define_msg -info_priority 2 -group UFSM -id 150 \
            -long_description  "Required information found." \
            -short_description "Flow System Information"

        define_msg -info_priority 2 -group UFSM -id 151 \
            -long_description  "Flow information." \
            -short_description "Flow System Information"

        define_msg -info_priority 2 -group UFSM -id 152 \
            -long_description  "Attribute value updated." \
            -short_description "Flow System Information"

        define_msg -info_priority 2 -group UFSM -id 153 \
            -long_description  "Executing user command." \
            -short_description "Flow System Information"

        define_msg -info_priority 2 -group UFSM -id 154 \
            -long_description  "A duplicated information was ignored." \
            -short_description "Flow System Information"

        define_msg -info_priority 2 -group UFSM -id 155 \
            -long_description  "Report generation." \
            -short_description "Flow System Information"

        define_msg -info_priority 2 -group UFSM -id 156 \
            -long_description  "Database generation." \
            -short_description "Flow System Information"

    }

    ## DFT
    if { [get_db message_groups UDFT] == "" } {
        define_msg -info_priority 2 -group UDFT -id 150 \
            -long_description  "Information." \
            -short_description "DFT Information"
    }
}
