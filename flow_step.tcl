## FLOW STEP DEFINITIONS ######################################################
##
## This file is mandatory for each eBe-based backend flow.
##
##   DEFINE_PROC                         FUNCTION
##   --------------------------------------------------------------------------
##   flow_step_start_flow                metric setup
##   flow_step_finish_flow               pushing of metric snapshot etc.
##   flow_step_init_cds_lib              parse main cds_lib and create local one
##   flow_step_init_design               reading of MMMC, physical libraries, power constraints (.cpf) and design initilization
##   flow_step_init_floorplan            init floorplan step (size, cut; place inst, IO, pins, array; stripes)
##   flow_step_power_route               all special route by
##                                          cfg_run_power_connection                (source .tcl and execute procedures)
##                                          cfg_finalize_power_std_cell_connection  (separate std cell connection)
##                                          cfg_fix_power_via_violations            (separate via fix step - old version)
##                                          cfg_run_final_power
##                                          add blockages "after_fp"                (add the blockages for routing)
##
##   flow_step_create_cost_group
##   flow_step_add_tracks
##   flow_step_run_place_opt
##   flow_step_add_clock_tree_spec
##   flow_step_add_clock_tree
##   flow_step_add_tieoffs
##   flow_step_add_spare_cell
##   flow_step_run_opt_postcts_hold
##   flow_step_add_fillers              FILLER and DECAP cells seams NOT respect MET1 routing --> TODO @@@ REVIEW (seen in 909)
##   flow_step_run_route
##   flow_step_run_opt_postroute
##   flow_step_write_netlist
##   flow_step_extract
##   flow_step_sign_off                 rd spef, flatten, STA, write SDF, remove blockages
##   flow_step_eco_start
##   flow_step_place_eco
##   flow_step_route_eco
##   flow_step_eco_finish
##   flow_step_report_area
##   flow_step_report_late_path
##   flow_step_report_early_path
##   flow_step_report_timing_late
##   flow_step_report_timing_early
##   flow_step_report_clock_timing
##   flow_step_report_power
##   flow_step_report_route_process
##   flow_step_report_route_drc
##   flow_step_report_route_density
##   flow_step_place_partitions
##   flow_step_add_feedthrus
##   flow_step_assign_pins
##   flow_step_write_partitions
##   flow_step_load_partition -save_db
##   flow_step_read_ilms -save_db


##############################################################################
define_proc flow_step_start_flow { } {
##############################################################################

    ## Set name and prefix for flow reports.
    ## NOTE: Cadence attributes 'flow_report_name' and 'flow_report_prefix' will
    ##       be used in several reporting flow steps (flow_step_report_*).
    ##       Therefore, flow step 'start_flow' needs to be run before running
    ##       any reporting flow step.
    set_db flow_report_name   [get_db epc_flow]
    set_db flow_report_prefix [get_db epc_flow]

    ## Create subdirectory for reports corresponding to current flow:
    if { ![file exists [get_db flow_report_directory]/[get_db flow_report_name]] } {
        file mkdir [get_db flow_report_directory]/[get_db flow_report_name]
    }

    ## flow_step_start_flow & flow_step_finish_flow
    if { [get_db flow_metrics_enable] == true } {
        ## Store non-default root attributes to metrics:
        set flow_root_config [report_obj -tcl]
        foreach key [dict keys $flow_root_config] {
            if { [string length [dict get $flow_root_config $key]] > 200} {
                dict set flow_root_config $key "\[long value truncated\]"
            }
        }
        set_metric -name flow.root_config -value $flow_root_config

        push_snapshot_stack; # Required to start a section for potential nested metric snapshots.
    }
}


##############################################################################
define_proc flow_step_finish_flow { } {
##############################################################################

    report_messages -all

    set_db oa_new_lib_compress_level 0; # Guarantee interoperability with Virtuoso (avoid oazip).

    ## Write database in OA format:
    set lib_name  [get_db flow_cell_name]_tmp_lib
    set cell_name [get_db flow_cell_name]
    set view_name [get_db epc_flow]
    imsg_id -id UFSM-156 -tl 6 -msg "flow_step_finish_flow ([get_db epc_flow]): Writing DB from Innovus as OA (write_db -oa_lib_cell_view {$lib_name $cell_name $view_name})..."
    set oa [list $lib_name $cell_name $view_name]

    write_db -oa_lib_cell_view $oa
    ## NOTE: In case the library is not yet defined in the cds.lib file, write_db is automatically adding it
    ##       at the end of the cds.lib file and the library gets created right at the top level of the working
    ##       directory. For some reason, the library gets also created at top level of the working directory
    ##       when there is no library directory at the specified location further down in the hierarchy
    ##       when 'write_db' is being called. An interesting observation related to that is that in case the
    ##       library directory is already existing, a data.dm file gets created while a tech.db file
    ##       gets created if it's not yet existing.

    switch [get_db epc_flow] {
        fp {
            ## Floorplan was the last flow step in non-OA mode.
            imsg_id -id UFSM-156 -tl 0 -msg "\n---------------------------------------------------\n--- STOP 'Init' flow, change to 'Std' flow, run ---\n---------------------------------------------------"
            ## Copy the final database from the local scratch to the server:
            copy_oa_view_to_server -view_src fp -view_dst floorplan
        }
        prects {
            imsg_id -id UFSM-156 -tl 8 -msg "flow_step_finish_flow: Finishing flow '[get_db epc_flow]', but nothing to be done."
        }
        cts {
            imsg_id -id UFSM-156 -tl 8 -msg "flow_step_finish_flow: Finishing flow '[get_db epc_flow]', but nothing to be done."
        }
        postcts {
            imsg_id -id UFSM-156 -tl 8 -msg "flow_step_finish_flow: Finishing flow '[get_db epc_flow]', but nothing to be done."
        }
        route {
            imsg_id -id UFSM-156 -tl 8 -msg "flow_step_finish_flow: Finishing flow '[get_db epc_flow]', but nothing to be done."
        }
        postroute {
            imsg_id -id UFSM-156 -tl 8 -msg "flow_step_finish_flow: Finishing flow '[get_db epc_flow]', but nothing to be done."
        }
        finalize {
            imsg_id -id UFSM-156 -tl 8 -msg "flow_step_finish_flow: Finishing flow '[get_db epc_flow]'..."

            ## DRC violation check
            check_drc  -check_implant_across_row -check_only all -check_routing_halo -check_cuts_in_same_via -limit 1000 -out_file [get_db flow_report_directory]/[get_db flow_report_name]/[get_db flow_cell_name]_finalize_drc.rpt
            # route_eco -fix_drc

            ## Generates an abstract, liberty files and optionally an interface logic model (ILM):
            generate_hard_macro_files
            ## parse generated files for absolute user paths
            set fileNameList [parse_replace_absolute_user_path -dir [get_db flow_db_directory]/$lib_name/$cell_name]
            ## Copy the final database from the local scratch to the server:
            if { !$::cell_is_top_level } {
              copy_oa_view_to_server -view_src abstract -view_dst abstract
            }
            ## Copy the final database from the local scratch to the server:
            copy_oa_view_to_server -view_src finalize -view_dst layout
            ## Copy content of result directory to server.
            ## NOTE: Only files starting with cell name are copied.
            file copy -force -- \
                {*}[glob -directory [get_db epc_flow_result_directory] [get_db flow_cell_name]*] \
                [get_db flow_cell_cfg_path]/[get_db epc_flow_place_route_output_directory]
            ## recover parse_replace_absolute_user_path
            foreach fileName $fileNameList {
                if [ file exists $fileName.bak] {
                    file copy -force -- $fileName.bak $fileName
                }
            }
        }
        default {
            imsg_id -id UFSM-156 -tl 6 -msg "flow_step_finish_flow ([get_db epc_flow]: Executing switch case 'default')"
        }
    }

    ## flow_step_start_flow & flow_step_finish_flow
    if { [get_db flow_metrics_enable] == true } {
        ## Update snapshot history:
        imsg_id -id UFSM-156 -tl 8 -msg "flow_step_finish_flow (sub-flow: [get_db epc_flow]): create_snapshot epc_[get_db epc_flow])"
        pop_snapshot_stack; # Required to end a section for potential nested metric snapshots.

        if { [lsearch -exact [get_db pins .is_clock] true] > -1 } {
            imsg_id -id UFSM-156 -tl 8 -msg "flow_step_finish_flow (sub-flow: [get_db epc_flow]): All metric categories are being reported."
            create_snapshot  -name epc_[get_db epc_flow]  -auto default  -categories all
        } else {
            #TODO: only virtual clocks (separate branch due to metrics bugs)
            if { [lsearch "fp prects cts postcts route postroute finalize" [get_db epc_flow]] > -1 } {
                imsg_id -id UFSM-156 -tl 8 -msg "flow_step_finish_flow (sub-flow: [get_db epc_flow]): All metric categories except 'clock', 'hold', and 'setup' are being reported."
                create_snapshot  -name epc_[get_db epc_flow]  -auto default  -categories "check design flow power route"; #TODO: without categories 'clock', 'hold' and 'setup' due to metrics bugs
            } else {
                imsg_id -id UFSM-156 -tl 8 -msg "flow_step_finish_flow (sub-flow: [get_db epc_flow]): All metric categories are being reported."
                create_snapshot  -name epc_[get_db epc_flow]  -auto default  -categories all
            }
        }

        ## Report metrics:
        set qor_file [file join [get_db flow_report_directory] [get_db flow_report_name] [get_db flow_report_prefix]_qor.rpt]
        imsg_id -id UFSM-156 -tl 8 -msg "flow_step_finish_flow ([get_db epc_flow]: report_qor $qor_file)"
        report_qor  -file $qor_file
        if { [get_db epc_flow] == [get_db flow_step_stop] } {
            set metric_file [get_db flow_report_directory]/qor.innovus.html
            imsg_id -id UFSM-156 -tl 8 -msg "flow_step_finish_flow ([get_db epc_flow]: report_metric in $metric_file)"
            report_metric  -format html  -file $metric_file
        }
    }
}



##############################################################################
define_proc flow_step_init_design { } {
##############################################################################
#
# HINT:
# - pure Innovus setup, but in common ui (stylus) mode
# - std cells and macros in Innovus has OA abstract views (bounding box, pins, antenna, blockage)
# - opening layout_routed view in Virtuoso requires a remaster (abstract -> layout)
# - Virtuoso layout view is loaded as abstract in Innovus (layout can set to visible)
# - Innovus can create an abstract on the fly (poor quality)
# - 'assemble_design' will flatten the physical hierachy - no undone! - saving WILL OVERWRITE!!
# -
# - get OA-flow triggered by loading one of these data combinations:
#   > Verilog netlist + OA reference libraries      --> looks not enougth to change into OA-flow
#   > OA design DB + OA reference libraries
#   > OA design DB + LEF files
#

# HINT: refer to flowtool genus_to_innovus step --> write_db -Innovus (ILM EXAMPLE):
# dbs/adc_analog_ctrl_regs.invs_setup.tcl:
#     source dbs/adc_analog_ctrl_regs.flowkit_settings.tcl            ;# FlowKit settings (just defines)
#     source dbs/adc_analog_ctrl_regs.invs_init.tcl                   ;# Design Import
#         read_mmmc dbs/adc_analog_ctrl_regs.mmmc.tcl                 ;# expanded version of source
#                                                                     ;# .sdc with ADDED group_path (in2out, in2reg, reg2out, reg2reg, cg_enable_group_clk_CI)
#         read_physical -oa_ref_libs {ohc15l ohc15l_digital}          ;# OA used for ILM
#         read_netlist be/genus_outputs/adc_analog_ctrl_regs.v.gz     ;# netlist
#         init_design                                                 ;# link
#
#     read_metric -id current dbs/adc_analog_ctrl_regs.metrics.json   ;# Reading metrics
#     source results/adc_analog_ctrl_regs.attrs.tcl                   ;# Reading Attributes (define and set)
#     read_def dbs/adc_analog_ctrl_regs.scan.def.gz                   ;# scan.def
#     source dbs/adc_analog_ctrl_regs.mode                            ;# Mode Setup
#     if {[is_attribute -obj_type port original_name] &&
#         [is_attribute -obj_type pin original_name] &&
#         [is_attribute -obj_type pin is_phase_inverted]} {
#       source dbs/adc_analog_ctrl_regs.wnm_attrs.tcl                 ;# Reading write_name_mapping (set port/pins with original name attribute)
#     }
#     eval_enc { set edi_pe::pegConsiderMacroLayersUnblocked 1 }
#     eval_enc { set edi_pe::pegPreRouteWireWidthBasedDensityCalModel 1 }
#

    ## SIMPLIFIED POWER definitions (replaced by design.cpf)
    #OBS: set_db init_power_nets vddd
    #OBS: set_db init_ground_nets vssd
    #OBS: ## innovus common ui
    #OBS: connect_global_net vddd -type pg_pin -pin_base_name vdd!
    #OBS: connect_global_net vssd -type pg_pin -pin_base_name gnd!

    # Load Genus modes
    #-------------------------------------------------------------------------------
    if [file exists [get_db flow_cell_cfg_path]/[get_db epc_flow_synthesis_output_directory]/[get_db flow_cell_name].mode] {
       source [get_db flow_cell_cfg_path]/[get_db epc_flow_synthesis_output_directory]/[get_db flow_cell_name].mode
    }

    # Enable removal and recovery check, above genus output will set it to
    # 'no_async', so we overwrite it to correct value!
    set_db timing_analysis_async_checks async

    ## Load timing modes and corners:
    set f [get_db flow_source_directory]/flow_mmmc.tcl
    if { [file exist $f] } {
        imsg_id -id UFSM-151 -tl 3 -msg "flow_step_init_design: define timing mmmc ..."
        read_mmmc $f
        ## verify by check db mmmc-file setting
        if { $f == [get_db init_mmmc_files] } {
            imsg_id -id UFSM-151 -tl 3 -msg "flow_step_init_design: successfully load mmmc file: $f"
        } else {
            emsg_id_stop -id USFM-100 -msg "flow_step_init_design: mmmc file load $f failed"
        }
    } else {
        emsg_id_stop -id UFSM-101 -msg "flow_step_init_design: read_mmmc file not found: $f"
    }

    ## Read in physical libraries (need to be defined in work_dir/cds.lib):
#    set ::enc::launch_quiet_mode 1
    imsg_id -id UFSM-151 -tl 3 -msg "flow_step_init_design: defining physical libraries ..."
    read_physical -oa_ref_libs [get_db init_oa_ref_libs]
#    set ::enc::launch_quiet_mode 0
    suspend 
    ## TECHNOLOGY UPDTE HINT:
    ## READ LEF AS PATCH (read_db -lef_files ...) IS NOT WORKING approach in OA-mode
    ## one working lef flow would be:
    ## 1. get a local technology OA copy and adjust the link in a local_cds.lib (don't touch the golden OA)
    ## 2. extract the tech.lef: oa2lef -lib ohc15l -lef tech.lef
    ## 3. patch the lef manually
    ## 4. overwrite the technology: oa2lef -libDefFile local_cds.lib -lib ohc15l -lef modified_tech.lef
    ## 5. start ebe/Innovus while change the pointer to local cds.lib:
    ##    flow_procs.tcl  > generate_cds_lib: "set main_cds  $::env(DIGITAL_WORK_DIR)/digital/info/local_cds.lib"
    ##
    ## to check the rules in innovus:
    ## foreach i [get_db via_def_rules] {
    ##     puts -nonewline "[format "%2i : %-30s  " [incr cnt] $i]"
    ##     foreach k {cut_spacing bottom_width top_width} {
    ##         puts -nonewline "[format "%-10s : %15s, " $k [get_db $i .$k]]"
    ##     }
    ##     puts ""
    ## }


    ## Load netlist by read_hdl / elaborate OR synthesis netlist:
#     set lib_name  [get_db flow_cell_name]_lib
#     set cell_name [get_db flow_cell_name]
#     set view_name netlist
#     set oa        [list $lib_name $cell_name $view_name]
    set f [get_db flow_cell_cfg_path]/[get_db epc_flow_synthesis_output_directory]/[get_db flow_cell_name].v.gz
    if { [file exist $f] } {
        imsg_id -id UFSM-151 -tl 3 -msg "flow_step_init_design: Reading netlist $f ..."
        read_netlist $f
#        read_netlist  -oa_cell_view $oa
    } else {
        emsg_id_stop -id UFSM-101 -msg "flow_step_init_design: read_netlist file not found: $f"
    }


    suspend
    ## Common Power Format: create new one except in case of explicit suppression
    set cpf [get_db flow_cell_name].cpf; # in working directory on scratch
    if { ![info exist ::cfg_auto_cpf_gen_disabled] || $::cfg_auto_cpf_gen_disabled == false } {
        generate_cpf -add_lib -cpf_out $cpf
        imsg_id -id UFSM-151 -tl 5 -msg "flow_step_init_design: created $cpf from template"
    } else {
        set cpf [file join [get_db flow_cell_cfg_path] [get_db flow_cell_name].cpf]; # on server
        if { [file exists $cpf] } {
            imsg_id -id UFSM-151 -tl 5 -msg "flow_step_init_design: Using manually created .cpf file: $cpf"
        } else {
            emsg_id_stop -id USFM-100 -msg "flow_step_init_design: Manually created .cpf file not available: $cpf"
        }
    }
    ## Supported Innovus Commands for power control:
    imsg_id -id UFSM-151 -tl 8 -msg "flow_step_init_design: read current power file with syntax check: $cpf"
    ## Read CPF file, this read commad performs a syntax check only.
    ## 'init_design' is required to bind the MMMC to power domains and create db objects like
    ## 'get_db power_domains' and 'pg_nets' (don't use 'commit_power_intent' before).
    read_power_intent -cpf $cpf
    if { $cpf == [lindex [get_db init_power_intent_files] 1] } {
        imsg_id -id UFSM-151 -tl 3 -msg "flow_step_init_design: successfully load power file: $cpf"
    } else {
        emsg_id_stop -id USFM-100 -msg "flow_step_init_design: power file load $cpf failed"
    }

    ## write_power_intent -cpf                       ;# get the current setting


    ## map/link all design parts and check for missing elements
    ## any detail reporting of the db (get_db e.g. libraries ...) before this step could
    ## trigger a Segmentation fault - which would be kind of expected (Cadence).
    ## with this step all .sdc constraints are applied and checked
    imsg_id -id UFSM-151 -tl 3 -msg "flow_step_init_design: link all together (init_design)"
    init_design

    suspend 
    imsg_id -id UFSM-151 -tl 8 -msg "flow_step_init_design: [get_db power_domains]"
    imsg_id -id UFSM-151 -tl 8 -msg "flow_step_init_design: [get_db pg_nets]"

    ## After init, commit the power once (as long as no modifications of .cpf is performed)
    commit_power_intent
    # write_power_intent -cpf; # report file if required
    ## Power/ground report:
    check_legacy_design -power_ground -no_html \
        -out_file [file join [get_db flow_report_directory] [get_db flow_report_name] \
                       [get_db flow_report_prefix]_power_ground.rpt];


    # Virtuoso floorplan: # you may load here a Virtuso floorplan view by
    # Virtuoso floorplan: read_oa <libname> <cellname> <viewname> -filter {boundary pin_shapes}
    # Virtuoso floorplan: # Add a core margin between the PR boundary and the rows
    # Virtuoso floorplan: update_floorplan -core_to_edge {left bottom right top}


    # or by gui
    # read_netlist -oa_cell_view {<libname> <cellname> <viewname>}


    # if required a new OA lib can created by Innovus (eg for ILM)
    # create_oa_lib -help [-no_compress] [-out_dir <path>]
    # create_oa_lib <oa_lib>  {-oa_attach_tech_lib <techLib> | -oa_copy_tech_lib <techLib> | -oa_reference_tech_libs <techLibList> }

    # save design in OA including lib and timing information
    # write_db -oa_lib_cell_view {<libname> <cellname> <viewname>}
    # or just view name. get the library from oa_design_lib, get cellname from design
    # write_db -oa_view {<view_name>}

    if { ![info exists ::cfg_dft_disabled] || $::cfg_dft_disabled == false } {
        ## Load scan.def if available:
        set f [get_db flow_cell_cfg_path]/[get_db epc_flow_synthesis_output_directory]/[get_db flow_cell_name].scan.def.gz
        if { [file exist $f] } {
            imsg_id -id UFSM-151 -tl 3 -msg "flow_step_init_design: Reading scan.def file..."
            read_def $f
        } else {
            wmsg_id -id USFM-120 -msg "flow_step_init_design: scan.def file not found: $f"
        }
    }

    # weak cell stuff for load and driver ...
    # set epc_weak_driver_load [get_db [vfind [vfind . -libcell $::epc_weak_driver] -libpin A] .capacitance]
    ## define new root attribute:
    imsg_id -id UFSM-151 -tl 3 -msg "flow_step_init_design: current weak   driver: [get_db flow_sdc_weak_driver]/[get_db flow_sdc_weak_driver_pin]"
    imsg_id -id UFSM-151 -tl 3 -msg "flow_step_init_design: current strong driver: [get_db flow_sdc_strong_driver]/[get_db flow_sdc_strong_driver_pin]"

    ################################################################################
    ## ATTRIBUTES APPLIED AFTER LOADING A LIBRARY OR DATABASE
    ################################################################################
    if { [get_db current_design] eq ""} {
        emsg_id_stop -id UFSM-100 "flow_step_init_design: No current design defined."
    } else {

        # Load Genus original name list
        #-------------------------------------------------------------------------------
        if [file exists [get_db flow_cell_cfg_path]/[get_db epc_flow_synthesis_output_directory]/[get_db flow_cell_name].wnm_attrs.tcl] {
            source [get_db flow_cell_cfg_path]/[get_db epc_flow_synthesis_output_directory]/[get_db flow_cell_name].wnm_attrs.tcl
        }

        # Design attributes  [get_db -category design]
        #-------------------------------------------------------------------------------
        set_db design_process_node            [get_db flow_tech_process]

        # Timing attributes  [get_db -category timing && delaycalc]
        #-------------------------------------------------------------------------------
        set_db timing_analysis_cppr           both
        set_db timing_analysis_type           ocv

        ## Refer to these ERROR and WARN:
        wmsg_id -id UFSM-151 -tl 0 -msg "flow_step_init_design (flow_step.tcl): Disable CTS power check completely to avoid IMPCCOPT-1044, IMPCCOPT-1110"
        #  **ERROR: (IMPCCOPT-1044): CTS has found the clock tree is inconsistent with the power management setup (diver different domain than receiver).
        #  **WARN: (IMPCCOPT-1110): Clock tree clk has power supply illegalities.
        #  1. force to abort: "set_db cts_manage_power_intent_violations false"
        #  2. disable all power management checks completely:
        set_db cts_consider_power_intent false

        # Floorplan attributes  [get_db -category floorplan]
        #-------------------------------------------------------------------------------
        if { [lindex [split [get_db program_version] .] 0] == 17 } {
            set_db finish_floorplan_active_objs   [list macro softBlkg core]
        } else {
            # new 18.x version uses different naming
            set_db finish_floorplan_active_objs   [list macro soft_blockage core]
        }

        # Power rail routing attributes.
        # Ignores placement blockage when connecting the follow pin.
        if { [info exist ::cfg_enable_route_special_ignore_obs] && $::cfg_enable_route_special_ignore_obs == true } {
           set_db route_special_core_pin_ignore_obs placement_blockage
        }

        # Placement attributes  [get_db -category place]
        #-------------------------------------------------------------------------------

        # Tieoff attributes  [get_db -category add_tieoffs]
        #-------------------------------------------------------------------------------
        set_db add_tieoffs_cells              {LOGIC0 LOGIC1}

        # Optimization attributes  [get_db -category opt]
        #-------------------------------------------------------------------------------
        set_db opt_new_inst_prefix            "[get_db flow_report_name]_"

        # Clock attributes  [get_db -category cts]
        #-------------------------------------------------------------------------------
        set_db cts_target_skew                                0.250; # [ns]
        set_db cts_target_max_transition_time                 0.572; # [ns], min. allowed value for CLKINV_X20
        ## NOTE: The purpose of the CTS settings above is just to have some default settings for initial designs.
        ##       In order to get proper CTS constraints, skew_group-specific skew targets should be specified in 'be/ccopt_manual.spec'.
        ##       Max. transition targets can already be specified in SDC files using the command 'set_clock_transition' which is
        ##       automatically converted into a ccopt command during clock tree specification generation (create_clock_tree_spec).

        set_db cts_buffer_cells               {CLKBUF_X1 CLKBUF_X2 CLKBUF_X3 CLKBUF_X4 CLKBUF_X6 CLKBUF_X8 CLKBUF_X12 CLKBUF_X16 CLKBUF_X20}
        set_db cts_inverter_cells             {CLKINV_X1 CLKINV_X2 CLKINV_X3 CLKINV_X4 CLKINV_X6 CLKINV_X8 CLKINV_X12 CLKINV_X16 CLKINV_X20}
        set_db cts_clock_gating_cells         {CLKGATE_X1 CLKGATEN_X1 CLKGATETST_X1}

        ## Route type definitions occur during the "init_floorplan" flow_step
        if { [get_db route_types] ne ""} {
          set_db cts_route_type_leaf          default
          set_db cts_route_type_trunk         default
          set_db cts_route_type_top           default
        }
        
        set_db si_delay_enable_report true

        # Filler attributes  [get_db -category add_fillers]
        #-------------------------------------------------------------------------------
        set_db add_fillers_cells              {DCAP_X16 DCAP_X8 DCAP_X4 FILLCELL_X64 FILLCELL_X32 FILLCELL_X16 FILLCELL_X8 FILLCELL_X4 FILLCELL_X2 FILLCELL_X1}
        ### TODO: check for FILLERCELL_X64_ACTIVE_POLY refer to filler post processing on mask generation: DRC rule "low coverage" on ACTIVE, POLY (and MET1)

        # Routing attributes  [get_db -category route]
        #-------------------------------------------------------------------------------

        if { [info exists ::cfg_top_routing_layer] } {
            if { $::cfg_top_routing_layer < 1 } {
                wmsg_id -id UFSM-151 -tl 1 -msg "flow_step_init_design: cfg_top_routing_layer < 1, use all layers"
            } elseif { $::cfg_top_routing_layer > 6 } {
                wmsg_id -id UFSM-151 -tl 1 -msg "flow_step_init_design: cfg_top_routing_layer > 6, use all layers"
            } else {
                set_db route_design_top_routing_layer        $::cfg_top_routing_layer
                set_db route_early_global_top_routing_layer  $::cfg_top_routing_layer
            }
        }
    }

    # Optimization attributes
    #-------------------------------------------------------------------------------
    if [info exists ::cfg_opt_power_effort] {
        set_db opt_power_effort                     $::cfg_opt_power_effort
    } else {
        set_db opt_power_effort                     low
    }
    if [info exists ::cfg_opt_setup_target_slack] {
        set_db opt_setup_target_slack               $::cfg_opt_setup_target_slack
    } else {
        set_db opt_setup_target_slack               0.3
    }
    if [info exists ::cfg_opt_hold_target_slack] {
        set_db opt_hold_target_slack                $::cfg_opt_hold_target_slack
    } else {
        set_db opt_hold_target_slack                0.1
    }
    if [info exists ::cfg_opt_area_recovery_setup_target_slack] {
        set_db opt_area_recovery_setup_target_slack $::cfg_opt_area_recovery_setup_target_slack
    } else {
        set_db opt_area_recovery_setup_target_slack 0.3
    }
}


##############################################################################
define_proc flow_step_init_floorplan { } {
##############################################################################
    ##
    ## FLOORPLAN STEP               TYPE    COMMENT
    ## -------------------------------------------------------------------------
    ## INTI: global settings            all     setup and tweaks: set grid snap, route drc check off
    ## INIT: create chip size           all     inital parameters in config
    ## INIT: irregular shaping          ilm     cfg_cut_arg/data_list
    ## PLACE: IO                        top     cfg_IO_data_list (default: placed around chip border)
    ## PLACE: PIN                       ilm     cfg_pin_placement
    ## POWER: main ring + modif         top     cfg_pwr_ring, cfg_pwr_ring_modif (design specific)
    ## PLACE: instances (wo. rails)     all     cfg_instance_arg/data_list (no MET1 rail cell)
    ## PLACE: create cells  (wo. rails) all     cfg_phys_instance_arg/data_list (no MET1 rail cell)
    ## PLACE: pins on pads              top     get PADS and place TMET pins (after cfg_phys)
    ## PLACE: inst arrays (wo. rails)   all     cfg_place_instances
    ## PALCE: cell and IO halos         all     default 5um halo
    ## PLACE: add pre_rail blockages    all     cfg_add_blockages (-when {pre_rail})
    ##                                          possibility to avoid MET1 routing
    ##                                          all place blockages are removed before filler add
    ## POWER: metal 1 rails             ilm     route_special
    ## PLACE: inst arrays (with rails)  all     cfg_place_instances -onrail true
    ##                                          use this to place instances ON rails
    ## PLACE: add fp blockages          all     cfg_add_blockages (-when {fp})
    ## PRESERVE INSTANCE                all     cfg_preserve_inst_list
    ##
    ## --> continue with "flow_step_power_route"


    ## -------------------------------------------------------------------------
    ## FLOORPLAN: global settings
    ##--------------------------------------------------------------------------
    ##

    ## Set inital die size, set grid to 0.01 (manufacturing) to get accurate sizes
    set_db floorplan_snap_die_grid manufacturing    ;# use for TOP (default: placement)
    ## For ILMs a snap to placement (vertical min. basecell width, horizontal row_height) would be ideal
    ## This setting would generate a die size extended to the outer metal 1 rails:
    ## create_floorplan -no_snap_to_grid \
    ##     -box_size [list 0 0                         $::cfg_CELL_WIDTH $::cfg_CELL_HEIGHT \
    ##                     0 $::cfg_CORE_TO_BOTTOM     $::cfg_CORE_WIDTH [expr $::cfg_CORE_HEIGHT + $::cfg_CORE_TO_BOTTOM] \
    ##                     0 $::cfg_CORE_TO_BOTTOM     $::cfg_CORE_WIDTH [expr $::cfg_CORE_HEIGHT + $::cfg_CORE_TO_BOTTOM] ]
    ##
    ## BUT some issues appears:
    ## - Pins placed on die border not on core edge
    ## - 2 DRC violations "checkFPlan;checkDieCoreOnGrid" on ll and ur corner
    ## - 2 DRC violations "Unknown reason." on each created stripe
    ##
    ## CONCLUSION: use core-box = die-box
    ## Only DRC violation type "Out Of Die" for top/bottom rail and stripe Vias
    ## clean DRC by reduce the heigth area minimal:
    # set_db check_drc_area [list 0 0.001 $::cfg_CELL_WIDTH [expr $::cfg_CELL_HEIGHT -0.001]]
    # check_drc
    ##
    ## HINT: don't use the default setting for split wide layers - it will kill the memory (version 18.1)
    ##       **WARN: (IMPDBTCL-321):	The attribute 'edit_wire_split_wide_wires' still works but will be obsolete in a future major release.
    set_db edit_wire_split_wide_wires {0}           ; # don't use this default setting - kills the memory
    ## HiNT: With innovus 18.1, a new check is set by default, which consumes a lot of memory
    ##       By disable this command, the auto created via arrays are WRONG
    ##       another option to use may be:
    ##       "set_db edit_wire_use_check_drc false; # can be used to turn off FGC (full geometric check).
    ##       but could not find any significant effect on memory
    set_db edit_wire_drc_on {0}                     ; # disable auto DRC check on routing, would consume a lot of memory
    # set_db edit_wire_use_check_drc false            ; # can be used to turn off FGC (full geometric check).
    ##       Due to the wrong array, switch off as well the autogenerate via array
    ##       and add them at the end by the DRC clean "update_power_vias" command at the end
    set_db edit_wire_create_crossover_vias {0}      ; ###TRIAL connect stripes of same nets automatically by via array
    ##
    ##

    ## -------------------------------------------------------------------------
    ## FLOORPLAN: create chip size
    ##--------------------------------------------------------------------------
    ##
    ## keep "floorplan_snap_core_grid:  placement" --> valid row setting (max. rows possible)
    ## db access : get_db designs .*box*
    imsg_id -id UFSM-151 -tl 6 -msg "flow_step_init_floorplan: set floorplan size (create_floorplan)"
    create_floorplan \
        -floorplan_origin llcorner \
        -core_margins_by die \
        -die_size $::cfg_CELL_WIDTH $::cfg_CELL_HEIGHT $::cfg_CORE_TO_LEFT $::cfg_CORE_TO_BOTTOM $::cfg_CORE_TO_RIGHT $::cfg_CORE_TO_TOP

    ## -------------------------------------------------------------------------
    ## FLOORPLAN: CHIP IRREGULAR SHAPE, on demand
    ## -------------------------------------------------------------------------
    ##
    ## for irregular floorplan shape use the cut data list
    if { [info exist ::cfg_cut_data_list] && [llength $::cfg_cut_data_list] > 0 } {
        imsg_id -id UFSM-151 -tl 6 -msg "flow_step_init_floorplan: perform irregular floorplan settings (eval_cut_floorplan_cmd)"
        eval_cut_floorplan_cmd \
            -arg_ptr    define_cut_arg_list \
            -dat_ptr    define_cut_data_list
    }


    ## -------------------------------------------------------------------------
    ## IO PLACE: (TOP) refer config (replace the old "eval_IO_place_cmd")
    ## -------------------------------------------------------------------------
    ## If any IO list is available, place them (mainly used in a io_top_design):
    if { [info exist ::cfg_IO_data_list] && [llength $::cfg_IO_data_list] > 0 } {
        imsg_id -id UFSM-151 -tl 6 -msg "flow_step_init_floorplan: place IO cells (eval_IO_place_grid_cmd)"
        ## check for the minimal IO to IO distance, use 500 um by default:
        if { [info exist ::cfg_min_io2io_distance] } {
            set minIOdist $::cfg_min_io2io_distance
            if { $minIOdist < 500.0 } {
                emsg_id_stop -id UFSM-101 -msg "flow_step_init_floorplan: IO to IO distance check is smaller than 500 um"
            }
        } else {
            imsg_id -id UFSM-151 -tl 6 -msg "flow_step_init_floorplan: use 500um as default IO to IO distance"
            set minIOdist 500.0
        }


        if { ![info exist ::cfg_IO_arg_list] } {
            emsg_id_stop -id UFSM-101 -msg "flow_step_init_floorplan: 'cfg_IO_arg_list' missing in config file"
        }
        if { ![info exist ::cfg_IO_row_spaces] } {
            emsg_id_stop -id UFSM-101 -msg "flow_step_init_floorplan: 'cfg_IO_row_spaces' missing in config file"
        }
        if { ![info exist ::cfg_IO_col_spaces] } {
            emsg_id_stop -id UFSM-101 -msg "flow_step_init_floorplan: 'cfg_IO_col_spaces' missing in config file"
        }

        eval_IO_place_grid_cmd \
            -arg_list       $::cfg_IO_arg_list \
            -dat_table      $::cfg_IO_data_list \
            -coor_tl        [subst $::cfg_IO_topleft_center] \
            -row_spaces     $::cfg_IO_row_spaces \
            -col_spaces     $::cfg_IO_col_spaces \
            -check_io2io    $minIOdist
    }


    ## -------------------------------------------------------------------------
    ## PIN PLACE: (ILM or TOP) refer config
    ## -------------------------------------------------------------------------

    ## On a pin list place them as well (for ILMs and for io_top_design on Analog on Top):
    if { [info exist ::cfg_pin_placement] && [llength $::cfg_pin_placement] > 0 } {
        imsg_id -id UFSM-151 -tl 6 -msg "flow_step_init_floorplan: place pins of ILM (eval_pin_place_cmd)"
        if { [info exist ::cfg_auto_pin_placement_enabled] && $::cfg_auto_pin_placement_enabled } {
            ## Automatically place all pins:
            imsg_id -id UFSM-151 -tl 7 -msg "flow_step_init_floorplan: Automatically placing ILM pins (assign_io_pins) ..."
            assign_io_pins
        } else {
            ## Manually place pins:
            imsg_id -id UFSM-151 -tl 7 -msg "flow_step_init_floorplan: Manually placing ILM pins (eval_pin_place_cmd) ..."
            eval_pin_place_cmd  -dat_ptr cfg_pin_placement
            ## In case multiple 'edit_pin' commands are executed consecutively, the root attribute 'assign_pins_edit_in_batch'
            ## should be set to 'true' in order to speed up the pin placement:
            set_db assign_pins_edit_in_batch true
            source [get_db flow_cell_name]_pin.tcl
            set_db assign_pins_edit_in_batch false; # Must be set back to 'false'.
        }
        check_pin_assignment -out_file [file join [get_db flow_report_directory] [get_db flow_report_name] [get_db flow_report_prefix]_pins.rpt]
     }

    ## -------------------------------------------------------------------------
    ## BUILD MAIN POWER RING
    ## -------------------------------------------------------------------------
    ## Run this after IO placement, because unplaced IO's are placed in the
    ## main power ring area. This could block the ring routing (when DRC check on).
    ## Loading the special routes by 'load_fp_xml_spr_file' (empty .xml) would :
    ## - remove "instance block"
    ## - keep   "instance IO"     (Type PAD, IO_SITE)
    ##
    ## BY reference matrix "cfg_pwr_ring"
    if { [info exist ::cfg_pwr_ring] && [llength $::cfg_pwr_ring] > 0 } {
        imsg_id -id UFSM-151 -tl 5 -msg "flow_step_init_floorplan: run power ring refer to matrix"

        ## For a clean start, remove all potential power routing
        imsg_id -id UFSM-151 -tl 2 -msg "flow_step_init_floorplan: remove all power for clean startup"
        delete_all_power_preroutes

        set_db edit_wire_drc_on {0}                     ; # disable auto DRC check on routing, would consume a lot of memory

        ## run ring creation
        set ts_start [clock seconds]
        if { [info exist ::cfg_pwr_ring_quick] && $::cfg_pwr_ring_quick == "true" } {
            ## quick run, but need to warn:
            emsg_id -id USFM-104 -msg "flow_step_init_floorplan: run power ring as quick trial, set 'cfg_pwr_ring_quick {false} for final version"
            pwr_ring_create -power_matrix $::cfg_pwr_ring -skipCon -skipVia
        } else {
            ## full run with time measureing and parameter seeting
            imsg_id -id UFSM-151 -tl 8 -msg "flow_step_init_floorplan: run all power ring (no skipping any step - takes a bit longer)"
            if { [info exist ::cfg_pwr_ring_param] && [llength $::cfg_pwr_ring] == 6 } {
                set d $::cfg_pwr_ring_param
                dict with d {}
                pwr_ring_create -power_matrix $::cfg_pwr_ring -connO [dict get $d "-connO"] -connW [dict get $d "-connW"] -connG [dict get $d "-connG"]
            } else {
            pwr_ring_create -power_matrix $::cfg_pwr_ring
            }
        }
        set ts_end [clock seconds]
        imsg_id -id UFSM-151 -tl 6 -msg "flow_step_init_floorplan: ring creation takes [expr ($ts_end - $ts_start)/60] minutes."

        ## addon run of design ring modification
        if { [info exist ::cfg_pwr_ring_modif] && [llength $::cfg_pwr_ring_modif] > 0 } {
            set f [file join [subst [get_db flow_cell_cfg_path]] innovus_inputs [string trim $::cfg_pwr_ring_modif]]
            if { [file exists $f] } {
                ## Perform sourcing script
                if { [catch {source $f} msg] } {
                    emsg_id_stop  -id USFM-100  -msg "flow_step_init_floorplan: fail to source $f, $msg"
                }
            } else {
                emsg_id_stop -id USFM-104 -msg "flow_step_init_floorplan: requested file $f not accessible."
            }
        }

        ## final step: add vias (exclude corners)
        set coreXll [lindex [lindex [get_db designs .core_bbox] 0] 0]
        set coreYll [lindex [lindex [get_db designs .core_bbox] 0] 1]
        set coreXur [lindex [lindex [get_db designs .core_bbox] 0] 2]
        set coreYur [lindex [lindex [get_db designs .core_bbox] 0] 3]

        set chipX   [lindex [lindex [get_db designs .bbox] 0] 2]
        set chipY   [lindex [lindex [get_db designs .bbox] 0] 3]

        ## vias on main power rail - avoid the 45° routed corners, but get first reconnect
        set exclude_corner 240
        ## bottom
        update_power_vias -add_vias {1} -orthogonal_only {1} -skip_via_on_pin {pad block cover standardcell physicalpin} \
                          -area [list $exclude_corner 0 [expr $chipX - $exclude_corner] $coreYll]

        ## top
        update_power_vias -add_vias {1} -orthogonal_only {1} -skip_via_on_pin {pad block cover standardcell physicalpin} \
                          -area [list $exclude_corner $coreYur [expr $chipX - $exclude_corner] $chipY]

        ## left
        update_power_vias -add_vias {1} -orthogonal_only {1} -skip_via_on_pin {pad block cover standardcell physicalpin} \
                          -area [list 0 $exclude_corner $coreXll [expr $chipY - $exclude_corner]]

        ## right
        update_power_vias -add_vias {1} -orthogonal_only {1} -skip_via_on_pin {pad block cover standardcell physicalpin} \
                          -area [list $coreXur $exclude_corner $chipX [expr $chipY - $exclude_corner]]
    }

    ## as final step save the ring a xml file
    if { [info exist ::cfg_pwr_xml_spr_save] && [llength $::cfg_pwr_xml_spr_save] > 0 } {
        # define the .xml file, the .xml.spr will be created automatically
        set fp_xml [file join [get_db flow_cell_cfg_path] innovus_inputs $::cfg_pwr_xml_spr_save]
        write_floorplan $fp_xml -sections special_route -xml
    }
    ## OR as alternative (18.10 45° route bug) as loading the special route of floorplan written file
    if { [info exist ::cfg_pwr_xml_spr_load] && [llength $::cfg_pwr_xml_spr_load] > 0 } {
        imsg_id -id UFSM-151 -tl 6 -msg "flow_step_init_floorplan: load $::cfg_pwr_xml_spr_load.spr via $::cfg_pwr_xml_spr_load."
        load_fp_xml_spr_file -fp_file $::cfg_pwr_xml_spr_load
    }

    ## verify
    imsg_id -id UFSM-151 -tl 5 -msg "flow_step_init_floorplan: check_floorplan"
    check_floorplan


    # check for available floorplan file then read in ### TBD @@@ (ongoing)
    #   - after manual modfication, the lists has to be updated somehow: .fp -> list
    #   - the commands below may overwrite the data from floorplan


    ## -------------------------------------------------------------------------
    ## INSTANCE PLACE: available and new refer config
    ## -------------------------------------------------------------------------
    if { [info exist ::cfg_instance_data_list] && [llength $::cfg_instance_data_list] > 0 } {
        imsg_id -id UFSM-151 -tl 6 -msg "flow_step_init_floorplan: place instances (eval_list_cmd -> place_inst)"
        eval_list_cmd \
            -skip_design 1 \
            -cmd place_inst \
            -arg_ptr cfg_instance_arg_list \
            -dat_ptr cfg_instance_data_list
    }

    ## -------------------------------------------------------------------------
    ## INSTANCE CREATE: available and new refer config
    ## -------------------------------------------------------------------------
    ## create new instance from the lib  ### TBD @@@  issue with OA/lib - currently not available
    if { [info exist ::cfg_phys_instance_data_list] && [llength $::cfg_phys_instance_data_list] > 0 } {
        imsg_id -id UFSM-151 -tl 6 -msg "flow_step_init_floorplan: create instances (eval_list_cmd -> create_inst)"
        eval_list_cmd \
            -skip_design 1 \
            -cmd create_inst \
            -arg_ptr cfg_phys_instance_arg_list \
            -dat_ptr cfg_phys_instance_data_list
     }


    ## -------------------------------------------------------------------------
    ## PIN on PADS: update PAD with a TMET pin for LVS
    ## -------------------------------------------------------------------------
    ## - because text layer seams not be tranfered
    ## - after physical placement (iso_ring over PADs seams to kill the pins)

    if { [info exist ::cfg_IO_data_list] && [llength $::cfg_IO_data_list] > 0 } {
    add_pins_to_IO_grid_placed_pads \
        -arg_list       $::cfg_IO_arg_list \
        -dat_table      $::cfg_IO_data_list
    }


# old    ## -------------------------------------------------------------------------
# old    ## INSTANCE ARRAY PLACE: base-cells or leaf-cells - refer to configuration file
# old    ## -------------------------------------------------------------------------
# old    ## Place the available cells in a array:
# old    if { [info exist ::cfg_array_data_list] && [llength $::cfg_array_data_list] > 0 } {
# old       imsg_id -id UFSM-151 -tl 6 -msg "flow_step_init_floorplan: place module arrays (eval_array_place_cmd)"
# old        eval_array_place_cmd \
# old            -arg_ptr    cfg_array_arg_list \
# old            -dat_ptr    cfg_array_data_list
# old    }

    ## -------------------------------------------------------------------------
    ## INSTANCE PLACE: base-cells or leaf-cells - refer to configuration file
    ##                 run placement without "-onrail true"
    ## -------------------------------------------------------------------------
    ## Place the available cells in a array:
    if { [info exist ::cfg_place_instances] && [llength $::cfg_place_instances] > 0 } {
        ## run trough all elements
        foreach e $::cfg_place_instances {
            ## first get the data as array
            set a $e
            dict with a {}
            ## check for option -onrail true but only for type of [fixed|placed|soft_fixed]
            set t [string trim [dict get $a -type]]
            if { ($t == "fixed" || $t == "placed" || $t == "soft_fixed") && [string trim [dict get $a -onrail]] == "true" } {
                ## execute instance placement of current instance
                imsg_id -id UFSM-151 -tl 6 -msg "flow_step_init_floorplan: skipp before MET1 rails: [dict get $a -name]"
            } else {
                imsg_id -id UFSM-151 -tl 6 -msg "flow_step_init_floorplan: place before MET1 rails: [dict get $a -name]"
                eval "eval_place_instances $a"
            }
        }
    }

    # Instance cluster, It might be merged with eval_place_instances in the
    # future
    eval "eval_cluster_instances"

    ## -------------------------------------------------------------------------
    ##  FLOORPLAN: add default halos on macros and IO
    ## -------------------------------------------------------------------------
    ##
    ## add halo around fixed placed cells
    # set_db [get_db insts -if {.place_status==fixed}] .place_halo_left 5.0
    # set_db insts -if {.place_status==fixed}] .place_halo_left 5.0
    # foreach i [get_db insts -if {.place_status==fixed}] {
    #     set_db $i .place_halo_left 5.0
    # }
    create_place_halo -all_blocks -halo_deltas {5.0 5.0 5.0 5.0}
    create_place_halo -all_io_pads -halo_deltas {5.0 5.0 5.0 5.0}

# NOT USED? TBL, 2018-12-17
#    if { [info exist ::cfg_add_block_halo_after_rail_list] && [llength $::cfg_add_block_halo_after_rail_list] > 0 } {
#      foreach i $::cfg_add_block_halo_after_rail_list {
#        if {[llength [get_db insts hinsts $i]] > 0} {
#           delete_place_halo -block $i
#        } else {
#           wmsg_id -id UFSM-120 -msg "There is no instance for halo: $i"
#        }
#      }
#    }


    ## -------------------------------------------------------------------------
    ## FLOORPLAN: add blockages and extend HALOs for instances (level pre_rail)
    ## -------------------------------------------------------------------------
    ##
    set level "pre_rail"
    if { [info exist ::cfg_add_blockages] && [llength $::cfg_add_blockages] > 0 } {
        # check for flow location level
        foreach blk $::cfg_add_blockages {
            # skip everything which does not contain mandatory key "-when"
            if { [regexp {\-when} $blk ] } {
                # get each single blockage and use it as dictionary (with empty body)
                set b $blk
                dict with b {}
                # when flow location is floorplan, call the blockage proc
                if { [string trim [dict get $b -when]] == $level } {
                    imsg_id -id UFSM-151 -tl 6 -msg "flow_step_init_floorplan: perform blockage [dict get $b -id] using [dict get $b -what]"
                    # call procedure with string trimmed values
                    eval_add_blockages \
                        -name  [string trim [dict get $b -id]] \
                        -layer [string trim [dict get $b -what]] \
                        -box   [string trim [dict get $b -where]]
                }
            }
        }
    }


    ## -------------------------------------------------------------------------
    ## POWER: add digital std cell power on MET1
    ## -------------------------------------------------------------------------
    imsg_id -id UFSM-151 -tl 5 -msg "flow_step_init_floorplan: before anything placed, run simple MET1 'route_special'"


    ## get the main digital power nets (refer to .cpf)
    set dig_pwr_net [get_db [get_db [get_db power_domains -if {.name==PDdig}] .primary_power_net] .name]
    set dig_gnd_net [get_db [get_db [get_db power_domains -if {.name==PDdig}] .primary_ground_net] .name]

    if { [llength $dig_pwr_net] != 1 } {
        emsg_id_stop -id USFM-104 -msg "flow_step_init_floorplan: no or invalid net for dig_pwr_net: '$dig_pwr_net'."
    }
    if { [llength $dig_gnd_net] != 1 } {
        emsg_id_stop -id USFM-104 -msg "flow_step_init_floorplan: no or invalid net for dig_gnd_net: '$dig_gnd_net'."
    }

    route_special   -nets [list $dig_pwr_net $dig_gnd_net] \
                    -connect core_pin \
                    -allow_layer_change 0 \
                    -allow_jogging 0 \
                    -layer_change_range { MET1 MET1 } \
                    -allow_layer_change 0 \
                    -detailed_log \
                    -target_via_layer_range { MET1 MET1 } \
                    -core_pin_check_stdcell_geometry

# NOT USED? TBL, 2018-12-17
#    ## Add halo only after power rail route for some blocks, this will not block power rail to connect access pin of blocks.
#    if { [info exist ::cfg_add_block_halo_after_rail_list] && [llength $::cfg_add_block_halo_after_rail_list] > 0 } {
#      foreach i $::cfg_add_block_halo_after_rail_list {
#        if {[llength [get_db insts hinsts $i]] > 0} {
#           create_place_halo -insts $i  -halo_deltas {5.0 5.0 5.0 5.0}
#        } else {
#           wmsg_id -id UFSM-120 -msg "There is no instance for halo: $i"
#        }
#     }
#    }


    ## -------------------------------------------------------------------------
    ## INSTANCE PLACE: base-cells or leaf-cells - refer to configuration file
    ##                 run placement on "-onrail true"
    ## -------------------------------------------------------------------------
    ## Place the available cells in a array:
    if { [info exist ::cfg_place_instances] && [llength $::cfg_place_instances] > 0 } {
        ## run trough all elements
        foreach e $::cfg_place_instances {
            ## first get the data as array
            set a $e
            dict with a {}
            ## check for option -onrail true but only for type of [fixed|placed|soft_fixed]
            set t [string trim [dict get $a -type]]
            if { ($t == "fixed" || $t == "placed" || $t == "soft_fixed") && [string trim [dict get $a -onrail]] == "true" } {
                ## execute instance placement of current instance
                imsg_id -id UFSM-151 -tl 6 -msg "flow_step_init_floorplan: place module arrays after MET1 RAIL placement"
                eval "eval_place_instances $a"

            }
        }
    }



    ## -------------------------------------------------------------------------
    ## FLOORPLAN: ADD BLOCKAGES (level fp)
    ## -------------------------------------------------------------------------
    ##
    set level "fp"
    if { [info exist ::cfg_add_blockages] && [llength $::cfg_add_blockages] > 0 } {
        # check for flow location level
        foreach blk $::cfg_add_blockages {
            # skip everything which does not contain mandatory key "-when"
            if { [regexp {\-when} $blk ] } {
                # get each single blockage and use it as dictionary (with empty body)
                set b $blk
                dict with b {}
                # when flow location is floorplan, call the blockage proc
                if { [string trim [dict get $b -when]] == $level } {
                    imsg_id -id UFSM-151 -tl 6 -msg "flow_step_init_floorplan: perform blockage [dict get $b -id] using [dict get $b -what]"
                    # call procedure with string trimmed values
                    eval_add_blockages \
                        -name  [string trim [dict get $b -id]] \
                        -layer [string trim [dict get $b -what]] \
                        -box   [string trim [dict get $b -where]]
                }
            }
        }
    }

    ## -------------------------------------------------------------------------
    ## PRESERVE INSTANCE: optional instances ("*" allowed)
    ## -------------------------------------------------------------------------
    ## Hint: this don't touch will be already set via genus run
    imsg_id -id UFSM-151 -tl 2 -msg "Preserving optional instances ..."
    if { [file exists [get_db flow_cell_cfg_path]/[get_db epc_flow_synthesis_output_directory]/[get_db flow_cell_name].preserve.tcl] } {
        source [get_db flow_cell_cfg_path]/[get_db epc_flow_synthesis_output_directory]/[get_db flow_cell_name].preserve.tcl
    } else {
        if { [info exists ::cfg_dont_touch_inst_list] && [llength $::cfg_dont_touch_inst_list] != 0 } {
           foreach tmp_preserve $::cfg_dont_touch_inst_list {
                  imsg_id -id UFSM-151 -tl 3 -msg "Preserving instances: $tmp_preserve"
                  set_db [get_db insts $tmp_preserve] .dont_touch true
           }

        } else {
           wmsg_id -id UFSM-120 -msg "No instances to be preserved defined."
        }

        if { [info exists ::cfg_dont_touch_size_ok_inst_list] && [llength $::cfg_dont_touch_size_ok_inst_list] != 0 } {
           foreach tmp_preserve $::cfg_dont_touch_size_ok_inst_list {
               imsg_id -id UFSM-151 -tl 3 -msg "Preserving instances(size_ok): $tmp_preserve"
               set_db [get_db insts $tmp_preserve] .dont_touch size_ok
           }
        } else {
           wmsg_id -id UFSM-120 -msg "No instances(size_ok) to be preserved defined."
        }
    }


    ## -------------------------------------------------------------------------
    ## PRESERVE INSTANCE ADD ON: give Innovus the full control back
    ## CLEANUP: somehow genus sets a dont_touch on a hinst which will prevent
    ##          cell resize/move/delete/insert --> search and set to 'none'
    ## -------------------------------------------------------------------------
    imsg_id -id UFSM-151 -tl 8 -msg "Preserving instances add on: check for hinst dont_touch not 'none'"
    foreach i [get_db hinsts -if {.dont_touch != "none"} ]  {
        set genus_setting [get_db $i .dont_touch]
        imsg_id -id UFSM-151 -msg "Preserving instances add on: Found .dont_touch set to '$genus_setting' for '$i'."
        ## Make sure that the attribute .dont_touch remains set for spare cells:
        if {[string first "i_spare_cells" $i] == -1 } {
            set_db $i .dont_touch "none"
            imsg_id -id UFSM-152 -tl 2 -msg "Preserving instances add on: Set .dont_touch from '$genus_setting' to '[get_db $i .dont_touch]'."
        }
    }

    ## -------------------------------------------------------------------------
    ## PRESERVE NET: optional nets ("*" allowed)
    ## -------------------------------------------------------------------------
    ## Hint: this don't touch will be already set via genus run
    imsg_id -id UFSM-151 -tl 2 -msg "Preserving optional nets ..."
    if { [info exists ::cfg_preserve_net_list] && [llength $::cfg_preserve_net_list] != 0 } {
        foreach tmp_preserve $::cfg_preserve_net_list {
            imsg_id -id UFSM-151 -tl 3 -msg "Preserving nets: $tmp_preserve"
            set_db [get_db nets $tmp_preserve] .dont_touch true
        }
    } else  {
        wmsg_id -id UFSM-120 -msg "No nets to be preserved defined."
    }

    ## -------------------------------------------------------------------------
    ## POWER: special route power rails - ### TBD @@@
    ## -------------------------------------------------------------------------
    ## rail connection and stripe power moved after inital placement



#    ## initialize floorplan object using DEF and/or floorplan files
#    read_floorplan          << PLACEHOLDER: FPLAN LOAD OPTIONS >>
#    read_def                << PLACEHOLDER: DEF LOAD OPTIONS >>
#
#    ## define route_types and/or route_rules
#    create_route_type -name leaf  << PLACEHOLDER: CLOCK LEAF ROUTE RULE >>
#    create_route_type -name trunk << PLACEHOLDER: CLOCK TRUNK ROUTE RULE >>
#    create_route_type -name top   << PLACEHOLDER: CLOCK TOP ROUTE RULE >>

    ## finish floorplan with auto-blockage insertion
    imsg_id -id UFSM-151 -tl 6 -msg "flow_step_init_floorplan: last cmd (finish_floorplan)"
    finish_floorplan  -fill_place_blockage soft 10.0
}


##############################################################################
define_proc flow_step_create_cost_group { } {
##############################################################################

    ## Clear existing path_groups
    get_db cost_groups -if {.name != default} -foreach {delete_obj $object}

    ## Add basic path_groups
    foreach mode [get_db constraint_modes -if {.is_setup}] {
        set_interactive_constraint_mode $mode
        group_path -name in2out -from [all_inputs] -to [all_outputs]
        if { [sizeof_collection [all_registers]] > 0} {
            group_path -name in2reg -from [all_inputs] -to [all_registers]
            group_path -name reg2out -from [all_registers] -to [all_outputs]
            group_path -name reg2reg -from [all_registers] -to [all_registers]
        }
    }
}


#=============================================================================
# Flow: Floorplaning
#=============================================================================

##############################################################################
define_proc flow_step_add_tracks { } {
##############################################################################
    ## generate tracks after creating floorplan
    add_tracks
    ## -> Adjust TMET preferred direction offset from 0.61 to 0.31.
    ## -> Generated pitch 0.54 in TMET is different from 1.22 defined in technology file in unpreferred direction.
    ## -> Generated pitch 1.24 in TMET is different from 1.22 defined in technology file in preferred direction.

    ## report requires placement
    # report_tracks

    ## ?
    # check_tracks
}

#===========================================================================
# Flow: Pre-CTS
#===========================================================================

define_proc flow_step_add_spare_cell { } {
  # Add spare cell
  if { [info exist ::cfg_spare_cell_inst_list]  && [llength $::cfg_spare_cell_inst_list] > 0 }  {
    foreach element $::cfg_spare_cell_inst_list {
      if { [llength [get_db insts $element]] > 0  } {
         set_spare_insts -insts $element
       } else {
         emsg_id_stop -id USFM-104 -msg "flow_step_add_spare_cell: Instance is not exist: $element"
       }
    }
  }
 }



##############################################################################
define_proc flow_step_add_port_diode { } {
##############################################################################
   # Add antenna diode at ports,e.g. analog input ports this will avoid antenna
   # violations during top-level layout DRC verification as digital PnR cannot
   # see inside wires in analog blocks.

   if { [info exist ::cfg_antenna_cell]             && [llength $::cfg_antenna_cell] > 0 &&
        [info exist ::cfg_antenna_fix_pos]          && [llength $::cfg_antenna_fix_pos] > 0 &&
        [info exist ::cfg_antenna_inst_pre_name]    && [llength $::cfg_antenna_inst_pre_name] > 0 &&
        [info exist ::cfg_antenna_port_list]        && [llength $::cfg_antenna_port_list] > 0 } {
      # Insert diode
        imsg_id -id UFSM-151 -tl 7 -msg "flow_step_add_port_diode :"
        if { $::cfg_antenna_fix_pos } {
            add_diode_at_port -antenna_cell $::cfg_antenna_cell -fixed_pos -prefix $::cfg_antenna_inst_pre_name -port_list $::cfg_antenna_port_list
        } else {
            add_diode_at_port -antenna_cell $::cfg_antenna_cell -prefix $::cfg_antenna_inst_pre_name -port_list $::cfg_antenna_port_list
        }

   }

}

##############################################################################
define_proc flow_step_run_place_opt { } {
##############################################################################
    ## perform global placement and ideal clock setup optimization
    ## remove the incremental option due to message (ECH):
    ## ERROR: Design must be placed before running "optDesign"
    ## orig: place_opt_design -incremental -report_dir debug -report_prefix [get_db flow_report_name]
    ## place_opt_design -report_dir [get_db flow_report_directory] -report_prefix [get_db flow_report_name]
    set_db net:epc909_pr_top/dug_D .dont_touch true 
    set_db net:epc909_pr_top/shg_0_D .dont_touch true 
    set_db net:epc909_pr_top/shg_1_D .dont_touch true
    set_db net:epc909_pr_top/shg_2_D .dont_touch true
    set_db net:epc909_pr_top/shg_3_D .dont_touch true
    set_db net:epc909_pr_top/shg_4_D .dont_touch true

    set_db place_global_module_padding {ld_delay 1.2 cgu 1.2 acq_ctrl 1.2 reg_bank 1.2 mem_ctrl 1.2}
    
proc density_screen {x1 y1 x2 y2 x y density} {

  set a1 $x1
  set b1 $y1
  set a2 [expr $a1 + $x]
  set b2 [expr $b1 + $y]
  
  create_place_blockage -type partial -density $density -area "$a1 $b1 $a2 $b2" -use_prefix
  #createPlaceBlockage -type partial -density $density -box "$a1 $b1 $a2 $b2" -prefixOn                              
  
  set j $x1
  set i $y1
  
  while {$i <= $y2} {
    while {$j < $x2} {
      set a1 [expr $a1 + $x]
      set a2 [expr $a1 + $x]
      set j $a2
      if {$a2 <= $x2} {
        create_place_blockage -type partial -density $density -area "$a1 $b1 $a2 $b2" -use_prefix
        #createPlaceBlockage -type partial -density $density -box "$a1 $b1 $a2 $b2" -prefixOn                     
      }
    }
  
    set b1 [expr $b1 + $y]
    set b2 [expr $b1 + $y]
    set i $b2
    set j $x1
    set a1 $x1
    set a2 [expr $a1 + $x]
    if {$b2 <= $y2} {
      create_place_blockage -type partial -density $density -area "$a1 $b1 $a2 $b2" -use_prefix      
      #createPlaceBlockage -type partial -density $density -box "$a1 $b1 $a2 $b2" -prefixOn                     
    }
  }
}

#density_screen 250 900 750 1400 100 100 50 

## BOTTOM
# density_screen 882 225 10888 430 100 100 70 
# density_screen 882 430 10888 1154 100 100 30 
# 
# ## top
# density_screen 515 9147 11081 9818 100 100 70  

    place_opt_design  -report_dir [file join [get_db flow_report_directory] [get_db flow_report_name]]  -report_prefix "run_place_opt"
    
    fix_cluster_insts
}

##############################################################################
define_proc flow_step_power_route { } {
##############################################################################
    ##
    ## -------------------------------------------------------------------------
    ## POWER: special route power refer to design
    ## -------------------------------------------------------------------------
    ##
    ## cfg_run_power_connection does contain a list of .tcl files and procedures
    ## this part parses this list, while:
    ##    	> all file.tcl get 'sourced'
    ##		> other names (wihtout ".tcl") are assumed as defined procedures and get executed
    ##
    ## OPTION - not implemented: Warning on not executed procs !
    ##

    ## get the main digital power nets (refer to .cpf): "PDdig"
    set dig_pwr_net [get_db [get_db [get_db power_domains -if {.name==PDdig}] .primary_power_net] .name]
    set dig_gnd_net [get_db [get_db [get_db power_domains -if {.name==PDdig}] .primary_ground_net] .name]

    if { [llength $dig_pwr_net] != 1 } {
        emsg_id_stop -id USFM-104 -msg "flow_step_power_route: no or invalid net for dig_pwr_net: '$dig_pwr_net'."
    }
    if { [llength $dig_gnd_net] != 1 } {
        emsg_id_stop -id USFM-104 -msg "flow_step_power_route: no or invalid net for dig_gnd_net: '$dig_gnd_net'."
    }

    ## run .tcl sources or procedures for power routing
    imsg_id -id UFSM-151 -tl 7 -msg "flow_step_power_route : start, sources design specific scripts from [file join [get_db flow_cell_cfg_path] innovus_inputs]"
    set ts_start [clock seconds]

    if { [info exist ::cfg_run_power_connection] && [llength $::cfg_run_power_connection] > 0 } {
        ## parse all lines
        foreach element $::cfg_run_power_connection {
            ## remove commented stuff, by posting the comment as info
            if { [regexp {\#} $element ] } {
                imsg_id -id UFSM-151 -tl 6 -msg "flow_step_power_route: comment in cfg_run_power_connection: '$element'"
            ## tcl files shall be sourced
            } elseif { [regexp {\.*.tcl} $element] } {
                set f [file join [get_db flow_cell_cfg_path] innovus_inputs [string trim $element]]
                if { [file exists $f] } {
                    ## Perform sourcing script
                    if { [catch {source $f} msg] } {
                        emsg_id_stop  -id USFM-100  -msg "flow_step_power_route: fail to source $f, $msg"
                    }
                } else {
                    emsg_id_stop -id USFM-104 -msg "flow_step_power_route: requested file $f not accessible."
                }
            } else {
                ## otherwise this is a procedure to execute
                $element
            }
        }
    }

    set ts_end [clock seconds]
    imsg_id -id UFSM-151 -tl 6 -msg "flow_step_power_route: extecute all prodedures took [expr ($ts_end - $ts_start)/60] minutes."


    ## check for power finalisation, otherwise used default
    if { [info exist ::cfg_finalize_power_std_cell_connection] && [llength $::cfg_finalize_power_std_cell_connection] > 0 } {
        imsg_id -id UFSM-151 -tl 6 -msg "flow_step_power_route: finalize power by user file: $::cfg_finalize_power_std_cell_connection"
        ## check the requested file
        set f [file join [get_db flow_cell_cfg_path] innovus_inputs [string trim $::cfg_finalize_power_std_cell_connection]]
        if { [file exists $f] } {
            ## Perform sourcing script
            if { [catch {source $f} msg] } {
                emsg_id_stop  -id USFM-100  -msg "flow_step_power_route: fail to source $f, $msg"
            }
        } else {
            emsg_id_stop -id USFM-104 -msg "flow_step_power_route: requested file $f not accessible."
        }
    } else {
        imsg_id -id UFSM-151 -tl 6 -msg "flow_step_power_route: finalize power by default settings "

        # OBS: done before:  ## ----------------------------------------------------------------------
        # OBS: done before:  ## POWER: route power rails on metal 1 after inital placement ### TBD @@@
        # OBS: done before:  ## ----------------------------------------------------------------------
        # OBS: done before:  ##
        # OBS: done before:  ## get the main digital power nets (refer to .cpf)
        # OBS: done before:  set dig_pwr_net [get_db [get_db [get_db power_domains -if {.name==PD*dig}] .primary_power_net] .name]
        # OBS: done before:  set dig_gnd_net [get_db [get_db [get_db power_domains -if {.name==PD*dig}] .primary_ground_net] .name]
        # OBS: done before:
        # OBS: done before:  ## stripes with no limits
        # OBS: done before:  # route_special   -nets { vddd vssd }
        # OBS: done before:
        # OBS: done before:  imsg_id -id UFSM-151 -tl 7 -msg "flow_step_power_route : route_special MET1"
        # OBS: done before:  route_special   -nets [list $dig_pwr_net $dig_gnd_net] \
        # OBS: done before:                  -layer_change_range { MET1 MET1 } \
        # OBS: done before:                  -allow_jogging 0 \
        # OBS: done before:                  -allow_layer_change 0 \
        # OBS: done before:                  -target_via_layer_range { MET1 MET1 }

		## ---------------------------------------------
		## POWER: special route stripes for ILM - ### TBD @@@
		## ---------------------------------------------

        ## -------------------------------------------------------------------
        ## FORCE VIA STACKS on RAIL CENTER (even the half is outside the cell)
        ## -------------------------------------------------------------------
        ## db_option:  add_stripes_via_using_exact_crossover_size
        ##             default = true                    false
        ## ----------------------------------------+------------------+--------
        ##    MET1                                 |                  |
        ## ---MET1   --+-----------------+--   - --|   #   #   #   #  |-   - center MET1 rail
        ##    MET1     |  #   #   #   #  |         |                  |      (at border of cell)
        ## ------------|-----------------|---------|------------------|--------
        ##             |                 |         |                  |
        ##             |       MET4      |         |       MET4       |   #  Via stacks
        ##
        ##    DRC:       clean                      geom. out-of-die viol.
        ##
		### TBD @@@  get some data from CELL config file
		### TBD @@@  may need to move after cell-place/rail-connect to control the VIA stacks
        #set_db generate_special_via_partial_overlap_threshold 1
        #set_db generate_special_via_opt_cross_via true
        set_db add_stripes_via_using_exact_crossover_size false

        ##
        ### TBD @@@  get some data from CELL config file
        ### TBD @@@  may need to move after cell-place/rail-connect to control the VIA stacks
        ###          "-create_pins 0" would avoid to create supply pins on MET4
        if { ! $::cell_is_top_level }  {
				imsg_id -id UFSM-151 -tl 7 -msg "flow_step_power_route : add for hard macro (add_stripes)"
					add_stripes -nets [list $dig_pwr_net $dig_gnd_net] \
						-layer MET4 \
						-direction vertical \
						-width $::cfg_POWER_STRIPE_WIDTH \
						-spacing $::cfg_HOR_ROUTING_PITCH \
						-start_from left \
						-start_offset $::cfg_POWER_STRIPE_OFFSET \
                                -set_to_set_distance $::cfg_POWER_STRIPE_SPACE
		}
    }



    # Fix power vias violations - this should get Obsolete with new technology rule update to get correct 3x3 via arrays (epc908 -f dev).
    # used in 908, but old version
     if { [info exist ::cfg_fix_power_via_violations] && [llength $::cfg_fix_power_via_violations] > 0 } {
         imsg_id -id UFSM-151 -tl 6 -msg "flow_step_power_route: fix power via violations by user file: $::cfg_fix_power_via_violations"
         ## check the requested file
         set f [file join [get_db flow_cell_cfg_path] innovus_inputs [string trim $::cfg_fix_power_via_violations]]
         if { [file exists $f] } {
             ## Perform sourcing script
             if { [catch {source $f} msg] } {
                 emsg_id_stop  -id USFM-100  -msg "flow_step_power_route: fail to source $f, $msg"
             }
         } else {
             emsg_id_stop -id USFM-104 -msg "flow_step_power_route: requested file $f not accessible."
         }
     }

    # Fix power vias violations - this should get Obsolete with new technology rule update to get correct 3x3 via arrays (epc908 -f dev).
    ## run .tcl sources or procedures for power routing
    imsg_id -id UFSM-151 -tl 7 -msg "flow_step_power_route : start, sources design specific scripts from [file join [get_db flow_cell_cfg_path] innovus_inputs]"
    if { [info exist ::cfg_run_final_power] && [llength $::cfg_run_final_power] > 0 } {
        ## parse all lines
        foreach element $::cfg_run_final_power {
            ## remove commented stuff, by posting the comment as info
            if { [regexp {\#} $element ] } {
                imsg_id -id UFSM-151 -tl 6 -msg "flow_step_power_route: comment in cfg_run_final_power: '$element'"
            ## tcl files shall be sourced
            } elseif { [regexp {\.*.tcl} $element] } {
                set f [file join [get_db flow_cell_cfg_path] innovus_inputs [string trim $element]]
                if { [file exists $f] } {
                    ## Perform sourcing script
                    if { [catch {source $f} msg] } {
                        emsg_id_stop  -id USFM-100  -msg "flow_step_power_route: fail to source $f, $msg"
                    }
                } else {
                    emsg_id_stop -id USFM-104 -msg "flow_step_power_route: requested file $f not accessible."
                }
            } else {
                ## otherwise this is a procedure to execute
                $element
            }
         }
     }


    ## -------------------------------------------------------------------------
    ## POWER: add blockages and extend HALOs for instances (level after_fp)
    ## -------------------------------------------------------------------------
    ## This will not affect power routing and only affact later signal routing
    set level "after_fp"
    if { [info exist ::cfg_add_blockages] && [llength $::cfg_add_blockages] > 0 } {
        # check for flow location level
        foreach blk $::cfg_add_blockages {
            # skip everything which does not contain mandatory key "-when"
            if { [regexp {\-when} $blk ] } {
                # get each single blockage and use it as dictionary (with empty body)
                set b $blk
                dict with b {}
                # when flow location is floorplan, call the blockage proc
                if { [string trim [dict get $b -when]] == $level } {
                    imsg_id -id UFSM-151 -tl 6 -msg "flow_step_init_floorplan: perform blockage [dict get $b -id] using [dict get $b -what]"
                    # call procedure with string trimmed values
                    eval_add_blockages \
                        -name  [string trim [dict get $b -id]] \
                        -layer [string trim [dict get $b -what]] \
                        -box   [string trim [dict get $b -where]]
                }
            }
        }
    }


    ## -------------------------------------------------------------------------
    ## POWER: add all ortogonal via in CORE - ### TBD @@@
    ## -------------------------------------------------------------------------


    ### TBD - TODO:  -->  Move to separate file after all routing:
    ### TBD - TODO:       innovus_inouts/epc909_finalize_power.tcl

}

#=============================================================================
# Flow: CTS
#=============================================================================

##############################################################################
define_proc flow_step_add_clock_tree_spec { } {
##############################################################################
#update_rc_corner -name rc_slow -pre_route_clock_cap 1.0751 -pre_route_clock_res 0.9234
#update_rc_corner -name rc_fast -pre_route_clock_cap 1.0751 -pre_route_clock_res 0.8928
		#set_db cts_clone_clock_gates true
    source [file join [get_db flow_cell_cfg_path] ccopt_balance_column_driver.spec];

    if { [info exist ::cfg_genclk_separate_skew_balance] && $::cfg_genclk_separate_skew_balance == true } {
      set_db cts_timing_connectivity_based_skew_groups clock_false_path
      #get_db cts_timing_connectivity_based_skew_groups
    }

    if { [llength [get_db clocks .sources]] > 0 } {

        if { [file exist [file join [get_db flow_cell_cfg_path] ccopt_manual_pre.spec]] } {
            source [file join [get_db flow_cell_cfg_path] ccopt_manual_pre.spec]; # manually generated part of clock tree specification
        }

        imsg_id  -id UFSM-151  -tl 2  -msg "flow_step_add_clock_tree_spec: Creating clock tree specification..."
        if { ![info exist ::cfg_auto_ccopt_spec_gen_disabled] || $::cfg_auto_ccopt_spec_gen_disabled == false } {
            create_clock_tree_spec  -out_file ccopt.spec
            source ccopt.spec; # automatically generated main part of clock tree specification
        }
        if { [file exist [file join [get_db flow_cell_cfg_path] ccopt_manual.spec]] } {
            source [file join [get_db flow_cell_cfg_path] ccopt_manual.spec]; # manually generated part of clock tree specification
          # source [file join [get_db flow_cell_cfg_path] ccopt_balance_column_driver.spec];
        }
    } else {
        wmsg_id  -id UFSM-121  -tl 0  -msg "flow_step_add_clock_tree_spec: No clock sources were found (TCLCMD-513). Generation of clock tree specification has been skipped therefore."
    }
}

##############################################################################
define_proc flow_step_add_clock_tree { } {
##############################################################################
    ## NOTE: 'get_clock_tree_sinks' only lists sinks belonging to clock trees
    ##       specified in clock tree specification.
    ## BUG: initial value of [get_clock_tree_sinks] delivers {{}} -> length = 1
    ##      second call of   [get_clock_tree_sinks] delivers {}   -> length = 0
    ##      nothing was working (join, list {*}, concat, ...)
    if { [llength [get_db clocks .sources]] > 0 && [llength [get_clock_tree_sinks]] > 0 } {
        ## Implement clock trees and propagated clock setup optimization
        imsg_id  -id UFSM-151  -tl 2  -msg "flow_step_add_clock_tree: Creating clock trees..."
        ccopt_design  -report_dir [file join [get_db flow_report_directory] [get_db flow_report_name]]  -report_prefix "add_clock_tree"
        #clock_design   
        report_clock_trees
    } else {
        wmsg_id  -id UFSM-121  -tl 0  -msg "flow_step_add_clock_tree_spec: No clock tree sinks were found. Clock tree synthesis has been skipped therefore."
    }
}

##############################################################################
define_proc flow_step_add_tieoffs { } {
##############################################################################

    ## In case there are ILMs, make sure that they get treated as black boxes when tie cells get added.
    ## Otherwise, Innovus sees pins of subcells in ILMs to be tied but cannot put tie cells inside ILMs (layout of hard macro is already given),
    ## i.e. adding tie cells fails for such pins. If ILMs are treated as black boxes, tie cells get properly added outside of the ILMs.
    if { [info exists ::cfg_hard_macros] && [llength [dict filter $::cfg_hard_macros value ilm]] > 0 && [eval_legacy dbIsHeadIlmFlattened] == 1 } {
        unflatten_ilm
    }

    if { [get_db add_tieoffs_cells] ne "" } {
        delete_tieoffs
        add_tieoffs  -matching_power_domains true
    }

    if { [info exists ::cfg_hard_macros] && [llength [dict filter $::cfg_hard_macros value ilm]] > 0 && [eval_legacy dbIsHeadIlmFlattened] == 0 } {
        flatten_ilm
    }
}

#=============================================================================
# Flow: Post-CTS
#=============================================================================

##############################################################################
define_proc flow_step_run_opt_postcts_hold { } {
##############################################################################

    ### TBD @@@ run_opt_postcts_hold: added settings by ECH
    ### No idea who add this, but we assume it's wrong - skip
    ### ---------------------------------------------------------------------
    ### 8.2.19 TWI/TBU: This is needed to remove ideal clock in the
    ### check_timing report.

    set_interactive_constraint_modes [all_constraint_modes -active]
    get_db analysis_views -foreach {
      set view_name $obj(.name)
      reset_clock_tree_latency [get_db clocks -if {.view_name == $view_name}]
      set_propagated_clock [all_clocks]
    }


    set_interactive_constraint_modes []

    ## Perform postcts hold optimization:
    opt_design  -post_cts -hold  -report_dir [file join [get_db flow_report_directory] [get_db flow_report_name]]  -report_prefix "opt_postcts_hold"

    ## Perform postcts clock skew optimization:
    if { [info exist ::cfg_enable_postcts_skew_repair] && $::cfg_enable_postcts_skew_repair == true } {
      clock_post_route_repair -enable_skew_repair true -enable_skew_repair_by_buffering true
    }

}

#=============================================================================
# Flow: Routing
#=============================================================================

##############################################################################
define_proc flow_step_add_fillers { } {
##############################################################################
    ## insert filler cells before final routing
    if { [get_db add_fillers_cells] ne "" } {

        ## to place filles everywhere possible, remove place blockages
        if { [lindex [split [get_db program_version] .] 0] <= 18 } {
            delete_place_blockages -all
        } else {
            delete_obj [get_db place_blockages]
        }

        ## but add again the blockgages from "pre_rail" (which contains MET1 blockages)
        ## reason: otherwise filles get placed even without MET1 supply rails
        set level "pre_rail"
        if { [info exist ::cfg_add_blockages] && [llength $::cfg_add_blockages] > 0 } {
            # check for flow location level
            foreach blk $::cfg_add_blockages {
                # skip everything which does not contain mandatory key "-when"
                if { [regexp {\-when} $blk ] } {
                    # get each single blockage and use it as dictionary (with empty body)
                    set b $blk
                    dict with b {}
                    # when flow location is floorplan, call the blockage proc
                    if { [string trim [dict get $b -when]] == $level } {
                        imsg_id -id UFSM-151 -tl 6 -msg "flow_step_init_floorplan: perform blockage [dict get $b -id] using [dict get $b -what]"
                        # call procedure with string trimmed values
                        eval_add_blockages \
                            -name  [string trim [dict get $b -id]] \
                            -layer [string trim [dict get $b -what]] \
                            -box   [string trim [dict get $b -where]]
                    }
                }
            }
        }

        ## add filler but respect user order
        ##
        ## Hint: This does add as well some DCAP cells, but distributed and not the maximal possible
        ##       For a maximum DCAP fill you could use:
        ##          foreach fill [get_db add_fillers_cells] dense {1.0 1.0 1.0 1.0 1.0 1.0 1.0 1.0 1.0 1.0} {
        ##              add_fillers -base_cells $fill -density $dense
        ##          }
        ##
        ##       To check the used cells:
        ##           foreach cell [get_db add_fillers_cells] {
        ##               puts "$cell : [llen [get_db [get_db insts -if {.base_name==FILLER*}] .base_cell -if {.name==$cell}]]"
        ##           }
        ##
        ##       To see the distribution
        ##           set cell "DCAP_X*"
        ##           select_inst_by_cell_name [get_db [get_db insts -if {.base_name==FILLER*}] .base_cell -if {.name==$cell}]
        ##
        set_db add_fillers_preserve_user_order true
        add_fillers
        ## For a more clean version, the halo's should get removed from all cells a pure fillers (no dcaps) should be added.
        # remove halo -all or soemthing like this
        # add_fillers -base_cells [regexp -all -inline {FILLCELL_X\d*} [get_db add_fillers_cells]]

        ## add on, calculate the final DCAP value (in fF from schematic @ 1.8V)
        set cap_list { DCAP_X16 164.5 DCAP_X8 68.6 DCAP_X4 20.6 }
        dict with cap_list {}
        set total_cap 0
        set cap_cells [regexp -all -inline {DCAP_X\d*} [get_db add_fillers_cells]]
        foreach cell $cap_cells {
            set total_cap [expr $total_cap + [llen [get_db [get_db insts -if {.base_name==FILLER*}] .base_cell -if {.name==$cell}]] * [dict get $cap_list $cell]]
        }
        imsg_id -id UFSM-151 -tl 2 -msg "flow_step_add_fillers: Total estimated DCAP capacitance: [expr $total_cap / 1000] pF"
    }
}

##############################################################################
define_proc flow_step_run_route { } {
##############################################################################

#update_rc_corner -name rc_slow -pre_route_clock_cap 1.0751 -pre_route_clock_res 0.9234
#update_rc_corner -name rc_fast -pre_route_clock_cap 1.0751 -pre_route_clock_res 0.8928

     ## preroute custom scripts
    # if { [info exist ::cfg_preroute_custom_script] && [llength $::cfg_preroute_custom_script] > 0 } {
    #     set f [file join [subst [get_db flow_cell_cfg_path]] innovus_inputs [string trim $::cfg_preroute_custom_script]]
    #     if { [file exists $f] } {
    #         ## Perform sourcing script
    #         if { [catch {source $f} msg] } {
    #             emsg_id_stop  -id USFM-100  -msg "flow_step_run_route: fail to source $f, $msg"
    #         }
    #     } else {
    #         emsg_id_stop -id USFM-104 -msg "flow_step_run_route: requested file $f not accessible."
    #     }
    # }





		set_db route_design_detail_post_route_swap_via none

    ## Perform detail routing and DRC cleanup:
    ## HINT: -track_opt -> "timing optimization after track assignment"
    ##       -> TQuantus model file will be generated and set automatically.
    route_design -track_opt; # timing- and signal integrity-driven mode by default
report_skew_groups -summary 
    ## NOTE: route_design is automatically creating a directory 'timingReports'
    ##       with some SI glitches report which cannot be redirected into the
    ##       'flow_report_directory' unfortunately.
    ##       Furthermore, it's creating the file 'rc_model.bin'.

    ## Settings taken over from toolkit flow (delaycal_enable_si = true, extract_rc_engine = post_route):
    ##
    ## NOTES:
    ##   - 'delaycal_enable_si' is automatically set to 'true' by route_design, i.e. for some part of the flow, SI is enabled anyhow.
    ##   - We don't have a license supporting SI during sign-off. Although there might be differences in timing paths (postroute vs sign-off),
    ##     we should disable SI as late as possible, i.e. Innovus should fix all timing issues related to SI even though SI is not checked
    ##     again during sign-off state.
    set_db delaycal_enable_si true
    set_db extract_rc_engine post_route

    ## Multicut vias might be interesting for the future:
    # set_db route_design_detail_post_route_swap_via multiCut
    # route_design -via_opt

   # Enable postroute skew repair
   if { [info exist ::cfg_enable_postroute_skew_repair] && $::cfg_enable_postroute_skew_repair == true } {
     delete_filler
     report_skew_groups -summary -out_file before_skew_repair.rpt
     clock_post_route_repair -enable_skew_repair true
     report_skew_groups -summary -out_file after_skew_repair.rpt
     if { [lindex [split [get_db program_version] .] 0] <= 18 } {
         delete_place_blockages -all
     } else {
         delete_obj [get_db place_blockages]
     }
     add_fillers
   }


}


##############################################################################
define_proc flow_step_fix_antenna { } {
##############################################################################
  ## Perform antenna fix for digital internal diode violations. This should be done after route stage

    ## check for correct location
    if { ![info exist ::cfg_enable_antenna_fix_after_route] || $::cfg_enable_antenna_fix_after_route == false  } {
      return
  }

  if { [info exist ::cfg_antenna_cell]  && [llength $::cfg_antenna_cell] > 0 && [info exist ::cfg_antenna_inst_pre_name] && [llength $::cfg_antenna_inst_pre_name] > 0 } {
        # check for antenna (required) and determine density (optional)
      check_process_antenna  \
            -out_file [file join [get_db flow_report_directory] [get_db flow_report_name] [get_db flow_cell_name]_route_antenna.rpt]

      delete_filler
      # Insert diode
      add_diode_after_route  -antenna_cell $::cfg_antenna_cell  -prefix $::cfg_antenna_inst_pre_name -antenna_file [file join [get_db flow_report_directory] [get_db flow_report_name] [get_db flow_cell_name]_route_antenna.rpt]
      place_detail
      add_fillers
      route_eco
  }

}

define_proc flow_step_fix_trans { } {
  ## check for correct location
  if { ![info exist ::cfg_enable_trans_fix_after_route] || $::cfg_enable_trans_fix_after_route == false  } {
      return
  }

  fix_trans_vio  -buf_cell "BUF_X8" -rel_dist_to_sink 0.6 -trans_vio_file "reports/postroute/postroute_opt.tran.gz"


}

#=============================================================================
# Flow: Post-Routing
#=============================================================================

##############################################################################
define_proc flow_step_run_opt_postroute { } {
##############################################################################
    ## perform postroute and SI based setup optimization
    #opt_design -post_route -setup -hold -report_dir debug -report_prefix [get_db flow_report_name]
    opt_design -post_route -setup -hold \
        -report_dir [file join [get_db flow_report_directory] [get_db flow_report_name]]\
        -report_prefix "postroute_opt"

    # Below is only used to fix some few DRC violations, so only enable in
    # final design stage
    if { [info exists ::cfg_enable_drc_fix_after_postroute] && $::cfg_enable_drc_fix_after_postroute  == true } {
      #delete_routes_with_violations
      check_drc -check_cuts_in_same_via -limit 1000
      route_eco -fix_drc
    }



}

#=============================================================================
# Flow: Exporting
#=============================================================================

##############################################################################
define_proc flow_step_write_netlist { } {
##############################################################################

    ## Write netlists (for simulation, Modus, LVS):
    set dst_dir [get_db epc_flow_result_directory]
    if { ![file exist $dst_dir] } {
        file mkdir $dst_dir
        imsg_id  -id UFSM-151  -tl 5  -msg "flow_step_write_netlist: Created new directory '$dst_dir'."
    }

    ## Delete unnecessary assign statements, e.g. 'obs' pin in hierarchical clock gating instances:
    delete_assigns

    ## Delete dangling ports (required to delete empty hierarchical instances directly connected to next higher-level hierarchical instance):
    if { [lindex [split [get_db program_version] .] 0] <= 18 } {
        delete_dangling_ports; # Deletes (at least some) power ports connected in RTL when using Innovus 19.11.
    }

    ## In order to make sure that netlist and layout are matching for LVS, we need to delete empty (due to optmization) hierarchical instances:
    delete_empty_hinsts

    ## In order to prevent empty module declarations for digital hard macros, we need to unflatten ILMs such that
    ## they are treated as instances instead of hierarchical instances when writing netlists:
    unflatten_ilm

    ## Write out a normal netlist, e.g. for simulation or Modus:
    write_netlist  -top_module_first  -exclude_leaf_cells  -omit_floating_ports  $dst_dir/[get_db flow_cell_name].v

    ## Write out a netlist for LVS with power/ground connections already defined in RTL only.
    ## NOTE 1: Implicit power/ground connections for std. cells will be defined using netSet in a SKILL
    ##         script when moving the final design to libs_oa.
    ## NOTE 2: Filler cells (FILLCELL_X*) are empty cells and not relevant for LVS therefore. Hence, we
    ##         do not add them in order not to blow up netlist and schematic unnecessarily.
    ## NOTE 3: Further cells can be added using the configuration variable 'cfg_phys_cells'.
    if { [info exists ::cfg_phys_cells] && [llength $::cfg_dft_disabled > 0] } {
        write_netlist  -include_phys_cells $::cfg_phys_cells  -exclude_leaf_cells  -omit_floating_ports  $dst_dir/[get_db flow_cell_name]_lvs.v
    } else {
        write_netlist  -include_phys_cells {DCAP_X4 DCAP_X8 DCAP_X16}  -exclude_leaf_cells  -omit_floating_ports  $dst_dir/[get_db flow_cell_name]_lvs.v
    }

    # write_netlist options:
    # -help                              # Prints out the command usage
    # <file_name>                        # Specifies the name of the file where the netlist is written (string, required)
    # -exclude_insts_of_cells <string>   # Excludes the instances of the specified cells from the netlist (string, optional)
    # -exclude_leaf_cells                # Excludes leaf cell definitions in the netlist (bool, optional)
    # -exclude_top_pg_ports <string>     # Excludes the specified power and ground port(s) from the top cell's module port list (string, optional)
    # -flat                              # Writes a flattened Verilog netlist (bool, optional)
    # -flatten_bus                       # Writes a flattened-bus Verilog netlist (bool, optional)
    # -ilm                               # Writes out a netlist with ILM guts (bool, optional)
    # -include_bump_cells                # Writes bump cell information to the netlist (bool, optional)
    # -include_pg_ports                  # Includes PG terms in module port list (bool, optional)
    # -include_phys_cells <string>       # Includes the physical instances of the specified cells (string, optional)
    # -include_phys_insts                # Includes physical instances, such as fillers (bool, optional)
    # -keep_all_backslash                # Keeps all backslash in names (bool, optional)
    # -line_length <integer>             # Specifies the maximum number of characters per line for module port lists and instance port connections (int, optional)
    # -module <string>                   # Saves the netlist of the specified module (string, optional)
    # -omit_floating_ports               # Omits unconnected instance ports from the saved netlist (bool, optional)
    # -only_blocks                       # Writes only macro or block cell definitions to the netlist (bool, optional)
    # -only_leaf_cells                   # Writes only leaf cell definitions in the netlist (bool, optional)
    # -only_std_cells                    # Writes only standard cell instances in the netlist (bool, optional)
    # -phys                              # Same as '-includePowerGround -includePhysicalInst' (bool, optional)
    # -top_module <cellName>             # Saves the specified module along with its submodules (string, optional)
    # -top_module_first                  # Write out top module first (bool, optional)
    # -update_tie_connections            # Replaces 1'b1 and 1'b0 constants with undriven nets in the output netlist (bool, optional)
    # -use_pg_ports                      # Use top-cell PG ports for the top module in a physical netlist created with -phys or -includePowerGround. (bool, optional)

    ## Flatten ILMs again:
    flatten_ilm

    ## Append netlists of instantiated hard macros if available:
    set netlist_top_path [file join [get_db epc_flow_result_directory] [get_db flow_cell_name].v]
    set fh_netlist_top [open $netlist_top_path a]

    if { [info exists ::cfg_hard_macros] } {
        imsg_id  -id UFSM-151  -tl 2  -msg "flow_step_write_netlist: Appending hard macro netlists to '$netlist_top_path' ..."
        foreach hm_path [dict keys $::cfg_hard_macros] {
            set hm_netlist_path [subst [file join $hm_path be [get_db epc_flow_place_route_output_directory] [file tail $hm_path].v]]
            if { [file exists $hm_netlist_path] } {
                set cell_name [file tail $hm_path]
                ## Make sure that module has not already been appended before:
                if { ![regexp  -line "^module\\s$cell_name\\s\\(\$" [epc::read_file $netlist_top_path]] } {
                    set fh_netlist_hm [open $hm_netlist_path r]
                    puts $fh_netlist_top "/* [string repeat # 150] */"
                    puts $fh_netlist_top "/* Appended from: $hm_netlist_path */"
                    puts $fh_netlist_top "\n[read $fh_netlist_hm]"
                    flush $fh_netlist_top; # To make sure that appended part is immediately written to the file, i.e. not buffered.
                    close $fh_netlist_hm
                    imsg_id  -id UFSM-151  -tl 3  -msg "flow_step_write_netlist: Appended netlist '$hm_netlist_path'."
                }
            }
        }
    }

    close $fh_netlist_top
}


##############################################################################
define_proc flow_step_extract { } {
##############################################################################

    imsg_id  -id UFSM-151  -tl 4  -msg "flow_step_extract: Generating QRC command file..."

    ## Generate a QRC extraction command file:
    if { [info exist ::cfg_auto_qrc_cmd_gen_enabled] && $::cfg_auto_qrc_cmd_gen_enabled == true } {
        set_db  extract_rc_use_qrc_oa_interface true
        write_extraction_spec  -out_dir qrc_data; # -> qrc.cmd template generated directly by Innovus
        imsg_id  -id UFSM-151 -tl 0  -msg "flow_step_extract: Generation of reference QRC command file finished. Innovus will be terminated now..."
        exit; # end of Innovus run to generate a reference QRC command file
    } else {
        generate_qrc_cmd_file; # -> qrc.cmd based on template from ESPROS
    }

    ## Check whether report directory is already existing and create one if that's not the case yet.
    if {![file exists [get_db flow_report_directory]]} {
        file mkdir [get_db flow_report_directory]
    }

    ## Perform parasitic extraction:
    if { [catch {exec qrc  -cmd qrc.cmd} msg] } {
        emsg_id_stop  -id USFM-100  -msg "flow_step_extract: QRC execution failed because $msg"
    }
}


##############################################################################
define_proc flow_step_sign_off { } {
##############################################################################

    ## Read the generated SPEF files providing extracted parasitic data and
    ## assign it to the corresponding RC corners.
    ## NOTE: The actual .spef file reading starts only when there is a SPEF
    ##       file/field specified for every ACTIVE corner in case the
    ##       specification is split into multiple commands.
    ## NOTE: The -spef_field is an optional parameter, and if not specified,
    ##       the software assumes the default fields of {1 2 3 ... N} where N
    ##       is equal to the number of RC corners provided in the -rc_corner list.
    ## NOTE: It seems like inactive RC corners are ignored when specified
    ##       in list of switch '-rc_corner'.


    ###read_spef \
    ###    -rc_corner [get_db [get_db rc_corners] .name] \
    ###    ./[get_db epc_flow_result_directory]/[get_db flow_cell_name].mv.spef.gz


    ## In case there are ILMs, make sure that ILMs are not treated as black boxes:
    if { [info exists ::cfg_hard_macros] && [llength [dict filter $::cfg_hard_macros value ilm]] > 0 && [eval_legacy dbIsHeadIlmFlattened] == 0 } {
        flatten_ilm
    }

    set spef_list "./[get_db epc_flow_result_directory]/[get_db flow_cell_name].mv.spef.gz"; # top level SPEF file
    foreach ilm_path [dict keys [dict filter $::cfg_hard_macros value ilm]] {
        set ilm_name [file tail $ilm_path]
        #lappend spef_list [set ilm_path]/be/innovus_outputs/${ilm_name}.mv.spef.gz
        set spef_list "$spef_list [subst [set ilm_path]/be/[get_db epc_flow_place_route_output_directory]/[set ilm_name].mv.spef.gz]"
    }

    imsg_id  -id UFSM-151  -tl 5 -msg "Reading SPEF files..."

    ## NOTE: The SPEF map must be matching to the extraction settings (in QRC command file).

    if { [info exists ::cfg_spef_map] && [llength $::cfg_spef_map] > 0 } {
        set rc_corner_name_list  ""
        set rc_corner_index_list ""
        array set rc_corner_id_by_name  {}

        foreach a $::cfg_spef_map {
           set rc_corner_name_list "$rc_corner_name_list [lindex $a 0]"
           set rc_corner_index_list "$rc_corner_index_list [lindex $a 1]"
           set rc_corner_id_by_name([lindex $a 0]) "[lindex $a 1]"
        }
    } else {
        ## If no SPEF map has been defined, use default settings:
        set rc_corner_name_list  [list "rc_fast" "rc_slow"]
        set rc_corner_index_list [list 1 2]
        array set rc_corner_id_by_name  {"rc_fast" 1 "rc_slow" 2}
        imsg_id  -id UFSM-151  -tl 5 -msg "Please specify spef map, now we are using default setting!"
    }


    ## Read all SPEF files (including the ones corresponding to ILMs):
    read_spef \
        -rc_corner $rc_corner_name_list -spef_field $rc_corner_index_list \
        $spef_list


    ## Report flattened final annotation percentage:
    report_annotated_parasitics


    ## Check that the I/O constraints are sane:
    file mkdir [file join [get_db flow_report_directory] [get_db flow_report_name]]; # Just to make sure that the directory exists.
    check_timing -verbose  > "[file join [get_db flow_report_directory] [get_db flow_report_name] [get_db flow_cell_name]_sign_off_check_timing.rpt.gz]"

## **ERROR: (IMPLIC-90):	This command "time_design -report_only -sign_off -timing_debug_report ..." does not have the necessary license to run with the current base license 'vdixl'. You ### must get access to one of the following optional licenses before you can run the command:  tpsxl tpsl vdst vtsxl vtsl vdsp.
##Use 'set_license_check -status' to see the current licenses in use and for more information.
##**WARN: (IMPDC-2611):	Switching -SIAware option back to false

    ## Disable signal integrity awareness because we don't have a license for this feature:
    set_db delaycal_enable_si false

    ## Static timing check: SETUP
    time_design  -report_only  -sign_off  -timing_debug_report  -slack_report  -path_report  -drv_report \
        -expanded_views  -report_dir [get_db flow_report_directory]/[get_db flow_report_name]  -report_prefix [get_db flow_cell_name]_sign_off

    ## Static timing check: HOLD
    time_design  -hold  -report_only  -sign_off  -timing_debug_report  -slack_report  -path_report \
        -expanded_views  -report_dir [get_db flow_report_directory]/[get_db flow_report_name]  -report_prefix [get_db flow_cell_name]_sign_off

    ## Here are some explanations to the used time_design switches:
    ## -sign_off:              Generates timing reports for sign-off timing analysis. By default, in this
    ##                         mode the software runs AAE delay calculation and QRC standalone sign-off extraction.
    ##                         You can use the -report_only parameter in addition to -sign_off to run a sign_off
    ##                         timing analysis based on external SPEF files.
    ## -report_only:           Specifies the use of existing extraction and timing analysis data to generate
    ##                         timing reports. When you use this parameter, the software does not run extraction;
    ##                         instead it uses data that is already in memory.
    ## -timing_debug_report:   Generates a compressed (.gz) detailed timing report in ASCII format. This report
    ##                         is used for debugging timing results using the timing debug feature in Innovus.


    ## save the original fast and slow view as list (due to overwrite of 'set_analysis_view' while SDF write)
    set all_view_slow_list [eval_legacy all_setup_analysis_views]
    set all_view_fast_list [eval_legacy all_hold_analysis_views]
    #all_constraint_modes -active
    ## Write out SDF files for active analysis views:
    foreach view {func test} {
        set view_slow_list {}
        set view_fast_list  {}
        ## extract views and order them to corner lists
        foreach av [get_db analysis_views] {
            if { [string first $view [get_db $av .name]] != -1 } {
                foreach corner {slow fast} {
                    if { [lindex [split [get_db $av .delay_corner] /] 1] == "dc_$corner" } {
                        lappend view_[set corner]_list [get_db $av .name]
                    }
                }
            }
        }
        puts "\n$view slow: '$view_slow_list', fast: '$view_fast_list'\n"

        ## write SDF
        foreach corner {fast slow} {
            if { [set view_[set corner]_list] != "" } {
                #set_analysis_view  -setup [set view_[set corner]_list] -hold [set view_[set corner]_list] -update_timing
                set_analysis_view  -setup [set view_[set corner]_list] -hold [set view_[set corner]_list]
                read_spef -rc_corner \
                   [list rc_$corner] -spef_field  [list $rc_corner_id_by_name(rc_${corner})] \
                   $spef_list

                if { [info exists ::cfg_write_sdf_exclude_blk_list] && [llength  $::cfg_write_sdf_exclude_blk_list] > 0 } {
                   write_sdf -edges check_edge -map_negative_delays -no_escape -exclude_cells $::cfg_write_sdf_exclude_blk_list  \
                       -recompute_delaycal -no_variation -map_negative_delays \
                       [get_db epc_flow_result_directory]/[get_db flow_cell_name]_layout_[set view]_[set corner].sdf.gz
                } else {
                   write_sdf -edges check_edge -map_negative_delays -no_escape  \
                       -recompute_delaycal -no_variation -map_negative_delays \
                       [get_db epc_flow_result_directory]/[get_db flow_cell_name]_layout_[set view]_[set corner].sdf.gz
                }
            }
        }

    }

    ## restore all views by saved values
    #set_analysis_view -setup $all_view_slow_list -hold $all_view_fast_list -update_timing
    set_analysis_view -setup $all_view_slow_list -hold $all_view_fast_list

    #read_spef \
    #    -rc_corner $rc_corner_name_list -spef_field $rc_corner_index_list \
    #    $spef_list

#    get_db [get_db analysis_views -if {.is_setup||.is_hold}] .name  -foreach \
#        {write_sdf  -view $object  -edges edged  -map_negative_delays  -no_escape \
#             [get_db epc_flow_result_directory]/[get_db flow_cell_name]_layout_[string range $object 3 end].sdf.gz}

    ## Here are some explanations to the used write_sdf switches:
    ## -edges:                 Specifies the edges values. Default: edged
    ##                         * check_edge - Keeps edge specifiers on timing check arcs but does not add
    ##                           edge specifiers on delay arcs.
    ##                         * edged - Keeps edge specifiers on timing check arcs and delay arcs.
    ##                         * library - Assumes that the sdf_edges attributes are specified in the .lib file.
    ##                           The default sdf_edges value of a timing arc is noedge.
    ##                         * noedge - Defined in Table 38-10.
    ## -map_negative_delays:   Converts all negative IOPATH delays to zero.
    ## -no_escape              Writes out object names without escaping hierarchical dividers. Normally, when writing
    ##                         out an SDF, the hierarchical divider (/) is escaped because it is a special character.

    ## Delete all blockages:
    if { [lindex [split [get_db program_version] .] 0] <= 18 } {
	delete_place_blockages -all
    } else {
	delete_obj [get_db place_blockages]
    }
    if { [lindex [split [get_db program_version] .] 0] <= 17 } {
        delete_route_blockages -all
    } else {
        # new 18.x version uses different naming
        delete_route_blockages -type all
    }

    ## Generate a DRC report after removing placement and routing blockages:
    check_drc  -check_implant_across_row -check_only all -check_routing_halo -check_cuts_in_same_via -limit 1000  -out_file [get_db flow_report_directory]/[get_db flow_report_name]/[get_db flow_cell_name]_layout_drc.rpt

    ## Delete all DRC markers:
    delete_drc_markers
}


##############################################################################
define_proc flow_step_add_time_stamp { } {
##############################################################################

    ## For some reason, labels added to the layer 'text' by the following commands
    ## are not visible in Virtuoso. Other layers like MET1 would work but that's not
    ## the idea. Hence, a time stamp will be added by a skill script at the end of the flow.
#     create_text -label [get_db flow_cell_name]                                                 -height 2.5  -point {1.0 3.0}  -alignment lower_left  -always_display true  -font roman  -layer text  -oa_purpose drawing
#     create_text -label "generated on [clock format  [clock seconds]  -format {%Y-%m-%d, %T}]"  -height 1.0  -point {1.0 1.0}  -alignment lower_left  -always_display true  -font roman  -layer text  -oa_purpose drawing

    ## NOTE: Text labels can be removed by "delete_obj [get_db designs .texts]".
}


#=============================================================================
# Flow: ECO
#=============================================================================

##############################################################################
define_proc flow_step_eco_start { } {
##############################################################################
}

##############################################################################
define_proc flow_step_place_eco { } {
##############################################################################
    place_eco
}

##############################################################################
define_proc flow_step_route_eco { } {
##############################################################################
    route_eco
}

##############################################################################
define_proc flow_step_eco_finish { } {
##############################################################################
}

## Reporting #################################################################

##############################################################################
define_proc flow_step_report_area { } {
##############################################################################
    report_summary -no_html -out_dir [get_db flow_report_directory] -out_file [file join [get_db flow_report_directory] [get_db flow_report_name] [get_db flow_report_prefix]_qor.rpt]
    report_area    -min_count 1000         -out_file [file join [get_db flow_report_directory] [get_db flow_report_name] [get_db flow_report_prefix]_area_summary.rpt]
}

##############################################################################
define_proc flow_step_report_late_path { } {
##############################################################################

    ## NOTE from the manual:
    ## All super commands such as time_design,  place_design,  and  opt_design
    ## flatten  the  ILMs  internally  and  hence  no  explicit flatten_ilm is
    ## required. However, you should use 'flatten_ilm' for other timing related
    ## commands.  For  example, for the report_timing command, you need to run
    ## flatten_ilm before reporting timing. It would not work in the unflattended mode
    if { [info exists ::cfg_hard_macros] && [llength [dict filter $::cfg_hard_macros value ilm]] > 0 && [eval_legacy dbIsHeadIlmFlattened] == 0 } {
        flatten_ilm
    }

    ## Reports that show detailed timing with Graph Based Analysis (GBA)
    report_timing -max_paths 5   -nworst 1 -path_type endpoint        > [get_db flow_report_directory]/[get_db flow_report_name]/[get_db flow_report_prefix]_setup_endpoint.rpt
    report_timing -max_paths 1   -nworst 1 -path_type full_clock -net > [get_db flow_report_directory]/[get_db flow_report_name]/[get_db flow_report_prefix]_setup_worst.rpt
    report_timing -max_paths 500 -nworst 1 -path_type full_clock      > [get_db flow_report_directory]/[get_db flow_report_name]/[get_db flow_report_prefix]_setup_gba.rpt

    ## Reports that show detailed timing with Path Based Analysis (PBA)
    #OBS?: if {[is_flow -inside flow:sta]} {
    #OBS?:   report_timing -max_paths 50 -nworst 1 -path_type full_clock -retime path_slew_propagation > [get_db flow_report_directory]/[get_db flow_report_name]/setup.pba.rpt
    #OBS?: }
}

##############################################################################
define_proc flow_step_report_early_path { } {
##############################################################################

    if { [info exists ::cfg_hard_macros] && [llength [dict filter $::cfg_hard_macros value ilm]] > 0 && [eval_legacy dbIsHeadIlmFlattened] == 0 } {
        flatten_ilm
    }

    #- Reports that show detailed early timing with Graph Based Analysis (GBA)
    report_timing -early -max_paths 5   -nworst 1 -path_type endpoint        > [get_db flow_report_directory]/[get_db flow_report_name]/[get_db flow_report_prefix]_hold_endpoint.rpt
    report_timing -early -max_paths 1   -nworst 1 -path_type full_clock -net > [get_db flow_report_directory]/[get_db flow_report_name]/[get_db flow_report_prefix]_hold_worst_max_path.rpt
    report_timing -early -max_paths 500 -nworst 1 -path_type full_clock      > [get_db flow_report_directory]/[get_db flow_report_name]/[get_db flow_report_prefix]_hold_gba.rpt

    #OBS?: #- Reports that show detailed timing with Path Based Analysis (PBA)
    #OBS?: if {[is_flow -inside flow:sta]} {
    #OBS?:   report_timing -early -max_paths 50 -nworst 1 -path_type full_clock -retime path_slew_propagation  > [get_db flow_report_directory]/[get_db flow_report_name]/hold.pba.rpt
    #OBS?: }
}

##############################################################################
define_proc flow_step_report_timing_late { } {
##############################################################################

    ## Update the timer for setup and write reports
    #time_design -expanded_views -report_only -report_dir debug -report_prefix [get_db flow_report_name]
    time_design -expanded_views -report_only \
        -report_dir [file join [get_db flow_report_directory] [get_db flow_report_name]] \
        -report_prefix "timing_late"

    if { [info exists ::cfg_hard_macros] && [llength [dict filter $::cfg_hard_macros value ilm]] > 0 && [eval_legacy dbIsHeadIlmFlattened] == 0 } {
        flatten_ilm; # Command 'report_analysis_summary' is not supported in unflattened state.
    }

    ## Reports that describe timing health
    report_analysis_summary -late                               > [file join [get_db flow_report_directory] [get_db flow_report_name] [get_db flow_report_prefix]_setup_analysis_summary.rpt]
    report_analysis_summary -late -merged_groups                > [file join [get_db flow_report_directory] [get_db flow_report_name] [get_db flow_report_prefix]_setup_group_summary.rpt]
    report_analysis_summary -late -merged_groups  -merged_views > [file join [get_db flow_report_directory] [get_db flow_report_name] [get_db flow_report_prefix]_setup_view_summary.rpt]
    report_constraint       -late -all_violators                > [file join [get_db flow_report_directory] [get_db flow_report_name] [get_db flow_report_prefix]_setup_all_violators.rpt]
    set_metric -name timing.drv.report_file -value                [file join [get_db flow_report_name] [get_db flow_report_prefix]setup.all_violators.rpt]
}

##############################################################################
define_proc flow_step_report_timing_early { } {
##############################################################################
    ## Update the timer for hold and write reports
    #time_design -expanded_views -hold -report_only -report_dir debug -report_prefix [get_db flow_report_name]
    time_design -expanded_views -hold -report_only \
        -report_dir [file join [get_db flow_report_directory] [get_db flow_report_name]] \
        -report_prefix "timing_early"

    if { [info exists ::cfg_hard_macros] && [llength [dict filter $::cfg_hard_macros value ilm]] > 0 && [eval_legacy dbIsHeadIlmFlattened] == 0 } {
        flatten_ilm; # Command 'report_analysis_summary' is not supported in unflattened state.
    }

    ## Reports that describe timing health
    report_analysis_summary -early                              > [file join [get_db flow_report_directory] [get_db flow_report_name] [get_db flow_report_prefix]_hold_analysis_summary.rpt]
    report_analysis_summary -early -merged_groups               > [file join [get_db flow_report_directory] [get_db flow_report_name] [get_db flow_report_prefix]_hold_group_summary.rpt]
    report_analysis_summary -early -merged_groups -merged_views > [file join [get_db flow_report_directory] [get_db flow_report_name] [get_db flow_report_prefix]_hold_view_summary.rpt]
    report_constraint       -early -all_violators               > [file join [get_db flow_report_directory] [get_db flow_report_name] [get_db flow_report_prefix]_hold_all_violators.rpt]
}

##############################################################################
define_proc flow_step_report_clock_timing { } {
##############################################################################
    ## Reports that check clock implementation
    report_clock_timing -type summary > [file join [get_db flow_report_directory] [get_db flow_report_name] [get_db flow_report_prefix]_clock_summary.rpt]
    report_clock_timing -type latency > [file join [get_db flow_report_directory] [get_db flow_report_name] [get_db flow_report_prefix]_clock_latency.rpt]
    report_clock_timing -type skew    > [file join [get_db flow_report_directory] [get_db flow_report_name] [get_db flow_report_prefix]_clock_skew.rpt]
}

##############################################################################
define_proc flow_step_report_power { } {
##############################################################################
    ## Ensure leakge power view is active when specified
    if { ([get_db power_leakage_power_view] != "") && \
            ([lsearch -exact [get_db [concat [get_db analysis_views -if {.is_setup}] [get_db analysis_views -if {.is_hold}]] .name] [get_db power_leakage_power_view]] == -1) } {
        set_analysis_view \
            -setup [lsort -unique [concat [get_db power_leakage_power_view] [get_db [get_db analysis_views -if {.is_setup}] .name]]] \
            -hold [get_db [get_db analysis_views -if {.is_hold}] .name]
    }
    report_power -no_wrap -out_file [file join [get_db flow_report_directory] [get_db flow_report_name] [get_db flow_report_prefix]_power_all.rpt]
}

##############################################################################
define_proc flow_step_report_route_process { } {
##############################################################################
    ## Reports that process rules
    check_process_antenna -out_file [file join [get_db flow_report_directory] [get_db flow_report_name] [get_db flow_report_prefix]_route_antenna.rpt]
    check_filler -out_file [file join [get_db flow_report_directory] [get_db flow_report_name] [get_db flow_report_prefix]_route_filler.rpt]
}

##############################################################################
define_proc flow_step_report_route_drc { } {
##############################################################################
    ## Reports that check signal routing
    check_drc -out_file [file join [get_db flow_report_directory] [get_db flow_report_name] [get_db flow_report_prefix]_route_drc.rpt]
    check_connectivity -out_file [file join [get_db flow_report_directory] [get_db flow_report_name] [get_db flow_report_prefix]_route_open.rpt]
}

##############################################################################
define_proc flow_step_report_route_density { } {
##############################################################################
    ## skip due to metal fill step in merge process before tapeout
    ## at this stage this report would generate lots of violations, even with the technology parameters
    ##       MINIMUMDENSITY, MAXIMUMDENSITY, DENSITYCHECKWINDOW, DENSITYCHECKSTEP
    ## innovus> get_db [get_db layers -if {.name == "*MET*"}] .density*
    # check_metal_density -report [file join [get_db flow_report_directory] [get_db flow_report_name] [get_db flow_report_prefix]_route_metal_density.rpt]

    ## skipp due to undefined parameters in technology (auto values results in big memory usage)
    ##
    ## BACKGROUND:
    ## The technology does define:
    ##       MINIMUMDENSITY, MAXIMUMDENSITY, DENSITYCHECKWINDOW, DENSITYCHECKSTEP
    ## for the metals only, but NOT for the VIAs (no restrictions)
    ## innovus> get_db [get_db layers -if {.name == "*VIA*"}] .density*
    ##
    ## by checking the cuts, innovus generates (small) default values for CONT, VIA[1..4], TVIA
    ##       window = 10 10, step = 5, density = 0.1152 .. 30
    ## which would consume a lot of memory
    ##
    ## to force the paramters, use:
    # set_via_fill -layer "VIA1 VIA2 VIA3 VIA4 TVIA" -window_size 100 100 -window_step 50 50 -min_density 0.0 -max_density 30
    # check_cut_density -out_file [file join [get_db flow_report_directory] [get_db flow_report_name] [get_db flow_report_prefix]_route_cut_density.rpt]
}


#===========================================================================
# Flow: create_partitions
#===========================================================================

##############################################################################
define_proc flow_step_place_partitions { } {
##############################################################################
    ## Place design w/partitions

    set_db flow_top_level [get_db designs .name]
    set_db flow_partition_list [get_db [get_db designs] .partitions.name]

    if { [get_db hinsts .partition.clones] != ""} {
        align_partition_clones
    }

    place_opt_design -report_dir [get_db flow_report_directory] -report_prefix partition_place
}

##############################################################################
define_proc flow_step_add_feedthrus { } {
##############################################################################
    ## Add partition feedthroughs
    route_early_global
    add_partition_feedthrus -route_based -write_topological_file topo.txt -net_mapping netmapping.txt -double_buffer
    set_db route_early_global_honor_partition_fence .
    route_early_global
}

##############################################################################
define_proc flow_step_assign_pins { } {
##############################################################################
    ## Assign pins
    assign_partition_pins
    check_pin_assignment
    report_unaligned_nets -out_file [get_db flow_report_directory]/unaligned.rpt
    set_db route_early_global_honor_partition_pin .
    route_early_global
}


##############################################################################
define_proc flow_step_write_partitions { } {
##############################################################################
   ## Generate partitions
    if { [get_db hinsts .partition.clones] != ""} {
    }
    create_timing_budget -justify
    commit_partition -build_scan
    write_partitions -dir [get_db flow_partition_directory] -def -def_scan

}


##############################################################################
define_proc flow_step_load_partition { } {
##############################################################################
    read_db .
}


##############################################################################
define_proc flow_step_read_ilms { } {
##############################################################################

    if { [info exists ::cfg_hard_macros] && [llength [dict filter $::cfg_hard_macros value ilm]] > 0 } {

        if { [eval_legacy dbIsHeadIlmFlattened] == 1 } {
            unflatten_ilm; # ILMs can only be added in unflattened mode.
        }

        ## Specify ILMs:
        foreach ilm_path [dict keys [dict filter $::cfg_hard_macros value ilm]] {
            set ilm_name [file tail $ilm_path]
            imsg_id  -id UFSM-151  -tl 5 -msg "Reading ILM $ilm_path ..."
            read_ilm -oa_cellview [list [set ilm_name]_lib [set ilm_name] abstract]
        }

        flatten_ilm

    } else {
        imsg_id  -id UFSM-151  -tl 1 -msg "Reading of ILMs has been skipped because no ILMs are specified in the configuration file."
    }

}
