Take row_driver_ctrl_pair as an example:
  source /usr/project-a/epc909/users/mcc/.noshell_env.bash
  get flow information from gui.
  export cell and flow information.
  
  export CELL_NAME=row_driver_ctrl_pair
  export CELL_CFG_DIR=be
  export CELL_PATH=/usr/project-a/epc909/users/mcc/digital/dev/blocks/row_driver_ctrl_top/row_driver_ctrl_pair
  export CELL_PROJECT=epc909
  export PROJECT=epc909
  export TEMP_PATH=/usr/eda_tooling-a/tools/digital/scripts/ebe/templates
  export DBG_LEVEL=9
  export DIS_MSG_LIMIT=0
  export LC_ALL=en_US.UTF-8
  export LANG=en_US.UTF-8
  export EBE_BIN_PATH=/usr/eda_tooling-a/tools/digital/scripts/ebe
  export TERMINAL_DBG_LEVEL=info
  export IUSVERSION=19.09-msi
  export INN_RESTORE_DB_FILE_CHECK=0
  echo "Display: '$DISPLAY'"
  export CELL_FLOW=init
  export CELL_FLOW_START=fp
  export CELL_FLOW_STOP=fp
  export CELL_FLOW_SUSPEND=
  export CELL_FLOW_START_DB=
  export CELL_FLOW_METRICS_EN=1
  export CDS_STYLUS_SOURCE_VERBOSE=0

  EXECUTE The command . Processing -files option
  
innovus -files /usr/project-a/epc909/users/mcc/digital/dev/blocks/row_driver_ctrl_top/row_driver_ctrl_pair/be/innovus_scripts/flow_run.tcl 
        -stylus 
        -lic_startup_options vdixl_capacity 
        -lic_startup vdixl 
        -disable_user_startup


env file
source /usr/project-a/epc909/users/mcc/.noshell_env.bash

export DIGITAL_SCRIPTS_PATH=/usr/eda_tooling-a/tools/digital/scripts
export TECH_PATH=/usr/eda_tooling-a/pdk/epc/ohc15l_1v5/fdk_ohc15l_2v2
export IOLIB_PATH=/usr/eda_tooling-a/pdk/epc/ohc15l_1v5/io_ohc15l_4v1
export DBTYPE=oa
export STDCELL_PATH=/usr/eda_tooling-a/pdk/epc/ohc15l_1v5/stdcell_ohc15l_1v2r4
export CDS_Netlisting_Mode=Analog
export RETICLE_PATH=/usr/eda_tooling-a/pdk/epc/ohc15l_1v5/reticle_ohc15l_1v4
export EDIBASE=innovus-19.1-msi
export CDS_VERSION=6.1.8-tbu
export PROJECT=epc909
export SVN_BASE_URL=svn://svn.ch.epc
export TECHNAME=ohc15l
export PROJECTS_LOCATION=/usr/project-a
export USER=mcc
export PROJECT_DIR=epc909
export PROJECTGROUP=sh.dsgn.projects
export UMASK=027
export ANALOG_IP_PATH=/usr/eda_tooling-a/pdk/epc/ohc15l_1v5/analog_ip_1v0
export DIGITAL_IP_PATH=/usr/eda_tooling-a/pdk/epc/ohc15l_1v5/digital_ip_1v0
export EPC_RULEDECKS=/usr/eda_tooling-a/pdk/epc/ohc15l_1v5/ruledecks_ohc15l_3v0/pvs
export ETBASE=modus-19.1-msi
export MMSIMBASE=spectre-19.1-msi
export EEPROM_PATH=/usr/eda_tooling-a/pdk/epc/ohc15l_1v5/eeprom_ohc15l_1v2
export HAS_DIGITAL=digital_ip_1v0
export EXTBASE=ext-19.1-msi
export LVDS_PATH=/usr/eda_tooling-a/pdk/epc/ohc15l_1v5/lvds_ohc15l_1v0
export OPTICAL_PATH=/usr/eda_tooling-a/pdk/epc/ohc15l_1v5/optical_simplon_ohc15l_1v0
export RCBASE=genus-19.1-msi
export NOSHELL=true


set initProjectShellCmdList {
    "source $::dotNoShellEnvFile"
    "cd [lindex [split [set ::[set tool]WorkDir] :] 1]"
    "export CELL_NAME=[file tail $::cellPath]"
    "export CELL_CFG_DIR=$::cellConfigDir"
    "export CELL_PATH=$::cellPath"
    "export CELL_PROJECT=$::env(PROJECT)"
    "export PROJECT=$::env(PROJECT)"
    "export TEMP_PATH=$::srcPath/templates"
    "export DBG_LEVEL=[set ::[set tool]DbgLevel]"
    "export DIS_MSG_LIMIT=[set ::innovusDisableMsgLimmit]"
    "export LC_ALL=en_US.UTF-8"
    "export LANG=en_US.UTF-8"
    "export EBE_BIN_PATH=$::srcPath"
    "export TERMINAL_DBG_LEVEL=$::terminalDebugLevel"
    "export IUSVERSION=$::env(IUSVERSION)"
    "export INN_RESTORE_DB_FILE_CHECK=[set ::innovusRestoreDbFileCheck]"
    "echo \"Display: '\\\$DISPLAY'\""
}



Project-cadence env variables are being set...

Cadence Innovus(TM) Implementation System.
Copyright 2019 Cadence Design Systems, Inc. All rights reserved worldwide.
Version:	v19.11-s128_1, built Tue Aug 20 20:54:34 PDT 2019
Options:	-files /usr/project-a/epc909/users/mcc/digital/dev/blocks/row_driver_ctrl_top/row_driver_ctrl_pair/be/innovus_scripts/flow_run.tcl -stylus -lic_startup_options vdixl_capacity -lic_startup vdixl -disable_user_startup 
Date:		Mon Mar  2 20:03:11 2020
Host:		cli-1013.cn.epc (x86_64 w/Linux 2.6.32-754.14.2.el6.x86_64) (6cores*12cpus*Intel(R) Xeon(R) CPU E5-1650 0 @ 3.20GHz 12288KB)
OS:		CentOS release 6.10 (Final)
License:
		vdixl	Virtuoso Digital Implementation XL	19.1	checkout succeeded
		Maximum number of instances allowed (1 x 50000).
		Optional license vdixl_capacity "VDI-XL Block Capacity Option" 19.1 checkout succeeded.
		Maximum number of instances allowed: 300000.

Create and set the environment variable TMPDIR to /tmp/innovus_temp_9487_cli-1013.cn.epc_mcc_7aTP23.
Change the soft stacksize limit to 0.2%RAM (64 mbytes). Set global soft_stack_size_limit to change the value.
[INFO] Loading PVS 19.11 fill procedures

**INFO:  MMMC transition support version v31-84 
#@ Processing -files option

execute the flow_run.tcl:


@innovus 1> source /usr/project-a/epc909/users/mcc/digital/dev/blocks/row_driver_ctrl_top/row_driver_ctrl_pair/be/innovus_scripts/flow_run.tcl
Sourcing file '/usr/eda_tooling-a/tools/digital/scripts/ebe/../utils/epc_procs.tcl'
Sourcing file '/usr/project-a/epc909/users/mcc/digital/dev/blocks/row_driver_ctrl_top/row_driver_ctrl_pair/be/innovus_scripts/flow_setup.tcl' ...
Info: no dedicated user messages in Innovus available, skip 'define_msg'
Sourcing file '/usr/project-a/epc909/users/mcc/digital/dev/blocks/row_driver_ctrl_top/row_driver_ctrl_pair/be/innovus_scripts/flow_procs.tcl' ...
Sourcing file '/usr/project-a/epc909/users/mcc/digital/dev/blocks/row_driver_ctrl_top/row_driver_ctrl_pair/be/innovus_scripts/flow_step.tcl' ...
EPC INFO: Sourcing cell configuration file /usr/project-a/epc909/users/mcc/digital/dev/blocks/row_driver_ctrl_top/row_driver_ctrl_pair/be/row_driver_ctrl_pair_config.tcl ...
Sourcing file '/usr/project-a/epc909/users/mcc/digital/dev/blocks/row_driver_ctrl_top/row_driver_ctrl_pair/be/innovus_scripts/flow_attribute.tcl' ...
EPC INFO: UEVM-150 Set debug level to '9'




