
create_library_set -name wcl_slow    -timing {....}
create_library_set -name wcl_fast    -timing {....}
create_library_set -name wcl_typical -timing {....}

create_opcond -name op_cond_wcl_slow    -process 1 -voltage 1.08 -temperature 125
create_opcond -name op_cond_wcl_fast    -process 1 -voltage 1.32 -temperature 125
create_opcond -name op_cond_wcl_typical -process 1 -voltage 1.25 -temperature 125

create_timing_condition -name timing_cond_wcl_slow    -opcond slow    -library_sets {wcl_slow}
create_timing_condition -name timing_cond_wcl_fast    -opcond fast    -library_sets {wcl_fast}
create_timing_condition -name timing_cond_wcl_typical -opcond typical -library_sets {wcl_typical}

create_rc_corner -name rc_corner -qrc_tech <path>/qrcTechFile

create_delay_corner -name delay_corner_wcl_slow\
                    -early_timing_condition timing_cond_wcl_slow\
                    -late_timing_condition  timing_cond_wcl_slow\
                    -early_rc_corner rc_corner\
                    -late_rc_corner rc_corner

create_delay_corner -name delay_corner_wcl_fast\
                    -early_timing_condition timing_cond_wcl_fast\
                    -late_timing_condition  timing_cond_wcl_fast\
                    -early_rc_corner rc_corner\
                    -late_rc_corner rc_corner

create_delay_corner -name delay_corner_wcl_typical\
                    -early_timing_condition timing_cond_wcl_typical\
                    -late_timing_condition  timing_cond_wcl_typical\
                    -early_rc_corner rc_corner\
                    -late_rc_corner rc_corner
                    
create_constraint_mode -name functional_wcl_slow    -sdc_files {slow.sdc}
create_constraint_mode -name functional_wcl_fast    -sdc_files {fast.sdc}
create_constraint_mode -name functional_wcl_typical -sdc_files {typical.sdc}
                    
create_analysis_view -name view_wcl_slow\
                     -constraint_mode functional_wcl_slow\
                     -delay_corner delay_corner_wcl_slow

create_analysis_view -name view_wcl_fast\
                     -constraint_mode functional_wcl_fast\
                     -delay_corner delay_corner_wcl_fast
                     
create_analysis_view -name view_wcl_typical\
                     -constraint_mode functional_wcl_typical\
                     -delay_corner delay_corner_wcl_typical

set_analysis_view -setup {view_wcl_slow view_wcl_fast view_wcl_typical}


===========================================================================
create_library_set -name LS1 -timing [list lib1.lib lib2.lib lib3.lib]
create_library_set -name LS2 -timing [list lib4.lib lib5.lib lib6.lib]
create_library_set -name LS3 -timing [list lib7.lib lib8.lib lib9.lib]
create_rc_corner -name QX -qrc_tech file.qrc
create_timing_condition -name TC1 -library_set LS1
create_timing_condition -name TC2 -library_set LS2
create_timing_condition -name TC3 -library_set LS3
create_delay_corner -name DC1 -timing_condition {TC1 PD2@TC2} -rc_corner QX
create_delay_corner -name DC2 -timing_condition {TC1 PD2@TC3} -rc_corner QX
create_constraint_mode -name CM1 -sdc_files cond1.sdc
create_constraint_mode -name CM2 -sdc_files cond2.sdc
create_analysis_view -name AV1 -constraint_mode CM1 -delay_corner DC1
create_analysis_view -name AV2 -constraint_mode CM2 -delay_corner DC2
set_analysis_view -setup AV1 -hold AV2

When  the set_analysis_view command is processed, the tool traces backwards from that command to determine the library set. In this case,
the analysis view specified by set_analysis_view is AV1. The AV1 analysis view points to domain corner DC1, which in turn points  to  two
library sets, LS1 and LS2. As a result, lib1.lib and lib2.lib are loaded into the database.
In  this  case,  lib3.lib is not loaded into the database during read_mmmc because it is not in the library sets required by the analysis
===========================================================================


=======
epc909
=======

  create_library_set  -name lib_fast  -timing $libs_fast_abs
  create_library_set  -name lib_slow  -timing $libs_slow_abs  
  create_library_set  -name lib_typ   -timing $libs_typ_abs
  
  create_rc_corner -name rc_fast -temperature $mmmc_op_condition_fast(temperature) \
      -qrc_tech  [get_db flow_tech_path]/ruledecks/LPE/qrcTechFile
  create_rc_corner -name rc_slow -temperature $mmmc_op_condition_slow(temperature) \
      -qrc_tech  [get_db flow_tech_path]/ruledecks/LPE/qrcTechFile  

  create_opcond -name op_fast -P $mmmc_op_condition_fast(process) 
                              -V $mmmc_op_condition_fast(voltage) 
                              -T $mmmc_op_condition_fast(temperature)
  create_opcond -name op_slow -P $mmmc_op_condition_slow(process) 
                              -V $mmmc_op_condition_slow(voltage) 
                              -T $mmmc_op_condition_slow(temperature)

  create_timing_condition -name tc_fast -library_sets lib_fast -opcond op_fast
  create_timing_condition -name tc_slow -library_sets lib_slow -opcond op_slow
  
  create_delay_corner -name dc_fast -rc_corner rc_fast -timing_condition tc_fast
  create_delay_corner -name dc_slow -rc_corner rc_slow -timing_condition tc_slow  

  create_constraint_mode  -name $cm  \
      -sdc_files     "$path_to_main_sdc_file $path_to_func_main_sdc_file $path_to_func_post_synth_sdc_file"  \
      -ilm_sdc_files "$path_to_main_sdc_file $path_to_func_main_sdc_file $path_to_func_post_synth_sdc_file $path_to_func_ilm_sdc_file"

  create_analysis_view -name "av_${cm_short}_fast" -constraint_mode $cm_name -delay_corner dc_fast
  create_analysis_view -name "av_${cm_short}_slow" -constraint_mode $cm_name -delay_corner dc_slow  


@innovus 8>  get_db analysis_views -foreach {puts $obj(.)}
analysis_view:row_driver_ctrl_pair/av_test_slow
analysis_view:row_driver_ctrl_pair/av_func_main_slow
analysis_view:row_driver_ctrl_pair/av_test_fast
analysis_view:row_driver_ctrl_pair/av_func_main_fast
4
@innovus 9> get_db constraint_modes -foreach {puts $obj(.)}
constraint_mode:row_driver_ctrl_pair/cm_func_main
constraint_mode:row_driver_ctrl_pair/cm_test
2
  
@innovus 6> get_db analysis_views -foreach {puts $obj(.)}
analysis_view:row_driver_ctrl_pair/av_test_slow
analysis_view:row_driver_ctrl_pair/av_func_main_slow
analysis_view:row_driver_ctrl_pair/av_test_fast
analysis_view:row_driver_ctrl_pair/av_func_main_fast  

   + Analysis View:  av_func_main_slow
     |
     + Delay Calc Corner: dc_slow
     |   |
     |   + timing_condition: tc_slow
     |   |    |
     |   |    + library_sets: lib_slow
     |   |        |
     |   |        + timing: /usr/project-a/epc909/users/mcc/analog/libs_oa/epc909_auxiliaries/ref_bgvref/timing/ref_bgvref_slow.lib
                             /usr/eda_tooling-a/pdk/epc/ohc15l_1v5/stdcell_ohc15l_1v2r4/liberty/LF150DC_HS_F_V1_4_worst_worst.lib
     |   |    |
     |   |    + opcond: op_slow
     |   |
     |   |
     |   |
     |   + rc_corner: rc_slow
     |   |    |
     |   |    + T: 125
     |   |    |
     |   |    + preRoute_res: 1
     |   |    |
     |   |    + postRoute_res: 1 1 1
     |   |    |
     |   |    + preRoute_cap: 1
     |   |    |
     |   |    + postRoute_cap: 1 1 1
     |   |    |
     |   |    + postRoute_xcap: 1 1 1
     |   |    |
     |   |    + preRoute_clkcap: 0
     |   |    |
     |   |    + postRoute_clkcap: 0 0 0
     |   |    |
     |   |    + preRoute_clkres: 0
     |   |    |
     |   |    + postRoute_clkres: 0 0 0
     |   |    |
     |   |    + qx_tech_file: /usr/eda_tooling-a/pdk/epc/ohc15l_1v5/fdk_ohc15l_2v2/ruledecks/LPE/qrcTechFile
     |   |
     |   + si_enabled: true
     |
     + Constraint Mode: cm_func_main
     |   |
     |   + SDC Constraint Files: /usr/project-a/epc909/users/mcc/digital/dev/blocks/row_driver_ctrl_top/row_driver_ctrl_pair/be/row_driver_ctrl_pair_main_constraints.sdc /usr/project-a/epc909/users/mcc/digital/dev/blocks/row_driver_ctrl_top/row_driver_ctrl_pair/be/row_driver_ctrl_pair_func_main_constraints.sdc
   |  


create_analysis_view    
  Creates an analysis view object that associates a delay calculation corner with a constraint mode
  Creates  an  analysis view object that associates a delay calculation corner with a constraint mode. An analysis view object provides all
  of the information necessary to control a single multi-mode multi-corner analysis.
  After creating analysis views, use the set_analysis_view command to specify which views to use for setup and hold optimization or timing analysis.
  Use this command after creating constraint modes (create_constraint_mode) and delay calculation corners (create_delay_corner).

create_delay_corner
  Creates  a  named delay calculation corner object that can be referenced later when creating an analysis view. A delay calculation corner
  provides all of the information necessary to control delay calculation for a specific view.  Each  corner  contains  information  on  the
  libraries  to use, the operating conditions with which the libraries should be accessed, and the RC extraction parameters to use for cal-
  culating parasitic data. Delay corner objects can be shared by multiple top-level analysis views.
         Note: The -early_* and -late_* parameters should not be used to represent best and worst case corners.  Instead use separate delay
         corners  to define Best-Case and Worst-Case differences. Use the -early_* and -late_* parameters within a single delay calculation
         corner to control on-chip variation.  In this latter situation different conditions or libraries will be used for the  launch  and
         capture paths of individual timing paths.  



