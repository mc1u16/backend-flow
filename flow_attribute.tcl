################################################################################
# Flow attribute: init flow attributes
#
# > write global root attributes refer to $env
# > write global root attributes refer to design configuration lists
# > set genus attributes for toolstart and synthesis
#
################################################################################

# project name
#-------------------------------------------------------------------------------
if [info exists env(PROJECT)] {
    set_db flow_project_name $env(PROJECT)
} else {
    emsg_id_stop -id UEVM-100 "PROJECT environment variable not defined."
}

# debug level (controlling msg in genus) and information level (innovus specific)
#-------------------------------------------------------------------------------
if [info exists env(DBG_LEVEL)] {
    set_db flow_debug_level  $env(DBG_LEVEL)
    # set_db information_level $env(DBG_LEVEL)       ;# not supported by innovus
} else {
    set_db flow_debug_level  1
    # set_db information_level 1                     ;# not supported by innovus
}
imsg_id -id UEVM-150 -msg "Set debug level to '[get_db flow_debug_level]'"

# technology settings
#-------------------------------------------------------------------------------
if [info exists env(TECHNOLOGY_NAME)] {
    set_db flow_tech_name $env(TECHNOLOGY_NAME)
} elseif [info exists env(TECHNAME)] {
    set_db flow_tech_name $env(TECHNAME)
} else {
    emsg_id_stop -id UEVM-100 -msg "TECHNOLOGY_NAME/TECHNAME environment variable not defined."
}

if [info exists env(STDCELL_PATH)] {
    set_db flow_tech_std_path $env(STDCELL_PATH)
} else {
    emsg_id_stop -id UEVM-100 -msg "STDCELL_PATH environment variable not defined."
}

if [info exists env(TECH_PATH)] {
    set_db flow_tech_path $env(TECH_PATH)
} else {
    emsg_id_stop -id UEVM-100 -msg "TECH_PATH environment variable not defined."
}

if { [get_db flow_tech_name] == "ohc15l" } {
    set_db flow_tech_process 150
} else {
    wmsg_id -id UEVM-120 -tl 0 -msg "WARNING: Process technology value not defined. Setting to maximum value."
    set_db flow_tech_process 250
}
set_db route_design_detail_fix_antenna true
set_db route_design_antenna_cell_name "ANTENNA"
set_db route_design_antenna_diode_insertion true 


# MESSAGE
#-------------------------------------------------------------------------------

## Get the full list of messages (innovus only):
if { [info exists env(DIS_MSG_LIMIT)] && $env(DIS_MSG_LIMIT) == 1 } {
    set_message -no_limit ;# -id list_of_msgIDs
    imsg_id -id UEVM-150 -msg "disable all message limmit"
}


# Control the amount of printed information:
# set_db information_level [get_db flow_debug_level]    ;# not supported in Innovus

# Flow
#-------------------------------------------------------------------------------
# Stop command execution on error
if { [get_db program_short_name] == "genus" } {
    set_db fail_on_error_mesg true
} elseif { [get_db program_short_name] == "innovus" } {
    set_db source_continue_on_error false       ;# default false
    set_db timing_continue_on_error false       ;# default false
}


# Control soft guide strength
# ---------------------------------------
if { [get_db program_short_name] == "innovus" } {
   set_db place_global_soft_guide_strength high
}

################################################################################
# ATTRIBUTES APPLIED BEFORE LOADING A LIBRARY OR DATABASE
################################################################################


## INNOVUS:
## --------
#set_library_unit -cap  1pf                  ;# default
#set_library_unit -time 1ns                  ;# default
## Sets the time and capacitance unit values for constraints and report generation.
#get_db timing_cap_unit
#get_db timing_time_unit
#get_capacitance_unit
#get_time_unit

# not supported: set_units -capacitance 1.0pf
# not supported: set_units -time 1.0ns



## Define the units for .sdc according to the unit definition in <CELL>_config.tcl:
## This is specific to innovus - TBD may need to define in flow_mmmc.tcl instead:
if { [get_db program_short_name] == "innovus" } {
    if { $::cfg_load_unit == "-picofarads" } {
        set_library_unit -cap 1pf
        imsg_id -id UFSM-151 -tl 6 -msg "flow_attribute.tcl: set capacitance_unit to [get_capacitance_unit]"
    } else {
        emsg_id_stop -id UEVM-100 -msg "flow_attribute.tcl: cfg_load_unit '$::cfg_load_unit' unkonwn"
    }
    if { $::cfg_time_unit == "-nanoseconds" } {
        set_library_unit -time 1ns
        imsg_id -id UFSM-151 -tl 6 -msg "flow_attribute.tcl: set time_unit to [get_time_unit]"
    } else {
        emsg_id_stop -id UEVM-100 -msg "flow_attribute.tcl: cfg_time_unit '$::cfg_time_unit' unkonwn"
    }
    imsg_id -id UFSM-151 -tl 6 -msg "flow_attribute.tcl: default resistance_unit is [get_resistance_unit]"
}


# General attributes  [get_db -category init]
#-------------------------------------------------------------------------------
#if {[info exists ::env(LSB_MAX_NUM_PROCESSORS)]} {
#  set_multi_cpu_usage -local_cpu  $::env(LSB_MAX_NUM_PROCESSORS)
#}

# extended CPU usage
if {[info exists ::env(LSB_MAX_NUM_PROCESSORS)]} {
  set_multi_cpu_usage -local_cpu $::env(LSB_MAX_NUM_PROCESSORS)
  if {[lsearch [list opt_signoff] [get_db [lindex [get_db flow_hier_path] end] .name]] >= 0} {
    set_multi_cpu_usage -remote_host $::env(LSB_MAX_NUM_PROCESSORS)
    set_distributed_hosts -local -shell_timeout 36000
  }
}


# update DB file check control
if { [lindex [split [get_db program_version] .] 0] >= 19 } {
	if [info exists ::env(INN_RESTORE_DB_FILE_CHECK)] {
	   if { $::env(INN_RESTORE_DB_FILE_CHECK) == 0 } {
		set_db read_db_file_check false
    		imsg_id -id UFSM-156 -tl 6 -msg "flow_attribute.tcl: disabling read_db_file_check"
	   } else {
		set_db read_db_file_check true
    		imsg_id -id UFSM-156 -tl 6 -msg "flow_attribute.tcl: enabling read_db_file_check"
	   }
	}	
} elseif { ![is_attribute -obj_type root read_db_file_check] } {
    define_attribute read_db_file_check -category flow -data_type bool  -obj_type root \
        -default_value 0 \
        -help_string "dummy attribute for backward compatibility, version 18.x or earlier"
}
 

# Number of CPU to use in RCP
#set_db opt_rcp_local_cpu 1             ;# default : 1


# HDL attributes [get_db *hdl*]                 NOT AVAILABE, SKIPPED for INNOVUS
# DFT attributes  [get_db *dft*]                NOT AVAILABE, SKIPPED for INNOVUS
# Optimization attributes  [get_db *netlist*]   DEFAULTS
# Optimization attributes  [get_db opt*]        DEFAULTS
# Optimization attributes  [get_db ccopt*]      DEFAULTS
# Datapath attributes  [get_db dp*]             NOT AVAILABE, SKIPPED for INNOVUS
# Physical Synthesis attributes  [get_db phy*]  NOT AVAILABE, SKIPPED for INNOVUS
# WRITE attributes [get_db write*]              DEFAULTS
# RETIME attributes [get_db *retime*]           DEFAULTS

# for setup of init settings after DB load, see: 'flow_step_init_innovus' in flow_step.tcl
