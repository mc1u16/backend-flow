Take row_driver_ctrl_pair as an example:

######################################################################
# TOOL GUI procedure
######################################################################

proc constructToolMainButton { tool {flowName ""} } {
    log::debug "[epc::get_timestamp] constructToolMainButton: run ..."


        ## Print debug level (to inform user and for the log):
        textMsg "INFO: Debug level: [set ::[set tool]DbgLevel]\n" $tool

NFO: Debug level: 9
INFO: Checking object directory...
INFO: Creating object directory...
INFO: Determining working directory name...
INFO: Creating working directory /usr/project-a/epc909/users/mcc/digital/dev/obj_ohc15l/blocks/row_driver_ctrl_top/row_driver_ctrl_pair/innovus_2020-03-02_3 on cli-1013 ...
INFO: Directory /usr/project-a/epc909/users/mcc/digital/dev/obj_ohc15l/blocks/row_driver_ctrl_top/row_driver_ctrl_pair/innovus_2020-03-02_3 has been created successfully.
INFO: Copying configuration files to working directory ...
INFO: Copy file '/usr/project-a/epc909/users/mcc/digital/dev/blocks/row_driver_ctrl_top/row_driver_ctrl_pair/be/ccopt_manual.spec' to cfg folder.
INFO: Copy file '/usr/project-a/epc909/users/mcc/digital/dev/blocks/row_driver_ctrl_top/row_driver_ctrl_pair/be/row_driver_ctrl_pair.cpf' to cfg folder.
INFO: Copy file '/usr/project-a/epc909/users/mcc/digital/dev/blocks/row_driver_ctrl_top/row_driver_ctrl_pair/be/row_driver_ctrl_pair_config.tcl' to cfg folder.
INFO: Copy file '/usr/project-a/epc909/users/mcc/digital/dev/blocks/row_driver_ctrl_top/row_driver_ctrl_pair/be/row_driver_ctrl_pair_func_main_constraints.sdc' to cfg folder.
INFO: Copy file '/usr/project-a/epc909/users/mcc/digital/dev/blocks/row_driver_ctrl_top/row_driver_ctrl_pair/be/row_driver_ctrl_pair_main_constraints.sdc' to cfg folder.
INFO: Copy file '/usr/project-a/epc909/users/mcc/digital/dev/blocks/row_driver_ctrl_top/row_driver_ctrl_pair/be/row_driver_ctrl_pair_test_constraints.sdc' to cfg folder.
INFO: Copy file '/usr/project-a/epc909/users/mcc/digital/dev/blocks/row_driver_ctrl_top/row_driver_ctrl_pair/be/innovus_scripts' to cfg folder.
INFO: Copying configuration files to working directory completed.
INFO: Updating soft link pointing to latest run directory...
INFO: Gathering flow information......
INFO: Do not load any database (flowLoad set to "")...
ssh -Y cli-1013
  
#####################################################
source /usr/project-a/epc909/users/mcc/.noshell_env.bash
env file
#####################################################

export DIGITAL_SCRIPTS_PATH=/usr/eda_tooling-a/tools/digital/scripts
export TECH_PATH=/usr/eda_tooling-a/pdk/epc/ohc15l_1v5/fdk_ohc15l_2v2
export IOLIB_PATH=/usr/eda_tooling-a/pdk/epc/ohc15l_1v5/io_ohc15l_4v1
export DBTYPE=oa
export STDCELL_PATH=/usr/eda_tooling-a/pdk/epc/ohc15l_1v5/stdcell_ohc15l_1v2r4
export CDS_Netlisting_Mode=Analog
export RETICLE_PATH=/usr/eda_tooling-a/pdk/epc/ohc15l_1v5/reticle_ohc15l_1v4
export EDIBASE=innovus-19.1-msi
export CDS_VERSION=6.1.8-tbu
export PROJECT=epc909
export SVN_BASE_URL=svn://svn.ch.epc
export TECHNAME=ohc15l
export PROJECTS_LOCATION=/usr/project-a
export USER=mcc
export PROJECT_DIR=epc909
export PROJECTGROUP=sh.dsgn.projects
export UMASK=027
export ANALOG_IP_PATH=/usr/eda_tooling-a/pdk/epc/ohc15l_1v5/analog_ip_1v0
export DIGITAL_IP_PATH=/usr/eda_tooling-a/pdk/epc/ohc15l_1v5/digital_ip_1v0
export EPC_RULEDECKS=/usr/eda_tooling-a/pdk/epc/ohc15l_1v5/ruledecks_ohc15l_3v0/pvs
export ETBASE=modus-19.1-msi
export MMSIMBASE=spectre-19.1-msi
export EEPROM_PATH=/usr/eda_tooling-a/pdk/epc/ohc15l_1v5/eeprom_ohc15l_1v2
export HAS_DIGITAL=digital_ip_1v0
export EXTBASE=ext-19.1-msi
export LVDS_PATH=/usr/eda_tooling-a/pdk/epc/ohc15l_1v5/lvds_ohc15l_1v0
export OPTICAL_PATH=/usr/eda_tooling-a/pdk/epc/ohc15l_1v5/optical_simplon_ohc15l_1v0
export RCBASE=genus-19.1-msi
export NOSHELL=true

########################################################
get flow information from gui.
export cell and flow information.
########################################################
  
cd /usr/project-a/epc909/users/mcc/digital/dev/obj_ohc15l/blocks/row_driver_ctrl_top/row_driver_ctrl_pair/innovus_2020-03-02_3
  
set initProjectShellCmdList {
    "source $::dotNoShellEnvFile"
    "cd [lindex [split [set ::[set tool]WorkDir] :] 1]"
    "export CELL_NAME=[file tail $::cellPath]"
    "export CELL_CFG_DIR=$::cellConfigDir"
    "export CELL_PATH=$::cellPath"
    "export CELL_PROJECT=$::env(PROJECT)"
    "export PROJECT=$::env(PROJECT)"
    "export TEMP_PATH=$::srcPath/templates"
    "export DBG_LEVEL=[set ::[set tool]DbgLevel]"
    "export DIS_MSG_LIMIT=[set ::innovusDisableMsgLimmit]"
    "export LC_ALL=en_US.UTF-8"
    "export LANG=en_US.UTF-8"
    "export EBE_BIN_PATH=$::srcPath"
    "export TERMINAL_DBG_LEVEL=$::terminalDebugLevel"
    "export IUSVERSION=$::env(IUSVERSION)"
    "export INN_RESTORE_DB_FILE_CHECK=[set ::innovusRestoreDbFileCheck]"
    "echo \"Display: '\\\$DISPLAY'\""
}

  
  export CELL_NAME=row_driver_ctrl_pair
  export CELL_CFG_DIR=be
  export CELL_PATH=/usr/project-a/epc909/users/mcc/digital/dev/blocks/row_driver_ctrl_top/row_driver_ctrl_pair
  export CELL_PROJECT=epc909
  export PROJECT=epc909
  export TEMP_PATH=/usr/eda_tooling-a/tools/digital/scripts/ebe/templates
  export DBG_LEVEL=9
  export DIS_MSG_LIMIT=0
  export LC_ALL=en_US.UTF-8
  export LANG=en_US.UTF-8
  export EBE_BIN_PATH=/usr/eda_tooling-a/tools/digital/scripts/ebe
  export TERMINAL_DBG_LEVEL=info
  export IUSVERSION=19.09-msi
  export INN_RESTORE_DB_FILE_CHECK=0
  echo "Display: '$DISPLAY'"
  ## this is from GUI
  export CELL_FLOW=init
  export CELL_FLOW_START=fp
  export CELL_FLOW_STOP=fp
  export CELL_FLOW_SUSPEND=
  export CELL_FLOW_START_DB=
  export CELL_FLOW_METRICS_EN=1
  export CDS_STYLUS_SOURCE_VERBOSE=0

###################################################  
EXECUTE The command . Processing -files option
###################################################

innovus -files /usr/project-a/epc909/users/mcc/digital/dev/blocks/row_driver_ctrl_top/row_driver_ctrl_pair/be/innovus_scripts/flow_run.tcl 
        -stylus 
        -lic_startup_options vdixl_capacity 
        -lic_startup vdixl 
        -disable_user_startup

Project-cadence env variables are being set...

Cadence Innovus(TM) Implementation System.
Copyright 2019 Cadence Design Systems, Inc. All rights reserved worldwide.
Version:	v19.11-s128_1, built Tue Aug 20 20:54:34 PDT 2019
Options:	-files /usr/project-a/epc909/users/mcc/digital/dev/blocks/row_driver_ctrl_top/row_driver_ctrl_pair/be/innovus_scripts/flow_run.tcl -stylus -lic_startup_options vdixl_capacity -lic_startup vdixl -disable_user_startup 
Date:		Mon Mar  2 20:03:11 2020
Host:		cli-1013.cn.epc (x86_64 w/Linux 2.6.32-754.14.2.el6.x86_64) (6cores*12cpus*Intel(R) Xeon(R) CPU E5-1650 0 @ 3.20GHz 12288KB)
OS:		CentOS release 6.10 (Final)
License:
		vdixl	Virtuoso Digital Implementation XL	19.1	checkout succeeded
		Maximum number of instances allowed (1 x 50000).
		Optional license vdixl_capacity "VDI-XL Block Capacity Option" 19.1 checkout succeeded.
		Maximum number of instances allowed: 300000.

Create and set the environment variable TMPDIR to /tmp/innovus_temp_9487_cli-1013.cn.epc_mcc_7aTP23.
Change the soft stacksize limit to 0.2%RAM (64 mbytes). Set global soft_stack_size_limit to change the value.
[INFO] Loading PVS 19.11 fill procedures

**INFO:  MMMC transition support version v31-84 

######################################################
@ Processing -files option
execute the flow_run.tcl:
######################################################


# MAIN FLOW RUN SCRIPT - executed when "Start" button is pressed.
# The same run script is used for:
#   * innovus (main)
#   * genus
#   * modus
#
# ORDER OF SCRIPT SOURCING:
# 1.  flow_run.tcl                      this script
#      define CELL_NAME and CELL_PATH
# 2.  eprocs (eg. epc_procs.tcl)        helper/utility procs
# 3.  flow_setup.tcl
# 4.  flow_procs.tcl
# 5.  flow_step.tcl
# 6.  <CELL>_config.tcl
# 7.  flow_attribute
# 8.  generate_cds_lib
# 9.  initialize_oa
# 10. flow_list [get_epc_flows]
# 11. Execute super-flow:
      foreach flow $flow_list {
        if { $flow == [get_db flow_step_suspend] } {
          suspend                 ; # script suspend DO NOT TOUCH THIS LINE!!!
        }
        run_epc_flow $flow
      }

@innovus 1> source /usr/project-a/epc909/users/mcc/digital/dev/blocks/row_driver_ctrl_top/row_driver_ctrl_pair/be/innovus_scripts/flow_run.tcl
Sourcing file '/usr/eda_tooling-a/tools/digital/scripts/ebe/../utils/epc_procs.tcl'
Sourcing file '/usr/project-a/epc909/users/mcc/digital/dev/blocks/row_driver_ctrl_top/row_driver_ctrl_pair/be/innovus_scripts/flow_setup.tcl' ...
Info: no dedicated user messages in Innovus available, skip 'define_msg'
Sourcing file '/usr/project-a/epc909/users/mcc/digital/dev/blocks/row_driver_ctrl_top/row_driver_ctrl_pair/be/innovus_scripts/flow_procs.tcl' ...
Sourcing file '/usr/project-a/epc909/users/mcc/digital/dev/blocks/row_driver_ctrl_top/row_driver_ctrl_pair/be/innovus_scripts/flow_step.tcl' ...
EPC INFO: Sourcing cell configuration file /usr/project-a/epc909/users/mcc/digital/dev/blocks/row_driver_ctrl_top/row_driver_ctrl_pair/be/row_driver_ctrl_pair_config.tcl ...
Sourcing file '/usr/project-a/epc909/users/mcc/digital/dev/blocks/row_driver_ctrl_top/row_driver_ctrl_pair/be/innovus_scripts/flow_attribute.tcl' ...
EPC INFO: UEVM-150 Set debug level to '9'



FLOW_RUN.TCL

################################################################################
## Flow Initialization
################################################################################

## Define flow:
if {![info exists ::env(CELL_FLOW)] } {
    set_db flow_current flow:std
} else {
    set_db flow_current flow:$::env(CELL_FLOW)
}

## Set start and stop sub-flows:
if { [info exist ::env(CELL_FLOW_START)] } {
    set_db flow_step_start $::env(CELL_FLOW_START)
    if { [info exist ::env(CELL_FLOW_STOP)] } {
        set_db flow_step_stop  $::env(CELL_FLOW_STOP)
    }
}

## Set start and stop sub-flows:
if { [info exist ::env(CELL_FLOW_SUSPEND)] } {
    set_db flow_step_suspend $::env(CELL_FLOW_SUSPEND)
}

## Set metric control:
if { [get_db program_short_name] != "modus" } {
    if { [info exist ::env(CELL_FLOW_METRICS_EN)] } {
        set_db flow_metrics_enable $::env(CELL_FLOW_METRICS_EN)
    }
}

## Generate local CDS library file in working directory on scratch:
#  only for innovus and genus
if { [get_db program_short_name] != "modus" } {
    generate_cds_lib
}

## OA-Mode Initialization (only for Innovus):
if { [get_db program_short_name] == "innovus" } {
    initialize_oa
}

## Create result directory:
if [is_attribute  -obj_type root  epc_flow_result_directory] {
    file mkdir [get_db epc_flow_result_directory]
}

## Determine sub-flows to be executed:
set flow_list [get_epc_flows] -- return a list of sub-flow. 


puts  [string repeat "\#" 80]
puts "\# FLOW:             [get_db flow_current]"
puts "\# SUB-FLOWS:        $flow_list"
puts "\# CELL NAME:        [get_db flow_cell_name]"        -- from flow_setup.tcl (env CELL_NAME)
puts "\# CELL PATH:        [get_db flow_cell_path]"        -- from flow_setup.tcl (env CELL_PATH)
puts "\# CELL CONF PATH:   [get_db flow_cell_cfg_path]"
puts "\# WORKING DIR:      [get_db flow_working_directory]"
puts  [string repeat "\#" 80]

run_epc_flow



set flow_list_init {
    fp
}

set flow_force_load_std { fp }

set flow_list_std {
    prects
    cts
    postcts
    route
    postroute
    finalize
}

set flow_step_list_fp {
    start_flow
    init_design
    init_floorplan
    power_route
    finish_flow
}

################################################################################
## Flow Procs: Returns a list of (sub-)flows.
################################################################################

#===========================
For example, normal mode fp:
#===========================
[get_db flow_current] flow:init
flow_list_name flow_list_init -- get from flow_setup.tcl. 
temp_flow_list flow_list_name. fp
set tmp_flow_start_index 0
set tmp_flow_stop_index  [llength $tmp_flow_list] 1

flow_step_start = fp.
flow_step_stop fp
tmp_flow_start_index 0
tmp_flow_stop_index 1

set flow_list [lrange $tmp_flow_list $tmp_flow_start_index $tmp_flow_stop_index]
flow_list [lrange fp 0 1] return fp. 

#===========================
For example, normal mode fp:
#===========================
[get_db flow_current] flow:std
flow_list_name flow_list_std
set flow_list_std {
    prects
    cts
    postcts
    route
    postroute
    finalize
}
temp_flow_list flow_list_name.

flow_step_start prects 
flow_step_stop finalize

set tmp_flow_start_index 0
set index [lsearch -exact $tmp_flow_list [get_db flow_step_stop]]
tmp_flow_stop_index 5
set flow_list [lrange $tmp_flow_list $tmp_flow_start_index $tmp_flow_stop_index]
flow_list [lrange std 0 5] return {prects cts postcts route postroute finalize}




foreach subflow {start stop} {
  set index [lsearch -exact $tmp_flow_list [get_db flow_step_[set subflow]]]
  set tmp_flow_[set subflow]_index $index


define_proc get_epc_flows -description "Returns a list of (sub-)flows." {
} {
    ## Get full flow:
    set flow_list_name flow_list_[string map {"flow:" ""} [get_db flow_current]]
    
    
    if { [info exist ::$flow_list_name] } {
        set tmp_flow_list [set ::$flow_list_name]
    } else {
        set tmp_flow_list ""; # e.g. when loading layout view of latest design just to have a look at it.
    }

    set tmp_flow_start_index 0
    set tmp_flow_stop_index  [llength $tmp_flow_list]
    if { [get_db flow_step_start] == "" && [get_db flow_step_stop] == "" } {
        ## Debug mode with loading of a database but without execution of sub-flows
        set flow_list {}
    } else {
        ## Normal mode with execution of selected sub-flows:
        foreach subflow {start stop} {
            if { [get_db flow_step_[set subflow]] != "" } {
                set index [lsearch -exact $tmp_flow_list [get_db flow_step_[set subflow]]]
                if { $index == -1 } {
                    ## Sub-flow not found
                    emsg_id_stop -id UFSM-102 -msg "Selected sub-flow '$subflow' not found."
                }
                set tmp_flow_[set subflow]_index $index
            }
        }
        ## Update list of (sub-)flows:

        set flow_list [lrange $tmp_flow_list $tmp_flow_start_index $tmp_flow_stop_index]
    }

    ## Load database:
    set database ""; # .db file (Genus) or OA view (Innovus)
    if [info exist ::env(CELL_FLOW_START_DB)] {
        if { $::env(CELL_FLOW_START_DB) != "" } {
            if { [get_db program_short_name] == "genus" } {
                set database [get_db flow_db_directory]/$::env(CELL_FLOW_START_DB)/$::env(CELL_FLOW_START_DB).db
            } elseif { [get_db program_short_name] == "innovus" } {
                set lib_name  [get_db flow_cell_name]_tmp_lib
                set cell_name [get_db flow_cell_name]
                set view_name $::env(CELL_FLOW_START_DB)
                set database [list $lib_name $cell_name $view_name]
            }
        }
    }

    if { $database != "" } {
        if { [get_db program_short_name] == "genus" } {
            if { ![file exists $database] } {
                emsg_id_stop -id USFM-100 -msg "Cannot find database '$database'"
            } else {
                imsg_id -id UFSM-151 -tl 5 -msg "Loading database '$database'..."
                read_db $database
            }
        } elseif { [get_db program_short_name] == "innovus" } {
            if { ![file exists [join "[get_db flow_db_directory] $database" /]] } {
                emsg_id_stop -id USFM-100 -msg "Cannot find OA library (with cell and view) '$database'."
            } else {
                imsg_id -id UFSM-151 -tl 5 -msg "get_epc_flows: Loading OA-database '$database'..."
                read_db -oa_lib_cell_view $database
            }
        }
    }

    ## Overwrite loaded database attributes with current ones such that they are matching to the current flow again:
    set_db flow_step_start         $::env(CELL_FLOW_START)
    set_db flow_step_stop          $::env(CELL_FLOW_STOP)
    set_db flow_current            flow:$::env(CELL_FLOW)
    set_db flow_cell_path          $::env(CELL_PATH)
    set_db flow_cell_cfg_path      [get_db flow_cell_path]/[get_db flow_cell_cfg_dir]
    set_db flow_working_directory  [string map [list "$::env(DIGITAL_WORK_DIR)" "$::env(OBJECTROOT)"] [get_db flow_cell_path]]

    return $flow_list

    
}

## Execute super-flow:
foreach flow $flow_list {
    if { $flow == [get_db flow_step_suspend] } {
        suspend                 ; # script suspend DO NOT TOUCH THIS LINE!!!
    }
    run_epc_flow $flow
}

## for example
## run_epc_flow fp. 


##############################################################################
## Flow Procs: public : Runs an epc_flow.
##############################################################################
define_proc run_epc_flow -description "Runs an epc_flow.\nNOTE: This command must not be confused with the Cadence command run_flow." {
    {flow "" {epc_flow} string required {epc_flow to be executed. Syntax: <my_flow[.my_flow_step]>}}
} {

    ## Make sure that hierarchical flow step depth is not too high:
    if { [llength [split $flow "."]] > 2 } {
        emsg_id -id UFSM-103 -msg "Hierarchical flow step depth is too high."
        return
    }
	
    ## Extract (sub-)flow and flow step.
    ## NOTE: A flow step is not required, i.e. $flow_step can be an empty string.
    set subflow   [lindex [split $flow "."] 0]
    set flow_step [lindex [split $flow "."] 1]

    ## Check whether a flow step is specified. If specified, check whether it is valid:
    if { $flow_step != "" } {
        if { [lsearch -exact [set ::flow_step_list_[set flow]] $flow_step] == -1 } {
            emsg_id -id UFSM-103 -msg "Flow step mismatch: '$flow_step' is not defined."
        }
    }

    ## Define flow:
    set_db epc_flow $subflow
    ## Print (sub-)flow:
    puts  [string repeat "\#" 80]
    puts "\# Starting flow [string toupper $subflow] ..."
    puts  [string repeat "\#" 80]

    ## Check whether a flow step is specified:
    if { $flow_step != "" } {
        ## Flow step specified -> execute only that one.
        set flow_steps $flow_step
    } else {
        ## Flow step not specified -> execute full set of flow steps defined for superordinate (sub-)flow.
        set flow_steps [set ::flow_step_list_[set subflow]]
    }

    # eval command and check for errors, report them with a trace
    foreach flow_step $flow_steps {

        puts "DEBUG flow_step: $flow_step"
        puts  [string repeat "\*" 80]
        puts "\* Starting flow step [string toupper $flow_step] ..."
        puts  [string repeat "\*" 80]

        set out [catch {eval flow_step_$flow_step} msg]

        if {$out} {
            #puts -id "UFSM-103" -msg "Flow step error in '$flow_step', error trace:"
            puts "EPC ERROR: Flow step error in '$flow_step', error trace:"
            puts "$::errorInfo"
            puts "EPC ERROR: CHECK ERROR ABOVE."
            return -code error "error during evaluation of 'flow_step_$flow_step'"
        }
    }
}

