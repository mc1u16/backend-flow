#################################################################################
## FLOW CUSTOMIZATIONS: DEFINE procedures
#################################################################################
##
##   ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
##   PROCEDURE NAME                       PARAMETERS                                       DESCRIPTION
##   ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
##   imsg_id                              -id -tl -msg                                     : prints information message with ID <id> if DEBUG LEVEL > <tl>
##   wmsg_id                              -id -tl -msg                                     : prints warning message with ID <id> if DEBUG LEVEL > <tl>
##   emsg_id                              -id     -msg                                     : prints error message with ID <id>
##   emsg_id_stop                         -id     -msg                                     : prints error message with ID <id>, terminates flow
##   generate_qrc_cmd_file                -arg_ptr -dat_ptr                                : Generates a command file for qrc extraction tool.
##   root_attribute_append                -attr -value                                     : Checks if abbtribute entry already exists, if not it is appended.
##   recursive_hdl_parser                 -ffile                                           : recursive .f file HDL parser
##                                                                                           Sets 3 root-attributes: flow_read_hdl_[pkg|vhdl|v]_list
##   eval_list_cmd                        -skip_design -cmd -arg_ptr -dat_ptr              : evaluate and execute a command parsing a list
##
##   eval_pin_place_cmd                   -dat_ptr                                         : Places ILM pins according to the pin placement specification (list of lists)
##                                                                                           stored in the configuration variable with the name given by -dat_ptr.
##   eval_cut_floorplan_cmd               -arg_ptr -dat_ptr                                : create irregular floorplan by cut sections
##   eval_IO_place_grid_cmd               -coor_tl -row/col_spaces -arg/data -check_io2io  : new grid based IO placement
##                                                                                           HINT: IO_cell_PO contains ALL IO's (update by local command)
##   eval_array_place_cmd (OLD)           -arg_ptr -dat_ptr                                : place array of leaf (place_inst) and hier module cells (fence, guide)
##   eval_place_instances                 -name -type -origin -onrail                      : refer to cfg_place_instances : leaf/hacro (place_inst), hinst (fence, guide)
##   eval_add_blockages                   -name -layer -box                                : add blockages
##   flow_step_action                     -start -flow -flow_step                          : adds flow step header, executes user defined commands
##   get_epc_flows                                                                         : Returns a list of (sub-)flows.
##   release_super_thread_lics                                                             : Reduces number of licenses in use checked out by super-threading to a single one.
##   print_superflow                                                                       : Prints the structure of the current super-flow.
##   generate_cds_lib                                                                      : Generates local CDS library file.
##   generate_cpf                         -cpf_out -add_lib                                : Parse template.cpf and create local version
##   initialize_oa                                                                         : Sets up OA mode for Innovus session.
##   run_epc_flow                                                                          : Runs an epc_flow.
##   get_absolute_path_list               -paths -files                                    : Create an absolute <path>/<file> stringlist
##   generate_hard_macro_files                                                             : Generates an abstract, liberty files and optionally an interface logic model (ILM).
##   copy_oa_view_to_server                                                                : Copies the final database from the local scratch to the server.
##   report_count_sequential              -max_level -min_ff                               : Reports the number of sequentiol elements
##
##   POWER ROUTING:
##   set_all_metals_and_vias_selection    -select -visible                                 : Set all wires and vias for selection.
##   select_wires_and_vias                -metals -vias                                    : Select dedicated wire and or via.
##   route_start_defaults                 none                                             : set a dedicated set routing start conditions.
##   route_end_defaults                   none                                             : Set a dedicated set routing end conditions.
##   interconnect_power_ring              -side -metal -rail -...                          : Cut a hole in the main power rails - version with MISSING LONGTITUDINAL CUT OF WIRES in innovus 18.x
##   (interconnect_power_ring_Xroute      -side -metal -rail -...                          : Cut a hole in the main power rails - version with MISSING LONGTITUDINAL CUT OF WIRES in innovus 18.x, wrong route direction)
##   pwr_ring_create                      -matrix -how2use                                 : Main power ring routing, based on a power matrix.

##   load_fp_xml_spr_file                 -fp_file                                         : load .spr data (from write_floorplan) by minimal generated fp_xml.spr_load file
##
##   myputs                               id string                                        : put to std_out or file
##   changeSelectWire2routeWire           -detail                                          : convert selected special wires to script
##   changeSelectWire2routeWireAddVia     -detail                                          : dito with the option for any offset and adding ortogonal VIAs
##   getResistOfSelectWires                                                                : get serial and paralell R values of all selected wires
##   parse_replace_absolute_user_path     -dir                                             : Parses all text files in a given directory (including sub-directories) for
##                                                                                           absolute user path settings and replace them with environment variables.


################################################################################
## Flow Procs: define user messages
################################################################################

## Creates sytem INFO message.
define_proc imsg_id {
    {id ""           -id  {string} {required} {ID string to be displayed}}
    {trigger_level 0 -tl  {int}    {optional} {Specifies message trigger level}}
    {msg ""          -msg {string} {required} {Message string to be displayed}}
} {
    if { [get_db flow_debug_level] >= $trigger_level } {
        ## Only Genus supports 'create_msg'.
        if { [get_db program_short_name] == "genus" } {
            create_msg $id $msg
        } else {
            puts "EPC INFO: $id $msg"
        }
    }
} -description "Information message procedure"

## Creates sytem WARNING message.
define_proc wmsg_id {
    {id ""           -id  {string} {required} {ID string to be displayed}}
    {trigger_level 0 -tl  {int}    {optional} {Specifies message trigger level}}
    {msg ""          -msg {string} {required} {Message string to be displayed}}
} {
    if { [get_db flow_debug_level] >= $trigger_level } {
        ## Only Genus supports 'create_msg'.
        if {[get_db program_short_name] == "genus"} {
            create_msg $id $msg
        } else {
            puts "EPC WARNING: $id $msg"
        }
    }
} -description "Warning message procedure"


## Creates sytem ERROR message.
define_proc emsg_id {
    {id ""  -id  {string} {required} {ID string to be displayed}}
    {msg "" -msg {string} {required} {Message string to be displayed}}
} {
    ## Only Genus supports 'create_msg'.
    if {[get_db program_short_name] == "genus"} {
        create_msg $id $msg
    } else {
        puts "EPC ERROR: $id $msg"
    }
} -description "Error message procedure"


## Creates sytem error message, prints error onto display and terminates.
define_proc emsg_id_stop {
    {id ""  -id  {string} {required} {ID string to be displayed}}
    {msg "" -msg {string} {required} {Message string to be displayed}}
} {
    ## Only Genus supports 'create_msg'.
    if {[get_db program_short_name] == "genus"} {
        create_msg $id $msg
    } else {
        puts "EPC ERROR: $id $msg"
    }
    puts stdout "\#\# EPC ERROR: $msg Returning ..."
    return -code -1 \
           -errorcode -1 \
           -errorinfo "ERROR: $msg" \
           "$id: ERROR: $msg"
#    exit -1
} -description "Error message procedure"

################################################################################
## Flow Procs: parse_replace_absolute_user_path
## The procedure parses a directory recursively for the occurance of absolute
## user paths. If found, the absolute user paths will be exchanged by a
## DIGITAL_WORK_DIR environment variable.
################################################################################

define_proc parse_replace_absolute_user_path {
    {dir  "" -dir  string required {define directory to parse all text files for absolte path settings}}
} {
    # check if directory exists
    if ![file exists $dir] {
        emsg_id -id USFM-101 -msg "Directory '$dir' does not exist. Parsing files skipped."
        return 0
    }

    # grep for absolte user path and replace, collect file list
    set searchPathDigital [subst $::env(DIGITAL_WORK_DIR)]
    set searchPathObject  [subst "/scratch/project-data/$::env(USER)/$::env(PROJECT)/digital/dev/obj_ohc15l"]
#    set searchPathObject  [subst $::env(OBJECTROOT)]
    set fileNameList      {}
    foreach elem {Digital Object} {
        if [catch [list exec grep -r [set searchPath$elem] $dir] resultList] {
            wmsg_id -id UFSM-120 -tl 6 -msg "No [string tolower $elem] dir files found by parser"
        } else {
            # extract file list
            foreach elem [split $resultList "\n"] {
                lappend fileNameList [lindex [split $elem :] 0]
            }
        }
    }

    # exchange path
    foreach fileName $fileNameList {
        if [file exists $fileName] {
            file copy -force -- $fileName $fileName.bak
            set ID [open $fileName.bak r]
            set OD [open $fileName w]
            while { [gets $ID line] >= 0 } {
                # search for absolute user digital path
                if { [string first "$searchPathDigital" $line] != -1 } {
                    set searchString "\{[set searchPathDigital]/(.*)\}"
                    regsub -all $searchString $line {[list $::env(DIGITAL_WORK_DIR)/\1]} line
                    regsub -all $searchPathDigital $line "\$::env\(DIGITAL_WORK_DIR\)" line
                }
                # search for absolute user object path
                if { [string first "$searchPathObject" $line] != -1 } {
                    regsub {\{/scratch/.*/dbs/(.*)\}} $line {[list $::env(PWD)/dbs/\1]} line
                    regsub {/scratch/.*/dbs/} $line "\$::env\(PWD\)/dbs/" line
                    }
                puts $OD $line
            }
            close $ID
            close $OD
#            file copy -force -- $fileName.server $fileName
        }

    }
    return $fileNameList

} -description "Parses all text files in a given directory (including sub-directories) for absolute user path settings and replace them with environment variables."

################################################################################
## Flow Procs: generate_qrc_cmd_file
## The procedure generates a new QRC command file which defines variables at the
## beginning followed by a template qrc command file. The new QRC command file
## can be executed by the Quantus tool to do standalone quantus QRC extraction.
################################################################################

define_proc generate_qrc_cmd_file \
    -description "Generates a new QRC command file which defines variables at the followed by a template qrc command file." {
} {
    ## Open the temporary file for reading:
    set file_name "qrc.cmd"
    imsg_id -id UFSM-151 -tl 6 -msg "generate_qrc_cmd_file: Opening new file '$file_name' for writing..."
    if [catch {set fh_qrc_file [open $file_name w]}] {
        puts stderr "Could not open $file_name for writing."
    }

    ## Generate technology library mapping file:
    set file_name_lib_mapping "techlib.defs"
    set fh_tech_file [open $file_name_lib_mapping w]

    #TODO: We copy the qrc_tech_file as long as we don't have the required folder structure in the PDK:
    ## Since only a typical QRC technology file is available in the PDK (status 2018-06-01)
    ## which is used for all RC corners in the MMMC setup, we can just use the
    ## QRC technology file from the typical corner:
    set techCornerPDK rc_typ;                                                        # The name reflects the fact that the PDK offfers only a typical QRC technology file.
    file mkdir $techCornerPDK;                                                       # Create a local directory for technology file.
    file copy [get_db [lindex [get_db rc_corners] 0] .qrc_tech_file] $techCornerPDK; # Since all RC corners use the same technology file, we can just take the first RC corner in the list.
    #    puts $fh_tech_file "DEFINE [get_db flow_tech_name] [get_db flow_tech_path]/ruledecks/LPE/"; #TODO: Needs to be adapted when PDK structure is updated.
    puts $fh_tech_file "DEFINE [get_db flow_tech_name] ."; #TODO: temporary
    close $fh_tech_file

    #TODO: And we need to create a local corner.defs file:
    set file_name_corner_defs "corner.defs"
    set fh_corner_defs [open $file_name_corner_defs w]

    foreach corner [get_db [get_db rc_corners] .name] {
        puts $fh_corner_defs "DEFINE $corner $techCornerPDK"
    }

    close $fh_corner_defs

    ## Add variable definitions at the beginning of the QRC command file:
    puts $fh_qrc_file "#-------------------------------------------------------------------------------"
    puts $fh_qrc_file "# Variable definitions written by procedure generate_qrc_cmd_file"
    puts $fh_qrc_file "#-------------------------------------------------------------------------------"
    puts $fh_qrc_file "set CELL                  [get_db flow_cell_name]"
    puts $fh_qrc_file "set TECH_LIB_MAPPING_FILE $file_name_lib_mapping"
    puts $fh_qrc_file "set TECH_NAME             [get_db flow_tech_name]"
    puts $fh_qrc_file "set TECH_CORNERS          \"[get_db [get_db rc_corners] .name]\""
    puts $fh_qrc_file "set CORNER_TEMPERATURES   \"[get_db [get_db rc_corners] .temperature]\""
    puts $fh_qrc_file "set LIB_DEF_FILE          cds.lib"
    puts $fh_qrc_file "set OUTPUT_DIR            [get_db epc_flow_result_directory]"
    puts $fh_qrc_file "set LOG_FILE              [get_db flow_report_directory]/[get_db flow_report_name]/[get_db flow_cell_name]_qrc.log"
    puts $fh_qrc_file ""

    ## Append the prepared template QRC file:
    set main_qrc_file [file join $::env(TEMP_PATH) qrc_main.cmd]
    imsg_id -id UFSM-151 -tl 6 -msg "Reading main QRC file '$main_qrc_file' for appending..."
    if [catch { set fh_main_qrc [open $main_qrc_file r] }] {
        puts stderr "Could not open $main_qrc_file for reading"
    }

    set content [read $fh_main_qrc]
    ## Write the prepared QRC file into the new QRC command file after the variables:
    puts $fh_qrc_file $content

    close $fh_main_qrc
    close $fh_qrc_file
}


################################################################################
## Flow Procs: public : Checks if abbtribute entry already exists, if not it is appended.
################################################################################

define_proc root_attribute_append  -description "Checks if abbtribute entry already exists, if not it is appended." {
    {attr        "" -attr        {string}  {required} {Attribute name}}
    {value       "" -value       {string}  {required} {Attribute value to be appended}}
} {
    ## Check if attribute exists:
    if { ![is_attribute -obj_type root $attr] } {
        emsg_id -d UFSM-102 -msg "root_attribute_append: Attribute '$attr' does not exist."
        return
    }
    ## Check if value is empty:
    if { $value == {} } {
        return
    }

    set attr_list [list [get_db $attr]]
    foreach elem $attr_list {
        puts "attr_list: '$elem'"
    }
    if { [string first $value [get_db $attr]] == -1 } {
        set_db -quiet $attr "[get_db $attr] $value"
        imsg_id -id UFSM-151 -tl 9 -msg "root_attribute_append: Value '$value' added to attribute '$attr'."
    } else {
        imsg_id -id UFSM-154 -tl 5 -msg "root_attribute_append: Duplicated value '$value' of attribute '$attr' not appended."
    }
}


################################################################################
## Flow Procs: define HDL parser
################################################################################
## TYPICAL .f file to parse                      COMMENT
##
## // global .f file sample                      [//.*] get fully removed before parsing
## -f ${DIGITAL_WORK_DIR}/path/file.f            further .f files to parse, 1st prio
## -f ${DIGITAL_WORK_DIR}/path/ilm.f             module .f files (ignore due to ignore_modules)
##                                               blank lines (ignore)
## -makelib ${OBJECTROOT}/path/lib               new lib definition
## -view rtl                                     RTL related lib start
## // main sources                               comment line (ignore)
## ${DIGITAL_WORK_DIR}/path/pkg.vhd              pkg for lib
## ${DIGITAL_WORK_DIR}/path/file.vhd             vhdl src
## ${DIGITAL_WORK_DIR}/path/conf.vhd             configuration (ignore due to ignore_files)
## ${DIGITAL_WORK_DIR}/path/ilm.vhd              modules src (ignore due to ignore_module)
## -f ${DIGITAL_WORK_DIR}/path/file.f            this will recursive call parser with "-curlib"
## -endlib                                       end lib section
##
## -makelib ${OBJECTROOT}/path/lib               new lib
## -view gate                                    section "gate", ignore all
## ...
## -endlib
##

# def_proc  name
#   {pram  def {show_param info} type [req.|opt.] {help}}
define_proc recursive_hdl_parser {
    {ffile  "" -ffile   {string} required {define the top-level .f file to parse trough source code}}
    {curlib "" -curlib  {string} optional {define a library for recursive handover }}
    } {

    # check for file existance, return after warning
    if ![file exists $ffile] {
        wmsg_id -id USFM-120 -msg "recursive_hdl_parser: Cannot find : '$ffile' - this part get skipped"
        return {}
    }

    # report potential current lib setting
    if { $curlib != "" } {
        imsg_id -id UHDL-152 -tl 9 -msg "recursive_hdl_parser: call with current lib: $curlib"
    } else {
        imsg_id -id UHDL-152 -tl 9 -msg "recursive_hdl_parser: no lib defined"
    }

    # open .f file for parsing
    set id [open $ffile r]
    # define all non supported views
    set skip_views "gate behav behavioral"
    set skip_view  "false"
    set skip_files "_conf.vhd"
    # NOTE: all files defined in src_exceptions in flow_attribute.tcl are ignored as well

    # set current_lib ""
    # line by line
    while { [gets $id line] >= 0 } {

        # Debug message
        imsg_id -id UHDL-152 -tl 9 -msg "recursive_hdl_parser: Performing line '$line' ..."

        # check for exception for synthesis
        set skip_line "false"
        foreach e [get_db flow_hdl_source_exclude] {
            if { [regexp [string map {$ \\$} $e] $line] } {
                set skip_line "true"
            }
        }
        # remove comment '//.*' from active line
        if { [regexp {//} $line] } {
            regsub {(.*)//+.*} $line {\1} line
            imsg_id -id UHDL-153 -tl 8 -msg "recursive_hdl_parser: found comment and remove: $line"
        }
        # old: set comment_pos [expr [string first "//" $line] -1]
        # old: if { $comment_pos >= 0 } {
        # old:     imsg_id -id UHDL-153 -tl 8 -msg "recursive_hdl_parser: remove comments"
        # old:     set line [string range $line 0 $comment_pos]
        # old: }
        # Trim potential spaces
        set line [string trimleft [string trimright $line]]

        # 0. skip empty lines
        if { [string length $line] == 0 } {
            imsg_id -id UHDL-153 -tl 8 -msg "recursive_hdl_parser: Skipped empty line"

        # 1. skip excluded lines
        } elseif { $skip_line == "true" } {
            imsg_id -id UHDL-153 -tl 5 -msg "recursive_hdl_parser: Skip line refer db attribute flow_hdl_source_exclude (default + cfg_hdl_source_exclude_list): $line"

        # 2. grep the library name on the "-makelib" indication
        } elseif { [string first "-makelib " $line] == 0 } {
            if { $curlib != "" } {
                # wmsg_id -id UHDL-121 -tl 0 -msg "recursive_hdl_parser: Found a single '-', please check $ffile"
                emsg_id -id UHDL-121 -msg "recursive_hdl_parser: current lib is $curlib, but new lib is defined: $line"
            } else {
                set curlib [file tail [string map [list "-makelib" "" " " ""] $line]]
                imsg_id -id UHDL-152 -tl 6 -msg "recursive_hdl_parser: Set current lib to '$curlib', "
            }

        # 3. grep the library name on the "-makelib" indication
        } elseif { [string first "-endlib " $line] == 0 } {
            set curlib ""
            set skip_view "false"
            imsg_id -id UHDL-152 -tl 6 -msg "recursive_hdl_parser: Got -endlib"

        # 4. detect any wrong views
        } elseif { [string first "-view" $line] == 0 } {
            foreach i $skip_views {
                if {$i == [string map [list "-view" "" " " ""] $line] } {
                    imsg_id -id UHDL-153 -tl 5 -msg "recursive_hdl_parser: View $i gets ignored."
                    set skip_view "true"
                }
            }

        # skip the current lines after any non supported views and "-endlib"
        } elseif { $skip_view == "true" } {
            imsg_id -id UHDL-153 -tl 8 -msg "    skipping: '$line'"

        # parse .f files
        } elseif { [string first "-f " $line] == 0 } {

            # remove "-f" and change environment variable index
            set line [string map [list "-f " ""] $line]
            set line [epc::expand_env_var -content $line -ignore_errors false]

            # recursive call
            imsg_id -id UHDL-152 -tl 5 -msg "recursive_hdl_parser: call -ffile $line -curlib $curlib"
            recursive_hdl_parser -ffile $line -curlib $curlib

        # process file after valid lib setting
        } elseif { $curlib != "" } {
            imsg_id -id UHDL-153 -tl 7 -msg "recursive_hdl_parser: parse $line for lib $curlib"

            set line [epc::expand_env_var -content $line -ignore_errors false]

            set skip_line "false"
            # do nothing when files to ignore
            # old: if { [lsearch -exact [split [get_db flow_hdl_source_exclude]] $line] != -1 } {}
            if { [regexp $line [get_db flow_hdl_source_exclude]] } {
                imsg_id -id UHDL-153 -tl 5 -msg "recursive_hdl_parser: Excluding '$line' due to exception"
                set skip_line "true"

            # check for configuration files
            } elseif { [string first "_conf.vhd" $line] != -1 } {
                foreach skip_file $skip_files {
                    if { [string first $skip_file [file tail [lindex [split $line] end]]] != -1 } {
                        set skip_line "true"
                        wmsg_id -id UHDL-120 -tl 5 -msg "Skipping HDL configuration source '$line'"
                        break
                    }
                }
            # check for tb packages
            } elseif { [string first "pkg_tb_" $line] != -1 && [string first ".vhd" $line] != -1 } {
                set skip_line "true"
                wmsg_id -id UHDL-120 -tl 5 -msg "Skipping testbench package '$line'"
            }

            # otherwise save
            if {$skip_line == "false"} {
                # change environment variable
                set line [epc::expand_env_var -content $line -ignore_errors false]

                # on package
                if { [string first "/pkg_" $line] >= 0 && [file extension $line] == ".vhd"} {
                    # add only if new argument does not exist
                    root_attribute_append -attr flow_read_hdl_pkg_list -value "$curlib $line"
                    # on vhdl
                } elseif { [file extension $line] == ".vhd" } {
                    # add only if new argument does not exist
                    root_attribute_append -attr flow_read_hdl_vhd_list -value $line
                # on verilog and verilog-ams
                } elseif { [file extension $line] == ".v" || [file extension $line] == ".vams"} {
                    # add only if new argument does not exist
                    root_attribute_append -attr flow_read_hdl_v_list -value $line
                } elseif { [string first "-endlib" $line] == 0 } {
                    set rtl_view "false"
                    imsg_id -id UHDL-150 -tl 5 -msg "recursive_hdl_parser: RTL view section ended"
                }
            }

            # on "-f" indicator ...
            # just a "-"
        } elseif { [string first "-" $line] >= 0 } {
            wmsg_id -id UHDL-121 -tl 0 -msg "recursive_hdl_parser: Found a single '-', please check $ffile"
        } else {
            wmsg_id -id UHDL-121 -tl 0 -msg "recursive_hdl_parser: This line could not be parsed: $line"
        }
    }
    close $id
    imsg_id -id UHDL-150 -tl 5 -msg "File $ffile closed"
    return {}

} -description "Recursive parser for HDL sources"

################################################################################
## Flow Procs: command evaluation by lists
################################################################################
# HINT: cmd itself could be extracted on the call because the list looks like:
#       <command>_arg_list
#       <command>_data_list
#

define_proc eval_list_cmd {
    {skip_design   0  -skip_design   {integer} {optional} {Skips asserting "-design <cell_name>" attribute if set greater 0}}
    {cmd           "" -cmd           {string}  {required} {Command string to be evaluated}}
    {arg_ptr       "" -arg_ptr       {string}  {required} {Command argument list pointer, <arg> is not added itself}}
    {dat_ptr       "" -dat_ptr       {string}  {required} {Command data list pointer: empty list or "-" are skipped}}
} {
    # check if list are existing
    if ![info exists ::[set arg_ptr]] {
        emsg_id_stop -id UFSM-101 -msg "eval_list_cmd ($cmd): Argument list is not as expected."
    }
    set arg_list [set ::[set arg_ptr]]
    if ![info exists ::[set dat_ptr]] {
        emsg_id_stop -id UFSM-101 -msg "eval_list_cmd ($cmd): Data list is not as expected."
    }
    set dat_list [set ::[set dat_ptr]]
    # check if list lengths are matching
    if { [llength $arg_list] != [llength [lindex $dat_list 0]] } {
        emsg_id_stop -id UFSM-101 -msg "eval_list_cmd ($cmd): List length of arguments and data does not match."
    }
    # check if list lengths is greater than 0
    if { [llength $arg_list] < 1 | [llength $dat_list] < 1 } {
        emsg_id_stop -id UFSM-101 -msg "eval_list_cmd ($cmd): List length of arguments and/or data do not match."
    }
    # construct command
    foreach dat_sub_list $dat_list {
        if { $skip_design == 0 } {
            set eval_cmd "$cmd -design [get_db flow_cell_name]"
        } else {
            set eval_cmd $cmd
        }
        for { set i 0 } { $i < [llength $arg_list] } { incr i } {
            # check if argument shall be skipped: when data is {-} or {}
            if { [lindex $dat_sub_list $i] != "-" && [lindex $dat_sub_list $i] != "" } {
# osolete when using <arg>  # check if pin argument shall be skipped: if argument is "pin"
# osolete when using <arg>  if { [lindex $arg_list $i] == "pin" } {                                             }
# osolete when using <arg>      append eval_cmd " [lindex $dat_sub_list $i]"
                # a '-' argument will use 'data' as '-data'
                if { [lindex $arg_list $i] == "-" } {
                    append eval_cmd " -[lindex $dat_sub_list $i]"
                # a '<arg>' argument has just informal character and just the data get added without argument
                } elseif { [string index [lindex $arg_list $i] 0] == "<" && [string index [lindex $arg_list $i] end] == ">" } {
                    append eval_cmd " [lindex $dat_sub_list $i]"
                # normal setting "-" + argument + data
                } else {
                    append eval_cmd " -[lindex $arg_list $i] [lindex $dat_sub_list $i]"
                }
            }
        }
        imsg_id -id UFSM-151 -tl 7 -msg "eval_list_cmd: evaluating cmd '$eval_cmd'"
        eval $eval_cmd
    }
}

################################################################################
## Flow Procs: command for pin placment by lists
################################################################################
##
## The procedure will generate a temporary pin list file that can be used to assign pins.
## The data list consists of several groups, each group can use different fields
## accoridng to the assignment requirments. Basically, we have
## '-pins','-edge_index','-layer_index','-start_position','-pin_offset','-pin_width'.
## The group distance and group range can also be set using optional fields such as '-group_offset' and '-group_range'.
##
## The procedure loops through every group and evaluates the field values.
## And then for each group, constructs the edit_pin command for the pins in the group.
##
## The procedure supports the abbreviation of pin names as below:
## 'a..z_[0:1]_a..z' range of indices,      e.g. cmp_ph[3:1]_SO                  --> places: {cmp_ph3_SO, cmp_ph2_SO, cmp_ph1_SO}
## 'a..z_[3:0]'      range of bus indices,  e.g. sat_sampled_DO[0:1]             --> places: {sat_sampled_DO[0], sat_sampled_DO[1]}
## 'a..z[]'          assign pins in groups, e.g. txa[],txb[],group_range=[384:1] --> places: {txa[1],txb[1]},{txa[2],txb[2]},{txa[384},txb[384]}, the group distance is set by group_offset.
##
## The structure of the procedure is as follows:
##    ## no special form
##    if { $special_1 == 0 && $special_2 == 0 && $special_3 == 0 } {
##
##    ## special case 1: my_signal_[x:y]_DI   my_signal_x_DI, ..., my_signal_y_DI
##    } else if {$special_1 == 1 &&  $special_2 == 0 && $special_3 == 0} {
##
##    ## special case 2: my_bus_DI[m:n]       my_bus_DI[m],   ..., my_bus_DI[n]
##    } else if {$special_1 == 0 &&  $special_2 == 1 && $special_3 == 0} {
##
##    ## combination of special case 1 and 2: my_bus_[x:y]_DI[m:n]
##    } else if {$special_1 == 1 &&  $special_2 == 1 && $special_3 == 0} {
##
##    ## distribute buses:   my_bus_x_DI[m], ...  my_bus_y_DI[m], ... my_bus_x_DI[n], ... my_bus_y_DI[n]
##      if { $distribute_bus == 1} {
##      }
##    ## my_bus_x_DI[m], ...  my_bus_x_DI[n], ... my_bus_y_DI[m], ... my_bus_y_DI[n]
##      else {
##      }
##
##    ## my_bus_DI[]
##    } else if {$special_3 == 1} {
##
##    ## my_bus_[x:y]_DI[]
##      if {$special_2 == 1} {
##      } else {
##      }
##    }
##

define_proc eval_pin_place_cmd {
    {dat_ptr       "" -dat_ptr    {string}  {required} {name of the configuration variable storing the pin placement specification (list of lists)}}
} {
    ## Check if cfg_pin_placement data are existing:
    if ![info exists ::[set dat_ptr]] {
        emsg_id_stop -id UFSM-101 -msg "eval_pin_place_cmd: Data list '-dat_ptr' is missing."
    }
    set dat_list [set ::[set dat_ptr]]

    ## Open the pin placement file to be created:
    set filename [get_db flow_cell_name]_pin.tcl

    imsg_id -id UFSM-151 -tl 7 -msg "eval_pin_place_cmd: Creating pin placement file '$filename' ..."

    if [catch {set file_id [open $filename w]}] {
        emsg_id_stop -id UFSM-101 -msg "eval_pin_place_cmd: Could not open $filename for writing."
    }
    ## Determine the number of groups in the data list:
    set nof_groups [llength $dat_list]

    ## loop over all groups in the list
    for {set group_index 0} {$group_index < $nof_groups} {set group_index [expr $group_index + 1]} {
        # set the variables for every group
        set group       [lindex $dat_list $group_index]
        # search the available field in the group and assign its data
        # to the variable.

        ## @@@TBD: (update) could be heavily improved with "dict"
        ## sample:
        ##     set info [list -pin A -layer {MET1 TMET} -offset 20 ]
        ##     dict with info {}
        ##     puts "   check pin:   [dict get $info -pin]"
        ##     puts "   on layer:    [dict get $info -layer]"
        ##     puts "   with offset: [dict get $info -offset]"
        ## hint: a liste element like '{ name }' will taken as ' name '
        ##       -> need: [string trim [dict get $var index]]

        if {[set index [lsearch $group -pins]]>=0} {
            set pins [lindex $group [expr $index +1]]
        } else { emsg_id_stop -id UFSM-101 -msg "eval_pin_place_cmd: The pins is not available in the group" }
        set nof_pins_in_group [llength $pins]

        if {[set index [lsearch $group -edge_index]]>=0} {
            set pin_edge [lindex $group [expr $index +1]]
        } else { emsg_id_stop -id UFSM-101 -msg "eval_pin_place_cmd: The edge_index value is not available in the group" }

        if {[set index [lsearch $group -layer_index]]>=0} {
            set pin_layer [lindex $group [expr $index +1]]
        } else { emsg_id_stop -id UFSM-101 -msg "eval_pin_place_cmd: The layer_index value is not available in the group" }

        if {[set index [lsearch $group -start_position]]>=0} {
            set start [lindex $group [expr $index +1]]
        } else { emsg_id_stop -id UFSM-101 -msg "eval_pin_place_cmd: The start_position value is not available in the group" }

        if {[set index [lsearch $group -pin_offset]]>=0} {
            set offset [lindex $group [expr $index +1]]
        } else { emsg_id_stop -id UFSM-101 -msg "eval_pin_place_cmd: The pin_offset value is not available in the group" }


        if {[set index [lsearch $group -pin_width]]>=0} {
            set pin_width [lindex $group [expr $index +1]]
        } else { emsg_id_stop -id UFSM-101 -msg "eval_pin_place_cmd: The pin_width value is not available in the group"}


        ## TBD: need to be tested
        if { [lsearch $group -distribute_indexed_buses] >= 0} {
            set distribute_indexed_buses "true"
        } else {
            set distribute_indexed_buses "false"
        }

        imsg_id -id UFSM-151 -tl 9 -msg "eval_pin_place_cmd: --- NEW GROUP $group_index --- abs start: x=[lindex $start 0], y=[lindex $start 1], layer=$pin_layer, side=$pin_edge, distribute_bus=$distribute_indexed_buses"

        if {[set index [lsearch $group -group_range]]>=0} {
            if {$distribute_indexed_buses} {
                emsg_id_stop -id UFSM-101 -msg "eval_pin_place_cmd: The group_range can't be used with '-distribute_indexed_buses'"
            } else {
                set group_range [lindex $group [expr $index + 1]]
            }
        } else {
            ## without defining a group, set range for a single loop
            set group_range {0:0}
        }

        ## set the pin_iteration variable, the group_range means that
        ## how many iteration times of the pin group.
        ## otherwise, we only need to do once.
        regexp {(\d*)(\:)(\d*)} $group_range match range_left sub2 range_right
        set pin_iteration [expr abs($range_left - $range_right) + 1]
        imsg_id -id UFSM-151 -tl 9 -msg "eval_pin_place_cmd: group range is set to $range_left : $range_right (total $pin_iteration groups)"

        if {[set index [lsearch $group -group_offset]]>=0} {
            if {$distribute_indexed_buses} {
                emsg_id_stop -id UFSM-101 -msg "eval_pin_place_cmd: The group_offset can't be used with '-distribute_indexed_buses'"
            } else {
                ## valid group_offset is saved
                set group_offset [lindex $group [expr $index +1]]
                if { ![string is integer $group_offset] && ![string is double $group_offset] } {
                    if { $group_offset == "continuous" } {
            set group_offset 0
                    } else {
                        emsg_id_stop -id UFSM-101 -msg "eval_pin_place_cmd: The group_offset '$group_offset' is unknown. Nothing or number or 'continuous' are allowed"
                    }
                }
                imsg_id -id UFSM-151 -tl 9 -msg "eval_pin_place_cmd: group_offset is $group_offset"
            }
        } else {
            ## Without any group offset, a continuous pin placement shall be defined (same like 'continuous')
            ## But this requires the full pin count in the group. This info will be available after first group is placed.
            set group_offset 0
            imsg_id -id UFSM-151 -tl 9 -msg "eval_pin_place_cmd: continuous group_offset for special_3 case"
        }

        for {set iteration_times 0} {$iteration_times < $pin_iteration} {incr iteration_times} {

            set pin_nof_special_1 0
            set pin_nof_special_2 0
            set pin_total_special 0
            set pins_placed_of_group 0

            imsg_id -id UFSM-151 -tl 9 -msg "eval_pin_place_cmd: loop over $nof_pins_in_group pin name, bus-group counter: $iteration_times (of range $range_left : $range_right)"
            for {set pin_index 0} {$pin_index < $nof_pins_in_group} {set pin_index [expr $pin_index + 1]} {
                set pin_name  [lindex $dat_list $group_index 1 $pin_index]
                # using regural expression to find the special case,
                # special_1 a..z[3:1]_,special_2 [3:1],special_3 a..z[] and extract the number, the regural expression
                set special_1 [regexp {(\[)(\d*)(\:)(\d*)(\])(\_)} $pin_name match sub1 range1from sub3 range1to sub5]
                set special_2 [regexp {(\[)(\d*)(\:)(\d*)(\])$} $pin_name match_2 sub_1 range2from sub_3 range2to sub_5]
                set special_3 [regexp {(\[)(\])} $pin_name match_3]
                set range_max 1
                imsg_id -id UFSM-151 -tl 9 -msg "eval_pin_place_cmd: pin: $pin_name, special_1: $special_1, special_2: $special_2, special_3: $special_3"

                # check for multi ranges - not supported
                if { ($special_1 == 1 &&  $special_2 == 1 && $special_3 == 1 ) || $special_1 > 1 || $special_2 > 1 || $special_3 > 1} {
                    emsg_id_stop -id UFSM-101 -msg "eval_pin_place_cmd: 'regexp' found multiple ranges in one signal: $pin_name. This is not supported."
                }

                # when the pin name is normal without any speicial cases

                if { $special_1 == 0 &&  $special_2 == 0 && $special_3 == 0 } {
                    imsg_id -id UFSM-151 -tl 9 -msg "eval_pin_place_cmd: pin: $pin_name no special format"

                    if {[expr fmod($pin_edge, 2)] == 0.0} {
                      set pos_x  [lindex $start 0]
                      set pos_y  [format "\[expr %s \+  %s \* %s\]" [lindex $start 1] $offset [expr $pin_index + $pin_total_special]]
                    } else {
                      set pos_x  [format "\[expr %s \+  %s \* %s\]" [lindex $start 0] $offset [expr $pin_index + $pin_total_special]]
                      set pos_y  [lindex $start 1]
                    }

                    # write the edit_pin command to the file.
                    puts $file_id [format "edit_pin -pin %s\t -layer %s -fixed_pin 1 -snap mgrid -edge %s -pin_width %s -assign %s\t %s \t\;\# x=%.2f y=%.2f" $pin_name $pin_layer $pin_edge $pin_width $pos_x $pos_y [subst $pos_x] [subst $pos_y]]
                    incr pins_placed_of_group

                    # when the pin_name using the special_1 form: [n:m]

                } elseif { $special_1 == 1 &&  $special_2 == 0 && $special_3 == 0 } {
                    imsg_id -id UFSM-151 -tl 9 -msg "eval_pin_place_cmd: pin: $pin_name format special_1 (module), index: $range1from .. $range1to"

                    # absolute value to define the range
                    set range [expr abs($range1from - $range1to) + 1]

                    # Loop over range start with '0' for placement
                    for {set i 0} {$i < $range } {incr i} {
                        # get the pin position refer to edge
                        # even edge number --> vertically
                        if {[expr $pin_edge % 2] == 0} {
                            set pos_x  [lindex $start 0]
                            # direct: set pos_y  [expr [lindex $start 1] + $offset * ($pin_index + $i + $pin_total_special)]
                            set pos_y  [format "\[expr %s \+  %s \* %s\]" [lindex $start 1] $offset [expr $pin_index + $i + $pin_total_special]]
                            # odd edge number --> horizontally
                        } else {
                            # direct: set pos_x  [expr [lindex $start 0] + $offset * ($pin_index + $i + $pin_total_special)]
                            set pos_x  [format "\[expr %s \+  %s \* %s\]" [lindex $start 0] $offset [expr $pin_index + $i + $pin_total_special]]
                            set pos_y  [lindex $start 1]
                        }
                        # refer to the given range, need to get the correct index for nameing
                        # special case of [n..n] error, force to correct use
                        if { $range1from == $range1to } {
                            emsg_id_stop -id UFSM-101 -msg "eval_pin_place_cmd: The range '$range1from..$range1to' is not valid. Use without '[]' or [$range1from]' for single bus signal."
                            # case of increment [[n..(n+m)]]
                        } elseif { $range1from < $range1to } {
                            set pin_name_special [regsub {\[\d*\:+\d*\]} $pin_name  [expr $i + $range1from]]
                            # otherwise it's decrement [[(n+m)..n]]
                        } else {
                            set pin_name_special [regsub {\[\d*\:+\d*\]} $pin_name  [expr $range1from - $i]]
                        }
                        # finally write edit_pin to file
                        puts $file_id [format "edit_pin -pin %s\t\t -layer %s -fixed_pin 1 -snap mgrid -edge %s -pin_width %s -assign %s\t %s \t\;\# x=%.2f y=%.2f" $pin_name_special $pin_layer $pin_edge $pin_width $pos_x $pos_y [subst $pos_x] [subst $pos_y]]
                        incr pins_placed_of_group
                    }
                    # get the summary for further groups
                    set pin_nof_special_1 [expr $range - 1]
                    set pin_total_special [expr  $pin_nof_special_1 +  $pin_total_special ]

                    # when the pin_name using the special_2 form: [n:m]

                } elseif {  $special_2 == 1 &&  $special_1 == 0 && $special_3 == 0 } {
                    imsg_id -id UFSM-151 -tl 9 -msg "eval_pin_place_cmd: pin: $pin_name format special_2 (bus), index: $range2from .. $range2to"

                    # absolute value to define the range
                    set range [expr abs($range2from - $range2to) + 1]

                    # Loop over range start with '0' for placement
                    for {set i 0} {$i < $range } {incr i} {
                        # get the pin position refer to edge
                        # even edge number --> vertically
                        if {[expr $pin_edge % 2] == 0} {
                            set pos_x  [lindex $start 0]
                            # direct: set pos_y  [expr [lindex $start 1] + $offset * ($pin_index + $i + $pin_total_special)]
                            set pos_y  [format "\[expr %s \+  %s \* %s\]" [lindex $start 1] $offset [expr $pin_index + $i + $pin_total_special]]
                            # odd edge number --> horizontally
                        } else {
                            # direct: set pos_x  [expr [lindex $start 0] + $offset * ($pin_index + $i + $pin_total_special)]
                            set pos_x  [format "\[expr %s \+  %s \* %s\]" [lindex $start 0] $offset [expr $pin_index + $i + $pin_total_special]]
                            set pos_y  [lindex $start 1]
                        }
                        # refer to the given range, need to get the correct index for nameing
                        # special case of [[n..n]] error, force to correct use
                        if { $range2from == $range2to } {
                            emsg_id_stop -id UFSM-101 -msg "eval_pin_place_cmd: The range '$range2from..$range2to' is not valid. Use without '[]' or [$range2from]' for single bus signal."
                            # case of increment [[n..(n+m)]]
                        } elseif { $range2from < $range2to } {
                            set pin_name_special [regsub {\[\d*\:+\d*\]} $pin_name  \[[expr $i + $range2from]\]]
                            # otherwise it's decrement [[(n+m)..n]]
                        } else {
                            set pin_name_special [regsub {\[\d*\:+\d*\]} $pin_name \[[expr $range2from - $i]\]]
                        }
                        # finally write edit_pin to file
                        puts $file_id [format "edit_pin -pin %s\t\t -layer %s -fixed_pin 1 -snap mgrid -edge %s -pin_width %s -assign %s\t %s \t\;\# x=%.2f y=%.2f" $pin_name_special $pin_layer $pin_edge $pin_width $pos_x $pos_y [subst $pos_x] [subst $pos_y]]
                        incr pins_placed_of_group
                    }
                    # get the summary for further groups
                    set pin_nof_special_2 [expr $range - 1]
                    set pin_total_special [expr  $pin_nof_special_2 +  $pin_total_special ]

                    # when the pin_name using the combination of special_1 and special_2
                    # form a..z[n:m]_a..z[n:m]

                } elseif { $special_1 == 1 &&  $special_2 == 1 && $special_3 == 0} {
                    imsg_id -id UFSM-151 -tl 9 -msg "eval_pin_place_cmd: pin: $pin_name format special_1 (module bus $range1from : $range1to) and special_2 (bus $range2from : $range2to)"
                    # absolute value to define the range

                    set range1 [expr abs($range1from - $range1to) + 1]
                    set range2 [expr abs($range2from - $range2to) + 1]

                    ## distribute_indexed_buses decides the order of nested loop: distribute = a[n] b[n] a[m] b[m] ...
                    if {$distribute_indexed_buses} {
                        ## --> loop special_2 placement
                        ##     --> special_1 placement

                        for {set i 0} {$i < $range2 } {incr i} {
                            if { $range2from == $range2to } {
                                emsg_id_stop -id UFSM-101 -msg "eval_pin_place_cmd: The range '$range2from..$range2to' is not valid. Use without '[]' or [$range2from]' for single bus signal."
                                # case of increment [[n..(n+m)]]
                            } elseif { $range2from < $range2to } {
                                set pin_name_special_1 [regsub {\[\d*\:+\d*\]$} $pin_name \[[expr $i + $range2from]\]]
                                # otherwise it's decrement [[(n+m)..n]]
                            } else {
                                set pin_name_special_1 [regsub {\[\d*\:+\d*\]$} $pin_name \[[expr $range2from - $i]\]]
                            }
                            for {set j 0} {$j < $range1 } {incr j} {
                                # get the pin position refer to edge
                                # even edge number --> vertically
                                if {[expr $pin_edge % 2] == 0} {
                                    set pos_x  [lindex $start 0]
                                    # direct: set pos_y  [expr [lindex $start 1] + $offset * ($pin_index + $i + $pin_total_special)]
                                    set pos_y  [format "\[expr %s \+  %s \* %s\]" [lindex $start 1] $offset [expr $pin_index + $i*$range1+$j + $pin_total_special]]
                                    # odd edge number --> horizontally
                                } else {
                                    # direct: set pos_x  [expr [lindex $start 0] + $offset * ($pin_index + $i + $pin_total_special)]
                                    set pos_x  [format "\[expr %s \+  %s \* %s\]" [lindex $start 0] $offset [expr $pin_index + $i*$range1+$j + $pin_total_special]]
                                    set pos_y  [lindex $start 1]
                                }

                                if { $range1from == $range1to } {
                                    emsg_id_stop -id UFSM-101 -msg "eval_pin_place_cmd: The range '$range1from..$range1to' is not valid. Use without '[]' or [$range1from]' for single bus signal."
                                    # case of increment [[n..(n+m)]]
                                } elseif { $range1from < $range1to } {
                                    set pin_name_special [regsub  {\[\d*\:\d*\]} $pin_name_special_1 [expr $j + $range1from]]
                                    # otherwise it's decrement [[(n+m)..n]]
                                } else {
                                    set pin_name_special [regsub {\[\d*\:\d*\]} $pin_name_special_1 [expr $range1from - $j]]
                                }

                                # finally write edit_pin to file
                                puts $file_id [format "edit_pin -pin %s\t\t -layer %s -fixed_pin 1 -snap mgrid -edge %s -pin_width %s -assign %s\t %s \t\;\# x=%.2f y=%.2f" $pin_name_special $pin_layer $pin_edge $pin_width $pos_x $pos_y [subst $pos_x] [subst $pos_y]]
                                incr pins_placed_of_group
                            }
                        }
                        ## distribute_bus decides the order of nested loop: not distribute = a[n] ... a[m] b[n] ... b[m]
                    } else {
                        for {set i 0} {$i < $range1 } {incr i} {
                            if { $range1from == $range1to } {
                                emsg_id_stop -id UFSM-101 -msg "eval_pin_place_cmd: The range '$range1from..$range1to' is not valid. Use without '[]' or [$range1from]' for single bus signal."
                                # case of increment [[n..(n+m)]]
                            } elseif { $range1from < $range1to } {
                                set pin_name_special_1 [regsub {\[\d*\:+\d*\]} $pin_name [expr $i + $range1from]]
                                # otherwise it's decrement [[(n+m)..n]]
                            } else {
                                set pin_name_special_1 [regsub {\[\d*\:+\d*\]} $pin_name [expr $range1from - $i]]
                            }
                            for {set j 0} {$j < $range2 } {incr j} {

                                # get the pin position refer to edge
                                # even edge number --> vertically
                                if {[expr $pin_edge % 2] == 0} {
                                    set pos_x  [lindex $start 0]
                                    # direct: set pos_y  [expr [lindex $start 1] + $offset * ($pin_index + $i + $pin_total_special)]
                                    set pos_y  [format "\[expr %s \+  %s \* %s\]" [lindex $start 1] $offset [expr $pin_index + $i*$range1+$j + $pin_total_special]]
                                    # odd edge number --> horizontally
                                } else {
                                    # direct: set pos_x  [expr [lindex $start 0] + $offset * ($pin_index + $i + $pin_total_special)]
                                    set pos_x  [format "\[expr %s \+  %s \* %s\]" [lindex $start 0] $offset [expr $pin_index + $i*$range1+$j + $pin_total_special]]
                                    set pos_y  [lindex $start 1]
                                }

                                if { $range2from == $range2to } {
                                    emsg_id_stop -id UFSM-101 -msg "eval_pin_place_cmd: The range '$range2from..$range2to' is not valid. Use without '[]' or [$range2from]' for single bus signal."
                                    # case of increment [[n..(n+m)]]
                                } elseif { $range2from < $range2to } {
                                    set pin_name_special [regsub  {\[\d*\:\d*\]$} $pin_name_special_1 \[[expr $j + $range2from]\]]
                                    # otherwise it's decrement [[(n+m)..n]]
                                } else {
                                    set pin_name_special [regsub {\[\d*\:\d*\]$} $pin_name_special_1 \[[expr $range2from - $j]\]]
                                }

                                # finally write edit_pin to file
                                puts $file_id [format "edit_pin -pin %s\t\t -layer %s -fixed_pin 1 -snap mgrid -edge %s -pin_width %s -assign %s\t %s \t\;\# x=%.2f y=%.2f" $pin_name_special $pin_layer $pin_edge $pin_width $pos_x $pos_y [subst $pos_x] [subst $pos_y]]
                                incr pins_placed_of_group
                            }
                        }
                    }
                    # get the summary for further groups
                    set pin_nof_special_2 [expr $range1*$range2 - 1]
                    set pin_total_special [expr  $pin_nof_special_2 +  $pin_total_special ]

                    ## when the pins uses special_3 case, repeatedly assign the pins in the group

                } elseif {$special_3 == 1 && $special_2 == 0} {
                    #fill the blank [] with the correct index refer to the group_range
                    if {$range_left < $range_right} {
                        set pin_name [ regsub {\[\]} $pin_name \[[expr $range_left + $iteration_times]\]]
                    } else {
                           set pin_name [ regsub {\[\]} $pin_name \[[expr $range_left - $iteration_times]\]]      ;# new hopfully correct version
                    }
                    imsg_id -id UFSM-151 -tl 9 -msg "eval_pin_place_cmd: pin: $pin_name format special_3 (place in group) "

                    ## simple case in special_3
                    if {$special_1 == 0} {

                        # according to
                        if {[expr $pin_edge % 2] == 0} {
                            # even (vertical) edge
                            set pos_x  [lindex $start 0]
                            set pos_y  [format "\[expr %s \+ %s \* %s\]" [expr [lindex $start 1]+$group_offset*$iteration_times] $offset [expr $pin_index +  $pin_total_special ]]
                        } else {
                            # odd (horizontal) edge
                            set pos_x  [format "\[expr %s \+ %s \* %s\]" [expr [lindex $start 0]+$group_offset*$iteration_times] $offset [expr $pin_index +  $pin_total_special ]]
                            set pos_y  [lindex $start 1]
                        }
                        imsg_id -id UFSM-151 -tl 9 -msg "eval_pin_place_cmd: place $pin_name on edge $pin_edge at ([subst $pos_x] [subst $pos_y]) = ($pos_x $pos_y)"
                        puts $file_id [format "edit_pin -pin %s\t\t -layer %s -fixed_pin 1 -snap mgrid -edge %s -pin_width %s -assign %s\t %s \t\;\# x=%.2f y=%.2f" $pin_name $pin_layer $pin_edge $pin_width $pos_x $pos_y [subst $pos_x] [subst $pos_y]]
                        incr pins_placed_of_group

                    ## special_1 (module index) in special_3
                    } else {

                        # Loop over range start with '0' for placement
                        set range [expr abs($range1from - $range1to) + 1]
                        for {set i 0} {$i < $range } {incr i} {
                            # get the pin position refer to edge
                            # even edge number --> vertically

                            if {[expr $pin_edge % 2] == 0} {
                                set pos_x  [lindex $start 0]
                                # direct: set pos_y  [expr [lindex $start 1] + $offset * ($pin_index + $i + $pin_total_special)]
                                set pos_y  [format "\[expr %s \+  %s \* %s\]" [expr [lindex $start 1]+$group_offset*$iteration_times] $offset [expr $pin_index + $i + $pin_total_special]]
                                # odd edge number --> horizontally
                            } else {
                                # direct: set pos_x  [expr [lindex $start 0] + $offset * ($pin_index + $i + $pin_total_special)]
                                set pos_x  [format "\[expr %s \+ %s \* %s\]" [expr [lindex $start 0]+$group_offset*$iteration_times] $offset [expr $pin_index + $i + $pin_total_special]]
                                set pos_y  [lindex $start 1]
                            }
                            # refer to the given range, need to get the correct index for nameing
                            # special case of [[n..n]] error, force to correct use
                            if { $range1from == $range1to } {
                                emsg_id_stop -id UFSM-101 -msg "eval_pin_place_cmd: The range '$range1from..$range1to' is not valid. Use without '[]' or [$range1from]' for single bus signal."
                                # case of increment [[n..(n+m)]]
                            } elseif { $range1from < $range1to } {
                                set pin_name_special [regsub {\[\d*\:+\d*\]} $pin_name  [expr $range1from + $i]]
                                # otherwise it's decrement [[(n+m)..n]]
                            } else {
                                set pin_name_special [regsub {\[\d*\:+\d*\]} $pin_name [expr $range1from - $i]]
                            }
                            # finally write edit_pin to file
                            imsg_id -id UFSM-151 -tl 9 -msg "eval_pin_place_cmd: place $pin_name_special on edge $pin_edge at ([subst $pos_x] [subst $pos_y]) = ($pos_x $pos_y)"
                            puts $file_id [format "edit_pin -pin %s\t\t -layer %s -fixed_pin 1 -snap mgrid -edge %s -pin_width %s -assign %s\t %s \;\# x=%.2f y=%.2f" $pin_name_special $pin_layer $pin_edge $pin_width $pos_x $pos_y [subst $pos_x] [subst $pos_y]]
                            incr pins_placed_of_group
                        }
                        # get the summary for further groups
                        set pin_nof_special_2 [expr $range - 1]
                        set pin_total_special [expr  $pin_nof_special_2 +  $pin_total_special ]

                    }

                    ## any other special case is not supported (bus in group definition)
                } else {
                    emsg_id_stop -id UFSM-101 -msg "eval_pin_place_cmd: in group placement, all busses need the same length ($group_range) with the identifier '\[\] in the pin list"
                }
            } ;# END LOOP over the pins in one group (pin_index)

            # A full group is placed, set the new group offset once
            if { $group_offset == 0 } {
                set group_offset [expr $pins_placed_of_group*$offset]; # OBS [expr ($pin_total_special + 1)*$offset]
                imsg_id -id UFSM-151 -tl 9 -msg "eval_pin_place_cmd: set new continuous group offset to $group_offset"
            }

        } ;# END LOOP over groups, defined as groups (iteration_times)
    } ;# END LOOP over all groups (group_index)
    close $file_id
}


################################################################################
## Flow Procs: command for create irregular florplan shape
################################################################################

define_proc eval_cut_floorplan_cmd {
      {arg_ptr       "" -arg_ptr    {string}  {required} {indentifier for dat_ptr (inst col row orientation placetype [pin])}}
      {dat_ptr       "" -dat_ptr    {string}  {required} {IO table refer arg_ptr}}
} {
    # check if argument and data pointer are existing
    if ![info exists ::[set arg_ptr]] {
        emsg_id_stop -id UFSM-101 -msg "eval_cut_floorplan_cmd: Argument list '-arg_ptr' list is missing."
    }
    set arg_list [set ::[set arg_ptr]]

    if ![info exists ::[set dat_ptr]] {
        emsg_id_stop -id UFSM-101 -msg "eval_cut_floorplan_cmd: Data list '-dat_ptr' is missing."
    }
    set dat_list [set ::[set dat_ptr]]

    # check if list lengths are matching
    if { [llength $arg_list] != [llength [lindex $dat_list 0]] } {
        emsg_id_stop -id UFSM-101 -msg "eval_cut_floorplan_cmd: List length of arguments and data does not match."
    }
    set nof_groups [llength $dat_list]

    for {set group_index 0} {$group_index < $nof_groups} {set group_index [expr $group_index + 1]} {
        set cmd   update_obj_floorplan_cut_box
        set coordinates_box  [lindex  $dat_list $group_index 0]
        set type  [lindex  $dat_list $group_index 1]
        set name  [lindex  $dat_list $group_index 2]
        set eval_cmd "$cmd -type $type -name $name -box_list $coordinates_box"
        imsg_id -id UFSM-151 -tl 7 -msg "eval_cut_floorplan_cmd: run '$eval_cmd'"
        eval $eval_cmd
    }
}


################################################################################
## Flow Procs: command for IO placment by lists and a grid matrix
## Hint: this new approch replaces the old eval_IO_place_cmd
##       OPEN: LVDS pads
################################################################################

define_proc eval_IO_place_grid_cmd {
    {coor_tl       "" -coor_tl     {string}  {required} {Coordinates of most top left PAD possible [um], pad-opening}}
    {check_io2io   "" -check_io2io {string}  {required} {Check for minimal distance between IO's [um]}}
    {row_spaces    "" -row_spaces  {string}  {required} {formula for the the row    distances: "n * dist + m * dist + ..."}}
    {col_spaces    "" -col_spaces  {string}  {required} {formula for the the column distances: "k * dist + l * dist + ..."}}
    {arg_list      "" -arg_list    {string}  {required} {indentifier for dat_table (inst col row orientation placetype [pin])}}
    {dat_table     "" -dat_table   {string}  {required} {IO table refer arg_list}}
} {

    ## procedure to change a formula for spaces into a list:
    ## f = { 1+ 2* 3 +4*5 + 7 } --> { 1  3  3  5  5  5  5  7 }
    ##                      return: { 1  4  7  12 17 32 27 34 }
    ## on failure returns a error message
    proc convert_formula2summarylist {f} {
        ## accept non-space formula as well
        # set f { 1+ 2* 3 +4*5 + 7 }
        set formula [string map {"+" " + " "*" " * "} $f]
        # set formula [regsub -all {(\d)\s*([\+\*])\s*(\d)} $formula {\1 \2 \3}
        # set formula [regsub -all {(\d)(\s*)([\+\*])(\s*)(\d)} $formula {\1 \2 \3}]

        ## check for pure numbers, summ and multiplyer sign
        if { ![regexp {[\d\+\*]+} $formula] || [regexp {[a-zA-Z]} $formula] } {
            return "fromula contains invalid characters, only use numers, '+', '*'"
        } else {
            ## first element has offest '0' (grid start point)
            set spc_list 0
            set mult 1
            set last 0
            ## parse trought formula and generate list
            foreach e $formula {
                # puts "TRIAL: $e"
                if { $e == "*" } {
                    set mult $last
                } elseif { $e == "+" } {
                    for {set i 0} {$i < $mult} {incr i} {
                        lappend spc_list [expr [lindex $spc_list end] + $last]
                    }
                    set mult 1
                } else {
                    set last $e
                }
            }
            ## at the end of the formula append the last element refer to mult
            for {set i 0} {$i < $mult} {incr i} {
                lappend spc_list [expr [lindex $spc_list end] + $e]
            }
            # puts "FINAL: $spc_list"
            return $spc_list
        }
    }

    ## checker for valid coordinates
    proc check_coordiate_format {coor} {
        if { [llength $coor] != 2 } {
            return "Coordinate is not a pair"
        }
        if { (![string is double [lindex $coor 0]] && ![string is integer [lindex $coor 0]]) ||
             (![string is double [lindex $coor 1]] && ![string is integer [lindex $coor 1]]) } {
            return "Coordinates are not integer or double"
        }
        return 1
    }

    ### @@@ TBD - This list should be placed as technology part
    # This placment requires the info about the pad opening
    # on r0 orientation: LL(0,0) -> PadOpeningCenter(x,y) offset
    # define a 2n list which can be used as dict
    #
    #
    #           IO PAD cell, orientation: R0
    #          +-----------------+
    #          | +-------+       |      This value get lost as soon the
    #          | |       |       |      abtract is taken from OA database
    #     -----|-|---#   |       |      Only the original PAD layout
    #    y |   | |   |   |       |      uses the center PadOpening (#)
    #      |   | +---|---+       |      as origin
    #     -----@-----|-----------+      @ = default origin lower left
    #          |---->|
    #             x
    #   cell type              x  y                        ### TBD put them to PRoject in a usfull place
    #
    #
    #
    # TBD: THIS LIST can be auto generated via a lef extract:
    # -------------------------------------------------------
    # # Format:  MACRO <IO_name>
    # #            ...
    # #            ORIGIN 50 49.97 ;   <-- this is the pad opening coordinate from lower left (@)
    # #            ...
    # #
    # cells="io"
    # cells="lef"
    # oa2lef -lef $cells.lef -lib ohc15l_$cells -noTech -views abstract
    # cat $cells.lef | awk 'BEGIN{print "set IO_cell_PO {"}{if($1 == "MACRO"){m=$2; found=1}; if($1 == "ORIGIN"){found=0; printf "   %-20s {%3.2f %3.2f}\n", m, $2, $3}} END {print "}"}'

    # IMPORTANT:    For Virtuoso the origin is (0,0) refer to layout
    #               Innovus uses the leftmost and lowest of any known layer
    #               (metal, blockage, pins, snapBoundary, ...)
    #               to create it's own lower-left-origin.
    #               This table contains this offset (normally PO center)
    #               A to big snapBoundary can result in a wrong cell layout/GDS view in Innovus
    #
    # HINTS:        lvds_TX_001     has the origin NOT on the PO of pn-pad, offest manual adjusted
    #
    #
    set IO_cell_PO {
       PAD_80x80_vdd018     {50.00 49.97}
       PAD_80x80_vdd050     {50.00 49.97}
       PAD_80x80_vss018     {50.00 49.97}
       PAD_80x80_vss050     {50.00 49.97}
       ai050_0_v2           {80.00 80.00}
       ai050_0_v3           {80.00 80.00}
       ai085mos_ledfb_v2    {80.00 115.00}
       aio033_0             {80.00 80.00}
       aio033_125mhz        {80.00 80.00}
       aio033_80            {80.00 80.00}
       aio033_rx            {80.00 80.00}
       aio050_0             {80.00 80.00}
       aio050_0_v2          {80.00 80.00}
       aio050_0_v3          {80.00 80.00}
       aio050led_0          {80.00 80.00}
       aio085_0             {80.00 80.00}
       aio085_ledfb         {80.00 80.00}
       aio085mos_ledfb      {80.00 114.50}
       aio085mos_ledfb_v2   {80.00 115.00}
       ao050_0_v2           {80.00 80.00}
       ao085_led_v2         {80.00 115.00}
       ao085mos_ledfb_v2    {80.00 115.00}
       blankpad_v2          {80.00 80.00}
       blankpad_v3          {80.00 80.00}
       blankpad_met5        {80.00 80.00}
       blankpad_met5_vdd    {80.00 80.00}
       blankpad_met5_vss    {80.00 80.00}
       breaker_002          {57.73 57.05}
       breaker_003          {61.50 57.00}
       gpio018              {75.00 75.00}
       gpio018_001          {75.00 75.00}
       gpio024              {80.00 80.00}
       gpio024_001          {80.00 80.00}
       gpio033              {80.00 80.00}
       gpio033_001          {80.00 80.00}
       gpio033_80mhz        {160.00 80.00}
       gpio033_80mhz_001    {160.00 80.00}
       gpio033_80mhz_002    {160.00 80.00}
       gpio033_ana          {80.00 80.00}
       gpio033cnr_80mhz     {80.00 170.00}
       gpio033cnr_80mhz_001 {80.00 170.00}
       gpio033cnr_80mhz_002 {80.00 170.00}
       gpio050              {80.00 80.00}
       i2c033fmp            {80.00 80.00}
       i2c033fmp_001        {80.00 80.00}
       i2c033fmp_002        {80.00 80.00}
       i2c050fmphs          {80.00 80.00}
       i2c050fmphs_002      {80.00 80.00}
       led120               {80.00 80.00}
       lvds_TX_001          {592.7 80.10}
       nreset033            {80.00 80.00}
       nreset033_001        {80.00 80.00}
       rail_clamp050        {62.26 44.26}
       ter033               {80.00 80.00}
       test_pad_mux_ai      {75.15 75.15}
       testai050_v3         {75.00 75.00}
       tie_hilo033          {1.04 1.07}
       vbs050               {80.00 80.00}
       vbs050_v2            {80.00 80.00}
       vbs050_v3            {80.00 80.00}
       vdd018               {80.00 80.00}
       vdd018_v2            {80.00 80.00}
       vdd018_v3            {80.00 80.00}
       vdd033               {80.00 105.00}
       vdd033_v2            {80.00 105.00}
       vdd033_v3            {80.00 105.00}
       vdd050               {80.00 80.00}
       vdd050_v2            {80.00 80.00}
       vdd050_v3            {80.00 80.00}
       vdd050px             {75.00 75.00}
       vdd070               {80.00 105.00}
       vdd070i              {75.00 100.00}
       vdd085               {105.00 104.98}
       vdd100               {80.00 80.00}
       vdd100_v2            {80.00 80.00}
       vdd100_v3            {80.00 80.00}
       vdd120               {80.00 80.00}
       vss018               {80.00 80.00}
       vss018_v2            {80.00 80.00}
       vss018_v3            {80.00 80.00}
       vss033               {80.00 105.00}
       vss033_v2            {80.00 105.00}
       vss033_v3            {80.00 105.00}
       vss050               {80.00 80.00}
       vss050_v2            {80.00 80.00}
       vss050_v3            {80.00 80.00}
       vss085               {80.00 110.00}
       vss085_v2            {80.00 110.00}
       vss085_v3            {80.00 110.00}
       vss120               {80.00 80.00}
       xtal018              {80.00 80.00}
       xtal018_1            {80.00 80.00}
       xtal018_2            {80.00 80.00}
       xtal018in_v2         {80.00 80.00}
       xtal018out_v2        {80.00 80.00}
    }

    ## Check the arguments
    ## is coordinate a valid pair format
    if { [set m [check_coordiate_format $coor_tl]] != 1 } {
        emsg_id_stop -id UFSM-101 -msg "eval_IO_place_grid_cmd: $m : -coor_tl = $coor_tl."
    }
    # check for top left coordinate: left x border of IO expected below 300um (~220um),
    #                                top  y border of IO expected above 400um (min. 220+80um)
    if { [lindex $coor_tl  0] < 300 | [lindex $coor_tl  1] < 300 } {
        emsg_id_stop -id UFSM-101 -msg "eval_IO_place_grid_cmd: TopLeft coordinates (x, y) below 300 : $tl_cent "
    }

    ## valid formula for row space grid
    if { [regexp {[a-z]} [set row_space_list [convert_formula2summarylist $row_spaces]]] } {
        emsg_id_stop -id UFSM-101 -msg "eval_IO_place_grid_cmd: row_spaces $row_space_list : $row_spaces."
    }
    imsg_id -id UFSM-151 -tl 7 -msg "eval_IO_place_grid_cmd: use a row    spacing of: $row_space_list"

    ## valid formula for column space grid
    if { [regexp {[a-z]} [set col_space_list [convert_formula2summarylist $col_spaces]]] } {
        emsg_id_stop -id UFSM-101 -msg "eval_IO_place_grid_cmd: row_spaces $col_space_list : $col_spaces."
    }
    imsg_id -id UFSM-151 -tl 7 -msg "eval_IO_place_grid_cmd: use a column spacing of: $col_space_list"

    ## IO to IO distance was checked on
    imsg_id -id UFSM-151 -tl 7 -msg "eval_IO_place_grid_cmd: use a IO to IO distance check of $check_io2io um"

    # check for identical arument and data list lengths
    if { [llength $arg_list] != [llength [lindex $dat_table 0]] } {
        emsg_id_stop -id UFSM-101 -msg "eval_IO_place_grid_cmd: List length of arguments and data does not match."
    }
    # expect a list length of 6 {inst col row orientation placetype pin}
    if { ![regexp {inst} $arg_list] ||
         ![regexp {col} $arg_list] ||
         ![regexp {row} $arg_list] ||
         ![regexp {orientation} $arg_list] ||
         ![regexp {placetype} $arg_list] ||
         ![regexp {pin} $arg_list] } {
        emsg_id_stop -id UFSM-101 -msg "eval_IO_place_grid_cmd: Argument list unexpected: 'inst col row orientation placetype pin' expected."
    }
    if { [llength [lindex $dat_table 0]] < 5 || [llength [lindex $dat_table 0]] > 6 } {
        emsg_id_stop -id UFSM-101 -msg "eval_IO_place_grid_cmd: Data list has unexpected length refer to arguments."
    }

    ## define a IO list for IO to IO distance check
    set IO_table {}

    ## run through IO data list
    foreach IO $dat_table {
        ## get the instance, type, col, row, orientation and placetype
        set inst   [lindex $IO [lsearch -exact $arg_list "inst"]]
        set col    [lindex $IO [lsearch -exact $arg_list "col"]]
        set row    [lindex $IO [lsearch -exact $arg_list "row"]]
        set rot    [lindex $IO [lsearch -exact $arg_list "orientation"]]
        set place  [lindex $IO [lsearch -exact $arg_list "placetype"]]
        set pinnbr [lindex $IO [lsearch -exact $arg_list "pin"]]
        imsg_id -id UFSM-151 -tl 8 -msg "eval_IO_place_grid_cmd: run on $inst"
        ## check for valid instance

        if { [get_db insts -if {.name==*$inst}] != "" } {
            ## set the full instance name
            set inst  [get_db [get_db insts -if {.name==*$inst}] .name]
            ## get the base cell
            set bcell [get_db [get_db insts $inst] .base_cell]
            set type  [get_db $bcell .name]
        ## test for bcell --> need to create a new cell
        } elseif { [get_db base_cells -if {.name==*$inst}] != "" } {
            set bcell [get_db base_cells -if {.name==*$inst}]
            set type  [get_db $bcell .name]
            if { $pinnbr == "" } {
                emsg_id_stop -id UFSM-101 -msg "eval_IO_place_grid_cmd: no pinnumber defined for base_cell $bcell. Need a identifier to create this IO."
            }
            imsg_id -id UFSM-151 -tl 9 -msg "eval_IO_place_grid_cmd: create instance $inst, unplaced"
            ## HINT:
            ## delete_inst -inst [get_db insts -if {.name=="*SDA_pad"}]     # does not work
            ## delete_inst -inst [get_db insts -if {.name=="*SDA_pad"}]     # does work
            create_inst -physical -cell $type -inst $pinnbr -status unplaced
            set inst [get_db [get_db insts -if {.name==$pinnbr}] .name]
        ## otherwise the instance name is wrong
        } else {
            emsg_id -id UFSM-101 -msg "eval_IO_place_grid_cmd: no valid instance or base cell found: $inst - will create a blankpad instead"
            emsg_id -id UFSM-101 -msg "eval_IO_place_grid_cmd: check the io_top.vhd and genus netlist.v"
            ## for debug NOT AVAIABLE PADS:
            set bcell [get_db base_cells -if {.name==blankpad_v3}]
            set type  [get_db $bcell .name]
            set pinnbr "${inst}_${pinnbr}"
            create_inst -physical -cell $type -inst $pinnbr -status unplaced
            set inst [get_db [get_db insts -if {.name==$pinnbr}] .name]
        }

        ## need to check whether bcell is available in the dictionary
        if { ![dict exists $IO_cell_PO $type] } {
            emsg_id_stop -id UFSM-101 -msg "eval_IO_place_grid_cmd: type of IO $inst ($type) not found in PadOpening table - need to update"
        }

        # HINT on bbox parameters:
        # .bbox.dx of the instance  is the x dimension according to the .orient -> differs on r0 and r90
        # .bbox.dx of the base_cell is the absolute x dimension (at R0)
        # .length is the longer dimension of the instance/cell, not dependent of orientation, but on construction
        # .width  is the shorter dimension of the instance/cell
        # just the .bbox.ur define the size on R0
        set paddx [get_db $bcell .bbox.ur.x]
        set paddy [get_db $bcell .bbox.ur.y]

        # calulate the absolute lower left origin of the IO refer to orientation and IO_cell_PO dict
        set POrelx    [expr [lindex $coor_tl  0]  + [lindex $col_space_list $col]]    ;# PadOpening shift right refer col number AND top left grid coordinate
        set POrely    [expr [lindex $coor_tl  1]  - [lindex $row_space_list $row]]    ;# PadOpening shift down  refer row setting AND top left grid coordinate
        set ll2centx  [lindex [dict get $IO_cell_PO $type] 0]                   ;# ll origin (@) to pad center (#) x-offset
        set ll2centy  [lindex [dict get $IO_cell_PO $type] 1]                   ;# ll origin (@) to pad center (#) y-offset

        # absolute ll coordinates refer to orientation                 PAD
        if {$rot == "r0"} {                                     ;#    c---b
            set padx [expr $POrelx - $ll2centx]                 ;#    | # |     # Pad opening center
            set pady [expr $POrely - $ll2centy]                 ;#    @---a     @ lower left origin

        } elseif {$rot == "r90"} {                              ;#    b---a
            set padx [expr $POrelx + $ll2centy - $paddy]        ;#    | # |
            set pady [expr $POrely - $ll2centx]                 ;#    c---@

        } elseif {$rot == "r180"} {                             ;#    a---@
            set padx [expr $POrelx + $ll2centx - $paddx]        ;#    | # |
            set pady [expr $POrely + $ll2centy - $paddy]        ;#    b---c

        } elseif {$rot == "r270"} {                             ;#    @---c
            set padx [expr $POrelx - $ll2centy]                 ;#    | # |
            set pady [expr $POrely + $ll2centy - $paddx]        ;#    a---b

        } elseif {$rot == "mx"} {                               ;#    @---a
            set padx [expr $POrelx - $ll2centx]                 ;#    |---|     --- mirror axis
            set pady [expr $POrely + $ll2centy - $paddy]        ;#    c---b

        } elseif {$rot == "my"} {                               ;#    b---c
            set padx [expr $POrelx + $ll2centx - $paddx]        ;#    | | |     | mirror axis
            set pady [expr $POrely - $ll2centy]                 ;#    a---@

        } elseif {$rot == "mx90"} {                             ;#    a---b
            set padx [expr $POrelx - $ll2centy]                 ;#    | / |     / mirror axis
            set pady [expr $POrely - $ll2centx]                 ;#    @---c

        } elseif {$rot == "my90"} {                             ;#    c---@
            set padx [expr $POrelx + $ll2centy - $paddy]        ;#    | \ |     \ mirror axis
            set pady [expr $POrely + $ll2centx - $paddx]        ;#    b---a

        } else {
            emsg_id_stop -id UFSM-101 -msg "eval_IO_place_grid_cmd: orientation of $inst is invalid, expected: r0 | r90 | r180 | r270 | mx | mx90 | my | my90"
        }

        # ## save ALL IO data      0      1     2           3                     4            5     6    7     8
        # lappend IO_table [list $inst $bcell $rot [list $POrelx $POrely] [list $padx $pady ] $pin $row $col $place]

        ## save ALL IO data for IO to IO distance check
        lappend IO_table [list $inst $POrelx $POrely]

        # execute command
        imsg_id -id UFSM-151 -tl 9 -msg "eval_IO_place_grid_cmd: 'place_inst $inst [list $padx $pady] $rot $place'"
        place_inst $inst [list $padx $pady] $rot $place

    }

    ## final IO to IO distance check
    imsg_id -id UFSM-151 -tl 0 -msg "eval_IO_place_grid_cmd: final IO to IO distance check"
    foreach i $IO_table {
        foreach j $IO_table {
            ## don't compare pad with itself
            if { [lindex $i 0] != [lindex $j 0]} {
                ## reduce compare range to neigbours refer to there PO position and check distance
                if { ([set dX [expr abs([lindex $i 1] - [lindex $j 1])]] < $check_io2io) &&
                     ([set dY [expr abs([lindex $i 2] - [lindex $j 2])]] < $check_io2io)} {
                    ## on a hit, need to calculate the abolute distance
                    if { [set dist [expr sqrt($dX**2 + $dY**2)]] < $check_io2io } {
                        emsg_id_stop -id UFSM-101 -msg "eval_IO_place_grid_cmd: PadOpening < $check_io2io: [lindex $i 0]---$dist um---[lindex $j 0]"
                    }
                }
            }
        }
    }
}

################################################################################
## Flow Procs: command for add pins to the already placed IO's
##
##  - IO need to be placed
##  - place pin to db defined location or on ll corner
##  - 'extern_pins' list is INCOMPLETE (manual definition, LVDS is special)
##  - 'power_pads'  list has to be updated as well
################################################################################


define_proc add_pins_to_IO_grid_placed_pads {
    {arg_list      "" -arg_list    {string}  {required} {indentifier for dat_table (inst col row orientation placetype [pin])}}
    {dat_table     "" -dat_table   {string}  {required} {IO table refer arg_list}}
} {

    # check for identical arument and data list lengths
    if { [llength $arg_list] != [llength [lindex $dat_table 0]] } {
        emsg_id_stop -id UFSM-101 -msg "add_pins_to_IO_grid_placed_pads: List length of arguments and data does not match."
    }
    # expect a list length of 6 {inst col row orientation placetype pin}
    if { ![regexp {inst} $arg_list] ||
         ![regexp {col} $arg_list] ||
         ![regexp {row} $arg_list] ||
         ![regexp {orientation} $arg_list] ||
         ![regexp {placetype} $arg_list] ||
         ![regexp {pin} $arg_list] } {
        emsg_id_stop -id UFSM-101 -msg "add_pins_to_IO_grid_placed_pads: Argument list unexpected: 'inst col row orientation placetype pin' expected."
    }
    if { [llength [lindex $dat_table 0]] < 5 || [llength [lindex $dat_table 0]] > 6 } {
        emsg_id_stop -id UFSM-101 -msg "add_pins_to_IO_grid_placed_pads: Data list has unexpected length refer to arguments."
    }

    ## run through IO data list
    foreach IO $dat_table {
        ## get the instance, type, col, row, orientation and placetype
        set inst   [lindex $IO [lsearch -exact $arg_list "inst"]]
        imsg_id -id UFSM-151 -tl 8 -msg "add_pins_to_IO_grid_placed_pads: run on $inst"

        ## add pins on top of each pad only when the inst is valid.
        ## skip add pins on dummy pad when the inst name matches a base cell.
        if { [get_db base_cells -if {.name==*$inst}] == "" } {

          ## check for valid instance, exclude spare (<inst>_<pinnumber>)
          if { [regexp {spare} [get_db insts -if {.name==*$inst}]] } {
              wmsg_id -id UFSM-101 -tl 0 -msg "add_pins_to_IO_grid_placed_pads: spare cell $inst has no valid pin to place - skipped"
              continue
          } elseif { [get_db insts -if {.name==*$inst}] != "" } {
              ## set the full instance name
              set inst  [get_db [get_db insts -if {.name==*$inst}] .name]
              ## get the base cell
              set bcell [get_db [get_db insts $inst] .base_cell]
              set type  [get_db $bcell .name]
          ## otherwise the instance name is wrong
          } else {
              wmsg_id -id UFSM-101 -tl 0 -msg "add_pins_to_IO_grid_placed_pads: no valid instance or base cell found: $inst - can't place pin"
              continue
          }

          ## ADD PINS on top of each PAD refer to LVS
          ## (placed pin info seams to be a text layer which get removed by OA transfer
          ##
          ## - PAD name
          ## - 1um x 1um TMET
          ## - 2 posibilies
          ##          normal pads has already a correct pin placed but wihout metal --> add TMET at this location
          ##          power and blankpads has no pin --> redefine and place in the origin corner 6um offset should hit TMET
          ## For a clean opperation there is a mapping required for the pad type and it's outside connection
          ##
          ## !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
          ## IT'S A MANUAL MAPPING - INCOMPLETE, but will STOP on missing entries
          ## !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
          ##
          ## This may help to find them:
          ##
          ## ## get all used pads
          ## set p [get_db insts -if {.is_pad == true}]
          ## ## get all used IO base cells
          ## set bc [lsort -unique [get_db $p .base_cell]]
          ## ## get all signal pins of use IO base cells
          ## foreach i $bc { puts "$i   [get_db [get_db $i .base_pins] .name]" }
          ## ## get all power pins of used IO base cells
          ## foreach i $bc { puts "$i   [get_db [get_db $i .pg_base_pins] .name]" }

    # cells="io"
    # cells="lvds"
    # oa2lef -lef $cells.lef -lib ohc15l_$cells -noTech -views abstract
    # cat $cells.lef | egrep "MACRO|PIN"
    # cat $cells.lef | egrep "MACRO"
          ##                base_cell             pin to extern (chip connection)
          set extern_pins { gpio018_001           {io_DIO}
                            aio033_125mhz         {clk_AI}
                            ai050_0_v3            {in_AI}
                            aio050_0_v3           {io_AIO}
                            lvds_TX               {vp_AIO  vn_AIO}
                            lvds_TX_001           {vp_AIO vn_AIO}
                            i2c033fmp_002         {io_DIO}
                            nreset033_001         {vin_DI}
                            testai050_v3          {test_AI}
                            blankpad_met5_vdd     {pwr_AI}
                            blankpad_met5_vss     {gnd_AI}
                            blankpad_v3           {blankpad_AI}
                            vdd018_v3             {vdd018}
                            vdd050_v3             {vdd050}
                            vss018_v3             {vss018}
                            vss050_v3             {vss050} }

          ##                list of base_cells which need to get a pin created
          set power_pads  { blankpad_met5_vdd
                            blankpad_met5_vss
                            blankpad_v3
                            vdd018_v3
                            vdd050_v3
                            vss018_v3
                            vss050_v3 }

          ## bcell is already defined, get it's name and test (instead of e.g. base_cell:gpio018_001)
          set bcellname [get_db $bcell .name]

          # ## This check can be usefull on extern_pins update (suspend instead exit on missing entry)
          # if ![regexp $bcellname $extern_pins] {
          #     wmsg_id -id UFSM-101 -msg "eval_IO_place_grid_cmd: base cell not in 'extern_pins' list"
          #     suspend
          # }

          for { set i  0 } { $i < [llength [dict get $extern_pins $bcellname]]} { incr i} {

            ## In case the current IO is a power pad
            if [regexp $bcellname $power_pads] {
                ## get the POWER pin of the cell to extern
                set p [get_db pg_pins $inst/[lindex [dict get $extern_pins $bcellname] $i] ]
                ## get the location of the instance and add the offset 6um
                set l [get_db inst:$inst .location]
                set posX [expr [lindex [lindex $l 0] 0] + 6]
                set posY [expr [lindex [lindex $l 0] 1] + 6]
            ## otherwise expect there is a pin placed a dedicated location
            } else {
                ## get SIGNAL the pin of the cell to extern
                set p [get_db pins $inst/[lindex [dict get $extern_pins $bcellname] $i] ]
                ## the location of pad pin is already correct somewhere on TMET
                set posX [lindex [lindex [get_db $p .location] 0] 0]
                set posY [lindex [lindex [get_db $p .location] 0] 1]
            }
            puts "MCC P : $p"
            puts "MCC [get_db [get_db $p .net] .name]"

            
            ## get the net name as well for the pin
            set cellpin [get_db [get_db $p .net] .name]
            puts "MCC cellpin: $cellpin"

            ## basic check and info
            if {$p == "" } {
                emsg_id_stop -id UFSM-101 -msg "add_pins_to_IO_grid_placed_pads: base_pin to extern of $inst not found, list 'extern_pins' may be incomplete"
            } elseif { $cellpin == "" } {
                emsg_id_stop -id UFSM-101 -msg "add_pins_to_IO_grid_placed_pads: no net found on $p"
            } else {
                imsg_id -id UFSM-151 -tl 7 -msg "add_pins_to_IO_grid_placed_pads: edit_pin $cellpin to $posX / $posY"
            }

            ## place pin inside! at x/y
            edit_pin -pin_width 1.0 -pin_depth 1.0 -layer TMET \
                     -fixed_pin 1 -global_location -side inside -snap mgrid \
                     -assign $posX $posY \
                     -pin $cellpin

          }
        }
    }
}


################################################################################
## Flow Procs: command for adding blockages by list
################################################################################
## Description
## ----------------------------------------------------------------------------------------------
## Commands:
##         create_flexfiller_route_blockage:
##         create_pin_blockage:
##     ->  create_place_blockage:
##         create_resize_blockage:
##     ->  create_route_blockage:
##         delete_pin_blockages:
##         delete_route_blockages:
##         reset_selective_blockage_gate:
##         set_route_blockage_default_layer:
##         set_selective_blockage_gate:
##
## create_place_blockage -name <string> -area {x1 y1 x2 y2} -type {hard}
## create_route_blockage -name <string> rects {{x1 y1 x2 y2}} -layers <layer+>
##
## command to get blockages:
## get_db route_blockages .name
## get_db route_blockages .layer
##
## command to delete blockage
## delete_route_blockages -type routes -name rblk_M5_up
##
## OPTION: negative number support for relative instance place would be nice
##         positive -> create_place_halo -insts
##         negative -> create_place_blockage -area   (only usefull for placed instances)

define_proc eval_add_blockages {
    {name   "" -name    {string}  {required} {unique indentifier name}}
    {layer  "" -layer   {string}  {required} {what to block: <instance> | cell | MET1 | MET2 | MET3 | MET4 | MET5 | TMET}}
    {box    "" -box     {string}  {required} {box: x1 y1 x2 y2 | all | <left bottom right top> for an instance}}
} {
    proc check_bbox_format {b} {
        foreach i $b {
            if { ![string is double $i] && ![string is integer $i] } {
                emsg_id_stop -id UFSM-101 -msg "eval_add_blockages: $b is not in 'box' format \[x1 y1 x2 y2 | all\] "
            }
        }
    }

    # check for box option all, replace with full design size
    if { [regexp {all} $box] > 0 } {
        set box [lindex [get_db designs .bbox] 0]
    }
    # check for format
    check_bbox_format $box
    # on type cellsoft use place blockage as soft
    if { [regexp {soft} $layer] > 0 } {
        imsg_id -id UFSM-151 -tl 8 -msg "eval_add_blockages: soft blockages $name"
        create_place_blockage -name $name -area $box -type {soft}
    # on type cell use place blockage
    } elseif { [regexp {cell} $layer] > 0 } {
        imsg_id -id UFSM-151 -tl 8 -msg "eval_add_blockages: hard blockages $name"
        create_place_blockage -name $name -area $box -type {hard}
    # else use route blockage for MET or VIA
    } elseif { [regexp {MET} $layer] > 0 || [regexp {VIA} $layer] > 0 } {
		foreach l $layer {
			imsg_id -id UFSM-151 -tl 8 -msg "eval_add_blockages: route $name of layer $l"
			set eval_cmd "create_route_blockage -name $name -rects [list $box] -layers $l"
			eval "$eval_cmd"
			#  create_route_blockage -name $name -rects [list $box] -layers [list $layer]
		}
    # else use halo araound instance
    } else {
        foreach i [get_db [get_db insts -if {.name==*$layer}] .name] {
            if { $i == "" } {
                emsg_id_stop -id UFSM-101 -msg "eval_add_blockages: instance $layer can't be found"
            } else {
                imsg_id -id UFSM-151 -tl 8 -msg "eval_add_blockages: create_place_halo -halo_deltas $box -insts $i"
                create_place_halo -halo_deltas $box -insts $i
            }
        }
    }
}

################################################################################
## Flow Procs: command for instance array placement by list (OLD VERSION)
################################################################################

# -----------------------------------------------
# HINT: setup for 'place_inst', 'create_guide', 'create_fence'
#       not included 'create_inst'
# -----------------------------------------------
define_proc eval_array_place_cmd {
    {arg_ptr       "" -arg_ptr    {string}  {required} {indentifier for dat_ptr (inst start offset [size|orientation] place)}}
    {dat_ptr       "" -dat_ptr    {string}  {required} {IO table refer arg_ptr}}
} {


    # check if argument and data pointer are existing
    if ![info exists ::[set arg_ptr]] {
        emsg_id_stop -id UFSM-101 -msg "eval_array_place_cmd: Argument list '-arg_ptr' list is missing."
    }
    set arg_list [set ::[set arg_ptr]]

    if ![info exists ::[set dat_ptr]] {
        emsg_id_stop -id UFSM-101 -msg "eval_array_place_cmd: Data list '-dat_ptr' is missing."
    }
    set dat_list [set ::[set dat_ptr]]


    # helpfull vector MAC
    proc vect_mac {a b {bm 1}} {
        # puts "a: $a, b: $b, bm: $bm"
        set x [expr [lindex $a 0] + [lindex $b 0] * $bm]
        set y [expr [lindex $a 1] + [lindex $b 1] * $bm]
        return [list $x $y]
    }

    # helpfull proc to check for unique instance for a dedicated index
    proc check_instance {i k} {
        if {$i == ""} {
            emsg_id_stop -id UFSM-101 -msg "eval_array_place_cmd: inst with index \[$k\] not found."
        }
        if {[llength $i] != 1} {
            emsg_id_stop -id UFSM-101 -msg "eval_array_place_cmd: inst with index \[$k\] not unique: $i"
        }
    }

    # checker for valid double
    proc check_two_double {d t} {
        if {[llength $d] != 2 || ![string is double [lindex $d 0]] || ![string is double [lindex $d 1]]} {
            emsg_id_stop -id UFSM-101 -msg "eval_array_place_cmd: $t double value not accaptable: '$d'"
        }
    }

    # checker for valid integer
    proc check_two_int {d t} {
        if {[llength $d] != 2 || ![string is integer [lindex $d 0]] || ![string is integer [lindex $d 1]]} {
            emsg_id_stop -id UFSM-101 -msg "eval_array_place_cmd: $t integer value not accaptable: '$d'"
        }
    }


    # run through array list
    foreach block $dat_list {
        # get each argument: inst, start, size, offset
        set inst   [lindex $block [lsearch -exact $arg_list "inst"]]
        set start  [subst [lindex $block [lsearch -exact $arg_list "start"]]]
        set size   [subst [lindex $block [lsearch -exact $arg_list "size"]]]
        set offset [subst [lindex $block [lsearch -exact $arg_list "offset"]]]
        set orient [lindex $block [lsearch -exact $arg_list "orientation"]]
        set place  [lindex $block [lsearch -exact $arg_list "type"]]

        # run some checks:
        # instance is expected to contain a range
        set backet_parts [llength [split $inst {\[\]}]]
        if { $backet_parts == 3 } {
        set inst_pre   [lindex [split $inst {\[\]}] 0]
        set inst_post  [lindex [split $inst {\[\]}] 2]
        set inst_range [lindex [split $inst {\[\]}] 1]
        set range_from [lindex [split $inst_range .] 0]
        set range_to   [lindex [split $inst_range .] 2]
        } elseif { $backet_parts == 1 } {
            set inst_pre   $inst
            set inst_post  ""
            set inst_range ""
            set range_from ""
            set range_to   ""
        } else {
            emsg_id_stop -id UFSM-101 -msg "eval_array_place_cmd: Unprocessable brackets found in $inst."
        }

        # expect correct values for
        check_two_double $start  "start"
        check_two_double $offset "offset"

        # define number of instance used
        if {$range_to == "" || $range_from == $range_to} {
            set inst_count 1
            # set end range to use the same loop style for a single element
            set range_to $range_from
        } else {
            check_two_int [list $range_from $range_to] "range index"
            set inst_count [expr abs($range_from - $range_to) + 1]
        }

        # get instance size if not defined (leaf cell: base_cell, ILM, HM), check for overlap
        if {$size == ""} {
            # get the size by db bbox
            set size [lindex [get_db [get_db insts -if {.name==*$inst_pre$range_from$inst_post}] .base_cell.bbox.ur] 0]
            # swapp the indicies on 90° rotation
            if {$orient == "r90" || $orient == "r270" || $orient == "mx90" || $orient == "my90"} {
                set size [list [lindex $size 1] [lindex $size 0]
            }
            set mod_type insts
            if {$inst_count > 1} {
                # on more than a single inst, at least one of the absolute offset has to be smaller than the size
                if {[expr abs([lindex $offset 0])] < [lindex $size 0] && [expr abs([lindex $offset 1])] < [lindex $size 1]} {
                emsg_id_stop -id UFSM-101 -msg "eval_array_place_cmd: offset ($offset) to small, instance ($size) overlaps."
            }
            }
        # otherwise it's a hierachical cell, overlap is allowed
        } else {
            check_two_double $size "size"
            set mod_type hinsts
        }

        # check for place refer to module_type
        if {$mod_type == "insts"} {
            if {$place != "-fixed" && $place != "-placed" && $place != "-soft_fixed"} {
                emsg_id_stop -id UFSM-101 -msg "eval_array_place_cmd: place argument not valid: '$place'"
            }
            set cmd "place_inst"
        } else {
            if {$place == "inst"} {
                emsg_id_stop -id UFSM-101 -msg "eval_array_place_cmd: place argument 'inst' not supported."
            }
            if {$place != "guide" && $place != "fence"} {
                emsg_id_stop -id UFSM-101 -msg "eval_array_place_cmd: place argument not valid: '$place'"
            }
            set cmd "create_$place"
        }

        # report
        imsg_id -id UFSM-151 -tl 9 -msg "eval_array_place_cmd: -------------------------------------------------------"
        imsg_id -id UFSM-151 -tl 9 -msg "eval_array_place_cmd: module_type:   $mod_type"
        imsg_id -id UFSM-151 -tl 9 -msg "eval_array_place_cmd: instance_pre:  $inst_pre"
        imsg_id -id UFSM-151 -tl 9 -msg "eval_array_place_cmd: instance_post: $inst_post"
        imsg_id -id UFSM-151 -tl 9 -msg "eval_array_place_cmd: instance_range: from $range_from to $range_to ($inst_count times)"

        # basic check for fit in chip
        #                startX           +  offsetX           * (inst-1)
        set lastX [expr [lindex $start 0] + [lindex $offset 0] * ($inst_count-1)]
        set lastY [expr [lindex $start 1] + [lindex $offset 1] * ($inst_count-1)]
        if {$lastX < 0 || $lastX > [get_db . .designs.bbox.ur.x]} {
            emsg_id_stop -id UFSM-101 -msg "eval_array_place_cmd: $inst_count * instance won't fit in chip width."
        }
        if {$lastY < 0 || $lastY > [get_db . .designs.bbox.ur.y]} {
            emsg_id_stop -id UFSM-101 -msg "eval_array_place_cmd: $inst_count * instance won't fit in chip height."
        }

        # finally place the stuff, select the command refer to the module type
        if {$mod_type == "insts"} {
            set cmd "place_inst"
        } else {
            set cmd "create_$place"
        }
        # report
        imsg_id -id UFSM-151 -tl 8 -msg "eval_array_place_cmd: selected command is: '$cmd'"

        # add single or multiple instance
        set counter 0
        # place single instance ('[integer]' in instance or no brackets at all)
        if {$range_from == $range_to} {
            # get and check instance
            set inst [get_db [get_db $mod_type -if {.name==*$inst_pre$range_from$inst_post}] .name]
            check_instance $inst $range_from
            # calculate start point
            set bboxll [vect_mac $start $offset $counter]
            # according to type define full command
            if {$mod_type == "insts"} {
                set eval_cmd "$cmd $inst $bboxll $orient $place"
            } else {
                # calculate the bbox ur
                set bboxur [vect_mac $bboxll $size]
                set eval_cmd "$cmd -name $inst -area [join [list $bboxll $bboxur]]"
            }
            incr counter
            # execute command
            imsg_id -id UFSM-151 -tl 7 -msg "eval_array_place_cmd: run '$eval_cmd'"
            eval "$eval_cmd"
        # count --UP-- trough instance
        } elseif {$range_from < $range_to} {
            for {set i $range_from} {$i <= $range_to} {incr i} {
                # get and check instance
                set inst [get_db [get_db $mod_type -if {.name==*$inst_pre$i$inst_post}] .name]
                check_instance $inst $i
                # calculate start point
                set bboxll [vect_mac $start $offset $counter]
                # according to type define full command
                if {$mod_type == "insts"} {
                    set eval_cmd "$cmd $inst $bboxll $orient $place"
                } else {
                    # calculate the bbox ur
                    set bboxur [vect_mac $bboxll $size]
                    set eval_cmd "$cmd -name $inst -area [join [list $bboxll $bboxur]]"
                }
                incr counter
                # execute command
                imsg_id -id UFSM-151 -tl 7 -msg "eval_array_place_cmd: run '$eval_cmd'"
                eval "$eval_cmd"
            }
        # count --DOWN-- trough instance
        } else {
            for {set i $range_from} {$i >= $range_to} {incr i -1} {
                # get and check instance
                set inst [get_db [get_db $mod_type -if {.name==*$inst_pre$i$inst_post}] .name]
                check_instance $inst $i
                # calculate start point
                set bboxll [vect_mac $start $offset $counter]
                # according to type define full command
                if {$mod_type == "insts"} {
                    set eval_cmd "$cmd $inst $bboxll $orient $place"
                } else {
                    # calculate the bbox ur
                    set bboxur [vect_mac $bboxll $size]
                    set eval_cmd "$cmd -name $inst -area [join [list $bboxll $bboxur]]"
                }
                incr counter
                # execute command
                imsg_id -id UFSM-151 -tl 7 -msg "eval_array_place_cmd: run '$eval_cmd'"
                eval "$eval_cmd"
            }
        }
    }
}

################################################################################
## Flow Procs: command for instance array placement by list
## NEW, does replace "eval_array_place_cmd" - but ARRAY placment is not VERIFIED !
################################################################################

# -----------------------------------------------
# HINT: refer to type, executes : 'place_inst', 'create_guide', 'create_fence'
#       not included 'create_inst'
# -----------------------------------------------
define_proc eval_place_instances {
    {info   "" -info   {string}  {optional} {just comment info on the cell}}
    {name   "" -name   {string}  {required} {name of inst or hinst}}
    {type   "" -type   {string}  {required} {possible type: [fixed|placed|soft_fixed] for inst OR [fence | guide] for hinst}}
    {origin "" -origin {string}  {required} {lower left origin {x y}}}
    {orient "" -orient {string}  {optional} {required for inst: R0 R90 R180 R270 MX MY MX90 MY90 }}
    {size   "" -size   {string}  {optional} {size {x y} required for hinst}}
    {offset "" -offset {string}  {optional} {offset for array place - currently not supported}}
    {onrail "" -onrail {string}  {optional} {not required, evaluated by flow_step_init_floorplanflow_step_init_floorplan}}
} {

    # helpfull vector MAC
    proc vect_mac {a b {bm 1}} {
        # puts "a: $a, b: $b, bm: $bm"
        set x [expr [lindex $a 0] + [lindex $b 0] * $bm]
        set y [expr [lindex $a 1] + [lindex $b 1] * $bm]
        return [list $x $y]
    }

    # helpfull proc to check for unique instance for a dedicated index
    #                    inst orig
    proc check_instance {i    o} {
        if {$i == ""} {
            emsg_id_stop -id UFSM-101 -msg "eval_place_instances: instance '$o' not found."
        }
        if {[llength $i] != 1} {
            emsg_id_stop -id UFSM-101 -msg "eval_place_instances: instance '$o' not unique: $i"
        }
    }

    # checker for valid double
    proc check_two_double {d t} {
        if {[llength $d] != 2 || ![string is double [lindex $d 0]] || ![string is double [lindex $d 1]]} {
            emsg_id_stop -id UFSM-101 -msg "eval_place_instances: $t double value not accaptable: '$d'"
        }
    }

    # checker for valid integer
    proc check_two_int {d t} {
        if {[llength $d] != 2 || ![string is integer [lindex $d 0]] || ![string is integer [lindex $d 1]]} {
            emsg_id_stop -id UFSM-101 -msg "eval_place_instances: $t integer value not accaptable: '$d'"
        }
    }

    ## expect correct values for
    check_two_double $origin  "lower left origin"

    ## check for array placeing - OPTIONAL, NOT IMPLEMENTED
    #TEST set name "something_is_very_special"
    if { [regexp {(.*)\[(\d*):(\d*)\](.*)} $name count pre range_from range_to post] } {
        #TEST puts "pre: $pre from: $range_from : $range_to post: $post"
        check_two_double $offset "array offset"
        # OBS: check_two_int [list $range_from $range_to] "range index"
        ## calculate number of used instances
        set inst_count [expr abs($range_from - $range_to) + 1]
    } else {
        set offset {0.0 0.0}
        set inst_count 1
    }

    ## check for type dependent parameters
    if { $type == "fixed" || $type == "placed" || $type == "soft_fixed" } {
        if { ![regexp $orient {R0 R90 R180 R270 MX MY MX90 MY90}] } {
            emsg_id_stop -id UFSM-101 -msg "eval_place_instances: orientation '$orient' is undefined or unknown (at $name)"
        }
        set mod_type insts
        set cmd "place_inst"
        ## get the size by db bbox
        #OPTION: set size [lindex [get_db [get_db insts -if {.name==*$inst_pre$range_from$inst_post}] .base_cell.bbox.ur] 0]
        set size [lindex [get_db [get_db insts -if {.name==*$name}] .base_cell.bbox.ur] 0]
    } elseif { $type == "fence" || $type == "guide" } {
        check_two_double $size  "size of $name"
        set mod_type hinsts
        set cmd "create_$type"
    } else {
        emsg_id_stop -id UFSM-101 -msg "eval_place_instances: type $type is unknown (at $name)"
    }

    #    # get instance size if not defined (leaf cell: base_cell, ILM, HM), check for overlap
    #    if {$size == ""} {
    #        # get the size by db bbox
    #        set size [lindex [get_db [get_db insts -if {.name==*$inst_pre$range_from$inst_post}] .base_cell.bbox.ur] 0]
    #        # swapp the indicies on 90° rotation
    #        if {$orient == "R90" || $orient == "R270" || $orient == "MX90" || $orient == "MY90"} {
    #            set size [list [lindex $size 1] [lindex $size 0]
    #        }
    #        set mod_type insts
    #        if {$inst_count > 1} {
    #           # on more than a single inst, at least one of the absolute offset has to be smaller than the size
    #           if {[expr abs([lindex $offset 0])] < [lindex $size 0] && [expr abs([lindex $offset 1])] < [lindex $size 1]} {
    #               emsg_id_stop -id UFSM-101 -msg "eval_array_place_cmd: offset ($offset) to small, instance ($size) overlaps."
    #           }
    #        }
    #    }

    # report
    imsg_id -id UFSM-151 -tl 9 -msg "eval_place_instances: -------------------------------------------------------"
    imsg_id -id UFSM-151 -tl 9 -msg "eval_place_instances: module_type:    $mod_type"
    imsg_id -id UFSM-151 -tl 9 -msg "eval_place_instances: name:           $name"
    if {$inst_count > 1} {
        imsg_id -id UFSM-151 -tl 9 -msg "eval_place_instances: instance array: $pre [$range_from : $range_to] $post"
    }
    imsg_id -id UFSM-151 -tl 8 -msg "eval_place_instances: selected command is: '$cmd'"

    # basic check for fit in chip
    #                startX           +  offsetX           * (inst-1)
    set lastX [expr [lindex $origin 0] + [lindex $offset 0] * ($inst_count-1)]
    set lastY [expr [lindex $origin 1] + [lindex $offset 1] * ($inst_count-1)]
    if {$lastX < 0 || $lastX > [get_db . .designs.bbox.ur.x]} {
        emsg_id_stop -id UFSM-101 -msg "eval_place_instances: $inst_count * instance won't fit in chip width."
    }
    if {$lastY < 0 || $lastY > [get_db . .designs.bbox.ur.y]} {
        emsg_id_stop -id UFSM-101 -msg "eval_place_instances: $inst_count * instance won't fit in chip height."
    }

    # PLACEMENT of instances
    set counter 0
    # place single instance ('[integer]' in instance or no brackets at all)
    if {$inst_count == 1} {
        # get and check instance
        set inst [get_db [get_db $mod_type -if {.name==*$name}] .name]
        check_instance $inst $name
        # calculate start point
        set bboxll [vect_mac $origin $offset $counter]
        # according to type define full command
        if {$mod_type == "insts"} {
            set eval_cmd "$cmd $inst $bboxll $orient -$type"
        } else {
            # calculate the bbox ur
            set bboxur [vect_mac $bboxll $size]
            set eval_cmd "$cmd -name $inst -area [join [list $bboxll $bboxur]]"
        }
        incr counter
        # execute command
        imsg_id -id UFSM-151 -tl 7 -msg "eval_place_instances: run '$eval_cmd'"
        eval "$eval_cmd"
    # count --UP-- trough instance
    } elseif {$range_from < $range_to} {
        for {set i $range_from} {$i <= $range_to} {incr i} {
            # get and check instance
            set inst [get_db [get_db $mod_type -if {.name==*$inst_pre$i$inst_post}] .name]
            check_instance $inst $inst_pre$i$inst_post
            # calculate start point
            set bboxll [vect_mac $origin $offset $counter]
            # according to type define full command
            if {$mod_type == "insts"} {
                set eval_cmd "$cmd $inst $bboxll $orient -$type"
            } else {
                # calculate the bbox ur
                set bboxur [vect_mac $bboxll $size]
                set eval_cmd "$cmd -name $inst -area [join [list $bboxll $bboxur]]"
            }
            incr counter
            # execute command
            imsg_id -id UFSM-151 -tl 7 -msg "eval_place_instances: run '$eval_cmd'"
            eval "$eval_cmd"
        }
    # count --DOWN-- trough instance
    } else {
        for {set i $range_from} {$i >= $range_to} {incr i -1} {
            # get and check instance
            set inst [get_db [get_db $mod_type -if {.name==*$inst_pre$i$inst_post}] .name]
            check_instance $inst $inst_pre$i$inst_post
            # calculate start point
            set bboxll [vect_mac $origin $offset $counter]
            # according to type define full command
            if {$mod_type == "insts"} {
                set eval_cmd "$cmd $inst $bboxll $orient -$type"
            } else {
                # calculate the bbox ur
                set bboxur [vect_mac $bboxll $size]
                set eval_cmd "$cmd -name $inst -area [join [list $bboxll $bboxur]]"
            }
            incr counter
            # execute command
            imsg_id -id UFSM-151 -tl 7 -msg "eval_place_instances: run '$eval_cmd'"
            eval "$eval_cmd"
        }
    }
}

# -----------------------------------------------
# Make hinst/module/inst cluster
# -----------------------------------------------
define_proc eval_cluster_instances {
    {info   "" -info   {string}  {optional} {inst/module cluster}}
} {

   set group_index 0
   if { [info exists ::cfg_inst_cluster]  && [llength $::cfg_inst_cluster] > 0 } {
    imsg_id -id UFSM-151 -tl 7 -msg "Create boundary for module/hinst/inst"
    foreach a $::cfg_inst_cluster {
         if {[lindex $a 1] == "module" } {
           foreach module_name [get_db modules [lindex $a 0]] {
              foreach hinst_name [get_db $module_name .hinsts] {
                 imsg_id -id UFSM-151 -tl 7 -msg "module: $hinst_name"
                 create_boundary_constraint -type cluster -hinst $hinst_name
              }
           }
         } elseif { [lindex $a 1] == "hinst" } {
           foreach hinst_name [get_db hinsts [lindex $a 0]] {
               imsg_id -id UFSM-151 -tl 7 -msg  "hinst: $hinst_name"
               create_boundary_constraint -type cluster -hinst $hinst_name
           }
         } elseif { [lindex $a 1] == "inst" } {
           create_group -name group_${group_index} -type cluster

           imsg_id -id UFSM-151 -tl 7 -msg  "Insts in group_${group_index}"
           foreach inst_name [get_db insts [lindex $a 0]] {
              imsg_id -id UFSM-151 -tl 7 -msg  "   inst: $inst_name"
              update_group -name group_${group_index} -add -objs $inst_name
           }
           set group_index [expr $group_index + 1]
         } else {
             imsg_id -id UFSM-151 -tl 0 -msg "eval_cluster_instances: Not regnized inst/module"
         }


      }
   }
}


define_proc fix_cluster_insts {
    {info   "" -info   {string}  {optional} {fix inst/module cluster}}
} {

   set group_index 0
   if { [info exists ::cfg_inst_cluster]  && [llength $::cfg_inst_cluster] > 0 } {
    imsg_id -id UFSM-151 -tl 7 -msg "Create boundary for module/hinst/inst"
    foreach a $::cfg_inst_cluster {
         if {[lindex $a 1] == "module" } {
           foreach module_name [get_db modules [lindex $a 0]] {
              #puts "module: $module_name"
              foreach hinst_name [get_db $module_name .hinsts] {
                 #puts "hinst_name: $hinst_name"
                 foreach inst [get_db $hinst_name .insts] {
                    set_db $inst .place_status fixed
                    imsg_id -id UFSM-151 -tl 0 -msg "fix inst pos. : $inst"
                 }
                 #imsg_id -id UFSM-151 -tl 7 -msg "module: $hinst_name"
                 #create_boundary_constraint -type cluster -hinst $hinst_name
              }
           }
         } elseif { [lindex $a 1] == "hinst" } {
           foreach hinst_name [get_db hinsts [lindex $a 0]] {
               foreach inst [get_db $hinst_name .insts] {
                  set_db $inst .place_status fixed
                  imsg_id -id UFSM-151 -tl 0 -msg "fix inst pos. : $inst"
               }
           }
         } elseif { [lindex $a 1] == "inst" } {
           foreach inst_name [get_db insts [lindex $a 0]] {
              set_db $inst .place_status fixed
              imsg_id -id UFSM-151 -tl 0 -msg "fix inst pos. : $inst"
           }
         } else {
             imsg_id -id UFSM-151 -tl 0 -msg "fix_cluster_insts: Not regnized inst/module"
         }


      }
   }
}



# It checks distance distribution
define_proc check_cluster_inst_dist {
    {info   "" -info   {string}  {optional} {inst/module cluster}}
} {

    # Return a box list
    proc max_box {x y rect} {
          set min_x [lindex $rect 0]
          set min_y [lindex $rect 1]
          set max_x [lindex $rect 2]
          set max_y [lindex $rect 3]

         if { $x < $min_x } {
           set r_min_x $x
         } else {
           set r_min_x $min_x
         }

         if { $y < $min_y } {
           set r_min_y $y
         } else {
           set r_min_y $min_y
         }

         if { $x > $max_x } {
           set r_max_x $x
         } else {
           set r_max_x $max_x
         }

         if { $y > $max_y } {
           set r_max_y $y
         } else {
           set r_max_y $max_y
         }

         return [list $r_min_x $r_min_y $r_max_x $r_max_y]
    }




    proc box_dxdy { rect } {

       set min_x [lindex $rect 0]
       set min_y [lindex $rect 1]
       set max_x [lindex $rect 2]
       set max_y [lindex $rect 3]

       set x [expr $max_x - $min_x]
       set y [expr $max_y - $min_y]

       return [list $x $y]
    }

   # Instance Distribution Range
   set group_index 0
   if { [info exists ::cfg_inst_cluster]  && [llength $::cfg_inst_cluster] > 0 } {
    foreach a $::cfg_inst_cluster {
         if {[lindex $a 1] == "module" } {
           foreach module_name [get_db modules [lindex $a 0]] {
              foreach hinst_name [get_db $module_name .hinsts] {
                 set rect [list 10000 10000 0  0]
                 foreach inst_name  [get_db $hinst_name .insts] {
                    if { [regexp "CTS_|fp_FE_" [get_db $inst_name .name]] == 0 } {
                      set x [get_db $inst_name .location.x]
                      set y [get_db $inst_name .location.y]
                      set rect [max_box $x $y $rect]
                    }
                 }
                 set xy [box_dxdy $rect]
                 imsg_id -id UFSM-151 -tl 0 -msg "dx:[lindex $xy 0], dy:[lindex $xy 1] -> module: $hinst_name => rect: [lindex $rect 0] [lindex $rect 0] [lindex $rect 0] [lindex $rect 0]"
              }
           }
         } elseif { [lindex $a 1] == "hinst" } {
           foreach hinst_name [get_db hinsts [lindex $a 0]] {
               set rect [list 10000 10000 0  0]
               foreach inst_name  [get_db $hinst_name .insts] {
                  if { [regexp "CTS_ccl|fp_FE_" [get_db $inst_name .name]] == 0 } {
                    set x [get_db $inst_name .location.x]
                    set y [get_db $inst_name .location.y]
                    set rect [max_box $x $y $rect]
                  }
               }
               set xy [box_dxdy $rect]
               imsg_id -id UFSM-151 -tl 0 -msg "dx:[lindex $xy 0], dy:[lindex $xy 1] -> hinst: $hinst_name => rect: [lindex $rect 0] [lindex $rect 0] [lindex $rect 0] [lindex $rect 0]"
           }
         } elseif { [lindex $a 1] == "inst" } {
           #create_group -name group_${group_index} -type cluster
           imsg_id -id UFSM-151 -tl 0 -msg  "Insts in group_${group_index}"
           foreach inst_name [get_db insts [lindex $a 0]] {
               if { [regexp "CTS_ccl|fp_FE_" [get_db $inst_name .name]] == 0 } {
                  set rect [list 10000 10000 0  0]
                  set x [get_db $inst_name .location.x]
                  set y [get_db $inst_name .location.y]
                  set rect [max_box $x $y $rect]
               }
           }
           set xy [box_dxdy $rect]

           imsg_id -id UFSM-151 -tl 0 -msg "dx:[lindex $xy 0], dy:[lindex $xy 1] -> inst:[lindex $xy 0] => rect: [lindex $rect 0] [lindex $rect 0] [lindex $rect 0] [lindex $rect 0]"

           set group_index [expr $group_index + 1]
         } else {
             imsg_id -id UFSM-151 -tl 0 -msg "eval_cluster_instances: Not recognized type"
         }


      }
   }
}


proc gui_show_cluster { hinst_name} {
   deselect_obj -all
   get_db insts $hinst_name/*  -if { .name != "*CTS_c*" && .name != "*fp_FE_*" && .name != "*DFT_lockup_latch*"  } -foreach {puts $obj(.name)} ]
   select_obj [get_db insts $hinst_name/*  -if { .name != "*CTS_c*" && .name != "*fp_FE_*" && .name != "*DFT_lockup_latch*"} ]
   gui_zoom -selected
}





# ---------------------------------
# displays a flow step header,
# evaluate command
# ---------------------------------

### @@@ DFT: This could be done by "edit_flow"
#
#  create_flow_step -name test_pre -owner tbl {
#      puts "EPC INFO: this is code right BEFORE init_design"
#  }
#  create_flow_step -name test_post -owner tbl {
#      puts "EPC INFO: this is code right AFTER init_design"
#  }
#
#  edit_flow -append  flow_step:test_post -after flow_step:init_design
#  edit_flow -prepend flow_step:test_pre  -before flow_step:init_design
#
#
#  Step status:
#       syn_generic.block_start                    success
#       syn_generic.init_oa                        success
#       syn_generic.hdl_parser                     success
#       syn_generic.init_design.test_pre           success
#       syn_generic.init_design                    success
#       syn_generic.init_design.test_post          success
#       syn_generic.init_design_after_elab         success
#
#  This manual flow changes, can't execute by a procedure,
#  but has to be placed in a separate file which has to be loaded with the flowtool
#


define_proc flow_step_action {
    {start       2  -start       {integer} {optional} {If set to 0, indicates flow step start; if set to 1, end of flow step; else ignore}}
    {flow       ""  -flow        {string}  {required} {Name of current flow}}
    {flow_step  ""  -flow_step   {string}  {required} {Name of current flow_step}}
} {

    # initial settings
    set flow        [string map {"flow:" ""} $flow]
    set flow_step   [string map {"flow_step:" ""} $flow_step]
    set h_flow      [string length $flow]
    set h_flow_step [string length $flow_step]
    set h_length    80

    # display flow step header
    if { $start == 1 | $start == 2 } {
        set start_pos [expr ($h_length / 2) - (($h_flow + $h_flow_step + 1) / 2)]
        set msg    "[string repeat "\#" $h_length]\n"
        append msg "\#[string repeat " " [expr $start_pos - 1]][set flow]:[set flow_step]\n"
        append msg "[string repeat "\#" $h_length]\n"
        puts  $msg
        append_epc_log -msg $msg
    }

    # check if command must be evaluated
    if [info exists ::flow_step_action_list] {
        foreach elem $::flow_step_action_list {
            if { [lindex $elem 0] == $flow_step && ([lindex $elem 1] == $start | $start == 2)} {
                imsg_id -id UFSM-153 -msg -tl 7 -msg "Evaluating user command '[lindex $elem 2]'"
                eval [lindex $elem 2]
            }
        }
    } else {
        puts "SHIT"
        exit -1
    }
}


################################################################################
## Flow Procs: Returns a list of (sub-)flows.
################################################################################

define_proc get_epc_flows -description "Returns a list of (sub-)flows." {
} {
    ## Get full flow:
    set flow_list_name flow_list_[string map {"flow:" ""} [get_db flow_current]]
    
    puts "MCC DEBUG(flow_list_name): $flow_list_name"

    if { [info exist ::$flow_list_name] } {
        set tmp_flow_list [set ::$flow_list_name]
    } else {
        set tmp_flow_list ""; # e.g. when loading layout view of latest design just to have a look at it.
    }
    puts "MCC DEBUG(tmp_flow_list): $tmp_flow_list"

    set tmp_flow_start_index 0
    set tmp_flow_stop_index  [llength $tmp_flow_list]
    if { [get_db flow_step_start] == "" && [get_db flow_step_stop] == "" } {
        ## Debug mode with loading of a database but without execution of sub-flows
        set flow_list {}
    } else {
        ## Normal mode with execution of selected sub-flows:
        foreach subflow {start stop} {
            if { [get_db flow_step_[set subflow]] != "" } {
                set index [lsearch -exact $tmp_flow_list [get_db flow_step_[set subflow]]]
                if { $index == -1 } {
                    ## Sub-flow not found
                    emsg_id_stop -id UFSM-102 -msg "Selected sub-flow '$subflow' not found."
                }
                set tmp_flow_[set subflow]_index $index
            }
        }
        ## Update list of (sub-)flows:
        puts "MCC DEBUG(flow_list) :[lrange $tmp_flow_list $tmp_flow_start_index $tmp_flow_stop_index] "
        puts "MCC DEBUG $tmp_flow_list $tmp_flow_start_index $tmp_flow_stop_index"

        set flow_list [lrange $tmp_flow_list $tmp_flow_start_index $tmp_flow_stop_index]
    }

    ## Load database:
    set database ""; # .db file (Genus) or OA view (Innovus)
    if [info exist ::env(CELL_FLOW_START_DB)] {
        if { $::env(CELL_FLOW_START_DB) != "" } {
            if { [get_db program_short_name] == "genus" } {
                set database [get_db flow_db_directory]/$::env(CELL_FLOW_START_DB)/$::env(CELL_FLOW_START_DB).db
            } elseif { [get_db program_short_name] == "innovus" } {
                set lib_name  [get_db flow_cell_name]_tmp_lib
                set cell_name [get_db flow_cell_name]
                set view_name $::env(CELL_FLOW_START_DB)
                set database [list $lib_name $cell_name $view_name]
            }
        }
    }

    if { $database != "" } {
        if { [get_db program_short_name] == "genus" } {
            if { ![file exists $database] } {
                emsg_id_stop -id USFM-100 -msg "Cannot find database '$database'"
            } else {
                imsg_id -id UFSM-151 -tl 5 -msg "Loading database '$database'..."
                read_db $database
            }
        } elseif { [get_db program_short_name] == "innovus" } {
            if { ![file exists [join "[get_db flow_db_directory] $database" /]] } {
                emsg_id_stop -id USFM-100 -msg "Cannot find OA library (with cell and view) '$database'."
            } else {
                imsg_id -id UFSM-151 -tl 5 -msg "get_epc_flows: Loading OA-database '$database'..."
                read_db -oa_lib_cell_view $database
            }
        }
    }

    ## Overwrite loaded database attributes with current ones such that they are matching to the current flow again:
    set_db flow_step_start         $::env(CELL_FLOW_START)
    set_db flow_step_stop          $::env(CELL_FLOW_STOP)
    set_db flow_current            flow:$::env(CELL_FLOW)
    set_db flow_cell_path          $::env(CELL_PATH)
    set_db flow_cell_cfg_path      [get_db flow_cell_path]/[get_db flow_cell_cfg_dir]
    set_db flow_working_directory  [string map [list "$::env(DIGITAL_WORK_DIR)" "$::env(OBJECTROOT)"] [get_db flow_cell_path]]

    return $flow_list

    
}


################################################################################
## Flow Procs: Reduces number of licenses in use checked out by super-threading to a single one.
################################################################################

define_proc release_super_thread_lics -description "Reduces number of licenses in use checked out by super-threading to a single one." {
} {
    ## Check for multiple checked out licenses. If multiple license are in use, release all except one.
    set lic_release_list {
        Virtuoso_Digital_Implem_XL
    }
    foreach lic_release $lic_release_list {
        ## Get number of checked out licenses:
        set lic_release_number [llength [lsearch -exact -all [get_license_list] $lic_release]]
        ## Check in licenses which should not be used:
        for { set i 1 } { $i < $lic_release_number } { incr i } {
            imsg_id -id UFSM-151 -tl 7 -msg "Releasing license '$lic_release' ..."
            license checkin $lic_release
        }
    }
}


##############################################################################
## Flow Procs: public : Prints the structure of the current super-flow.
##############################################################################

define_proc print_superflow -description "Prints the structure of the current super-flow." {
} {
    set flow  [string map {"flow:" ""} [get_db flow_current]]
    set space [string repeat " " [expr 11 - [string length "$flow ="]]]
    puts "Super-flow: $flow$space = \{[join [set ::flow_list_[set flow]]]\}"
    foreach subflow [set ::flow_list_[set flow]] {
        set space [string repeat " " [expr 11 - [string length "$subflow ="]]]
        puts "(Sub-)flow: $subflow$space = \{[join [set ::flow_step_list_[set subflow]]]\}"
    }
}

##############################################################################
## Flow Procs: public : Generates local CDS library file.
##############################################################################
define_proc generate_cds_lib -description "Generates local CDS library file." {
} {
    imsg_id -id USFM-151 -tl 6 -msg "generate_cds_lib: start"

    ## by default a start of 'cds' will use:
    ## $VSC_RUN_DIR/cds.lib == $ANALOG_HOME/cds.lib
    ##     --> INCLUDE $PROJECT_PATH/common/project-presets/cds.lib

    ## INIT at different stages:
    ## 1. NO cds.lib available   --> get main cds.lib, skip unsupported lines (exclude_pattern)
    ## 2. ILM OA LIBs avalable   --> append the path as DEFINE

    ## Define name in run-directory:
    set cds_lib     cds.lib

    ## Set main_cds    /usr/project-a/[get_db flow_project_name]/common/project-presets/cds.lib
    ## set main_cds    $::env(CDS_PROJECT)/cds.lib
    set main_cds  /usr/project-a/epc909/users/mcc/analog/cds.lib
    
### OBS ## FOR taking a different cds.lib you may change this link (CRITICAL APPROACH)
### OBS set main_cds  $::env(DIGITAL_WORK_DIR)/digital/info/cds_modifed_tech.lib

    if { ![file exists $main_cds] } {
        emsg_id -id USFM-100 -msg "generate_cds_lib: Cannot find main cds.lib '$main_cds'."
    }

    set analog_cds  $::env(ANALOG_HOME)/cds.lib
    if { ![file exists $analog_cds] } {
        emsg_id -id USFM-100 -msg "generate_cds_lib: Cannot find analog cds.lib '$main_cds'."
    }

    # TBD: as well remove the ILM - will be added at the end
    # TBD: This we have to discuss, how to access OA DB for write
    # TBD: NO DESIGN SPECIFIC PARTS  - epc635_adc_analog_ctrl_regs_lib is an exception in trial !
    # HINT: this exclude DOES control the view-level of cell layout in innovus !
    set exclude_pattern {
        connectLib
        $::env(PROJECT)_sim
        epc635_adc_analog_ctrl_regs_lib
        $::env(PROJECT)_LVS
        monitors_ohc15l
        reticle_ohc15l
        optical_ohc15l
    }

    #obs:  create soft link to master cds.lib - make a copy - will be overwritten by write_db
    #obs:  file link -symbolic cds.lib $::env(CDS_PROJECT)/cds.lib
    #obs:  file copy $::env(CDS_PROJECT)/cds.lib .

    ## In case cds.lib file is missing in run directory:
    if { ![file exists $cds_lib] } {
        ## Parse the main cds.lib and remove not supported parts:
        if [catch { set cds_fp [open $cds_lib w] }] {
            emsg_id -id USFM-100 -msg "generate_cds_lib: Could not open $cds_lib for writing"
        }
        ## Add a header
        puts $cds_fp "#-------------------------------------------------------------------------------"
        puts $cds_fp "# file:      cds.lib"
        puts $cds_fp "# create by: flow_step_generate_cds_lib"
        puts $cds_fp "# ref:       $main_cds"
        puts $cds_fp "#"
        puts $cds_fp "# Description"
        puts $cds_fp "# This file provides the paths to the OA-libraries for genus / innovus flow"
        puts $cds_fp "# (including the technology)."
        puts $cds_fp "#"
        puts $cds_fp "# It is created on the base of main cds.lib, but skipp unsoprted libs"
        puts $cds_fp "# the project relevant part for genus / innovus 'read_physical'"
        puts $cds_fp "#-------------------------------------------------------------------------------"
        puts $cds_fp ""

        ## Open the reference cds.lib file for reading:
        if [catch { set main_fp [open $main_cds r] }] {
            emsg_id -id USFM-100 -msg "generate_cds_lib: Could not open $main_cds for reading"
        }

        ## Parse and append each line to local cds.lib if not skipped by exclude_pattern:
        while { [gets $main_fp line ] != -1} {
            set skip "false"
            foreach i [join [subst $exclude_pattern]] {
                if  { [string first $i $line] >= 0} {
                    set skip "true"
                }
            }
            if { $skip == "false" } {
                puts $cds_fp $line
            } else {
                imsg_id -id USFM-151 -tl 8 -msg "generate_cds_lib: skip for local cds.lib: $line "
            }
        }
        ## Reference is not longer required.
        close $main_fp

        if { [get_db program_short_name] == "innovus" } {
            ## Defintion of library corresponding to top level cell of the flow:
            ## NOTE: This can be chip top level or an ILM.
            puts $cds_fp ""
            puts $cds_fp "#-------------------------------------------------------------------------------"
            puts $cds_fp "#-- DEFINITION OF LOCAL OA LIBRARY FOR INNOVUS RUN"
            puts $cds_fp "#-------------------------------------------------------------------------------"
            puts $cds_fp ""
            # working directory on scratch
            set tmp_lib_dir [get_db flow_db_directory]/[get_db flow_cell_name]_tmp_lib;
            puts $cds_fp "DEFINE [get_db flow_cell_name]_tmp_lib $tmp_lib_dir"
            # directory on server where results should be stored (replace env path with env)
            set target_lib_dir [get_db flow_cell_cfg_path]/[get_db epc_flow_place_route_output_directory]/[get_db flow_cell_name]_lib;
            puts $cds_fp "DEFINE [get_db flow_cell_name]_lib     [string map [list $::env(DIGITAL_WORK_DIR) "\$\{DIGITAL_WORK_DIR\}"] $target_lib_dir]"
        }

        ## Add definitions of ILM libraries required in flow top level cell:
        if { [info exists ::cfg_hard_macros] && [llength [dict filter $::cfg_hard_macros value ilm]] != 0 } {
            puts $cds_fp ""
            puts $cds_fp "#-------------------------------------------------------------------------------"
            puts $cds_fp "#-- DEFINITION OF ILM SOURCE LIBRARIES"
            puts $cds_fp "#-------------------------------------------------------------------------------"
            puts $cds_fp ""

            foreach ilm_path [dict keys [dict filter $::cfg_hard_macros value ilm]] {
                # OA library location for each ILM (replace env path with env)
                set loc [file join [subst $ilm_path] [file tail [get_db flow_cell_cfg_path]] [get_db epc_flow_place_route_output_directory]]
                set lib [file tail $ilm_path]_lib
                puts $cds_fp "DEFINE $lib  [string map [list $::env(DIGITAL_WORK_DIR) "\$\{DIGITAL_WORK_DIR\}"] $loc/$lib]"
            }
        }



        ## Add definitions of BlackBox libraries required in flow top level cell:
        if { [info exists ::cfg_hard_macros] && [llength [dict filter $::cfg_hard_macros value bb]] != 0 } {
            puts $cds_fp ""
            puts $cds_fp "#-------------------------------------------------------------------------------"
            puts $cds_fp "#-- DEFINITION OF BB SOURCE LIBRARIES IN DIGITAL DIRECTORY"
            puts $cds_fp "#-------------------------------------------------------------------------------"
            puts $cds_fp ""

            foreach bb_path [dict keys [dict filter $::cfg_hard_macros value bb]] {
              if { [regexp {digital/dev/digital} [subst $bb_path]] } {
                   # OA library location for each ILM (replace env path with env)
                   set loc [file join [subst $bb_path] [file tail [get_db flow_cell_cfg_path]] [get_db epc_flow_place_route_output_directory]]
                   set lib [file tail $bb_path]_lib
                   puts $cds_fp "DEFINE $lib  [string map [list $::env(DIGITAL_WORK_DIR) "\$\{DIGITAL_WORK_DIR\}"] $loc/$lib]"
                }
            }
        }


        ## End of local cds.lib
        close $cds_fp

        if { [get_db program_short_name] == "innovus" } {
            ## Create subdirectory for databases/libraries:
            set lib_name  [get_db flow_cell_name]_tmp_lib
            if { ![file exists [get_db flow_db_directory]/$lib_name] } {
                file mkdir [get_db flow_db_directory]/$lib_name
            }
            if { ![file exists $target_lib_dir] } {
                file mkdir $target_lib_dir
            }
        }
    }
}


##############################################################################
define_proc generate_cpf -description "Parse template.cpf for project and create local version." {
    {cpf "" -cpf_out {string} {required} {target cpf file} }
    {add_lib false -add_lib {bool} {optional} {seach for keyword 'define_library_set' and add available libraries} }
} {
##############################################################################
    ## define global power domains
    ## WARN: (IMPVL-159):	Pin '\gnd! ' of cell 'DCAP_X16' is defined in LEF but not in the timing library.
    ## WARN: (IMPVL-159):	Pin '\vdd! ' of cell 'DCAP_X16' is defined in LEF but not in the timing library.
    ## OBS: imsg_id -id UFSM-151 -tl 3 -msg "init_cpf: define power (currently by comd, later by .cpf file ..."
    ## OBS: set_db init_power_nets  {VDD \vdd! }
    ## OBS: set_db init_ground_nets {VSS \vss! }
    #OBS: ## Define global power domains:
    #OBS: # WARN: (IMPVL-159):	Pin '\gnd! ' of cell 'DCAP_X16' is defined in LEF but not in the timing library.
    #OBS: # WARN: (IMPVL-159):	Pin '\vdd! ' of cell 'DCAP_X16' is defined in LEF but not in the timing library.
    #OBS: imsg_id -id UFSM-151 -tl 3 -msg "init_cpf: defining power (currently by commads, later by .cpf file ..."
    #OBS: set_db init_power_nets  {VDD \vdd! }
    #OBS: set_db init_ground_nets {VSS \vss! }
    #OBS: # TODO: global net connections
    #OBS: # this may be replaced with the global .cpf to get more detail power
    #OBS: # read_power_intent -cpf [get_db flow_cell_cfg_path]/[get_db flow_cell_name].cpf

    ## NOTE: .cpf files only accept full library paths and no external variables.
    ##       Therefore, the generation of a .cpf file is based on parsing a template.

    ## Check for template:
    set cpf_template [file join $::env(TEMP_PATH) template.cpf]
    if { ![file exists $cpf_template] } {
        emsg_id_stop  -id UFSM-101  -msg "generate_cpf: Could not find template: '$cpf_template'."
    }

    ## Define the location for inserting the libraries (use a required keyword):
    set keyword  "generate_cpf"
    set kw_found [expr {!$add_lib}]
    if [catch { set cpf_fp [open $cpf w] }] {
        emsg_id -id USFM-100 -msg "generate_cpf: Could not open $cpf for writing"
    }
    ## add header
    puts $cpf_fp "#-------------------------------------------------------------------------------"
    puts $cpf_fp "# file:      $cpf (Common Power Format)"
    puts $cpf_fp "# create by: flow_procs.tcl: generate_cpf (call in flow_step_init_design) "
    puts $cpf_fp "# ref:       $cpf_template, but added define_library_set, because extern tcl"
    puts $cpf_fp "#            variables are not supported"
    puts $cpf_fp "#-------------------------------------------------------------------------------"
    puts $cpf_fp ""

    ## open reference to be read
    if [catch { set template_fp [open $cpf_template r] }] {
        emsg_id -id USFM-100 -msg "generate_cpf: Could not open $template_fp for reading"
    }
    ## parse each line and append to local cds.lib if not skipped by exclude_pattern
    while { [gets $template_fp line ] != -1} {
        ## search for Keyword and insert section
        if { ($add_lib == "true") && [string first $keyword $line] >= 0 } {
            puts $cpf_fp $line
            puts $cpf_fp "## automatically inserted part by generate_cpf procedure"
            if { [info exist ::cfg_typical_corner_only] && $::cfg_typical_corner_only == true } {
                puts $cpf_fp "define_library_set -name lib_typ  -libraries \"[get_absolute_path_list -env_replace -paths $::lib_search_paths -files $::libs_typ]\""
            } else {
                puts $cpf_fp "define_library_set -name lib_fast -libraries \"[get_absolute_path_list -env_replace -paths $::lib_search_paths -files $::libs_slow]\""
                puts $cpf_fp "define_library_set -name lib_slow -libraries \"[get_absolute_path_list -env_replace -paths $::lib_search_paths -files $::libs_fast]\""
            }
            puts $cpf_fp "## automatically inserted part end"
            set kw_found "true"
        } else {
            puts $cpf_fp $line
        }
    }
    ## Reference is not longer required:
    close $template_fp
    ## Close local cds.lib:
    close $cpf_fp
    ## Check for successfull insertion section at the end, otherwise scrap the generated file:
    if { $kw_found == "false" } {
        file delete -force -- $cpf
        emsg_id_stop -id USFM-100 -msg "generate_cpf: Cound not find keyword '$keyword' in $cpf_template"
    } else {
        imsg_id -id UFSM-151 -tl 4 -msg "generate_cpf: check the log 'Loading CPF file $cpf ...' for details"
    }
}

##############################################################################
## Flow Procs: public : Sets up OA mode for Innovus session.
##############################################################################

define_proc initialize_oa -description "Sets up OA mode for Innovus session." {
} {
    # ensure 100% interoperability of design data between Innovus and Virtuoso
    # refer to Cadence (Bojan):
    # The OA cellview is locked between the restore and save steps with "-updateMode true".
    # It provides the ability to import an OA cellview created from Virtuoso that contains objects
    # which are not understood by Innovus (guard rings etc.).
    # Innovus displays only shapes for these objects, but they can't be edited. When the digital part in Innovus
    # is completed, the tool updates only the objects modified by Innovus in the original OpenAccess cellview.
    #
    # It doesn't refer to the libs defined in cds.lib, only the data stored in OA DB cellview for the design.
    # It means that your analog team can still modify libraries, but can't modify the same OA DB cellview
    # at the same time.
    set_db oa_update_mode true                      ;# correct processing of customized cells in innovus db, default false
    ;# for read_db and write_db -oa_lib_cell_view
    ;# need to be set BEFORE importing OA design
    ;# WILL AUTO-LOOK THE OA DATABASE on restore while in innovus session


    set_db oa_use_virtuoso_color true               ;# use display colors of virtuoso, default false

    # a local cds.lib is required on launch directory
    # cds.lib shal contain all lib pointers (std cells, IOs, macros) in OA format (abstract, layout, symbol, ... data.dm):
    #
    # INCLUDE $TECH_PATH/cds.lib        # defines: ohc15l, ohc15l_l, ohc15l_xl
    #                                   # technology (BJT, CAP, DIO; MOS, RES, VIAS ...), tech.db, ...
    #
    # INCLUDE $STDCELL_PATH/cds.lib     # defines: ohc15l_digital
    #                                   # all digital cells (AO21 .. ANTENNA .. DCAP .. FILLCELL ... )
    #
    # what about
    # $IOLIB_PATH/cds.lib               # defines: ohc15l_io
    #                                   # all IO cells (PAD.. aio.. blank.. breaker.. esd.. gpio.. i2c.. iso.. vdd.. vss.. xtal..)
    # $ANALOG_WORK_DIR/*                # project specific OA cells
    #
    #
    # As long as no 'setup.loc' is defined, Cadence will take default search order for cds.lib in
    # .                   current directory
    # @LIBRARY            libs listed in the lib definition file    auto, reserved keyword
    # $CDS_WORKAREA*      user workarea                             not defined by EPC
    # $CDS_SEARCHDIR*     no longer set by applications             not defined by EPC
    # $HOME               home directory                            $HOME
    # $CDS_PROJECT*       project storage area                      EPC project setup will place here the main cds.lib
    #                                                               --> this will define all libs: tech, digital cells, IO, macros

    # Each reference library is processed using the abstract view name list ( init_oa_abstract_views ).
    # Example: ref-lib = "lib1 lib2", abstract-view = "abstract  abstract2"
    #          LEF MACRO information is processed for lib1 with the abstract view.
    #          Then, for any cells in lib1 that do not have abstract but do have abstract2,
    #          that view is processed for MACRO information.
    #          If a cell has both views, the first one is used.
    #          The process then is repeated for lib2.

    set_db init_oa_abstract_views abstract                  ;# Specifies OA view names to examine to find the equivalent LEF MACRO information (for example, PINS, OBS, FOREIGN).
                                                            ;# default is null ( "" ), which means, the tool uses 'abstract' as the value.

    set_db init_oa_layout_views layout                      ;# Specifies the list of OA view names to be processed for cell layout viewing (GDSII equivalent). Default {}

    ## Other settings:
    # set_db init_oa_design_lib <libname>                     ;# for OA design read (instead of netlist) lib  name to be used. Default {}
    # set_db init_oa_design_cell [get_db flow_cell_name]      ;# for OA design read (instead of netlist) cell name to be used. Default {}
    # set_db init_oa_design_view layout                       ;# for OA design read (instead of netlist) view name to be used. Default {}

    # set_db init_oa_search_libs {}                           ;# ref libs for cell search in design, which are not in init_oa_ref_libs

    # set_db init_oa_default_rule                             ;# override default contraint group, need to contain: vlaid layers and vias, width, spacing
    # set_db init_oa_special_rule                             ;# defines new contraint group for sroute

    ## A non-unique netlist with repeated modules cannot be modified or optimized. Hence, we uniquify it.
    ## If init_design_uniquify is set to 1, a module that is used more than once in the netlist will
    ## be made unique during init_design.
    set_db init_design_uniquify 1

    ## Mapping of analysis views between top cell and ILMs:
    if { ([info exists ::cfg_hard_macros] && [llength [dict filter $::cfg_hard_macros value ilm]] > 0) && [info exist ::cfg_analysis_view_mappings_top_to_ilm] } {
        foreach elem $::cfg_analysis_view_mappings_top_to_ilm {
            eval_legacy "setIlmMode  -map view  -top [lindex $elem 0]  -ilmCell [lindex $elem 1]  -ilm [lindex $elem 2]"
        }
    }

    set_db ilm_keep_async true
    set_db ilm_keep_loopback true

}


##############################################################################
## Flow Procs: public : Runs an epc_flow.
##############################################################################
define_proc run_epc_flow -description "Runs an epc_flow.\nNOTE: This command must not be confused with the Cadence command run_flow." {
    {flow "" {epc_flow} string required {epc_flow to be executed. Syntax: <my_flow[.my_flow_step]>}}
} {

    ## Make sure that hierarchical flow step depth is not too high:
    if { [llength [split $flow "."]] > 2 } {
        emsg_id -id UFSM-103 -msg "Hierarchical flow step depth is too high."
        return
    }
	
    puts "MCC DEBUG(FLOW): $flow"

    ## Extract (sub-)flow and flow step.
    ## NOTE: A flow step is not required, i.e. $flow_step can be an empty string.
    set subflow   [lindex [split $flow "."] 0]
    set flow_step [lindex [split $flow "."] 1]

 puts "MCC DEBUG(subflow): $subflow"
 puts "MCC DEBUG(flow_step): $flow_step"

    ## Check whether a flow step is specified. If specified, check whether it is valid:
    if { $flow_step != "" } {
        if { [lsearch -exact [set ::flow_step_list_[set flow]] $flow_step] == -1 } {
            emsg_id -id UFSM-103 -msg "Flow step mismatch: '$flow_step' is not defined."
        }
    }

    ## Define flow:
    set_db epc_flow $subflow
    ## Print (sub-)flow:
    puts  [string repeat "\#" 80]
    puts "\# Starting flow [string toupper $subflow] ..."
    puts  [string repeat "\#" 80]

    ## Check whether a flow step is specified:
    if { $flow_step != "" } {
        ## Flow step specified -> execute only that one.
        set flow_steps $flow_step
    } else {
        ## Flow step not specified -> execute full set of flow steps defined for superordinate (sub-)flow.
        set flow_steps [set ::flow_step_list_[set subflow]]
    }

    puts "DEBUG flow_steps: $flow_steps"

    # eval command and check for errors, report them with a trace
    foreach flow_step $flow_steps {

        puts "DEBUG flow_step: $flow_step"
        puts  [string repeat "\*" 80]
        puts "\* Starting flow step [string toupper $flow_step] ..."
        puts  [string repeat "\*" 80]

        set out [catch {eval flow_step_$flow_step} msg]

        if {$out} {
            #puts -id "UFSM-103" -msg "Flow step error in '$flow_step', error trace:"
            puts "EPC ERROR: Flow step error in '$flow_step', error trace:"
            puts "$::errorInfo"
            puts "EPC ERROR: CHECK ERROR ABOVE."
            return -code error "error during evaluation of 'flow_step_$flow_step'"
        }
    }
}

##############################################################################
## Flow Procs: public : Create an absolute <path>/<file> stringlist.
##############################################################################
define_proc get_absolute_path_list -description "create an absolute <path>/<file> stringlist for .tcl files" {
    {paths  "" -paths {string} {required} {path(s) where the files are expected} }
    {f_list "" -files {string} {required} {expected files in the paths} }
    {replace false -env_replace {bool} {optional} {add to replace env variables for tcl syntax} }
} {
    ## create the lib file list with absolute path for innovus style: {path/file1 path/file2 ... }
    set file_list_absolute ""
    ## try for all paths
    foreach p [subst $paths] {
        ## check each file create slow lib
        foreach f [subst $f_list] {
            # when file exist and not already in the list
            if { [file exists "$p/$f"] && [string first "$p/$f" $file_list_absolute] == -1 } {
                # replace known environment globals in .tcl style:
                set env_file "$p/$f"
                if {$replace} {
                    foreach e_list {STDCELL_PATH STDCELL_PATH} {
                        if {[info exist ::env($e_list)]} {
                            # extern-style: set env_file "[string map [list $::env($e_list) "\$\{$e_list\}"] $env_file]"
                            set env_file "[string map [list $::env($e_list) "\$::env($e_list)"] $env_file]"
            }
        }
    }
                # # STDCELL_PATH
                # set env_file "[string map [list $::env(STDCELL_PATH) "\$\{STDCELL_PATH\}"] $env_file]"
                # # TECH_PATH
                # set env_file "[string map [list $::env(TECH_PATH) "\$\{TECH_PATH\}"] $env_file]"
                lappend file_list_absolute $env_file
            }
        }
    }
    imsg_id -id UFSM-151 -tl 8 -msg "get_absolute_path_list: create new filelist: '[join $file_list_absolute " "]'"
    # return as string list to be able to resolve $::env()
    return [join $file_list_absolute " "]
}


##############################################################################
## Flow Procs: public : Write hard macro descriptions
##############################################################################
define_proc generate_hard_macro_files  -description "Generates an abstract, liberty files and optionally an interface logic model (ILM)." {
} {

    ## Write abstract and liberty files if cell is not a chip top level cell:
    if { !$::cell_is_top_level }  {

        set lib_name  [get_db flow_cell_name]_tmp_lib
        set cell_name [get_db flow_cell_name]
        set view_name abstract
        set oa        [list $lib_name $cell_name $view_name]

        ## Write ABSTRACT for the hard macro:
        # check for antenna (required) and determine density (optional)
        check_process_antenna  \
            -lef_file [file join [get_db epc_flow_result_directory] [get_db flow_cell_name]_ilm_antenna.lef] \
            -out_file [file join [get_db flow_report_directory] [get_db flow_report_name] [get_db flow_cell_name]_ilm_antenna.rpt]
        ## NOTE: LEF file generation is actually not required in epc flow and will even be
        ##       removed in future Innovus releases. However, it cannot be disabled currently.

        ## Metal density checks should not be performed because no requirements are set right now.
        ## However, please note that we are not doing the metal filling in Innovus but later during the merging flow.
        # check_metal_density  -report [file join [get_db flow_report_directory] [get_db flow_report_name] [get_db flow_cell_name]_ilm_metal_density.rpt]

        #set lef [get_db flow_db_directory]/ilm.lef
        #write_lef_abstract $lef -cut_obs_min_spacing -extract_block_pg_pin_layers {1 2} -stripe_pins -pg_pin_layers {5}
        # extern: exec lef2oa ...

        ## limit routing layer for abstract generation
        ## (checks for valid value is performed in attributes setting in flow_step_init_design
        if { [info exists ::cfg_top_routing_layer] } {
            set top_layer $::cfg_top_routing_layer
        } else {
            set top_layer 6
        }
        ## extract pin MET1 for abstract
        if { [info exists ::cfg_abstract_with_rail_pins] } {
            set stripe_pin "all"
        } else {
            set stripe_pin "top"
        }
        ## Hint: -cut_obs_min_spacing  seams to minmize the spacing, which generate too small power spacings on the pins of half rails
        ##       -extract_block_pg_pin_layers : removed due to **ERROR: (IMPOAX-26):	Could not create oaPin object for Terminal 'vssd'. OA Exception
        ##       Abstract in Virtuoso looks ok (power rails extracted over full width)
        write_oa_abstract  -top_layer $top_layer -oa_lib $lib_name  -oa_view $view_name  -cut_obs -stripe_pin $stripe_pin

        ## Run verilogAnnotate to set terminal attributes required to use generated abstract in top level Innovus flow.
        imsg_id -id UFSM-151 -tl 3 -msg "generate_hard_macro_files: Running verilogAnnotate ..."
        if { [catch [list exec verilogAnnotate  -refLibs $lib_name  -refViews "abstract layout"  -verilog [get_db epc_flow_result_directory]/[get_db flow_cell_name].v] msg] } {
            emsg_id  -id USFM-104  -msg "generate_hard_macro_files: verilogAnnotate failed with:\n---------- START OF MESSAGE-----------\n$msg\n---------- END OF MESSAGE -------------\n"
        }

        if { ![info exists ::cfg_generate_ilm] || $::cfg_generate_ilm == "true" }  {
            ## Create additional ILM timing data (.gz): .v, .def, .place, .sdc. .spef
            ## NOTE: Cell view needs to be existing already, otherwise you get:
            ##       ERROR: (IMPILM-475): Could not find view paths <lib>/<cell>/<non_data_view>.
            write_ilm  -oa_cellview $oa  -type_flex_ilm ilm  -opt_stage postCTS
        }

        ## Copy the OA abstract from the working directory on scratch to the CELL directory on the server:
        set src_dir [get_db flow_db_directory]/$lib_name/$cell_name/$view_name
        set dst_dir [get_db flow_cell_cfg_path]/[get_db epc_flow_place_route_output_directory]/[get_db flow_cell_name]_lib/$cell_name

#        if { [file exists $dst_dir/$view_name] } {
#            file delete -force -- $dst_dir/$view_name
#        } elseif { ![file exists $dst_dir] } {
#            file mkdir $dst_dir
#        }
#
#        file copy -force -- $src_dir $dst_dir

        ## Generate liberty (.lib) files for active analysis views (if not disabled).
        ## We need to make sure that liberty files are not duplicated due to different modes.
        ## Hence, liberty files are only generated for analysis views 'av_main', 'av_main_func' or 'av_func_X'.
        if  { ![info exists ::cfg_liberty_file_generation_disabled] || $::cfg_liberty_file_generation_disabled == false } {

            ## Finally create a liberty file:
            ## -> Check for timing failure to gurantee correct liberty output.
            check_timing
            ## Create all the required corner models (slow fast).
            ## The mode is irrelevant (identical liberty), just take <func>
            ##
            ## output:
            ##      <CELL_NAME>_<speed>.lib                        liberty
            ##      model.asrt.<analysis_view>                     IO sdc
            ##      model.asrt.<analysis_view>.latchInferredMCP    ???
            ##
            ## HINT:
            ## -include_power_ground: create 'pg_pin(){ direction : inout;}'
            ##
            set view_name timing
            set target_dir [file join [get_db flow_db_directory] $lib_name $cell_name $view_name]
            if { ![file exists $target_dir] } {
                file mkdir $target_dir
            }

            ## Very strange effect on write_timing_model -max_num_loads 16 -max_num_transitions 16 :
            ## default in innovus 18.10: 64, default in 18.12: 32
            ## > lib file get the tripple size 2.5 GB (slow/fast) instead of 800 MB
            ## > runtime 4:10h per liberty
            ## > massive lower delay timing (max 2ns instead 3.5ns
            ## Quite unexpected refer to documentation
            get_db [get_db analysis_views -if {.is_setup||.is_hold}] .name -foreach {
                if { [regexp {(func|main)} $object] } {
                    set_analysis_view  -setup $object  -hold $object;
                    wmsg_id -id USFM-150  -msg "proc generate_hard_macro_files: modify prameters: write_timing_model -max_num_loads 8 -max_num_transitions 8 (default both 32)"
                    write_timing_model $target_dir/[get_db flow_cell_name]_[lindex [split $object _] end].lib \
                        -max_num_loads 8 -max_num_transitions 8 \
                        -include_power_ground  -view $object
                }
            }

            ## Copy the generated liberty files from the working directory on scratch to the CELL directory on the server:
            set src_dir $target_dir
            set dst_dir [get_db flow_cell_cfg_path]/[get_db epc_flow_place_route_output_directory]/[get_db flow_cell_name]_lib/$cell_name/$view_name
            if { ![file exists $dst_dir] } {
                file mkdir $dst_dir
            }
            file copy -force -- {*}[glob -directory $src_dir [get_db flow_cell_name]*.lib] $dst_dir
        } else {
            wmsg_id -id USFM-150  -msg "proc generate_hard_macro_files: ****************************************************************************"
            wmsg_id -id USFM-150  -msg "proc generate_hard_macro_files: CRITICAL: Liberty generation DISABLED (cfg_liberty_file_generation_disabled)"
            wmsg_id -id USFM-150  -msg "proc generate_hard_macro_files: ****************************************************************************"
        }

    }; # if { !$::cell_is_top_level }

}


##############################################################################
## Flow Procs: public : Report FFs
##############################################################################
define_proc report_count_sequential  -description "Reports the number if sequential elements per level." {
    {max_level     4          -max_level   {integer} {optional} {max level to report (default 4)}}
    {min_ff        0          -min_ff      {integer} {optional} {min amount of ff to report instance (default 0)}}
    {quiet     false          -quiet   {bool} {optional} {do not print anything}}
} {
    set cnt [llength [get_db designs .insts -if .is_sequential]]
    set level 1
    set content ""
    if {! $quiet } {
        puts "[string repeat .. [expr $level -1]]($level) top: $cnt"
    }
    append content "[string repeat .. [expr $level -1]]($level) top: $cnt\n"
    set top_hinsts [get_db designs .local_hinsts]
    append content [_report_number_of_ffs $top_hinsts $max_level $min_ff $quiet]
    return $content
}
# uxiliary proc
proc _report_number_of_ffs {name {level_max 4} {ff_min 0} {quiet false}} {
    set content ""
    foreach d $name {
        set cnt [llength [get_db $d .insts -if .is_sequential]]
        #set cnt 0
        #get_db $d .insts -if .is_sequential -foreach {incr cnt}
        set level [llength [file split $d]]
        if { $cnt >= $ff_min} {
            if {! $quiet} {
                puts "[string repeat .. [expr $level -1]]($level) [file tail $d]: $cnt"
            }
            append content "[string repeat .. [expr $level -1]]($level) [file tail $d]: $cnt\n"
        }
        #puts "Get hints for '$d'"
        set hints [get_db $d .local_hinsts]
        if {$level < $level_max } {
            append content [_report_number_of_ffs $hints $level_max $ff_min $quiet]
        }
    }
    return $content
}



##############################################################################
## Flow Procs: public : Copy the final database from the local scratch to the server.
##############################################################################
define_proc copy_oa_view_to_server  -description "Copies the final database from the local scratch to the server." {
    {view_src     ""          -view_src   {string} {required} {define source view name}}
    {view_dst     ""          -view_dst   {string} {required} {define destination view name}}
} {
    #### Copy the final database from the local scratch to the server:
    set lib_name  [get_db flow_cell_name]_tmp_lib
    set cell_name [get_db flow_cell_name]
    set view_name $view_src

    set src_dir [get_db flow_db_directory]/$lib_name/$cell_name/$view_name
    set dst_dir [get_db flow_cell_cfg_path]/[get_db epc_flow_place_route_output_directory]/[get_db flow_cell_name]_lib/$cell_name
    imsg_id -id UFSM-151 -tl 7 -msg "proc copy_oa_view_to_server: \n      from : $src_dir \n      to   : $dst_dir/$view_dst"

    ## check if source directory exists
    if ![file exists $src_dir] {
        if { $view_src != "abstract" || $::cell_is_top_level == 0 }  {
            emsg_id  -id USFM-107  -msg "copy_oa_view_to_server: Source directory '$src_dir' not found"
            return
        }
    }

    ## Forcefully delete the previous version of view_dst (and view_src) in destination directory
    ## then copy the view and rename it:
    file delete -force $dst_dir/$view_src
    file delete -force $dst_dir/$view_dst
    file mkdir $dst_dir; # Just to be sure that the directory exists.
    file copy $src_dir $dst_dir
    if { $view_name != $view_dst } {
        file rename -force -- $dst_dir/$view_name $dst_dir/$view_dst
    }

    ## Copy data.dm from temporary library to destination library:
    set src_file [get_db flow_db_directory]/$lib_name/data.dm
    set dst_file [get_db flow_cell_cfg_path]/[get_db epc_flow_place_route_output_directory]/[get_db flow_cell_name]_lib/data.dm
    ## Due to .nfs lock file try to copy:
    if { [catch {file copy -force $src_file $dst_file} msg] } {
        ## Otherwise, try to delete it first:
        if { [catch {file delete -force $dst_file} msg] } {
            emsg_id  -id USFM-104  -msg "copy_oa_view_to_server: Post-processing (rm data.dm) failed because:\n$msg"
        }
        if { [catch {file copy -force $src_file $dst_file} msg] } {
            emsg_id  -id USFM-104  -msg "copy_oa_view_to_server: Post-processing (cp data.dm) failed because:\n$msg"
        }
    }
}

##############################################################################
## POWER ROUTING procs for FLOORPLANNING POWER
##############################################################################

## GENERAL procs:
define_proc set_all_metals_and_vias_selection -description "Set all wires and vias for selection." {
    {sel 0 -select  {integer}  {required} {set '0' for deselect, '1' for select}}
    {vis 1 -visible {integer}  {optional}  {set '0' for visible off, '1' for visible (default)}}
} {
    # all metals
    foreach obj {M0 M1 M2 M3 M4 M5 M6} {
        set_layer_preference all${obj} -is_visible $vis -is_selectable $sel
    }
    # all Contacts
    foreach obj {M1 M2 M3 M4 M5 M6} {
        set_layer_preference all${obj}Cont -is_visible $vis -is_selectable $sel
    }
}


define_proc select_wires_and_vias -description "Select dedicated wire and or via." {
    {metals "" -metals {string}  {optional} {list of metals MET1, m1, 1, .. TMET to enable selction}}
    {vias   "" -vias   {string}  {optional} {list of vias VIA1, via12, 12 .. VIA5 to enable selction}}
} {
    # metal selection
    foreach obj $metals {
        set i [string tolower $obj]
        if { $i == 1 || $i == "m1" || $i == "met1" } {
            set_layer_preference allM1 -is_visible 1 -is_selectable 1
        }
        if { $i == 2 || $i == "m2" || $i == "met2" } {
            set_layer_preference allM2 -is_visible 1 -is_selectable 1
        }
        if { $i == 3 || $i == "m3" || $i == "met3" } {
            set_layer_preference allM3 -is_visible 1 -is_selectable 1
        }
        if { $i == 4 || $i == "m4" || $i == "met4" } {
            set_layer_preference allM4 -is_visible 1 -is_selectable 1
        }
        if { $i == 5 || $i == "m5" || $i == "met5" } {
            set_layer_preference allM5 -is_visible 1 -is_selectable 1
        }
        if { $i == 6 || $i == "m6" || $i == "met6" || $i == "tmet" } {
            set_layer_preference allM6 -is_visible 1 -is_selectable 1
        }
    }

    # via selection
    foreach obj $vias {
        set i [string tolower $obj]
        if { $i == 12 || $i == "via1" || $i == "via12" } {
            set_layer_preference allM2Cont -is_visible 1 -is_selectable 1
        }
        if { $i == 23 || $i == "via2" || $i == "via23" } {
            set_layer_preference allM3Cont -is_visible 1 -is_selectable 1
        }
        if { $i == 34 || $i == "via3" || $i == "via34" } {
            set_layer_preference allM4Cont -is_visible 1 -is_selectable 1
        }
        if { $i == 45 || $i == "via4" || $i == "via45" } {
            set_layer_preference allM5Cont -is_visible 1 -is_selectable 1
        }
        if { $i == 56 || $i == "via5" || $i == "via56" || $i == "tvia" } {
            set_layer_preference allM6Cont -is_visible 1 -is_selectable 1
        }
    }
}


define_proc route_start_defaults -description "set a dedicated set routing start conditions." {
} {
    # gui_set_tool add_wire
    # set_db edit_wire_drc_on {0}                     ; # disable auto DRC check on routing, would consume a lot of memory
    # set_db edit_wire_drc_on {1}                     ; # is default: @@@ TRIAL TBD
    # set_db edit_wire_use_check_drc false            ; # can be used to turn off FGC (full geometric check).
    gui_set_tool addWire                            ; # or this set wire tool  TBD @@@
    set_db edit_wire_shape {stripe}
    set_db edit_wire_type special                   ; # define special route
    set_db edit_wire_spacing_horizontal {2}         ; # save default for power TMET
    set_db edit_wire_spacing_vertical {2}           ; # (for <TMET 1um would be fine)
    set_db edit_wire_allow_45_degree {0}            ; # avoid 45deg route when possible (issue with DRC clean via setting)
    set_db edit_wire_align_wire_at_pin {1}          ; # may snap to pin center
    set_db edit_wire_snap_to {pg pin special}       ; # works for "non-core" pins, don't try to align on any row, but to special to get clean connections
    set_db edit_wire_look_up_layers {0}             ; ###TRIAL reduce to 1, to avoid shorts by via creation due to drc check off (default 10)
    set_db edit_wire_look_down_layers {0}           ; ###TRIAL reduce to 1, to avoid shorts by via creation due to drc check off (default 10)
    set_db edit_wire_orthogonal_connection_only {1} ; ###TRIAL create via on change layer while use the same direction, system default is '0', but prefer to make too much vias than to less
    set_db edit_wire_create_via_on_pin {0}          ; # supress via creation on IO pin, to avoid wrong stacking vias (no via view in abstracts)
    set_db edit_wire_create_crossover_vias {0}      ; ###TRIAL connect stripes of same nets automatically
    # global set: set_db edit_wire_split_wide_wires {0}           ; # don't use this default setting - kills the memory

    # # V17.10: innovus -stylus:
    # edit_set_route -shape {stripe}
    # edit_set_route -spacing_horizontal {2}         ; # save default for power TMET
    # edit_set_route -spacing_vertical {2}           ; # (for <TMET 1um would be fine)
    # edit_set_route -allow_45_degree {0}            ; # avoid 45deg route when possible (issue with DRC clean via setting)
    # edit_set_route -align_wire_at_pin {1}          ; # may snap to pin center
    # edit_set_route -snap_special_wire {1}          ; # works for "non-core" pins, don't try to align on any row, but to special to get clean connections
    # edit_set_route -look_up_layers {10}            ; # by default connect all layers
    # edit_set_route -look_down_layers {10}          ; # by default connect all layers
    # edit_set_route -orthogonal_connection_only {0} ; # create via on change layer while use the same direction, system default is '0', but prefer to make too much vias than to less
    # edit_set_route -create_via_on_pin {0}          ; # supress via creation on IO pin, to avoid wrong stacking vias (no via view in abstracts)
    # edit_set_route -create_crossover_vias {1}      ; # connect stripes of same nets automatically
}

define_proc route_end_defaults -description "Set a dedicated set routing end conditions." {
} {
    #own proc:  allWiresVSviasV         ; # enable all wires
    gui_set_tool select     ; # back to selection
    gui_deselect -all
    # set_db edit_wire_drc_on {1}     ; # set back auto DRC check to default (only useful for manual routing due to power consumption)
    # set_db edit_wire_drc_on {0}                     ; # disable auto DRC check on routing, would consume a lot of memory
}


##############################################################################
## POWER ROUTING : Interconnect main power rails (timeconsuming)
##############################################################################
define_proc interconnect_power_ring -description "Cut a hole in the main power rails - version with MISSING LONGTITUDINAL CUT OF WIRES in innovus 18.x" {
    {side   ""  -side     {string}  {required} {[t|b|l|r]           select top, bottom left right side}}
    {metal  ""  -metal    {string}  {required} {[MET1..MET5|TMET]   one didicated metal}}
    {rail   ""  -rail     {string}  {required} {[0|1|2|3|4] from outer rail to inner rail}}
    {center ""  -center   {string}  {required} {center location of the cut from left chip edge (side t/b) or bottom chip edge (side l/r)}}
    {width  ""  -width    {string}  {required} {width of the cut, ortogonal to the rail}}
    {length ""  -length   {string}  {required} {lenght of the cut, in rail direction}}
    {pos    ""  -position {string}  {required} {[center | inner | outer] position of the hole refer to side}}
    {cutgap 0.0 -cutgap   {string}  {optional} {additional gap around the hole}}
    {fill   ""  -fill     {string}  {optional} {fill the hole with a dedicated netname, requires -cutgap}}
    {save_off false -save_off {bool}    {optional} {default act only on "metal" layer, otherwise user need to control}}
    {orthogonal false -orthogonal {bool}    {optional} {add filler as ortogonal route (finalize by 'update_power_vias -add_vias {1} -orthogonal_only {1}')}}
    {force_via  false -force_via  {bool} {optional} {MISSING option to create via on the 'fill' to upper and lower layer by 'update_power_vias -orthogonal_only {0}'}}
    {show   {}  -show     {}        {optional} {show detail view of planned hole cut, without performing}}
} {
    ## INFO:
    ## cut out a hole in one of the main power rails and add a route (filler) if requested
    ## First cut a piece by cross cuts, then eighter
    ## 1. resize-move-copy-move:
    # edit_update_route_width -width_horizontal $new_width -width_vertical $railW     ; # resize to center
    # edit_move_routes -dx 0 -dy [expr ($new_width - $railW)/2]                       ; # shift to lower side
    # edit_duplicate_routes -layer_horizontal $metal                                  ; # dublicate snippet
    # edit_move_routes -dx 0 -dy [expr $railW - $new_width]                           ; # move to upper side
    ## or 2. remove-draw1-draw2 (used version)
    ## This was required because wire longitudinal cuts is not longer supported
    ##
    ## Sample:
    ## interconnect_power_ring -side b -metal vdd018 -rail 0 -center 300 -width 8 -length 13 -pos center -show
    ##
    ## Hint:
    ## The rerouting should be done along the rails direction, for potential further rail-cuts
    ## for a version wihtout via (run update_power_vias at the end) the reconnect metal piece in the
    ##     center, shall be route ortogonal (to get a via array)

    ## define main parameters (fix or db)
    set gap 2
    set all_rails 5     ; # count of rails
    if { ![info exists ::cfg_SEALRING] } {
        emsg_id_stop -id USFM-100  -msg "proc interconnect_power_ring: No cfg_SEALRING defined"
    } else {
        set sr $::cfg_SEALRING
    }
    set chipX [get_db [get_db designs] .bbox.ur.x]
    set chipY [get_db [get_db designs] .bbox.ur.y]
    set core2edge [get_db [get_db designs] .core_bbox.ll.x]
    set ringW [expr $core2edge - $sr]
    set railW [expr $ringW/$all_rails - $gap]
    set net "undefined"

    ## check for correct parameters, otherwise show help
    if { [string length $metal] != 4 || [string first $metal "MET1 MET2 MET3 MET4 MET5 TMET"] < 0 } {
        emsg_id -id USFM-100  -msg "interconnect_power_ring: metal not correct defined, use: 'MET1' | 'MET2' | 'MET3' | 'MET4' | 'MET5' | 'TMET'"
        set show "true"
    }
    if { !$save_off } {
       ## select only dedicated layer, gurantee nothing else is affected
       set_all_metals_and_vias_selection -select 0 -visible 0
       select_wires_and_vias -metals $metal
       deselect_obj -all
    }

    if { [string is integer $rail] } {
        if { $rail < 0 || $rail > [expr $all_rails - 1] } {
            emsg_id -id USFM-100  -msg "proc interconnect_power_ring: rail expects rail from outer (0) to inner (4) rail"
            set show "true"
        }
        # combine check side and set absolute startpoint for hole cut: lower or leftmost edge of the rail (which is outer to inner)
        switch -exact $side {
            b       {set edgeLL [expr $sr + $rail*($railW + $gap)]}
            t       {set edgeLL [expr $chipY - $sr - ($rail+1)*($railW + $gap) + $gap]}
            l       {set edgeLL [expr $sr + $rail*($railW + $gap)]}
            r       {set edgeLL [expr $chipX - $sr - ($rail+1)*($railW + $gap) + $gap]}
            default {
                emsg_id -id USFM-100  -msg "interconnect_power_ring: side not correct defined, use 't' | 'b' | 'l' | 'r'"
                set show "true"
            }
        }
    }
    if { $width >= $ringW } {
        emsg_id -id USFM-100  -msg "proc interconnect_power_ring: the hole punsh (width = $width um) will fully cut the rail $rail"
    } elseif { $width > $ringW/2 } {
        wmsg_id -id USFM-150  -msg "proc interconnect_power_ring: the hole punsh (width = $width um) into rail $rail is quite wide"
    }
    if { $center < $core2edge || ($center > [expr $chipX - $core2edge] && [string first $side "tb"] >= 0)  || ($center > [expr $chipY - $core2edge] && [string first $side "lr"] >= 0)} {
        emsg_id_stop -id USFM-100  -msg "proc interconnect_power_ring: center ($center) out of chip core range $core2edge to [expr $chipX - $core2edge] / [expr $chipY - $core2edge] um"
    }
    if { ($width > [expr $chipX - $core2edge] && [string first $side "tb"] >= 0)  || ($width > [expr $chipY - $core2edge] && [string first $side "lr"] >= 0)} {
        emsg_id_stop -id USFM-100  -msg "proc interconnect_power_ring: width value ($width) out of chip core range 0.0 to [expr $chipX - $core2edge] / [expr $chipY - $core2edge] um"
    }
    if { $pos != "center" && $pos != "inner" && $pos != "outer"  && $pos != "center_invert" } {
        emsg_id_stop -id USFM-100  -msg "proc interconnect_power_ring: position $pos unknown, need to be one of 'center' | 'inner' | 'outer'"
    }

    ## get net_name of current layer by selecting the planned hole by line
    gui_set_tool select                                             ; # change mode (optional?)
    if { $side == "t" || $side == "b" } {
        gui_select -line [list $center [expr $edgeLL + $railW/2 - $width/2] $center [expr $edgeLL + $railW/2 + $width/2]] ; # select by center
    } else {
        gui_select -line [list [expr $edgeLL + $railW/2 - $width/2] $center [expr $edgeLL + $railW/2 + $width/2] $center] ; # select by center
    }
    # check for consistent net name (one or all the same)
    set net [get_db selected .net.name]                             ; # to identify the net_name of current metal
    if { $net == "" } {
        emsg_id_stop -id USFM-100  -msg "proc interconnect_power_ring: No net name found on $side side rail $rail at position $center"
    } elseif { [llength $net] != [llength [lsearch -all $net [lindex $net 0]]] } {
        emsg_id_stop -id USFM-100  -msg "proc interconnect_power_ring: Inconsistant net name ($net) found on $side side rail $rail at position $center"
    }

    ## checks for filling
    if { $fill != "" } {
        ## filler on center with non identical net_names, expect -cutgap
        if { $cutgap == 0 && $pos == "center" && $net != $fill } {
            emsg_id -id USFM-100  -msg "proc interconnect_power_ring: Filler $fill on different net ($net) requires a -cutgap"
            return
        } elseif { $pos != "center" } {
            wmsg_id -id USFM-150  -msg "proc interconnect_power_ring: Filling only supported with '-position center'"
        }
    }

    ## info summary
    imsg_id -id UFSM-151 -tl 9 -msg "proc interconnect_power_ring: On $side side at rail $rail of $metal at $center um, $width um wide, from top $fill to down $net"

    # help
    if { $show != "" } {
        puts " "
        puts "INFO: please use this parameters for interconnect_power_ring:"
        puts " "
        puts "      -side     t|b|l|r   select top, bottom left right side"
        puts "      -metal    METx      one didicated metal: MET1, MET2, MET3, MET4, MET5, TMET"
        puts "      -rail     rail      integer for rail number for outer '0' to innermost '4'"
        puts "      -center   um        center of the cut (X-value for top or bottom)"
        puts "                                            (Y-value for left or right side)"
        puts "      -width    um        width of the cut"
        puts "      -length   um        length of the cut"
        puts "      -cutgap   um        optional gap around the hole cut ($cutgap)"
        puts "      -position center    center the hole in the rail"
        puts "                inner     cut at core in the rail"
        puts "                outer     cut at chip border side of the rail"
        puts "                center_invert cut at chip border side of the rail"
        puts "      -fill     net_name  optional filling the gap with a different net"
        puts "                          allow vias to top and bottom layer,"
        puts "                          requires a -cutgap when net_name differs"
        puts "      -save_off           by default only the '-metal' get affected"
        puts "                          setting this, the user take care on the layer (speedup)"
        puts " "
        puts "      -force_via          MISSING OPTION: to create VIA to lower an upper layer due"
        puts "                          refer to the 'orthogonal' issue, in case the upper and lower"
        puts "                          layer has not the same direction"
        puts "                          (final 'update_power_vias -add_vias {1} -orthogonal_only {1}' would NOT create all vias!)"
        puts " "
        if {$pos == "center_invert"} {
            puts "       TOP or BOTTOM VIEW of position CENTER "
            puts "      "
            puts "      chip"
            puts "      edge (left chip edge for -side t/b) or bottom chip edge for -side l/r)"
            puts "      |"
            puts "      |"
            puts "      |----distance---> ${center} um"
            puts "      |                   |           absolute position:"
            puts "      |    core side      .            "
            puts "      |  _ _ ______       |       ____________[expr $edgeLL + $railW]_um___________________________________________________"
            puts "      |            |             |             "
            puts "      |            +------|------+ ---/-               rail $rail (of [expr $all_rails-1], '0' = outermost) on $metal"
            puts "      |            center inverted         [expr $width + 2*$cutgap] um"
            puts "      |            +------|------+ ---/-"
            puts "      |    _ ______|      .      |____________[expr $edgeLL]_um__ (\$edgeLL)___________________________________________"
            puts "      |            :      |      :  "
            puts "      |    chip    :             :"
            puts "      |    edge    :<----------->:"
            puts "      |    side        [expr $length + 2*$cutgap] um "
            puts "      |"
            puts "      +-----------------------------------------------------------"
        } elseif {$pos == "inner"} {
            puts "       BOTTOM VIEW of position INNER rail cut "
            puts "      "
            puts "      chip"
            puts "      edge (left chip edge for -side t/b) or bottom chip edge for -side l/r)"
            puts "      |"
            puts "      |"
            puts "      |----distance---> ${center} um"
            puts "      |                   |           absolute position:"
            puts "      |    core side      .            "
            puts "      |  _ _ _____________|___________________[expr $edgeLL + $railW]_um___________________________________________________"
            puts "      |            |      .      |    |                 rail $rail (of [expr $all_rails-1], '0' = outermost) on $metal"
            if { $fill == ""} {
                puts "      |            |  inner cut  |    | [expr $width + 2*$cutgap] um"
            } else {
                puts "      |            | ########### |    | [expr $width + 2*$cutgap] um    center filled with $fill"
            }
            puts "      |            +------|------+ ---/-"
            puts "      |            :      .      :             "
            puts "      |    _ ______:______|______:____________[expr $edgeLL]_um__ (\$edgeLL)___________________________________________"
            puts "      |            :      .      :  "
            puts "      |    chip    :      |      :"
            puts "      |    edge    :<----------->:"
            puts "      |    side        [expr $length + 2*$cutgap] um "
            puts "      |"
            puts "      +-----------------------------------------------------------"
        } elseif {$pos == "outer"} {
            # CENTER IS DEFAULT
            puts "       BOTTOM VIEW of position OUTER cut "
            puts "      "
            puts "      chip"
            puts "      edge (left chip edge for -side t/b) or bottom chip edge for -side l/r)"
            puts "      |"
            puts "      |"
            puts "      |----distance---> ${center} um"
            puts "      |                   |           absolute position:"
            puts "      |    core side      .            "
            puts "      |  _ _ _____________|___________________[expr $edgeLL + $railW]_um___________________________________________________"
            puts "      |                                        "
            puts "      |                            "
            puts "      |            +------|------+ ---/-               rail $rail (of [expr $all_rails-1], '0' = outermost) on $metal"
            if { $fill == ""} {
                puts "      |          - |  outer cut  |    | [expr $width + 2*$cutgap] um"
            } else {
                puts "      |          - | ########### |    | [expr $width + 2*$cutgap] um    center filled with $fill"
            }
            puts "      |    _ ______|______.______|____________[expr $edgeLL]_um__ (\$edgeLL)___________________________________________"
            puts "      |            :      |      :  "
            puts "      |    chip    :             :"
            puts "      |    edge    :<----------->:"
            puts "      |    side        [expr $length + 2*$cutgap] um "
            puts "      |"
            puts "      +-----------------------------------------------------------"
        } else {
            # CENTER IS DEFAULT
            puts "       TOP or BOTTOM VIEW of position CENTER "
        puts "      "
        puts "      chip"
        puts "      edge (left chip edge for -side t/b) or bottom chip edge for -side l/r)"
        puts "      |"
        puts "      |"
        puts "      |----distance---> ${center} um"
        puts "      |                   |           absolute position:"
        puts "      |    core side      .            "
        puts "      |  _ _ _____________|___________________[expr $edgeLL + $railW]_um___________________________________________________"
        puts "      |                                        "
        puts "      |            +------|------+ ---/-               rail $rail (of [expr $all_rails-1], '0' = outermost) on $metal"
        if { $fill == ""} {
                puts "      |          - | center cut  |    | [expr $width + 2*$cutgap] um"
        } else {
            puts "      |          - | ########### |    | [expr $width + 2*$cutgap] um    center filled with $fill"
        }
        puts "      |            +------|------+ ---/-"
        puts "      |    _ ______:______.______:____________[expr $edgeLL]_um__ (\$edgeLL)___________________________________________"
        puts "      |            :      |      :  "
        puts "      |    chip    :             :"
        puts "      |    edge    :<----------->:"
        puts "      |    side        [expr $length + 2*$cutgap] um "
        puts "      |"
        puts "      +-----------------------------------------------------------"
        }
        return
    }

    ## dedicated start of routing
    route_start_defaults
    ## set up for the reroute pieces (finish cut + filler)
    set_db edit_wire_layer_horizontal $metal                                            ; # set h metal
    set_db edit_wire_layer_vertical   $metal                                            ; # set v metal
    set_db edit_wire_snap_to {}                                                         ; # aviod any extensions, create own overlap for savety route
    ## net_name not equals, a cut with redraw is required - side independent settings
    if { $fill != $net } {
        # define reroute width
        if { $pos == "center" } {
            set rWidth [expr ($railW - $width)/2.0 - $cutgap]                               ; # get side width of route incl. gap - float!
        } elseif { $pos == "center_invert" } {
            set rWidth $width
        } else {
            set rWidth [expr $railW - $width - $cutgap]                                     ; # get one side width of route incl. gap - float!
        }
        set_db edit_wire_width_horizontal $rWidth                                           ; # set h width
        set_db edit_wire_width_vertical   $rWidth                                           ; # set v width
        set_db edit_wire_look_up_layers {0}                                                 ; # no connection to layers
        set_db edit_wire_look_down_layers {0}                                               ; # no connection to layers

    }
    # punch the hole on bottom or top, but only when the request filler not match the current net_name
    if {$side == "b" || $side == "t"} {
        if { $fill != $net } {
            # cut definitions - starting from lower edge of rail ($edgeLL)
            set posXl   [expr $center - $length/2 - $cutgap]                                ; # left side X
            set posXr   [expr $center + $length/2 + $cutgap]                                ; # right side X
            set posYl   [expr $edgeLL - 0.1]                                                ; # edgeLL Y with overcut
            set posYu   [expr $edgeLL + $railW + 0.1]                                       ; # end Y with overcut
            gui_set_tool cutWire                                                            ; # change mode (optional?)
            edit_cut_route -line [list $posXl $posYl $posXl $posYu] -only_visible_wires     ; # cut left side
            edit_cut_route -line [list $posXr $posYl $posXr $posYu] -only_visible_wires     ; # cut right side
            # gui_select -point [expr ($posXl + $posXr)/2] [expr ($posYl + $posYu)/2]         ; # select by center
            gui_select -rect $posXl $posYl $posXr $posYu                                    ; # select by area
            set_db edit_wire_nets $net                                                      ; # set net name from selection
            delete_selected_from_floorplan                                                  ; # remove selected
            gui_set_tool addWire                                                            ; # set wire tool (required to avoid warning)
            ## route along the rails for future cuts
            set shiftX [expr $length/2 + $cutgap + 1.0]                                     ; # route with overlap
            if { $pos == "center" || ($pos == "outer" && $side == "t") || ($pos == "inner" && $side == "b") } { ; # route one side
                edit_add_route_point [expr $center - $shiftX] [expr $edgeLL + $rWidth/2]            ; # from left"
                edit_end_route_point [expr $center + $shiftX] [expr $edgeLL + $rWidth/2]            ; # to rigth "
            }
            if { $pos == "center" || ($pos == "inner" && $side == "t") || ($pos == "outer" && $side == "b") } { ; # route other side
                edit_add_route_point [expr $center - $shiftX] [expr $edgeLL + $railW - $rWidth/2]   ; # from left"
                edit_end_route_point [expr $center + $shiftX] [expr $edgeLL + $railW - $rWidth/2]   ; # to rigth "
            }
            if { $pos == "center_invert" } {                                                       ; # route center only
                edit_add_route_point [expr $center - $shiftX] [expr $edgeLL + $railW/2]             ; # from left"
                edit_end_route_point [expr $center + $shiftX] [expr $edgeLL + $railW/2]             ; # to rigth "
            }
        }
        # on request add filler metal if required
        if { $pos == "center" && $fill != "" } {
            if { $orthogonal } {
                # obs: select_wires_and_vias -vias {12 23 34 45 56}                                ; # enable all vias for auto create
                set_db edit_wire_width_horizontal $length                                    ; # set filler width
                set_db edit_wire_width_vertical   $length                                    ; # set filler width
                set_db edit_wire_nets             $fill                                     ; # set filler net name
                # obs: set_db edit_wire_look_up_layers   {0}                                       ; # no via generation at all
                # obs: set_db edit_wire_look_down_layers {0}                                       ; # no via generation at all
                gui_set_tool addWire                                                        ; # set wire tool (required to avoid warning)
                edit_add_route_point [expr $center] [expr $edgeLL + $railW/2 - $width/2]    ; # from left (with overlap)"
                edit_end_route_point [expr $center] [expr $edgeLL + $railW/2 + $width/2]    ; # to rigth (with overlap)"
            } else {
            select_wires_and_vias -vias {12 23 34 45 56}                                ; # enable all vias for auto create
            set_db edit_wire_width_horizontal $width                                    ; # set filler width
            set_db edit_wire_width_vertical   $width                                    ; # set filler width
            set_db edit_wire_nets             $fill                                     ; # set filler net name
            set_db edit_wire_look_up_layers   {1}                                       ; # allow single connection to top layer (auto on name match)
            set_db edit_wire_look_down_layers {1}                                       ; # allow single connection to bottom layer (auto on name match)
            gui_set_tool addWire                                                        ; # set wire tool (required to avoid warning)
                edit_add_route_point [expr $center - $length/2] [expr $edgeLL + $railW/2]   ; # from lower (with overlap)"
                edit_end_route_point [expr $center + $length/2] [expr $edgeLL + $railW/2]   ; # to upper (with overlap)"
            }
        }

    # punch the hole on left or right side, but only when the request filler not match the current net_name
    } elseif {$side == "l" || $side == "r"} {
        if { $fill != $net } {
            # starting from leftmost edge of rail ($edgeLL)
            set posYl   [expr $center - $length/2 - $cutgap]                                ; # left side X
            set posYu   [expr $center + $length/2 + $cutgap]                                ; # right side X
            set posXl   [expr $edgeLL - 0.1]                                                 ; # edgeLL Y with overcut
            set posXr   [expr $edgeLL + $railW + 0.1]                                        ; # end Y with overcut
            gui_set_tool cutWire                                                            ; # change mode (optional?)
            edit_cut_route -line [list $posXl $posYl $posXr $posYl] -only_visible_wires     ; # box cut
            edit_cut_route -line [list $posXl $posYu $posXr $posYu] -only_visible_wires     ; # box cut
            # gui_select -point [expr ($posXl + $posXr)/2] [expr ($posYl + $posYu)/2]         ; # select by center
            gui_select -rect $posXl $posYl $posXr $posYu                                    ; # select by area
            set_db edit_wire_nets $net                                                      ; # set net name from selection
            delete_selected_from_floorplan                                                  ; # remove selected
            gui_set_tool addWire                                                            ; # set wire tool (required to avoid warning)
            set shiftY [expr $length/2 + $cutgap + 1.0]                                     ; # route with overlap
            if { $pos == "center" || ($pos == "inner" && $side == "l") || ($pos == "outer" && $side == "r") } {  ; # route one
                edit_add_route_point [expr $edgeLL + $rWidth/2] [expr $center - $shiftY]            ; # from left"
                edit_end_route_point [expr $edgeLL + $rWidth/2] [expr $center + $shiftY]            ; # to rigth "
            }
            if { $pos == "center" || ($pos == "outer" && $side == "l") || ($pos == "inner" && $side == "r") } {  ; # route one
                edit_add_route_point [expr $edgeLL + $railW - $rWidth/2] [expr $center - $shiftY]   ; # from left"
                edit_end_route_point [expr $edgeLL + $railW - $rWidth/2] [expr $center + $shiftY]   ; # to rigth "
            }
            if { $pos == "center_invert" } {                                                        ; # route center only
                edit_add_route_point [expr $edgeLL + $railW/2] [expr $center - $shiftY]             ; # from left"
                edit_end_route_point [expr $edgeLL + $railW/2] [expr $center + $shiftY]             ; # to rigth "
            }
        }
        # on request add filler metal if required
        if { $pos == "center" && $fill != "" } {
            if { $orthogonal } {
                # obs: select_wires_and_vias -vias {12 23 34 45 56}                                ; # enable all vias for auto create
                set_db edit_wire_width_horizontal $length                                    ; # set filler width
                set_db edit_wire_width_vertical   $length                                    ; # set filler width
                set_db edit_wire_nets             $fill                                     ; # set filler net name
                # obs: set_db edit_wire_look_up_layers   {0}                                       ; # no via generation at all
                # obs: set_db edit_wire_look_down_layers {0}                                       ; # no via generation at all
                gui_set_tool addWire                                                        ; # set wire tool (required to avoid warning)
                edit_add_route_point [expr $edgeLL + $railW/2 - $width/2] [expr $center]    ; # from left (with overlap)"
                edit_end_route_point [expr $edgeLL + $railW/2 + $width/2] [expr $center]    ; # to rigth (with overlap)"
            } else {
            select_wires_and_vias -vias {12 23 34 45 56}                                ; # enable all vias
            set_db edit_wire_width_horizontal $width                                    ; # set filler width
            set_db edit_wire_width_vertical   $width                                    ; # set filler width
            set_db edit_wire_nets             $fill                                     ; # set filler net name
            set_db edit_wire_look_up_layers   {1}                                       ; # allow single connection to top layer (auto on name match)
            set_db edit_wire_look_down_layers {1}                                       ; # allow single connection to bottom layer (auto on name match)
            gui_set_tool addWire                                                        ; # set wire tool (required to avoid warning)
            edit_add_route_point [expr $edgeLL + $railW/2] [expr $center - $length/2]   ; # from left (with overlap)"
            edit_end_route_point [expr $edgeLL + $railW/2] [expr $center + $length/2]   ; # to rigth (with overlap)"
        }
    }
    }
    # set_all_metals_and_vias_selection -select 0 -visible 1
    # gui_redraw
}; # end interconnect_power_ring


##############################################################################
## Flow Procs: Power ring draw as special route.
##############################################################################
# to reload this file:
# source /usr/project-a/epc635/users/tbl/digital/dev/digital/epc635_pr_top/be/innovus_scripts/flow_procs.tcl

define_proc pwr_ring_create -description "Main power ring routing, based on a power matrix."  {
    { matrix  ""      -power_matrix {string}  {required}  {Power matrix defining rails and connections.} }
    { how2use "false" -how2use      {}        {optional}  {Detail description by 'pwr_ring_create -power_matrix 0 -how2use'} }
    { skipRng "false" -skipRng      {}        {optional}  {skip part 1 - ring generation and return} }
    { skipCon "false" -skipCon      {}        {optional}  {skip part 2 - layer connectivity generation and return} }
    { skipVia "false" -skipVia      {}        {optional}  {skip part 3 - via generation and return} }
    { connO   30      -connO        {string}  {optional}  {reconnection between rails: startoffest } }
    { connW   15      -connW        {string}  {optional}  {reconnection between rails: width } }
    { connG   35      -connG        {string}  {optional}  {reconnection between rails: gap (slicing)} }
} {
    imsg_id -id UFSM-151 -tl 6 -msg "proc pwr_ring_create: start"

    ## define setting used for the help as well
    set gap 2                                               ; # required gap between fat metal routes
    set chipX [get_db [get_db designs] .bbox.ur.x]          ; # get chip dimensions
    set chipY [get_db [get_db designs] .bbox.ur.y]          ; #
    set core2edge [get_db [get_db designs] .core_bbox.ll.x] ; # get core size
    # corner cut defined to minimum as default (only with innovus 18.12)
    set coner45cut 12.0                                     ; # alow a 45° cut due to silicon mechanical corner stress
    # OBSOLETE, required for innovus 18.10 (bug):   set coner45cut  0.0                                     ; # alow a 45° cut due to silicon mechanical corner stress
    # TBD @@@ no 45° due to Innovus 18.10 bug -> Case Number: Case 46285443

    set offsetSc $connO     ; # PART 2: start offset from CORE edge
    set connectW $connW     ; # PART 2: connection width between neighboring layers
    set connectG $connG     ; # PART 2: connection gap (sliceing)

    set offsetSv  80.0      ; # PART 3: start offset from core edge
    set viaL       8.0      ; # PART 3: via length
    set viaW       5.0      ; # PART 3: via width, centered on each rail
    #ORIG: set viaD     165.0      ; # PART 3: via distance between schemes
    set viaD     250.0      ; # PART 3: via distance between schemes
    set cutgap     1.0      ; # PART 3: cut around via for feedtrough connections (technology

    if { ![info exists ::cfg_SEALRING] } {
        emsg_id_stop -id USFM-100  -msg "proc pwr_ring_create: No cfg_SEALRING defined"
    } else {
        set sr $::cfg_SEALRING
    }
    set ringW [expr $core2edge - $sr]
    set railW [expr $ringW/5 - $gap]
    if { $railW != 38 } {
        wmsg_id -id UFSM-130 -tl 2 -msg "proc pwr_ring_create: single rail width is set to $railW um"
    } else {
        imsg_id -id UFSM-151 -tl 6 -msg "proc pwr_ring_create: single rail width is set to 38um (as expected) "
    }

    ## expect a pwr_ring setting with a name/connection matrix of 11 rows by 10 colums
    set fail "false"
    if { [llen $matrix] != 11 || [llen [lindex $matrix 0]] != 10 } {
        emsg_id -id USFM-100  -msg "proc pwr_ring_create: pwr_ring matrix size not 11x10"
        set fail "true"
    }

    if { $how2use || $fail} {
        puts ""
        puts "======================================================================================"
        puts "Required matrix sample:"
        puts "--------------------------------------------------------------------------------------"
        puts ""
        puts "        seal  outer                               inner"
        puts "        ring  rail                                rail"
        puts "  chip  |  |--------------------200um-------------------| chip"
        puts "  border|20|--38--|2|--38--|2|--38--|2|--38--|2|--38--|2| core"
        puts "        |  |                                            |"
        puts ""
        puts "          {{ vddd  =  vddd  =  vddd  =  vddd  =  vddd    TMET  }"
        puts "           {  -V       V-       -V       V-       -V     VIA56 }"
        puts "           { vssd  =  vssd  =  vssd  =  vssd  =  vssd    MET5  }"
        puts "           {  VI       IV       VI       IV       VI     VIA45 }"
        puts "           { vddd  =  vddd  =  vddd  =  vddd  =  vddd    MET4  }"
        puts "           {  I-       -I       I-       -I       I-     VIA34 }"
        puts "           { vssd  =  vssd  =  vssd  =  vssd  =  vssd    MET3  }"
        puts "           {  II       II       II       II       II     VIA23 }"
        puts "           { vssd  =  vssd  =  vssd  =  vssd  =  vssd    MET2  }"
        puts "           {  --       --       --       --       --     VIA12 }"
        puts "           { vdda  =  vdda  =  vdda  |  vssa  =  vssa    MET1  }}"
        puts ""
        puts "LEGEND:"
        puts ""
        puts "    STEP 1"
        puts "    vxxx    : power net name, defines the routes araound the die"
        puts ""
        puts "    STEP 2"
        puts "    METx    : layer name, use '=' for connection, or '|' for isolation"
        puts "    =       : horizontal metal connection on same layer"
        puts "    |       : no connection (horizontal isolation)"
        puts ""
        puts "    STEP 3"
        puts "    VIAnm   : via name, use 'V^-|' for connect trough layers, direct or isolate, "
        puts "              one symbol per scheme - Dual scheme sample (alternating):"
        puts "    --      : no via (vertical isolation) for both schemes s0 and s1"
        puts "    V-      : via through ONE cut window to next layer s0, no via for s1"
        puts "    IV      : via for s0, and down through ONE cut window for s1"
        puts "    -I      : no via for scheme 0, via connection between layers for s1"
        puts "    ^^^                                                        "
        puts "    |||               SAMPLE:    vddd        vdddvdddvdddvddd    layer n+1"
        puts "    ||+-- scheme n                V                ||||              "
        puts "    |+--- scheme 1               vssd   =>   vssd  ||||  vssd    layer n  "
        puts "    +---- scheme 0                I                ||||              "
        puts "                                 vddd        vdddvdddvdddvddd    layer n-1"
        puts "                                             |-----38um-----|        "
        puts "--------------------------------------------------------------------------------------"
        puts " "
    }

    ## stop with the hint for detail info
    if { ([llen $matrix] != 11 || [llen [lindex $matrix 0]] != 10) && !$how2use} {
        emsg_id_stop -id USFM-100  -msg "proc pwr_ring_create: for details, use: 'pwr_ring_create -power_matrix 0 -how2use'"
    }

    ## detail HOW2USE information:
    if { $how2use } {
        puts "--------------------------------------------------------------------------------------"
        puts "DETAIL on HOW2USE power ring - STEP 1 of 3 - draw ring"
        puts "--------------------------------------------------------------------------------------"
        puts "      "
        puts "               chip width = $chipX um"
        puts "          +---------------------------------+   X:   Start point of ring route"
        puts "          |     _______________________     |   A-H: route 5 * $railW um around the chip"
        puts "          |   C                         B   |        core2edge = $core2edge um "
        puts "          |  /                           \\  |      "
        puts "          | D                             A |      "
        puts "   cut    | |                             | |   chip heigth = $chipY um  "
        puts " --view---|-|-----                        X |        area   = [expr $chipX*$chipY/pow(10,6)] mm2"
        puts "    of    | |                             | |      "
        puts "  matrix  | E                             H |   Hint:"
        puts "          |  \\                           /  |   on big chips a forced a gap would"
        puts "          |   F_________________________G   |   break the electrical loop"
        puts "       REF#---------------------------------+     "
        puts "            |-| Coner45cut DISTANCE = $coner45cut"
        puts " "
        puts "--------------------------------------------------------------------------------------"
        puts " "
        puts "DETAIL on HOW2USE power ring - STEP 2 of 3 - reconnect same layers"
        puts "--------------------------------------------------------------------------------------"
        puts "define distances to control the metal reconnect on same layer"
        puts " "
        puts "        seal      Detail lower left corner         @ $core2edge um"
        puts "        ring      TOP view                              |"
        puts "  chip  |  |---------------------200--------------------| chip"
        puts "  border|20|--38--|2|--38--|2|--38--|2|--38--|2|--38--|2| core"
        puts "        |  :      : :      : :      : :      : :      : |"
        puts "        |  |. . . | |. . . | |. . . | |. . . | |. . . | |"
        puts "        |  |.MET. | |. . . | |. . . | |. . . | |. . . | |"
        puts "        |  |. . . | |. . . | |. . . | |. . . | |. . . | |"
        puts "        |  |. . . +-+. . . +-+. . . +-+. . . +-+. . . | |"
        puts "        |  |. . . . . . . . . . . . . . . . . . . . . | |"
        puts "        |  |. . . +-+. . . +-+. . . +-+. . . +-+. . . |-|------/--"
        puts "        |  |. . . | |. . . | |. . . | |. . . | |. . . | |      |"
        puts "        |  |. . . | |. . . | |. . . | |. . . | |. . . | |      |"
        puts "        |  |. . . | |. . . | |. . . | |. . . | |. . . | |   $connectG um (gap)"
        puts "        |  |. . . | |. . . | |. . . | |. . . | |. . . | |      |"
        puts "        |  |. . . +-+. . . +-+. . . +-+. . . +-+. . . |-|------/--"
        puts "        |  |. . . . . . . . . . . . . . . . . . . . . | |   $connectW um (width)"
        puts "        |  |. . . +-+. . . +-+. . . +-+. . . +-+. . . |-|------/--"
        puts "        |  |. . . | |. . . | |. . . | |. . . | |. . . | |      |"
        puts "        |  |. . . | |. . . | |. . . | |. . . | |. . . | |   $offsetSc um (core offest)"
        puts "        |  |. . . | |. . . | |. . . | |. . . | |. . . | |      |"
        puts "        |  |. . . | |. . . | |. . . | |. . . | |. . . | +------/----- - -"
        puts "        |  |. . . | |. . . | |. . . | |. . . | |. . . +-------------- - -"
        puts "        |  |outer | |. . . | |. . . | |. . . | |. .inner . . . . . ."
        puts "        |  |metal | |. . . | |. . . | |. . . | |. .metal rail 0 . . . "
        puts "        |  |rail 4| |. . . | |. . . | |. . . | +--------------------- - -"
        puts "        |  |. . . | |. . . | |. . . | |. . . +----------------------- - -"
        puts "        |  |. . . | |. . . | |. . . | |. . . . . . . . . . . . . . . "
        puts "        |  :      : :      : :      : :"
        puts "        0"
        puts " "
        puts "--------------------------------------------------------------------------------------"
        puts " "
        puts "DETAIL on HOW2USE power ring - STEP 3 of 3 - reconned layers by via"
        puts "--------------------------------------------------------------------------------------"
        puts "define the distances to control the metal reconnect on by vias"
        puts " "
        puts "                 Detail lower left corner"
        puts "        seal     'X' : V via ($viaL x $viaW)um CENTRED on each rail"
        puts "        ring "
        puts "  chip  |  |---------------------200--------------------| chip "
        puts "  border|20|--38--|2|--38--|2|--38--|2|--38--|2|--38--|2| core "
        puts "        |  .      . .      . .      . .      . .      . ."
        puts "        |  .      . .      . .      . .      . .      . ."
        puts " scheme |  .      . .      . .      . .      .  $viaW um (cut: [expr $viaW + 2*$cutgap] um)"
        puts "   n    |  .      . .      . .      . .      .--->| |<--- "
        puts "        |  .      . .      . .      . .      . .  | | . ."
        puts "        |  |      | |      | |      | |      | |      | |"
        puts " scheme |  |  +-+ | |      | |  +-+ | |      | |  +-+ |-|-------/--"
        puts "   1    |  |  |X| | |      | |  |X| | |      | |  |X| | |---/-  |   $viaL um "
        puts "        |  |  +-+ | |      | |  +-+ | |      | |  +-+ |-|---|---/--(cut: [expr $viaL + 2*$cutgap] um) "
        puts "        |  |      +-+      +-+      +-+      +-+      | |   | "
        puts "        |  |                                          | |   | "
        puts "        |  |      +-+      +-+      +-+      +-+      | | $viaD um"
        puts "        |  |      | |      | |      | |      | |      | |   | "
        puts " scheme |  |      | |  +-+ | |      | |  +-+ | |      | |   | "
        puts "   0    |  |      | |  |X| | |      | |  |X| | |      | |---/-- "
        puts "        |  |      | |  +-+ | |      | |  +-+ | |      | |   | "
        puts "        |  |      | |      | |      | |      | |      | |   |"
        puts "        |  |      +-+      +-+      +-+      +-+      | | $offsetSv "
        puts "        |  |                                          | |   |"
        puts "        |  |      +-+      +-+      +-+      +-+      | |   | "
        puts "        |  |      | |      | |      | |      | |      | |   |"
        puts "        |  |      | |      | |      | |      | |      | +---/----- - -"
        puts "        |  |      | |      | |      | |      | |      +------------- - -"
        puts "        |  |outer | |      | |      | |      | |  inner"
        puts "        |  |metal | |      | |      | |      | |  metal rail "
        puts "        |  |rail  | |      | |      | |      | +--------------------- - -"
        puts "        |  |      | |      | |      | |      +----------------------- - -"
        puts "        |  :      : :      : :      : :"
        puts "        0"
        puts "======================================================================================"
        return
    }

    if { $skipRng } { return }

    # save a reduced 6x6 matrix with net and layer names only
    set powerring {}
    foreach i $matrix {
        if { [string first "VIA" $i] >= 0 } { continue }
        set row ""
        foreach c {0 2 4 6 8 9} {
            lappend row [lindex $i $c]
        }
        lappend powerring $row
    }


    ## get some parameters from config and floorplan and run some checks
    if { [get_db [get_db designs] .core_bbox.ll.y] != $core2edge } {
        emsg_id_stop -id USFM-100  -msg "proc pwr_ring_create: chip to core (bottom) not equal."
    }
    if { [expr $chipX - [get_db [get_db designs] .core_bbox.ur.x]] != $core2edge } {
        emsg_id_stop -id USFM-100  -msg "proc pwr_ring_create: chip to core (right) not equal."
    }
    if { [expr $chipY - [get_db [get_db designs] .core_bbox.ur.y]] != $core2edge } {
        emsg_id_stop -id USFM-100  -msg "proc pwr_ring_create: chip to core (top) not equal."
    }

    ############################################################################
    ## FIRST PART: route power ring refer to matrix
    ############################################################################

    imsg_id -id UFSM-151 -tl 6 -msg "proc pwr_ring_create: FIRST PART: route power ring refer to matrix"

    #   +-----------------------+
    #   |     _____________     |   X: Start point of ring route
    #   |   C               B   |      The matrix lists the layers is in opposite order, but
    #   |  /                 \  |      on multi routes always the center of the leftmost
    #   | D                   A |      (or bottom) route is used as reference. Start on
    #   | |                   | |      left side would require complicate 45° calculations.
    # --|-|----matrix         X |
    #   | |    description    | |
    #   | E                   H |      @@@
    #   |  \                 /  |      somewhere there should be forced a gap to cut
    #   |   F_______________G   |      electrical loop because of chip area
    #   #-----------------------+
    #     | | Coner45cut DISTANCE

    set offset     [expr $core2edge - $railW/2 -$gap]       ; # from die edge to center of innermost rail

    # dedicated start of routing
    route_start_defaults
    # set up for the ring
    set_db edit_wire_shape            {ring}
    set_db edit_wire_allow_45_degree  {1}
    set_db edit_wire_look_up_layers   {0}       ; # avoid intermediate connection
    set_db edit_wire_look_down_layers {0}       ; # connect them later
    set_db edit_wire_width_horizontal $railW
    set_db edit_wire_width_vertical   $railW
    #              outer ..........    inner    place Coordinates are the center of the outer rail
    # if routing starts on rigth side (X) or left side the rail orgin is the center of leftmost route
    # define the corner width

    ## TOOL isssue - on 45° angle
    if { $coner45cut > 0 } {
        set_db edit_wire_allow_45_degree  {1}
        foreach i $powerring {
            set_db edit_wire_layer_horizontal [lindex $i 5]
            set_db edit_wire_layer_vertical   [lindex $i 5]
            ## beacause the route starts on right corner, the netnames need to be swapped (Matrix refers to left edge)
            set_db edit_wire_nets             [lreverse [lrange $i 0 4]]
            # start and end on a horizontal Track to avoid congestions
            edit_add_route_point [expr $chipX - $offset]                [expr $chipY / 2 - 1.0]                ; # X overlap by 1um
            edit_add_route_point [expr $chipX - $offset]                [expr $chipY - $coner45cut - $offset]  ; # A
            edit_add_route_point [expr $chipX - $coner45cut - $offset]  [expr $chipY - $offset]                ; # B
            edit_add_route_point [expr $offset + $coner45cut]           [expr $chipY - $offset]                ; # C
            edit_add_route_point [expr $offset]                         [expr $chipY - $offset - $coner45cut]  ; # D
            edit_add_route_point [expr $offset]                         [expr $coner45cut + $offset]           ; # E
            edit_add_route_point [expr $coner45cut + $offset]           [expr $offset]                         ; # F
            edit_add_route_point [expr $chipX - $coner45cut - $offset]  [expr $offset]                         ; # G
            edit_add_route_point [expr $chipX - $offset]                [expr $coner45cut + $offset]           ; # H
            edit_end_route_point [expr $chipX - $offset]                [expr $chipY/2]                        ; # X
        }
    ## INTERMEDIATE solution due to TOOL isssue on 45° angle
    #  worscase - 45° is not working at all,     we could route inside the pad
    #
    #   +------------------------------         +------------------------------
    #   |/  _________________main rail          |         |   _______main rail
    #   |  /+---+                               |   +---+ |  |
    #   |  ||pad|                               |   |pad| |  |
    #   |  |+---+                               |   +---+ |  |
    #   |  |                                    |---------+  |
    #   |  |                                    |   _________|
    #   |  |                                    |  |
    #
    #   Even "better" option: create a corner macro in Virtuoso

    } else {
        set_db edit_wire_allow_45_degree  {0}
        foreach i $powerring {
            set_db edit_wire_layer_horizontal [lindex $i 5]
            set_db edit_wire_layer_vertical   [lindex $i 5]
            set_db edit_wire_nets             [lreverse [lrange $i 0 4]]    ; # see explanation above
            # start and end on a horizontal Track to avoid congestions
            edit_add_route_point [expr $chipX - $offset]                [expr $chipY / 2 - 1.0]                ; # X overlap by 1um
            edit_add_route_point [expr $chipX - $offset]                [expr $chipY - $offset]  ; # ur
            edit_add_route_point [expr $offset]           [expr $chipY - $offset]                ; # ul
            edit_add_route_point [expr $offset]                         [expr $offset]           ; # ll
            edit_add_route_point [expr $chipX - $offset]  [expr $offset]                         ; # lr
            edit_end_route_point [expr $chipX - $offset]                [expr $chipY/2]                        ; # X
        }
    }

    # cleanup with dedicated end of routing
    route_end_defaults

    if { ! $skipCon } {

        ############################################################################
        ## SECOND PART: reconnect power rails on same layer refer to matrix
        ############################################################################
        imsg_id -id UFSM-151 -tl 6 -msg "proc pwr_ring_create: SECOND PART: reconnect power rails on same layer refer to matrix"

        # dedicated start of routing
        route_start_defaults
        # set up fot the ring
        set_db edit_wire_shape            {stripe}
        set_db edit_wire_look_up_layers   {0}                   ; # avoid intermediate connection
        set_db edit_wire_look_down_layers {0}                   ; # connect them later
        set_db edit_wire_width_horizontal [expr $gap + 2*1.0]   ; # reconnect width is gap + overlap on each side
        set_db edit_wire_width_vertical   [expr $gap + 2*1.0]   ; # reconnect width is gap + overlap on each side
        set_db edit_wire_snap_to {}                             ; # no snap at all, create own overlap

        ## reconnect between layers (uses only 'MET' lines of the matrix)
        foreach row $matrix {
            imsg_id -id UFSM-151 -tl 9 -msg "proc pwr_ring_create: SECOND PART: reconnect layer [lindex $row 9]"
            if { [string first "VIA" $row] >= 0 } { continue }          ; # skipp VIA rows

            ## expect a connection symbol on this position of each metal layer, others get ignored
            foreach c {1 3 5 7} {
                if { [lindex $row $c] == "=" } {
                    # check for same netname before reconnect
                    if { [lindex $row [expr $c-1]] != [lindex $row [expr $c+1]] } {
                        emsg_id_stop -id USFM-100  -msg "proc pwr_ring_create: try to connect non identical nets in pwr_con0 matrix ([lreplace $i $c $c X])"
                    }
                    # define layer and net
                    set_db edit_wire_layer_horizontal [lindex $row 9]
                    set_db edit_wire_layer_vertical   [lindex $row 9]
                    set_db edit_wire_nets             [lindex $row [expr $c-1]]  ; # one of the two net names

                    ## BOTTOM and TOP reconnect, outer to inner rail, left to right (Matrix reference is reflecting the bottom side)
                    set count 0
                    set rCenterYb [expr $sr + $railW + $gap/2 + ($c-1)/2*($railW + $gap)]       ; # Y bottom center of the connections routes
                    set rCenterYt [expr $chipY - $rCenterYb]                                    ; # Y top center of the connections routes
                    set rStartX   [expr $core2edge + $offsetSc]
                    set rEndX     [expr $chipX - $rStartX - $connectW]
#                    set sX [expr $core2edge + $offsetSc + $connectW/2]
#                    # start point Y refer to layer '($c-1)/2' fron outer to inner
#                    #            |---gap/2 in rail---|   |---rail offset--------|
#                    set sY [expr $sr + $railW - $gap/2 + ($c-1)/2*($railW + $gap)]
                    # over the full length
                    while { $rStartX < $rEndX } {
                        edit_add_route_point $rStartX $rCenterYb                    ; # bottom from left
                        edit_end_route_point [expr $rStartX + $connectW] $rCenterYb ; # to rigth
                        edit_add_route_point $rStartX $rCenterYt                    ; # top from left
                        edit_end_route_point [expr $rStartX + $connectW] $rCenterYt ; # to rigth
                        set rStartX [expr $rStartX + $connectW + $connectG]         ; # set next connection
                        incr count
                    }
                    imsg_id -id UFSM-151 -tl 9 -msg "proc pwr_ring_create: reconnected bottom/top [lindex $row 9] rail [expr ($c-1)/2+1] to [expr ($c-1)/2+2] of [lindex $row [expr $c-1]] ($count each)"

#                    ## TOP reconnect, outer to inner rail, left to right
#                    set count 0
#                    set sX [expr $core2edge + $offsetSc + $connectW/2]
#                    set sY [expr $chipY - $sr - $railW + $gap/2 - ($c-1)/2*($railW + $gap)]
#                    # over the full length
#                    while { $sX < [expr $chipX - $core2edge - $connectW - $offsetSc] } {
#                        edit_add_route_point $sX $sY                    ; # start
#                        edit_end_route_point $sX [expr $sY - 2*$gap]    ; # gap/2 in first rail to gap/2 into next rail
#                        set sX [expr $sX + $connectG + $connectW]       ; # set next connection
#                        incr count
#                    }
#                    imsg_id -id UFSM-151 -tl 9 -msg "proc pwr_ring_create: reconnected top    [lindex $row 9] rail [expr ($c-1)/2+1] to [expr ($c-1)/2+2] of [lindex $row [expr $c-1]] ($count)"

                    ## LEFT and RIGHT reconnect, outer to inner rail, bottom to top (Matrix reference is reflecting the left side)
                    set count 0
                    set rCenterXl [expr $sr + $railW + $gap/2 + ($c-1)/2*($railW + $gap)]       ; # X left center of the connections routes
                    set rCenterXr [expr $chipX - $rCenterXl]                                    ; # X right center of the connections routes
                    set rStartY   [expr $core2edge + $offsetSc]
                    set rEndY     [expr $chipY - $rStartY - $connectW]

#                    set sX [expr $sr + $railW - $gap/2 + ($c-1)/2*($railW + $gap)]
#                    set sY [expr $core2edge + $offsetSc + $connectW/2]
                    # over the full length
                    while { $rStartY < $rEndY } {
                        edit_add_route_point $rCenterXl $rStartY                    ; # left from bottom
                        edit_end_route_point $rCenterXl [expr $rStartY + $connectW] ; # to upside
                        edit_add_route_point $rCenterXr $rStartY                    ; # right from bottom
                        edit_end_route_point $rCenterXr [expr $rStartY + $connectW] ; # to upside
                        set rStartY [expr $rStartY + $connectW + $connectG]         ; # set next connection
                        incr count
                    }
                    imsg_id -id UFSM-151 -tl 9 -msg "proc pwr_ring_create: reconnected left / rigth  [lindex $row 9] rail [expr ($c-1)/2+1] to [expr ($c-1)/2+2] of [lindex $row [expr $c-1]] ($count each)"

#                    ## RIGHT reconnect, outer to inner rail, bottom to top
#                    set count 0
#                    set sX [expr $chipX - $sr - $railW + $gap/2 - ($c-1)/2*($railW + $gap)]
#                    set sY [expr $core2edge + $offsetSc + $connectW/2]
#                    # over the full length
#                    while { $sY < [expr $chipY - $core2edge - $connectW - $offsetSc] } {
#                        edit_add_route_point $sX $sY                    ; # start
#                        edit_end_route_point [expr $sX - 2*$gap] $sY    ; # gap/2 in first rail to gap/2 into next rail
#                        set sY [expr $sY + $connectG + $connectW]       ; # set next connection
#                        incr count
#                    }
#                    imsg_id -id UFSM-151 -tl 9 -msg "proc pwr_ring_create: reconnected right  [lindex $row 9] rail [expr ($c-1)/2+1] to [expr ($c-1)/2+2] of [lindex $row [expr $c-1]] ($count)"
                }
            }
        }
        route_end_defaults
    }

    if { $skipVia } { return }

    ############################################################################
    ## THIRD PART: reconnect power rails by vias refer to matrix VIA rows
    ############################################################################

    imsg_id -id UFSM-151 -tl 6 -msg "proc pwr_ring_create: THIRD PART: reconnect power rails by vias refer to matrix"


    # dedicated start of routing
    route_start_defaults
    # set up fot the ring
    set_db edit_wire_shape            {stripe}
    set_db edit_wire_look_up_layers   {1}       ; # controlls the via stack generation
    set_db edit_wire_look_down_layers {1}       ; # controlls the via stack generation
    set_db edit_wire_width_horizontal $viaL
    set_db edit_wire_width_vertical   $viaL
    set_db edit_wire_snap_to {}                 ; # no snap at all

    ## check the matrix for identical count of schemes
    set scheme_count [string length [lindex [lindex $matrix 1] 0]]
    foreach row $matrix {
        if { [string first "MET" $row] >= 0 } { continue }          ; # skipp MET rows
        foreach c {0 1 2 3 4} {
            if { [string length [lindex $row $c]] != $scheme_count } {
                emsg_id -id USFM-100  -msg "proc pwr_ring_create: scheme connection count ($scheme_count) is inconsistent: '[lreplace $row $c $c X]'"
                return
            }
        }
    }

    imsg_id -id UFSM-151 -tl 8 -msg "-------------------------------------\nproc pwr_ring_create: use following matrix:\n$matrix\n-------------------------------------"

    ## for each of the 4 sides: bottom, right, top, left:
    ## full run: b r t l
    foreach side { b r t l } {
        ## reconnect by via requires the via-layer, one metal above and 2 metal + via below for all checks
        ## full run: 1 3 5 7 9
        foreach i { 1 3 5 7 9 } {                       ; #                         SAMPLE: { vddd  =  vddd  =  vddd  =  vddd  =  vddd    TMET  }
            set top [lindex $matrix [expr $i-1]]        ; # metal above the via layer       {  -V       V-       -V       V-       -V     VIA56 }
            set via [lindex $matrix $i]                 ; # via layer with 'V' and 'I'      { vssd  =  vssd  =  vssd  =  vssd  =  vssd    MET5  }   top
            set bot [lindex $matrix [expr $i+1]]        ; # metal below via layer           {  VI       IV       VI       IV       VI     VIA45 }   via  <-- i=3
            if { $i == 9 } {                            ; #                                 { vddd  =  vddd  =  vddd  =  vddd  =  vddd    MET4  }   bot
                set lvia { --  --  --  --  --  VIA }    ; # virtual layer                   {  I-       -I       I-       -I       I-     VIA34 }   lvia
                set lmet { --  --  --  --  --  MET }    ; # virtual layer                   { vssd  =  vssd  =  vssd  =  vssd  =  vssd    MET3  }   lmet
            } else {                                    ; #                                 {  II       II       II       II       II     VIA23 }
                set lvia [lindex $matrix [expr $i+2]]   ; # lower via layer                 { vssd  =  vssd  =  vssd  =  vssd  =  vssd    MET2  }
                set lmet [lindex $matrix [expr $i+3]]   ; # lower metal layer               {  --       --       --       --       --     VIA12 }
            }                                           ; #                                 { vdda  =  vdda  =  vdda  |  vssa  =  vssa    MET1  }

            imsg_id -id UFSM-151 -tl 8 -msg "proc pwr_ring_create: Run connections of [lindex $via 5] of side $side"
            ## each connection outer to inner rail
            ## full run: 0 1 2 3 4
            foreach grp { 0 1 2 3 4 } {
                set cgrp [lindex $via  $grp]                ; # scheme group of connection
                set lgrp [lindex $lvia $grp]                ; # lower scheme group of connection (verification for 'V')

                ## finally for each scheme
                set s 0                                             ; # start with scheme 0
                foreach c [split $cgrp {}] l [split $lgrp {}] {     ; # run over all available schemes
                    imsg_id -id UFSM-151 -tl 9 -msg "proc pwr_ring_create: conn: $c, lower: $l, scheme: $s"
                    ## set position of reconnect refer to side, rail, scheme (both case the same)
                    set start [expr $core2edge + $offsetSv + $s*$viaD]
                    set step  [expr $scheme_count*$viaD]
                    if { $side == "b" || $side == "t" } {
                        set last  [expr $chipX - $core2edge - $offsetSv]
                    } elseif { $side == "l" || $side == "r" } {
                        set last  [expr $chipY - $core2edge - $offsetSv]
                    }
                    ## CASE 1: trough metal connection
                    if { $c == "V" } {
                        ## a 'V' need a 'I' on the via below (this is correct for via trough ONE layer)
                        if { $l != "I" } {
                            emsg_id_stop -id USFM-100  -msg "proc pwr_ring_create: The [expr $grp+1] cut trough 'V' miss a corrsponding via 'I' below in power-ring matrix: \n\t$via\n\t$lvia"
                        }
                        ## the metal above has to match the lmetal (this is correct for via trough ONE layer)
                        if { [lindex $top [expr $grp*2]] != [lindex $lmet [expr $grp*2]] } {
                            emsg_id_stop -id USFM-100  -msg "proc pwr_ring_create: The [expr $grp+1] metal does not match for trough-via in power-ring matrix: \n\t$top\n\t$via\n\t$lmet"
                        }
                        ## all checks are fine, CUT A HOLE and reconnect
                        ## to speedup use -no_save option and control the metal upfront
                        set_all_metals_and_vias_selection -select 0 -visible 0
                        select_wires_and_vias -metals [lindex $bot 9]
                        deselect_obj -all
                        ## run the loop
                        imsg_id -id UFSM-151 -tl 8 -msg "proc pwr_ring_create: call interconnect_power_ring from $start to $last step $step, pram: -side $side -metal [lindex $bot 9] -rail $grp -center <loop> -width $viaW -length $viaL -position center -fill [lindex $top [expr $grp*2]] -cutgap $cutgap ("
                        for { set pos $start } { $pos < $last } { set pos [expr $pos + $step] } {
                            interconnect_power_ring -orthogonal -save_off -side $side -metal [lindex $bot 9] -rail $grp -center $pos -width $viaW -length $viaL -position center -fill [lindex $top [expr $grp*2]] -cutgap $cutgap; # call subroutine with cut hole and reconnect "
                        }
                    }
                    ## CASE 2: bottom to top metal connection
                    if { $c == "I" } {
                        ## on a 'I' connection, just connect top and botton layer when nets are matching
                        ## to speedup use -no_save option (no cut is performed, but selection requires accurete metal definition)
                        set_all_metals_and_vias_selection -select 0 -visible 0
                        select_wires_and_vias -metals [lindex $bot 9]
                        deselect_obj -all
                        ## run the loop
                        if { [lindex $top [expr $grp*2]] == [lindex $bot [expr $grp*2]] } {
                            ## position of reconnect refer to side, rail, scheme
                            imsg_id -id UFSM-151 -tl 8 -msg "proc pwr_ring_create: call interconnect_power_ring from $start to $last step $step, pram: -side $side -metal [lindex $bot 9] -rail $grp -center <loop> -width $viaW -length $viaL -position center -fill [lindex $top [expr $grp*2]] ("
                            for { set pos $start } { $pos < $last } { set pos [expr $pos + $step] } {
                                interconnect_power_ring -orthogonal -save_off -side $side -metal [lindex $bot 9] -rail $grp -center $pos -width $viaW -length $viaL -position center -fill [lindex $top [expr $grp*2]]; # call subroutine with cut hole and reconnect "
                            }
                        }
                    }
                    incr s;     # next scheme
                }
            }
        }
    }
    ############################################################################
    ## FOURTH PART: add CMIM cap between vias (not implemented feature)
    ############################################################################
    ## Hint: CMIM cap @25°C, 5V!: 0.98 fF/um², max. 30 aA/um² leackage (DRM doc)
    ## REF:  https://eda.espros.com/e-documents/ohc15l_drm/ohc15l_drm.pdf
    ##
    ## CMIM can be drawn in Virtuoso as pCell like via-arrays from 2x2um up to 150x150um
    ## min. space 1.5um, space to Tvia min. 0.5um, space to met5 edge min 0.5um
    ## Innovus may require a well defined cell for the 38um rails
    ##

    # imsg_id -id UFSM-151 -tl 6 -msg "proc pwr_ring_create: FOURTH PART: add MIM caps in rails (not implemented)"
}

################################################################################
## Flow Support Procs:
################################################################################

## load a special route floorplan file 'file.mxl.spr' by creating a minimal file.xml
## The spr file can be created by: write_floorplan file.xml -sections special_route -xml
##
## GENERATE CONTENT OF file.xml.spr_load:
## <?xml version="1.0" encoding="ISO-8859-1"?>
## <FPlanRoot>
##     <Version>8</Version>
##     <HeadBox llx="0.000000" lly="0.000000" urx="31600.000000" ury="25600.000000" />
##     <IoBox llx="150.000000" lly="160.000000" urx="31440.000000" ury="25450.000000" />
##     <CoreBox llx="220.000000" lly="220.000000" urx="31380.000000" ury="25380.000000" />
##     <SprFile>test_pf.xml.spr</SprFile>
##     ... (see below)
## </FPlanRoot>
##
## OTHER PARTS of XML file:
## ------------------------
## <DefRows>, <Tracks> , <GCellGrids>, <PhysicalNets>, <Groups>, <Constraints>, <HierarchicalPartitions>
## <Blocks> (marcos + plankpad_v3), <IOs>, <IOPins>, <PowerDomains>, <GlobalNetConnections>
define_proc load_fp_xml_spr_file -description "create a minimal fp.xml file to load a special route file, generated by 'write_floorplan file.xml -sections special_route -xml'." {
    {fp_xml "" -fp_file  {string} {required} {define floorplan written file (.xml file expected)}}
} {
    imsg_id -id USFM-151 -tl 6 -msg "load_fp_xml_spr_file: start"

    ## extend the filename with expected location
    set fp_xml [file join [get_db flow_cell_cfg_path] innovus_inputs $fp_xml]
    ## check for available spr file xml file
    if { ![file exists $fp_xml.spr] } {
        emsg_id_stop -id USFM-100 -msg "load_fp_xml_spr_file: can't find according spr file $fp_xml.spr \n Shall be generated by 'write_floorplan file.xml -sections special_route -xml'"
    }

    # create .xml file (parse would be possible, but more complex):
    set spr [lindex [file split $fp_xml] end]
    if [catch { set xml_fp [open $fp_xml.spr_load w] }] {
        emsg_id -id USFM-100 -msg "load_fp_xml_spr_file: Could not open $fp_xml for writing"
    }
    puts $xml_fp "<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>"
    puts $xml_fp "<FPlanRoot>"
    puts $xml_fp "    <Version>8</Version>"
    puts $xml_fp "    <HeadBox llx=\"[lindex [lindex [get_db designs .bbox] 0] 0]\"   lly=\"[lindex [lindex [get_db designs .bbox] 0] 1]\"   urx=\"[lindex [lindex [get_db designs .bbox] 0] 2]\" ury=\"[lindex [lindex [get_db designs .bbox] 0] 3]\" />"
    puts $xml_fp "    <IoBox   llx=\"[lindex [lindex [get_db designs .io_bbox] 0] 0]\" lly=\"[lindex [lindex [get_db designs .io_bbox] 0] 1]\" urx=\"[lindex [lindex [get_db designs .io_bbox] 0] 2]\" ury=\"[lindex [lindex [get_db designs .io_bbox] 0] 3]\" />"
    puts $xml_fp "    <CoreBox llx=\"[lindex [lindex [get_db designs .core_bbox] 0] 0]\" lly=\"[lindex [lindex [get_db designs .core_bbox] 0] 1]\" urx=\"[lindex [lindex [get_db designs .core_bbox] 0] 2]\" ury=\"[lindex [lindex [get_db designs .core_bbox] 0] 3]\" />"
    puts $xml_fp "    <SprFile>[lindex [file split $fp_xml] end].spr</SprFile>"
    puts $xml_fp "</FPlanRoot>"
    close $xml_fp

    # load routing (.xml.spr) by minimal file.xml setup:
    read_floorplan $fp_xml.spr_load -xml
}




################################################################################
## Flow Support Procs: Little helpers
################################################################################

## myputs: print to shell or to file
proc myputs {{id} {str}} {
    if {$id == "to_shell"} {
        puts $str
    } else {
        puts $id $str
    }
}



################################################################################
## Flow Support Procs: changeSelectWire2routeWire            READY FOR USE
################################################################################
# to source this file:
# source /usr/project-a/epc908/users/tbl/digital/dev/digital/epc908_pr_top/be/innovus_scripts/flow_procs.tcl
define_proc changeSelectWire2routeWire {
    {detail         false       -detail     {bool}   {optional} {Detail help on this comand}}
    {show           false       -show       {bool}   {optional} {Show the selected routes}}
    {loop           ""          -loop       {string} {optional} {Cast wire setting within loop: -loop {count x-step y-step}}}
    {offset         ""          -offset     {string} {optional} {add a base offset for all wires: -offset {ox oy}}}
    {repeat         ""          -repeat     {string} {optional} {repeat the offset by a list: -repeat {a b-e g k}}}
    {mirror         ""          -mirror     {string} {optional} {mirror all coordinates on a axis {xAxis yAxis}, {0 0} = mo mirror, not use with loop/repeat}}
    {no_snap        false       -no_snap    {bool}   {optional} {snap to route or pin by default - NEED TO VERIFY}}
    {procname       ""          -procname   {string} {optional} {pack the command into a procedure}}
    {title          ""          -title      {string} {optional} {Add a title to describe the route}}
    {append_file    "to_shell"  -file       {string} {optional} {Select file to add your route, default is stdout}}
} {

    ## helpfull 2D vector MAC:  a + b [* bm]
    proc vect_mac {a b {bm 1}} {
        # puts "a: $a, b: $b, bm: $bm"
        set x [expr [lindex $a 0] + [lindex $b 0] * $bm]
        set y [expr [lindex $a 1] + [lindex $b 1] * $bm]
        return [list $x $y]
    }

    ## helpfull 2D vector MultiMAC:  va [*vam] + vb [* vbm] + vc [* vcm]
    proc vect_mmac {va {vam {1 1}} vb {vbm {1 1}} vc {vcm {1 1}} } {
        # puts "va: $va, vb: $vb, vam: $vam, vbm: $vbm"
        set x [expr [lindex $va 0] * [lindex $vam 0] + [lindex $vb 0] * [lindex $vbm 0] + [lindex $vc 0] * [lindex $vcm 0]]
        set y [expr [lindex $va 1] * [lindex $vam 1] + [lindex $vb 1] * [lindex $vbm 1] + [lindex $vc 1] * [lindex $vcm 1]]
        return [list $x $y]
    }

    ## expand a repeat list {2 4- 6  11 13 -17         34}
    ## to                   {2 4 5 6 11 13 14 15 16 17 34}
    proc expand_count_list {a} {
        proc range_ab {a b} {
            set r {}; for {set i $a} {$i <= $b} {incr i} {lappend r $i}
            return "$r"
        }
        regsub -all {(\d+)\s*\-\s*(\d+)} $a {[range_ab \1 \2]} b
        return [subst $b ]
    }

    ## print exteded help
    if { $detail } {
        puts "#---------------------------------------------"
        puts "#  RECIPE to use changeSelectWire2routeWire"
        puts "#---------------------------------------------"
        puts "#  1. manual draw some wires -> HINT"
        puts "#  2. deselectAll"
        puts "#  3. select any routed wires "
        puts "#     shift+click to add wires; 'select by line' may be usefull"
        puts "#     check blue number on bottom right or 'changeSelectWire2routeWire -show'"
        puts "#  4. run 'changeSelectWire2routeWire'"
        puts "#     put this to script: 'changeSelectWire2routeWire -file <string> -procname <string>'"
        puts "#     use the option of 'changeSelectWire2routeWire -help'"
        puts "#"
        puts "#"
        puts "# HINTs: (Cadence search 'Innovus Keyboard Shortcuts'"
        puts "#    Route from a pin - "
        puts "#    > Enable enable CELL > Pin shapes"
        puts "#    > Enable Querry (Q-button lower left) -> highlight the pin on mouseover "
        puts "#    > may need to disable layers above the pin layer"
        puts "#    > open edit route: e "
        puts "#    > mouse over pin"
        puts "#    > when highlighted <shift>-S to take over layer, netname and width"
        puts "#    > change layer and width for the other direction"
        puts "#    > start route (shift-a)"
        puts "#"
        puts "#  Not supported:"
        puts "#    - no regular wire, only special route"
        puts "#    - no shielding"
        puts "#    - no pinadjust check"
        puts "#"
        puts "#  supported:"
        puts "#    - auto sort the routes by name, layer, width"
        puts "#    - add a select comment to find a wire"
        puts "#    - use only routes of all selection"
        puts "#    - basic offset to shift selcted wires: -offset {x-offset y-offset}"
        puts "#    - loop possible:    -loop {count x-step y-step}"
        puts "#    - mirror support:   -mirror {atXaxis atYaxis}"
        puts "#    - create procedure: -procname <name>"
        puts "#    - append to file:   -file <name>"
        puts "#    - add a tilte for detail description"
        puts "#"
        puts "#---------------------------------------------"
        return
    }

    set addSpace ""         ; # space setting for visual alignment

    ## for each selected element filter for object type
    set routes [get_db selected -if {.obj_type == "special_wire"}]              ; # list of routes
    set rCount [llength $routes]                                                ; # route    counter
    set vCount [llength [get_db selected -if {.obj_type == "special_via"}]]     ; # via      counter
    set iCount [llength [get_db selected -if {.obj_type == "inst"}]]            ; # instance counter


    puts "$rCount routes, $vCount vias, $iCount instances"

    # create a list of all routes for sorting
    set rList []
    foreach i $routes {
        ## get parameters of each route
        set rStart [lindex [get_db $i .path] 0]   ; # path: {x1 y1} {x2 y2}
        set rEnd   [lindex [get_db $i .path] 1]   ; # path: {x1 y1} {x2 y2}
        set wid    [get_db $i .width]             ; # width
        set met    [get_db $i .layer.name]        ; # layer
        set net    [get_db $i .net.name]          ; # netname
        set box    [get_db $i .rect]              ; # box of wire to add selection
        # elements.          0    1    2    3       4     5
        lappend rList [list $net $met $wid $rStart $rEnd $box]
    }
    # sort all elements by default (reduce command size)
    set rList [lsort $rList]

    ## pure show of selected wires
    if { $show } {
        puts "#--------------------------------------------------"
        puts "# nbr : net    metal width {startX startY} {endX endY} {box}"
        set cnt 0
        foreach i $rList {
            puts "# [incr cnt] : $i"
        }
        puts "#--------------------------------------------------"
        puts "# Total repeated : [llength [expand_count_list $repeat]] times"
        puts "#--------------------------------------------------"
        puts "# to select them"
        foreach i $rList {
            puts "select_routes -layer [lindex $i 1] -area [lindex $i 5]"
        }
        puts "#--------------------------------------------------"
        return
    }

    if { $title == "" } {
        if { $procname == "" } {
            set title "NEW ROUTE"
        } else {
            set title " PROCEDURE : $procname"
        }
    }

    set header "create by : $::env(USER) on [clock format [clock seconds] -format "%a %T %d-%b-%Y"] (changeSelectWire2routeWire)"

    # option file print, open file
    if { $append_file == "to_shell" } {
        set fp $append_file
    } else {
        set fp [open $append_file a]
    }

    imsg_id -id UFSM-151 -tl 6 -msg "changeSelectWire2routeWire: "
    myputs $fp "###_____________________________________________________________________________________###"
    myputs $fp "### [format "%-83s" $title ] ###"
    myputs $fp "###_____________________________________________________________________________________###"
    myputs $fp "###"
    myputs $fp "### $header"
    myputs $fp "### info      : total $rCount routes ($vCount vias, $iCount instances) selected"
    myputs $fp "###---------------------------------------------------------------------------------------"

    # option pack cmd's into a proc
    if { $procname != "" } {
        myputs $fp "proc $procname {} \{"
        append addSpace "    "
    }

    myputs $fp "${addSpace}# SET OPTIONS"
    myputs $fp "${addSpace}route_start_defaults                     ; # main default setting, add exceptions below"
    if { $no_snap } {
        myputs $fp "${addSpace}set_db edit_wire_align_wire_at_pin {0}          ; # no adjust to pin center"
        myputs $fp "${addSpace}set_db edit_wire_snap_to {}                     ; # no snap to power, pin or special"
        myputs $fp "${addSpace}# set_db edit_wire_snap_to {pg pin special}     ; # default"
    }


    # puts "============================================================="
    # foreach i $route {puts $i}
    # puts "============================================================="
    # set default offset if not defined
    if { $offset == "" } {
        set offset {0.0 0.0}
    }

    # cast with loop
    if { $loop != "" } {
        myputs $fp "${addSpace}for \{set i 0\} \{\$i < [lindex $loop 0]\} \{incr i\} \{"
        append addSpace "    "
        myputs $fp "${addSpace}set oX \[expr \$i*[lindex $loop 1]\]"
        myputs $fp "${addSpace}set oY \[expr \$i*[lindex $loop 2]\]"
    } elseif { $repeat != "" } {
        myputs $fp "${addSpace}foreach mult \{[expand_count_list $repeat]\} \{"
        append addSpace "    "
        myputs $fp "${addSpace}set oX \[expr \$mult*[lindex $offset 0]\]"
        myputs $fp "${addSpace}set oY \[expr \$mult*[lindex $offset 1]\]"
    }

    ## default values
    set lastwid 0               ; #
    set lastmet ""              ; #
    set lastnet ""              ; #

    # route each wire
    foreach i $rList {
        ## get parameters of each route
        set net    [lindex $i 0]
        set met    [lindex $i 1]
        set wid    [lindex $i 2]
        if { $repeat == "" } {
            ## no mirroring by unset
            if { $mirror == "" } {
        set rStart [vect_mac [lindex $i 3] $offset]
        set rEnd   [vect_mac [lindex $i 4] $offset]
            ## no mirroring by {0 0} vector
            } elseif { [lindex $mirror 0] == 0 && [lindex $mirror 1] == 0 } {
                set rStart [vect_mac [lindex $i 3] $offset]
                set rEnd   [vect_mac [lindex $i 4] $offset]
            ## mirror on X
            } elseif { [lindex $mirror 0] != 0 && [lindex $mirror 1] == 0 } {
                set rStart [vect_mmac [lindex $i 3] {-1 1} $mirror {2 0} $offset]
                set rEnd   [vect_mmac [lindex $i 4] {-1 1} $mirror {2 0} $offset]
            ## mirror on Y
            } elseif { [lindex $mirror 0] == 0 && [lindex $mirror 1] != 0 } {
                set rStart [vect_mmac [lindex $i 3] {1 -1} $mirror {0 2} $offset]
                set rEnd   [vect_mmac [lindex $i 4] {1 -1} $mirror {0 2} $offset]
            ## mirror on X and Y
            } elseif { [lindex $mirror 0] != 0 && [lindex $mirror 1] != 0 } {
                set rStart [vect_mmac [lindex $i 3] {-1 -1} $mirror {2 2} $offset]
                set rEnd   [vect_mmac [lindex $i 4] {-1 -1} $mirror {2 2} $offset]
            }
        } else {
            set rStart [lindex $i 3]
            set rEnd   [lindex $i 4]
        }
        set box    [lindex $i 5]

        # on a change, update the db
        if {$lastnet != $net} {
            myputs $fp  "${addSpace}#-----------------------------------------"
            myputs $fp  "${addSpace}set_db edit_wire_nets $net"
        }
        if {$lastmet != $met} {
            myputs $fp  "${addSpace}set_db edit_wire_layer_horizontal $met"
            myputs $fp  "${addSpace}set_db edit_wire_layer_vertical   $met"
        }
        if {$lastwid != $wid} {
            myputs $fp  "${addSpace}set_db edit_wire_width_horizontal $wid"
            myputs $fp  "${addSpace}set_db edit_wire_width_vertical   $wid"
        }

        # route
        if { $loop != "" || $repeat != "" } {
            myputs $fp  [format "${addSpace}edit_add_route_point \[expr %9.3f + \$oX\] \[expr %9.3f + \$oY\]" [lindex $rStart 0] [lindex $rStart 1]]
            myputs $fp  [format "${addSpace}edit_end_route_point \[expr %9.3f + \$oX\] \[expr %9.3f + \$oY\]; # select_routes -area %s -layer %s" [lindex $rEnd 0] [lindex $rEnd 1] $box $met]
        } else {
            myputs $fp  [format "${addSpace}edit_add_route_point %9.3f %9.3f" [lindex $rStart 0] [lindex $rStart 1]]
            myputs $fp  [format "${addSpace}edit_end_route_point %9.3f %9.3f; # select_routes -area %s -layer %s" [lindex $rEnd 0] [lindex $rEnd 1] $box $met]
        }
        # save current for next setting
        set lastnet $net
        set lastmet $met
        set lastwid $wid
    }

    # complete loop by reduce space by 4
    if { $loop != "" || $repeat != "" } {
        set addSpace [string range $addSpace 0 end-4]
        myputs $fp  "${addSpace}\}"
    }

    # footer
    myputs $fp  "${addSpace}#-----------------------------------------"
    myputs $fp  "${addSpace}# route_end_defaults;     # optional reset routing options"
    # obs: myputs $fp  "${addSpace}#obs: editCancelRoute;  # leave wire mode"
    # obs: myputs $fp  "${addSpace}#obs: uiSetTool select; # back to selection"

    # complete proc
    if { $procname != "" } {
        set addSpace [string range $addSpace 0 end-4]
        myputs $fp "${addSpace}\}; # end proc $procname"
        myputs $fp  ""
    }

    # close file and info
    if { $append_file != "to_shell" } {
        close $fp
        if { $procname == "" } {
            imsg_id -id UFSM-151 -tl 0 -msg "changeSelectWire2routeWire: wrote data to $append_file"
            puts "source $append_file"
        } else {
            imsg_id -id UFSM-151 -tl 0 -msg "changeSelectWire2routeWire: wrote data as procedure $procname to $append_file"
            puts "source $append_file; $procname"
        }
    }

}; # end changeSelectWire2routeWire


################################################################################
## Flow Support Procs: changeSelectWire2routeWireAddVia      READY but not fully tested
################################################################################
# to source this file: env
# source $::env(DIGITAL_WORK_DIR)/digital/$::env(PROJECT)_pr_top/be/innovus_scripts/flow_procs.tcl
define_proc changeSelectWire2routeWireAddVia {
    {detail         false       -detail     {bool}   {optional} {Detail help on this comand}}
    {show           false       -show       {bool}   {optional} {Show the selected routes}}
    {loop           ""          -loop       {string} {optional} {Cast wire setting within loop: -loop {count x-step y-step}}}
    {offset         ""          -offset     {string} {optional} {add a base offset for all wires: -offset {ox oy}}}
    {repeat         ""          -repeat     {string} {optional} {repeat the offset by a list: -repeat {a b-e g k}}}
    {mirror         ""          -mirror     {string} {optional} {mirror all coordinates on a axis {xAxis yAxis}, {0 0} = mo mirror, not use with loop/repeat}}
    {no_snap        false       -no_snap    {bool}   {optional} {snap to route or pin by default - NEED TO VERIFY}}
    {procname       ""          -procname   {string} {optional} {pack the command into a procedure}}
    {title          ""          -title      {string} {optional} {Add a title to describe the route}}
    {append_file    "to_shell"  -file       {string} {optional} {Select file to add your route, default is stdout}}
    {add_via        ""          -add_via    {string} {optional} {add via on ONE metal selection {METn} after routing by 'update_power_vias -add_vias {1} -orthogonal_only {0}' }}
} {

    ## helpfull 2D vector MAC:  a + b [* bm]
    proc vect_mac {a b {bm 1}} {
        # puts "a: $a, b: $b, bm: $bm"
        set x [expr [lindex $a 0] + [lindex $b 0] * $bm]
        set y [expr [lindex $a 1] + [lindex $b 1] * $bm]
        return [list $x $y]
    }

    ## helpfull 2D vector MultiMAC:  va [*vam] + vb [* vbm] + vc [* vcm]
    proc vect_mmac {va {vam {1 1}} vb {vbm {1 1}} vc {vcm {1 1}} } {
        # puts "va: $va, vb: $vb, vam: $vam, vbm: $vbm"
        set x [expr [lindex $va 0] * [lindex $vam 0] + [lindex $vb 0] * [lindex $vbm 0] + [lindex $vc 0] * [lindex $vcm 0]]
        set y [expr [lindex $va 1] * [lindex $vam 1] + [lindex $vb 1] * [lindex $vbm 1] + [lindex $vc 1] * [lindex $vcm 1]]
        return [list $x $y]
    }

    ## expand a repeat list {2 4- 6  11 13 -17         34}
    ## to                   {2 4 5 6 11 13 14 15 16 17 34}
    proc expand_count_list {a} {
        proc range_ab {a b} {
            set r {}; for {set i $a} {$i <= $b} {incr i} {lappend r $i}
            return "$r"
        }
        regsub -all {(\d+)\s*\-\s*(\d+)} $a {[range_ab \1 \2]} b
        return [subst $b ]
    }

    ## print exteded help
    if { $detail } {
        puts "#---------------------------------------------"
        puts "#  RECIPE to use changeSelectWire2routeWireAddVia"
        puts "#---------------------------------------------"
        puts "#  1. manual draw some wires -> HINT"
        puts "#  2. deselectAll"
        puts "#  3. select any routed wires "
        puts "#     shift+click to add wires; 'select by line' may be usefull"
        puts "#     check blue number on bottom right or 'changeSelectWire2routeWireAddVia -show'"
        puts "#  4. run 'changeSelectWire2routeWireAddVia'"
        puts "#     put this to script: 'changeSelectWire2routeWireAddVia -file <string> -procname <string>'"
        puts "#     use the option of 'changeSelectWire2routeWireAddVia -help'"
        puts "#"
        puts "#"
        puts "# HINTs: (Cadence search 'Innovus Keyboard Shortcuts'"
        puts "#    Route from a pin - "
        puts "#    > Enable enable CELL > Pin shapes"
        puts "#    > Enable Querry (Q-button lower left) -> highlight the pin on mouseover "
        puts "#    > may need to disable layers above the pin layer"
        puts "#    > open edit route: e "
        puts "#    > mouse over pin"
        puts "#    > when highlighted <shift>-S to take over layer, netname and width"
        puts "#    > change layer and width for the other direction"
        puts "#    > start route (shift-a)"
        puts "#"
        puts "#  Not supported:"
        puts "#    - no regular wire, only special route"
        puts "#    - no shielding"
        puts "#    - no pinadjust check"
        puts "#"
        puts "#  supported:"
        puts "#    - auto sort the routes by name, layer, width"
        puts "#    - add a select comment to find a wire"
        puts "#    - use only routes of all selection"
        puts "#    - basic offset to shift selcted wires: -offset {x-offset y-offset}"
        puts "#    - loop possible:    -loop {count x-step y-step}"
        puts "#    - mirror support:   -mirror {atXaxis atYaxis}"
        puts "#    - create procedure: -procname <name>"
        puts "#    - append to file:   -file <name>"
        puts "#    - add a tilte for detail description"
        puts "#    - add ortogonal vias refer to one layer"
        puts "#"
        puts "#"
        puts "#---------------------------------------------"
        return
    }

    set addSpace ""         ; # space setting for visual alignment

    ## for each selected element filter for object type
    set routes [get_db selected -if {.obj_type == "special_wire"}]              ; # list of routes
    set rCount [llength $routes]                                                ; # route    counter
    set vCount [llength [get_db selected -if {.obj_type == "special_via"}]]     ; # via      counter
    set iCount [llength [get_db selected -if {.obj_type == "inst"}]]            ; # instance counter


    puts "$rCount routes, $vCount vias, $iCount instances"

    # create a list of all routes for sorting
    set rList []
    foreach i $routes {
        ## get parameters of each route
        set rStart [lindex [get_db $i .path] 0]   ; # path: {x1 y1} {x2 y2}
        set rEnd   [lindex [get_db $i .path] 1]   ; # path: {x1 y1} {x2 y2}
        set wid    [get_db $i .width]             ; # width
        set met    [get_db $i .layer.name]        ; # layer
        set net    [get_db $i .net.name]          ; # netname
        set box    [get_db $i .rect]              ; # box of wire to add selection
        # elements.          0    1    2    3       4     5
        lappend rList [list $net $met $wid $rStart $rEnd $box]
    }
    # sort all elements by default (reduce command size)
    set rList [lsort $rList]

    ## pure show of selected wires
    if { $show } {
        puts "#--------------------------------------------------"
        puts "# nbr : net    metal width {startX startY} {endX endY} {box}"
        set cnt 0
        foreach i $rList {
            puts "# [incr cnt] : $i"
        }
        puts "#--------------------------------------------------"
        puts "# Total repeated : [llength [expand_count_list $repeat]] times"
        puts "#--------------------------------------------------"
        puts "# to select them"
        foreach i $rList {
            puts "select_routes -layer [lindex $i 1] -area [lindex $i 5]"
        }
        puts "#--------------------------------------------------"
        return
    }

    if { $title == "" } {
        if { $procname == "" } {
            set title "NEW ROUTE"
        } else {
            set title " PROCEDURE : $procname"
        }
    }

    set header "create by : $::env(USER) on [clock format [clock seconds] -format "%a %T %d-%b-%Y"] (changeSelectWire2routeWireAddVia)"

    # option file print, open file
    if { $append_file == "to_shell" } {
        set fp $append_file
    } else {
        set fp [open $append_file a]
    }

    imsg_id -id UFSM-151 -tl 6 -msg "changeSelectWire2routeWireAddVia: "
    myputs $fp "###_____________________________________________________________________________________###"
    myputs $fp "### [format "%-83s" $title ] ###"
    myputs $fp "###_____________________________________________________________________________________###"
    myputs $fp "###"
    myputs $fp "### $header"
    myputs $fp "### info      : total $rCount routes ($vCount vias, $iCount instances) selected"
    myputs $fp "###---------------------------------------------------------------------------------------"

    # option pack cmd's into a proc
    if { $procname != "" } {
        myputs $fp "proc $procname {} \{"
        append addSpace "    "
    }

    myputs $fp "${addSpace}# SET OPTIONS"
    myputs $fp "${addSpace}route_start_defaults                     ; # main default setting, add exceptions below"
    if { $no_snap } {
        myputs $fp "${addSpace}set_db edit_wire_align_wire_at_pin {0}          ; # no adjust to pin center"
        myputs $fp "${addSpace}set_db edit_wire_snap_to {}                     ; # no snap to power, pin or special"
        myputs $fp "${addSpace}# set_db edit_wire_snap_to {pg pin special}     ; # default"
    }


    # puts "============================================================="
    # foreach i $route {puts $i}
    # puts "============================================================="
    # set default offset if not defined
    if { $offset == "" } {
        set offset {0.0 0.0}
    }

    # cast with loop
    if { $loop != "" } {
        myputs $fp "${addSpace}for \{set i 0\} \{\$i < [lindex $loop 0]\} \{incr i\} \{"
        append addSpace "    "
        myputs $fp "${addSpace}set oX \[expr \$i*[lindex $loop 1] + [lindex $offset 0]\]"
        myputs $fp "${addSpace}set oY \[expr \$i*[lindex $loop 2] + [lindex $offset 1]\]"
    } elseif { $repeat != "" } {
        myputs $fp "${addSpace}foreach mult \{[expand_count_list $repeat]\} \{"
        append addSpace "    "
        myputs $fp "${addSpace}set oX \[expr \$mult*[lindex $offset 0]\]"
        myputs $fp "${addSpace}set oY \[expr \$mult*[lindex $offset 1]\]"
    } else {
        myputs $fp "${addSpace}set oX [lindex $offset 0]"
        myputs $fp "${addSpace}set oY [lindex $offset 1]"
    }

    ## default values
    set lastwid 0               ; #
    set lastmet ""              ; #
    set lastnet ""              ; #
    set addFinalVias {}         ; # list of vias to add after routing block

    # route each wire
    foreach i $rList {
        ## get parameters of each route
        set net    [lindex $i 0]
        set met    [lindex $i 1]
        set wid    [lindex $i 2]
        if { $repeat == "" } {
            ## no mirroring by unset
            if { $mirror == "" } {
        set rStart [vect_mac [lindex $i 3] $offset]
        set rEnd   [vect_mac [lindex $i 4] $offset]
            ## no mirroring by {0 0} vector
            } elseif { [lindex $mirror 0] == 0 && [lindex $mirror 1] == 0 } {
                set rStart [vect_mac [lindex $i 3] $offset]
                set rEnd   [vect_mac [lindex $i 4] $offset]
            ## mirror on X
            } elseif { [lindex $mirror 0] != 0 && [lindex $mirror 1] == 0 } {
                set rStart [vect_mmac [lindex $i 3] {-1 1} $mirror {2 0} $offset]
                set rEnd   [vect_mmac [lindex $i 4] {-1 1} $mirror {2 0} $offset]
            ## mirror on Y
            } elseif { [lindex $mirror 0] == 0 && [lindex $mirror 1] != 0 } {
                set rStart [vect_mmac [lindex $i 3] {1 -1} $mirror {0 2} $offset]
                set rEnd   [vect_mmac [lindex $i 4] {1 -1} $mirror {0 2} $offset]
            ## mirror on X and Y
            } elseif { [lindex $mirror 0] != 0 && [lindex $mirror 1] != 0 } {
                set rStart [vect_mmac [lindex $i 3] {-1 -1} $mirror {2 2} $offset]
                set rEnd   [vect_mmac [lindex $i 4] {-1 -1} $mirror {2 2} $offset]
            }
        } else {
            set rStart [lindex $i 3]
            set rEnd   [lindex $i 4]
        }
        set box    [lindex $i 5]

        # on a change, update the db
        if {$lastnet != $net} {
            myputs $fp  "${addSpace}#-----------------------------------------"
            myputs $fp  "${addSpace}set_db edit_wire_nets $net"
        }
        if {$lastmet != $met} {
            myputs $fp  "${addSpace}set_db edit_wire_layer_horizontal $met"
            myputs $fp  "${addSpace}set_db edit_wire_layer_vertical   $met"
        }
        if {$lastwid != $wid} {
            myputs $fp  "${addSpace}set_db edit_wire_width_horizontal $wid"
            myputs $fp  "${addSpace}set_db edit_wire_width_vertical   $wid"
        }

        # route - always with offset
            myputs $fp  [format "${addSpace}edit_add_route_point \[expr %9.3f + \$oX\] \[expr %9.3f + \$oY\]" [lindex $rStart 0] [lindex $rStart 1]]
            myputs $fp  [format "${addSpace}edit_end_route_point \[expr %9.3f + \$oX\] \[expr %9.3f + \$oY\]; # select_routes -area %s -layer %s" [lindex $rEnd 0] [lindex $rEnd 1] $box $met]
#        if { $loop != "" || $repeat != "" } {
#        } else {
#            myputs $fp  [format "${addSpace}edit_add_route_point %9.3f %9.3f" [lindex $rStart 0] [lindex $rStart 1]]
#            myputs $fp  [format "${addSpace}edit_end_route_point %9.3f %9.3f; # select_routes -area %s -layer %s" [lindex $rEnd 0] [lindex $rEnd 1] $box $met]
#        }
        # save current for next setting
        set lastnet $net
        set lastmet $met
        set lastwid $wid

        # remember VIAs to add on request
        if { $add_via == $met } {
            set b [lindex $box 0]
            lappend addFinalVias "update_power_vias -add_vias {1} -orthogonal_only {0} -area \[list \[expr [lindex $b 0] + \$oX\] \[expr [lindex $b 1] + \$oY\] \[expr [lindex $b 2] + \$oX\] \[expr [lindex $b 3] + \$oY\]\]"
        }
    }


    # complete loop by reduce space by 4
    if { $loop != "" || $repeat != "" } {
        set addSpace [string range $addSpace 0 end-4]
        myputs $fp  "${addSpace}\}"
    }

    # Add final power vias as loop
    if { [llength $addFinalVias] > 0 } {
        myputs $fp "${addSpace}#-----------------------------------------"
        myputs $fp "${addSpace}# add update_power_vias on request for $add_via"
        # add loop header
        if { $loop != "" } {
            myputs $fp "${addSpace}for \{set i 0\} \{\$i < [lindex $loop 0]\} \{incr i\} \{"
            append addSpace "    "
            myputs $fp "${addSpace}set oX \[expr \$i*[lindex $loop 1] + [lindex $offset 0]\]"
            myputs $fp "${addSpace}set oY \[expr \$i*[lindex $loop 2] + [lindex $offset 1]\]"
        } elseif { $repeat != "" } {
            myputs $fp "${addSpace}foreach mult \{[expand_count_list $repeat]\} \{"
            append addSpace "    "
            myputs $fp "${addSpace}set oX \[expr \$mult*[lindex $offset 0]\]"
            myputs $fp "${addSpace}set oY \[expr \$mult*[lindex $offset 1]\]"
        } else {
            myputs $fp "${addSpace}set oX [lindex $offset 0]"
            myputs $fp "${addSpace}set oY [lindex $offset 1]"
        }

        # add the list of stored via locations
        foreach i $addFinalVias {
            myputs $fp "${addSpace}$i"
        }

        # complete loop by reduce space by 4
        if { $loop != "" || $repeat != "" } {
            set addSpace [string range $addSpace 0 end-4]
            myputs $fp  "${addSpace}\}"
        }
    }
    # footer
    myputs $fp  "${addSpace}#-----------------------------------------"

    myputs $fp  "${addSpace}# route_end_defaults;     # optional reset routing options"
    # obs: myputs $fp  "${addSpace}#obs: editCancelRoute;  # leave wire mode"
    # obs: myputs $fp  "${addSpace}#obs: uiSetTool select; # back to selection"

    # complete proc
    if { $procname != "" } {
        set addSpace [string range $addSpace 0 end-4]
        myputs $fp "${addSpace}\}; # end proc $procname"
        myputs $fp  ""
    }

    # close file and info
    if { $append_file != "to_shell" } {
        close $fp
        if { $procname == "" } {
            imsg_id -id UFSM-151 -tl 0 -msg "changeSelectWire2routeWireAddVia: wrote data to $append_file"
            puts "source $append_file"
        } else {
            imsg_id -id UFSM-151 -tl 0 -msg "changeSelectWire2routeWireAddVia: wrote data as procedure $procname to $append_file"
            puts "source $append_file; $procname"
        }
    }

}; # end changeSelectWire2routeWireAddVia

################################################################################
## Flow Support Procs: getResistOfSelectWires               READY TO USE
################################################################################
# to source this file:
# source /usr/project-a/epc908/users/tbl/digital/dev/digital/epc908_pr_top/be/innovus_scripts/flow_procs.tcl
define_proc getResistOfSelectWires {
    {detail         false       -detail     {bool}   {optional} {Detail help on this comand}}
    {show           false       -show       {bool}   {optional} {Show the selected routes}}
} {

    ## print exteded help
    if { $detail } {
        puts "#---------------------------------------------"
        puts "#  RECIPE to use getResistOfSelectWires"
        puts "#---------------------------------------------"
        puts "#  1. select any routed wires"
        puts "#  2. run 'getResistOfSelectWires'"
        puts "#"
        puts "# HINTs:"
        puts "#"
        puts "#  Not supported:"
        puts "#    - no regular wire, only special route"
        puts "#    - no vias"
        puts "#"
        puts "#  supported:"
        puts "#"
        puts "#---------------------------------------------"
        return
    }

    ## for each selected element filter for object type
    set routes [get_db selected -if {.obj_type == "special_wire"}]              ; # list of routes
    set rCount [llength $routes]                                                ; # route    counter
    set vCount [llength [get_db selected -if {.obj_type == "special_via"}]]     ; # via      counter
    set iCount [llength [get_db selected -if {.obj_type == "inst"}]]            ; # instance counter
    set wCount [llength [get_db selected -if {.obj_type == "wire"}]]            ; # wire counter
    set summSerR  0                                                             ; # resistance serial sum
    set summParR  1e9                                                           ; # resistance parallel sum
    set summL  0                                                                ; # length summary
    set minL      1e9                                                           ; # minimal length
    set maxL      0                                                             ; # maximal length

    puts "$rCount routes, $vCount vias, $iCount instances, $wCount wires"

    # create a list of all routes for calculation
    set rList []
    foreach i $routes {
        ## get parameters of each route
        set rStart [lindex [get_db $i .path] 0]     ; # path: {x1 y1} {x2 y2}
        set rEnd   [lindex [get_db $i .path] 1]     ; # path: {x1 y1} {x2 y2}
        set wid    [get_db $i .width]               ; # width
        set met    [get_db $i .layer.name]          ; # layer
        set net    [get_db $i .net.name]            ; # netname
        set box    [get_db $i .rect]                ; # box of wire to add selection
        set lenght [expr [get_db $i .area] / $wid]  ; # length of wire
        # elements.          0    1    2    3       4     5    6
        lappend rList [list $net $met $wid $rStart $rEnd $box $lenght]
    }

    # sort all elements by default (reduce command size)
    #set rList [lsort $rList]

    ## pure show of selected wires
    if { $show } {
        puts "#--------------------------------------------------"
        puts "# nbr : net    metal width {startX startY} {endX endY} {box}"
        set cnt 0
        foreach i $rList {
            puts "# [incr cnt] : $i"
        }
        puts "#--------------------------------------------------"
        return
    }

    ## resitive calculation, base values in Ohm/sq.
    set Rmet {MET1 0.09 MET2 0.09 MET3 0.09 MET4 0.09 MET5 0.075 TMET 0.035}
    dict with Rmet {}

    foreach i $rList {
        # length calculations
        set l [lindex $i 6]
        if { $maxL < $l } { set maxL $l }
        if { $minL > $l } { set minL $l }
        # get wire width
        set w [lindex $i 2]
        # resist =   Ohm/sq.                       * length /  width
        set r [expr [dict get $Rmet [lindex $i 1]] * $l / $w]
        # relative resist for 1000 um
        set rrel [expr [dict get $Rmet [lindex $i 1]] * 1000 / $w]
        set b [lindex [lindex $i 5] 0]
        # puts "box: $b , [lindex $b 0], [lindex $b 1], [lindex $b 2], [lindex $b 3]"
        set xSize [format "%f" [expr [lindex $b 2] - [lindex $b 0]]]
        set ySize [format "%f" [expr [lindex $b 3] - [lindex $b 1]]]
        if { $xSize == [lindex $i 2] } {
            set dir "vertical"
        } elseif { $ySize == [lindex $i 2] } {
            set dir "horizontal"
        } else {
            puts "Warning size to width missmatch on net [lindex $i 0] (wid: [lindex $i 2], xW: $xSize, yW: $ySize) : select_routes -layer [lindex $i 1] -area [lindex $i 5]"
        }
        puts "Res: [format "%8.3f" $r ] Ohm for $dir net [lindex $i 0] ([format "%6.1f" $l ] um, [format "%8.3f" $rrel] Ohm/mm): select_routes -layer [lindex $i 1] -area [lindex $i 5]"
        set summSerR [expr $summSerR + $r]
        set summParR [expr 1/(1/$summParR + 1/$r)]
        set summL [expr $summL + $l]
    }
    puts "     [format "%8.3f" $summSerR ] Ohm serial   summary, length [format "%8.1f" $summL ] um"
    puts "     [format "%8.3f" $summParR ] Ohm parallel summary, length [format "%8.1f" $minL ] - [format "%8.1f" $maxL ] um"

}; # end getResistOfSelectWires


##############################################################################
# Name : add_diode_at_port
# Description: Add antenna diode at specified ports/pins in a list
##############################################################################

 define_proc add_diode_at_port {
    {antenna_cell     "ANTENNA"   -antenna_cell     {string}  {required} {Antenna cell name} }
    {fixed_pos        false       -fixed_pos        {bool}    {required} {Insert antenna cell in a fixed position, Must set false when it's not placed!} }
    {prefix           ""          -prefix           {string}  {required} {Prefix name for inserted antenna diode} }
    {port_list        ""          -port_list        {string}  {required} {Port/Pin list to be inserted with antenna diode, can contain '*' } }
 } {

   #catch { file delete tmp_IO_port_net }
   #if [ catch { set in_file_h [ open "tmp_IO_port_net" w ] } ] {
   #   puts "\# ERROR: unable to read the file tmp_buffer_IO"
   #   return 0
   #}

    set diode_cnt  0
    set hier_split "/"

    # Dump port connection net name to file  tmp_IO_port_net
    foreach from_pin $port_list {
       # Check port or pins exist
       if {[llength [set pins_obj [get_db  ports  $from_pin]]] > 0 } {
          set is_port 1
        } elseif { [llength [set pins_obj [get_db pins $from_pin]]] > 0  } {
          set is_port 0
        } else {
           wmsg_id -id USFM-120 -msg "The port is not available:: $from_pin"
           continue
        }

        foreach pin_obj $pins_obj {
            # Get Port location
            set port_x [get_db  $pin_obj .location.x]
            set port_y [get_db  $pin_obj .location.y]


            # Check port direction
            if { [get_db $pin_obj .direction] == "in" } {
                set direction "input"
                imsg_id -id UFSM-151 -tl 9 -msg "Input port/pin: $pin_obj"
            } elseif { [get_db $pin_obj .direction] == "out" } {
                set direction  "output"
                imsg_id -id UFSM-151 -tl 9 -msg "Output port/pin: $pin_obj"
            } elseif {  [get_db $pin_obj .direction] == "inout" } {
                set direction  "inout"
                imsg_id -id UFSM-151 -tl 9 -msg "InOut port/pin: $pin_obj"
            } else {
                wmsg_id -id USFM-120 -msg "Cannot recognize the port/pin direction: $pin_obj"
                continue
            }



            if {[set netName [get_db $pin_obj .net.name]] == ""} {
                wmsg_id -id USFM-120 -msg "No net connects to the port/pin: $pin_obj"
                continue
            }

            set is_float_pin  1



            if {$is_port == 1 } {
                if {$direction == "input" } {
                   set pin_list [get_db net:$netName .loads]
                 # output or inout
                 } else {
                   set pin_list [get_db net:$netName .drivers]
                 }

                 foreach pin $pin_list {
                    #set pin_x [get_db  $pin .location.x]
                    #set pin_y [get_db  $pin .location.y]
                    set inst_name [get_db $pin .inst.name]
                    set is_placed [get_db inst:$inst_name .place_status]
                    # Don't care whether fanout is placed or not
                    set is_float_pin 0
                    #if { ![string match $is_placed "unplaced"] } {
                    #   set is_float_pin  0
                    #   break
                    #}
                }
             } else {
               # Directly created diode near the pin
               if {[llength [set inst_name [get_db $pin_obj .inst.name]]] > 0 } {
                 set pin $pin_obj
                 set is_float_pin 0
               }

             }


             if { $is_float_pin == 0 } {
                 if { $fixed_pos } {
                    imsg_id -id UFSM-151 -tl 0 -msg "create_diode pin($direction): $pin  -loc $port_x $port_y"
                    create_diode -diode_cell $antenna_cell -pin $inst_name [get_db $pin .base_name] -prefix ${prefix}_antenna_fix_${diode_cnt} -loc $port_x $port_y
                 } else {
                    imsg_id -id UFSM-151 -tl 0 -msg "create_diode pin($direction): $pin"
                    create_diode -diode_cell $antenna_cell -pin $inst_name  [get_db $pin .base_name] -prefix ${prefix}_antenna_fix_${diode_cnt}
                 }
                 set diode_cnt [expr $diode_cnt + 1]
             }
         }


    }

    # Legalize these cells
    set inst_list ""
    foreach a  [get_db insts *${prefix}_antenna_fix_*] {
      lappend inst_list *[get_db $a .base_name]
    }

    if { [llength $inst_list] > 0 } {
       place_detail -inst $inst_list
       set_db [get_db insts *${prefix}_antenna_fix_*] .place_status fixed
    }

    #close $in_file_h

    # Buffer IO Pin
    #attachIOBuffer -selNetFile "tmp_IO_port_net" -markFixed -in $in_buf -out $out_buf -baseName "EPC_ANTENNA"

    imsg_id -id UFSM-151 -tl 0 -msg "Finish inserting ANTENNA in ports"
    imsg_id -id UFSM-151 -tl 0 -msg "Total inserted Antenna cells: $diode_cnt"

 }



##############################################################################
# Name:    add_diode_after_route
# Description:
#          This script attaches antenna diodes to the input pins residing in antenna violations file
#          which is generated by check_process_antenna command.
##############################################################################
define_proc add_diode_after_route -description "Add antenna to input pin with antenna vilation instance" {

  {antenna_cell "ANTENNA"  -antenna_cell {string}  {required}  {Antenna cell name from library } }
  {prefix       ""         -prefix       {string}  {required}  {Prefix name with inserted diode} }
  {antenna_file  ""        -antenna_file {string} {required} {Antenna file generated by check_process_antenna} }

} {




  if { ![file exists $antenna_file] } {
    imsg_id -id UFSM-151 -tl 7 -msg "Cannot open $antenna_file, Please make sure check_process_antenna is executed before this step ..."
    return
  }

  if [catch {open $antenna_file r} fileId] {
    imsg_id -id UFSM-151 -tl 7 -msg "Cannot open $antenna_file ..."
    return
  } else {
    set diode_cnt 0
    while { [gets $fileId line ] != -1 } {
      # Search for lines matching "instName (cellName) pinName" that have violations
      if {[regexp {^  (\S+) (\S+) (\S+)} $line] == 1} {
        # Remove extra white space
        regsub -all -- {[[:space:]]+} $line " " line
        set line [string trimlef $line]
        # Store instance and pin name to insert diodes on
        set instName [lindex [split $line] 0]
        # Modify instance name if it contains escaped characters:
        set escapedInstName ""
        foreach hier [split $instName /] {
          if {[regexp {\[|\]|\.} $hier] == 1} {
            set hier "\\$hier "
          }
          set escapedInstName "$escapedInstName$hier/"
          set instName $escapedInstName
        }
        # Remove last '/'
        regsub {/$} $instName {} instName
        set pinName [lindex [split $line] 2]

        if {$instName != ""} {
          set instLoc_X [get_db inst:$instName .location.x]
          set instLoc_Y [get_db inst:$instName .location.y]

          imsg_id -id UFSM-151 -tl 7 -msg "Insert antenna diode at Inst:$instName Pin:$pinName, Loc: ($instLoc_X $instLoc_Y)"

          # Attach diode and place at location of instance
          create_diode -prefix ${prefix}_${diode_cnt}  -diode_cell $antenna_cell -pin $instName $pinName -loc $instLoc_X $instLoc_Y
          set diode_cnt [expr $diode_cnt + 1]

        }
      }
    }
  }

  imsg_id -id UFSM-151 -tl 7 -msg "Summary Insert antenna diode num: $diode_cnt"
  close $fileId

  # Legalize placement of diodes and run ecoRoute to route them
  #place_detail
  #route_eco
}

##############################################################################
# Name:    fix_trans_vio
# Description:
#          Fix transition violations according to report file from time_design
##############################################################################
define_proc fix_trans_vio -description "Fix transition violations" {
  {buf_cell "BUF_X4"         -buf_cell          {string}  {optional}  {buffer cell name from library } }
  {rel_dist_to_sink  0.2     -rel_dist_to_sink  {float}   {optional}  {buffer insertion location} }
  {trans_vio_file  ""        -trans_vio_file    {string}  {required}  {time_design report} }

} {



  if { ![file exists $trans_vio_file] } {
    imsg_id -id UFSM-151 -tl 7 -msg "Cannot open $trans_vio_file, Please make sure time_design is executed before this step ..."
    return
  }

  if [catch {open "| zcat $trans_vio_file" r} fileId] {
    imsg_id -id UFSM-151 -tl 7 -msg "Cannot open $trans_vio_file ..."
    return
  } else {
    set diode_cnt 0
    while { [gets $fileId line ] != -1 } {
      #puts "line : $line"
      # Ignore comment
      if {[regexp {^#} $line] == 1 } {
        continue
      }
      # Ignore *info
      if {[regexp {^\*info} $line] == 1 } {
        continue
      }

      if { [regexp {^(\S+)} $line netName] == 1 } {
        #puts "netName: $netName"
        continue
      }

      if { [regexp {^\s+(\S+)\s+(\S+)\s+(\S+)\s+(\S+)\s+(\S+)\s+(.+)} $line dummy instPin maxTran Tran Translack CellPort Remark] } {
        # Only consider real violations
        #puts "instPin: $instPin"
        if { [regexp {R} $Remark] } {
          lappend pins_by_netName($netName) $instPin
        }
      }
    }
  }


    # Don't refine place and update timing every time.
    set_db eco_refine_place false
    set_db eco_update_timing false
    # Don't
    set_db place_detail_preserve_routing true
    set_db place_detail_remove_affected_routing false


    set  need_eco 0
    # Add buffer every 2500um
    set MAX_DIST 3500
    # Get driver pin location
    foreach netName [array name pins_by_netName] {
      set drv_pin [get_db [get_db nets $netName] .drivers]
      set drv_loc_x [get_db $drv_pin  .location.x]
      set drv_loc_y [get_db $drv_pin  .location.x]


      foreach pin $pins_by_netName($netName) {
        set load_loc_x [get_db [get_db pins $pin]  .location.x]
        set load_loc_y [get_db [get_db pins $pin]  .location.x]
        set dist [expr sqrt(pow($drv_loc_x-$load_loc_x,2)+pow($drv_loc_y-$load_loc_y,2))]
        set num_of_buf [expr ceil($dist/$MAX_DIST)]
        if {$num_of_buf <= 2} {
          set num_of_buf 1
          set buf_dist [expr $dist/2.0]
          imsg_id -id UFSM-151 -tl 7 -msg " eco_add_repeater -cell $buf_cell -pins $pin -relative_distance_to_sink 0.5"
          eco_add_repeater -cell $buf_cell -pins $pin -relative_distance_to_sink 0.5
        } else {
          set num_of_buf [expr $num_of_buf -1]
          set buf_dist [expr $dist/$num_of_buf]
          for {set i 0} {$i < $num_of_buf } {incr i} {
             set ratio [expr ($dist-$buf_dist)/$dist]
             imsg_id -id UFSM-151 -tl 7 -msg " eco_add_repeater -cell $buf_cell -pins $pin -relative_distance_to_sink $ratio"
             eco_add_repeater -cell $buf_cell -pins $pin -relative_distance_to_sink  $ratio
             set dist [expr $dist - $buf_dist]
             if { $dist <= $buf_dist } {
               break
             }
             puts "Remainging dist($i): $dist, $ratio"
          }
        }
        set  need_eco 1
        #imsg_id -id UFSM-151 -tl 7 -msg " eco_add_repeater -cell $buf_cell -pins $pin -relative_distance_to_sink $rel_dist_to_sink"
        #eco_add_repeater -cell $buf_cell -pins $pin -relative_distance_to_sink  $rel_dist_to_sink
      }

    }

    if { $need_eco == 1 } {
      # Refine some illegal cells
      # Only refine 'Placed' insts, place_eco place 'Unplaced' insts
      place_detail -eco true

      # Return flow default filler flow
      delete_filler
      flow_step_add_fillers

      # Run ECO routing
      set_db route_design_with_eco true
      route_eco
      #DRC available, use below commands
      #set_db route_design_with_eco true
      #set_db route_design_eco_only_in_layers 1:3
      #set_db route_design_selected_net_only false
      #route_global_detail 100.0 1200.0 350.0 600.0
     }


}



##############################################################################
## The procedure below (provided by Marios Karatzias from Cadence) is required
## to fix a bug in the unified metrics where critical external delays on ports
## and nets are not handled properly in Genus 17.2.
##############################################################################

if { [lindex [split [get_db program_version] .] 0] == 17 } {
    proc um::capture_genus_timing_paths {args} {
        set point_list [list]
        set timing_data $args
        dict set point_list slack [dict get [lindex $timing_data 5] slack]
        dict set point_list begin_point [get_db [dict get [lindex $timing_data 5] startpoint] .name]
        dict set point_list end_point [get_db [dict get [lindex $timing_data 5] endpoint] .name]
        dict set point_list view_name [get_db [dict get [lindex $timing_data 5] view] .name]

        dict set point_list depth [expr {round([llength [lindex $timing_data 3]]+1/2)}]

        set clock [dict get [lindex [lindex $timing_data 4] 0] pin]
        dict set point_list  capture_clock [get_db $clock .base_name]
        dict set point_list  uncertainty [get_db $clock .setup_uncertainty]
        dict set point_list  capture_clock_latency [dict get [lindex [lindex $timing_data 4] 0] arrival]

        set count 0
        foreach pin [lindex $timing_data 3] {
            if { [what_is [dict get $pin "pin"]] == "external_delay" }  {continue}   ; set pin_list [list]
            incr count
            #if {($count == 1) || ($count == 2) || ($count == [expr $num_pins-1]) || ($count == $num_pins)} {
            #  continue
            #}
            if {![dict exists $pin type]} {continue}
            if {[dict get $pin type] eq "launch"} {
                dict set point_list launch_clock [get_db [dict get $pin "pin"] .base_name]
                dict set point_list launch_clock_latency [dict get $pin arrival]
            }
            if {[regexp "^clock" [dict get $pin "pin"]]} { continue }
            if {[regexp "^hpin" [dict get $pin "pin"]]} { continue }
            lappend pin_list pin_name [get_db [dict get $pin "pin"] .name]
            lappend pin_list net_name [get_db [dict get $pin "pin"] .net.name] ;					     if { [what_is [dict get $pin "pin"]] == "port" } {lappend pin_list cell_name "" } else { lappend pin_list cell_name [get_db [dict get $pin "pin"] .base_pin.base_cell.name] }
            lappend pin_list fanout [dict get $pin fanout]
            lappend pin_list delay [dict get $pin delay]
            if {[dict get $pin slew] ne ""} {
                lappend pin_list slew [dict get $pin slew]
            } else {
                lappend pin_list slew 0.0
            }
            if {[dict get $pin X] ne ""} {
                lappend pin_list x_coordinate [dict get $pin X]
            } else {
                lappend pin_list x_coordinate 0.0
            }
            if {[dict get $pin Y] ne ""} {
                lappend pin_list y_coordinate [dict get $pin Y]
            } else {
                lappend pin_list y_coordinate 0.0
            }
            lappend pins $pin_list
        }
        dict set point_list "pins" $pins
        lappend ::um::temp_paths $point_list
        dict set ::um::genus_timing_paths all $::um::temp_paths
    }
}
